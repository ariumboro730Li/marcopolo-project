<?php
date_default_timezone_set('Asia/Jakarta');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//
Route::get('/', 'HomeController@index')->name('home');
Route::get('logout', array('as' => 'logout', 'uses' => 'HomeController@logout'));

//Route::get('/logout', array('as' => 'logout', 'uses' => 'Auth\LoginController@logout'));

//Route::get('/setting/user', function () { return view('setting.list_user'); });
//Route::get('/setting/user/add_user', function () { return view('setting.add_user'); });
//Route::get('/setting/user/edit_user', function () { return view('setting.edit_user'); });
//Route::get('/setting/password', function () { return view('setting.change_password'); });

Route::get('/rca',array('as' => 'rca.index', 'uses' => 'RCAController@index'));


//Route::get('/ldaplogin', 'Auth\LoginController@showLoginForm')->name('login');
//Route::post('/ldaplogin', 'Auth\LoginController@login');
Route::get('/ldaplogin',array('as' => 'ldap.index', 'uses' => 'UserController@LoginLDAP'));
//Route::post('/ldaplogin',array('as' => 'ldap.login', 'uses' => 'UserController@LoginLDAP'));

Route::get('/rca/tracking', array('as' => 'rca.tracking', 'uses' => 'RCAController@tracking'));
Route::get('/rca/dashboard', function () { return view('modul_rca.dashboard'); });

Route::get('/rca/hapus/{rca}',array('as' => 'rca.hapus', 'uses' => 'RCAController@HapusRCA'));
Route::get('/rca/edit/{rca}',array('as' => 'rca.edit', 'uses' => 'RCAController@EditRCA'));
Route::post('/rca/edit/',array('as' => 'rca.update', 'uses' => 'RCAController@UpdateRCA'));
Route::get('/rca/tambah/',array('as' => 'rca.tambah', 'uses' => 'RCAController@TambahRCA'));
Route::post('/rca/tambah/',array('as' => 'rca.tambah.baru', 'uses' => 'RCAController@TambahRCABaru'));
Route::get('/rca/export/',array('as' => 'rca.export', 'uses' => 'RCAController@ExportRCA'));
Route::get('/rca/export/tes',function () { return view('modul_rca.export.model1'); });
Route::get('/rca/export/{id}',array('as' => 'rca.export.data', 'uses' => 'RCAController@ExportRCAData'));
Route::get('/rca/view/{id}',array('as' => 'rca.view.data', 'uses' => 'RCAController@ViewRCAData'));
Route::get('/rca/export/model1', function () { return view('modul_rca.export.model1'); });

Route::get('/inventory/ix/edit', function () { return view('modul_inventory.edit_ix'); });

Route::get('/inventory/input', function () { return view('modul_inventory.input_inventory'); });
Route::get('/inventory/search', function () { return view('modul_inventory.inventory_search'); });
Route::get('/inventory/access/edit', function () { return view('modul_inventory.edit_access'); });
Route::get('/inventory/ipbb/edit', function () { return view('modul_inventory.edit_ipbb'); });
Route::get('/inventory/ix/edit', function () { return view('modul_inventory.edit_ix'); });
Route::get('/inventory/detail', function () { return view('modul_inventory.detail_inventory'); });

Route::get('/inventory', array('as' => 'inventory.dashboard', 'uses' => 'DatekController@Dashboard'));
Route::get('/inventory/access', array('as' => 'inventory.access', 'uses' => 'DatekController@Access'));
Route::get('/inventory/access/edit/{access}', array('as' => 'inventory.access.show', 'uses' => 'DatekController@ShowAccess'));
Route::post('/inventory/access/edit/',array('as' => 'inventory.access.update', 'uses' => 'DatekController@updateAccess'));
Route::get('/inventory/access/import', array('as' => 'inventory.access.import', 'uses' => 'DatekController@ImportAccess'));
Route::post('/inventory/access/import', array('as' => 'inventory.access.import', 'uses' => 'DatekController@PostImportAccess'));
Route::post('/inventory/access/import/save', array('as' => 'inventory.access.import.save', 'uses' => 'DatekController@SaveImportAccess'));
Route::get('/inventory/access/detail/{regional}', array('as' => 'inventory.access.detail', 'uses' => 'DatekController@DetailAccess'));
Route::get('/inventory/access/overthreshold', array('as' => 'inventory.access.overthreshold', 'uses' => 'DatekController@AccessOverThreshold'));
Route::get('/inventory/access/detail/delete/{data}', array('as' => 'inventory.access.detail.delete', 'uses' => 'DatekController@DeleteAccess'));


Route::get('/inventory/getBackhaul', array('as' => 'inventory.getBackhaul', 'uses' => 'DatekController@getBackhaul'));
Route::get('/inventory/accessApi', array('as' => 'inventory.accessApi', 'uses' => 'DatekController@SynchronizeOpenAPI'));


Route::get('/inventory/backhaul', array('as' => 'inventory.backhaul', 'uses' => 'DatekController@Backhaul'));
Route::get('/inventory/backhaul/import', array('as' => 'inventory.backhaul.import', 'uses' => 'DatekController@ImportBackhaul'));
Route::post('/inventory/backhaul/import', array('as' => 'inventory.backhaul.import', 'uses' => 'DatekController@PostImportBackhaul'));
Route::post('/inventory/backhaul/import/save', array('as' => 'inventory.backhaul.import.save', 'uses' => 'DatekController@SaveImportBackhaul'));
Route::get('/inventory/backhaul/edit/{backhaul}', array('as' => 'inventory.backhaul.show', 'uses' => 'DatekController@ShowBackhaul'));
Route::post('/inventory/backhaul/edit/',array('as' => 'inventory.backhaul.update', 'uses' => 'DatekController@update'));
Route::get('/inventory/backhaul/detail/{regional}',array('as' => 'inventory.backhaul.detail', 'uses' => 'DatekController@DetailBackhaul'));
Route::get('/inventory/backhaul/overthreshold', array('as' => 'inventory.backhaul.overthreshold', 'uses' => 'DatekController@BackhaulOverThreshold'));
Route::get('/inventory/backhaul/detail/delete/{data}',array('as' => 'inventory.backhaul.detail.delete', 'uses' => 'DatekController@DeleteBackhaul'));


Route::get('/inventory/OneIPMPLS', array('as' => 'inventory.OneIPMPLS', 'uses' => 'DatekController@OneIPMPLS'));
Route::get('/inventory/OneIPMPLS/import', array('as' => 'inventory.OneIPMPLS.import', 'uses' => 'DatekController@ImportOneIPMPLS'));
Route::post('/inventory/OneIPMPLS/import', array('as' => 'inventory.OneIPMPLS.import', 'uses' => 'DatekController@PostImportOneIPMPLS'));
Route::post('/inventory/OneIPMPLS/import/save', array('as' => 'inventory.OneIPMPLS.import.save', 'uses' => 'DatekController@SaveImportOneIPMPLS'));
Route::get('/inventory/OneIPMPLS/edit/{OneIPMPLS}', array('as' => 'inventory.OneIPMPLS.show', 'uses' => 'DatekController@ShowOneIPMPLS'));
Route::post('/inventory/OneIPMPLS/edit/',array('as' => 'inventory.OneIPMPLS.update', 'uses' => 'DatekController@update'));
// Route::get('/inventory/OneIPMPLS/detail/{regional}',array('as' => 'inventory.OneIPMPLS.detail', 'uses' => 'DatekController@DetailOneIPMPLS'));
Route::get('/inventory/OneIPMPLS/overthreshold', array('as' => 'inventory.OneIPMPLS.overthreshold', 'uses' => 'DatekController@OneIPMPLSOverThreshold'));
Route::get('/inventory/OneIPMPLS/detail/delete/{data}',array('as' => 'inventory.OneIPMPLS.detail.delete', 'uses' => 'DatekController@DeleteOneIPMPLS'));


Route::get('/inventory/ipran',array('as' => 'inventory.ipran', 'uses' => 'DatekController@Ipran'));
Route::get('/inventory/ipran/import', array('as' => 'inventory.ipran.import', 'uses' => 'DatekController@ImportIpran'));
Route::post('/inventory/ipran/import', array('as' => 'inventory.ipran.import', 'uses' => 'DatekController@PostImportIpran'));
Route::post('/inventory/ipran/import/save', array('as' => 'inventory.ipran.import.save', 'uses' => 'DatekController@SaveImportIpran'));
Route::get('/inventory/ipran/edit/{ipran}', array('as' => 'inventory.ipran.show', 'uses' => 'DatekController@ShowIpran'));
Route::post('/inventory/ipran/edit/',array('as' => 'inventory.ipran.update', 'uses' => 'DatekController@updateIPBB'));
// Route::get('/inventory/ipran/detail/{regional}', array('as' => 'inventory.ipran.detail', 'uses' => 'DatekController@DetailIpran'));
Route::get('/inventory/ipran/overthreshold', array('as' => 'inventory.ipran.overthreshold', 'uses' => 'DatekController@IpranOverThreshold'));
// Route::get('/inventory/ipran/detail/delete/{data}', array('as' => 'inventory.ipran.detail.delete', 'uses' => 'DatekController@DeleteIpran'));

Route::get('/inventory/ipran/detail/delete/{data}', array('as' => 'inventory.ipran.detail.delete', 'uses' => 'IpranController@DeleteIpran'));
Route::get('/inventory/ipran/detail/{regional}', array('as' => 'inventory.ipran.detail', 'uses' => 'IpranController@DetailIpran'));



Route::get('/inventory/ipbb',array('as' => 'inventory.ipbb', 'uses' => 'DatekController@Ipbb'));
Route::get('/inventory/ipbb/import', array('as' => 'inventory.ipbb.import', 'uses' => 'DatekController@ImportIpbb'));
Route::post('/inventory/ipbb/import', array('as' => 'inventory.ipbb.import', 'uses' => 'DatekController@PostImportIpbb'));
Route::post('/inventory/ipbb/import/save', array('as' => 'inventory.ipbb.import.save', 'uses' => 'DatekController@SaveImportIpbb'));
Route::get('/inventory/ipbb/edit/{ipbb}', array('as' => 'inventory.ipbb.show', 'uses' => 'DatekController@ShowIpbb'));
Route::post('/inventory/ipbb/edit/',array('as' => 'inventory.ipbb.update', 'uses' => 'DatekController@update'));
// Route::get('/inventory/ipbb/detail/{regional}', array('as' => 'inventory.ipbb.detail', 'uses' => 'DatekController@DetailIpbb'));
// Route::get('/inventory/ipbb/detail/{regional}', array('as' => 'inventory.ipbb.detail', 'uses' => 'IpbbController@DetailIpbb'));
Route::get('/inventory/ipbb/detail/{regional}', array('as' => 'inventory.ipbb.detail', 'uses' => 'IpbbController@DetailIpran'));
Route::get('/inventory/ipbb/overthreshold', array('as' => 'inventory.ipbb.overthreshold', 'uses' => 'DatekController@IpbbOverThreshold'));
// Route::get('/inventory/ipbb/detail/delete/{data}', array('as' => 'inventory.ipbb.detail.delete', 'uses' => 'DatekController@DeleteIpbb'));


Route::get('/inventory/ix', array('as' => 'inventory.ix', 'uses' => 'DatekController@Ix'));
Route::get('/inventory/ix/import', array('as' => 'inventory.ix.import', 'uses' => 'DatekController@ImportIx'));
Route::post('/inventory/ix/import', array('as' => 'inventory.ix.import', 'uses' => 'DatekController@PostImportIx'));
Route::post('/inventory/ix/import/save', array('as' => 'inventory.ix.import.save', 'uses' => 'DatekController@SaveImportIx'));
Route::get('/inventory/ix/edit/{ix}', array('as' => 'inventory.ix.show', 'uses' => 'DatekController@ShowIx'));

// Route::get('/inventory/ix/edit_metroe_end1_telkom/', array('as' => 'inventory.ix.edit_metroe_end1_telkom', 'uses' => 'DatekController@EditMetroeEnd1TelkomIx'));
Route::get('/inventory/ix/edit_metroe_end1_telkom/{ix}/{param}', array('as' => 'inventory.ix.edit_metroe_end1_telkom', 'uses' => 'DatekController@EditMetroeEnd1TelkomIx'));
Route::get('/inventory/ix/edit_router_end1_tsel/{ix}/{param}', array('as' => 'inventory.ix.edit_router_end1_tsel', 'uses' => 'DatekController@EditRouterEnd1TelkomIx'));

Route::post('/inventory/ix/edit/',array('as' => 'inventory.ix.update', 'uses' => 'DatekController@updateIX'));
// Route::get('/inventory/ix/detail/{regional}', array('as' => 'inventory.ix.detail', 'uses' => 'DatekController@DetailIx'));
Route::get('/inventory/ix/detail/{regional}', array('as' => 'inventory.ix.detail', 'uses' => 'IxController@DetailIx'));
Route::get('/inventory/ix/overthreshold', array('as' => 'inventory.ix.overthreshold', 'uses' => 'DatekController@IxOverThreshold'));
Route::get('/inventory/ix/detail/delete/{data}', array('as' => 'inventory.ix.detail.delete', 'uses' => 'DatekController@DeleteIx'));

//Route::get('/crq', function () { return view('modul_crq.list_crq'); });
//Route::get('/crq/input', function () { return view('modul_crq.input_crq'); });
//Route::get('/crq/edit', function () { return view('modul_crq.edit_crq'); });

Route::get('/crq',array('as' => 'crq.index', 'uses' => 'CRQController@index'));

Route::get('/crq/addmore',array('as' => 'crq.admore', 'uses' => 'CRQController@addMore'));
Route::post("addmore","CRQController@addMorePost")->name('addmorePost');
Route::get('/crq/hapus/{crq}',array('as' => 'crq.hapus', 'uses' => 'CRQController@HapusCRQ'));
Route::get('/crq/TambahLokasi/{crq}',array('as' => 'crq.tambahlokasi', 'uses' => 'CRQController@EditCRQ'));
Route::get('/crq/edit/{crq}',array('as' => 'crq.edit', 'uses' => 'CRQController@EditCRQ'));
Route::post('/crq/edit/',array('as' => 'crq.update', 'uses' => 'CRQController@UpdateCRQ'));
Route::get('/crq/detail/edit/{crq}',array('as' => 'crq.detail.edit', 'uses' => 'CRQController@EditDetailCRQ'));
Route::post('/crq/detail/edit/',array('as' => 'crq.detail.c', 'uses' => 'CRQController@UpdateDetailCRQ'));
Route::get('/crq/detail/hapus/{crq}',array('as' => 'crq.detail.hapus', 'uses' => 'CRQController@HapusDetailCRQ'));
Route::get('/crq/tambah/',array('as' => 'crq.tambah', 'uses' => 'CRQController@TambahCRQ'));
Route::post('/crq/tambah/',array('as' => 'crq.tambah.baru', 'uses' => 'CRQController@TambahCRQBaru'));
Route::get('/crq/tambah/detail/{crq}',array('as' => 'crq.tambah.detail', 'uses' => 'CRQController@TambahCRQDetail'));
Route::post('/crq/tambah/detail',array('as' => 'crq.tambah.baru.detail', 'uses' => 'CRQController@TambahCRQBaruDetail'));

//Route::resource('rca','RCAController');
//Route::resource('inventory','DatekController');
//Route::resource('crq','CRQController');

Route::get('import-export-csv-excel',array('as'=>'excel.import','uses'=>'FileController@importExportExcelORCSV'));
Route::post('import-csv-excel',array('as'=>'import-csv-excel','uses'=>'DatekController@importFileIntoDB'));


Route::get('/inventory/getbackhaul', array('as' => 'inventory.backhaul.get', 'uses' => 'DatekController@getBackhaul'));
Route::get('/inventory/getoneip', array('as' => 'inventory.backhaul.get', 'uses' => 'DatekController@getOneip'));
Route::get('/inventory/getipran', array('as' => 'inventory.ipran.get', 'uses' => 'DatekController@getIpran'));
Route::get('/inventory/getix', array('as' => 'inventory.ix.get', 'uses' => 'DatekController@getIX'));
Route::get('/inventory/getipbb', array('as' => 'inventory.ipbb.get', 'uses' => 'DatekController@getIPBB'));
Route::get('/inventory/getumax', array('as' => 'inventory.umax.get', 'uses' => 'DatekController@getumax'));
Route::get('/inventory/getucap', array('as' => 'inventory.ucap.get', 'uses' => 'DatekController@getucap'));
Route::get('/inventory/getucapsummary', array('as' => 'inventory.ucapsummary.get', 'uses' => 'DatekController@getucapsummary'));
Route::get('/inventory/getutilaccess', array('as' => 'inventory.utilaccess.get', 'uses' => 'DatekController@getUtilAccess'));
Route::get('/inventory/getutilbackhaul', array('as' => 'inventory.utilbackhaul.get', 'uses' => 'DatekController@getUtilBackhaul'));
Route::get('/inventory/synchronizeutil', array('as' => 'inventory.synchronize.get', 'uses' => 'DatekController@SynchronizeUtil'));
Route::get('/inventory/synchronizeopenapi', array('as' => 'openapi.synchronize.get', 'uses' => 'DatekController@SynchronizeOpenAPI'));
Route::get('/inventory/synchronizecity', array('as' => 'inventory.synchronize.city', 'uses' => 'DatekController@SynchronizeCities'));
Route::get('/inventory/getutilopenapi', array('as' => 'inventory.utilopenapi.get', 'uses' => 'DatekController@GetUtilOpenAPI'));
Route::get('/inventory/cleanduplicate', array('as' => 'inventory.cleanduplicate.get', 'uses' => 'DatekController@CleanDuplicate'));
Route::get('/inventory/checkapilink', array('as' => 'inventory.checkapilink.get', 'uses' => 'DatekController@CheckAPILink'));
Route::get('/inventory/checkldap', array('as' => 'inventory.checkldap.get', 'uses' => 'DatekController@CheckLDAP'));
Route::get('/inventory/checksetting', array('as' => 'inventory.checksetting.get', 'uses' => 'DatekController@CheckSetting'));
Route::get('/inventory/gettsel', array('as' => 'manual.tsel.get', 'uses' => 'ManualCommand@gettsel'));

/* DETAIL REGIONAL DATA FROM MAPS */

Route::get('/inventory/regional1', function () { return view('modul_inventory.regional.regional1'); });
Route::get('/inventory/regional2', function () { return view('modul_inventory.regional.regional2'); });
Route::get('/inventory/regional3', function () { return view('modul_inventory.regional.regional3'); });
Route::get('/inventory/regional4', function () { return view('modul_inventory.regional.regional4'); });
Route::get('/inventory/regional5', function () { return view('modul_inventory.regional.regional5'); });
Route::get('/inventory/regional6', function () { return view('modul_inventory.regional.regional6'); });
Route::get('/inventory/regional7', function () { return view('modul_inventory.regional.regional7'); });

/* DETAIL REGIONAL DATA FROM MAPS */

Route::get('/inventory/getsummary', array('as' => 'inventory.summary.get', 'uses' => 'DatekController@getSummary'));
Route::get('/inventory/getundetected', array('as' => 'inventory.undetected.get', 'uses' => 'DatekController@getUndetected'));


Route::get('/inventory/getapiutil', array('as' => 'inventory.apiutil.get', 'uses' => 'DatekController@GetApiUtil'));
Route::get('/inventory/saveapiutil', array('as' => 'inventory.apiutil.save', 'uses' => 'DatekController@SaveApiUtil'));
Route::get('/inventory/checkport', array('as' => 'inventory.port.check', 'uses' => 'DatekController@CheckPort'));


Route::post('/inventory/search/', array('as' => 'inventory.search', 'uses' => 'DatekController@Search'));



Route::get('/setting/jovice', array('as' => 'jovice.api.set', 'uses' => 'HomeController@settingjovice'));



Route::get('/inventory/export/access/{access}', ['as' => 'inventory.export.access', 'uses' => 'ExportController@access']);
Route::get('/inventory/export/backhaul/{backhaul}', ['as' => 'inventory.export.backhaul', 'uses' => 'ExportController@backhaul']);
Route::get('/inventory/export/OneIPMPLS/{OneIPMPLS}', ['as' => 'inventory.export.OneIPMPLS', 'uses' => 'ExportController@OneIPMPLS']);
Route::get('/inventory/export/ipbb/{ipbb}', ['as' => 'inventory.export.ipbb', 'uses' => 'ExportController@ipbb']);
Route::get('/inventory/export/ipran/{ipran}', ['as' => 'inventory.export.ipran', 'uses' => 'ExportController@ipran']);
Route::get('/inventory/export/ix/{ix}', ['as' => 'inventory.export.ix', 'uses' => 'ExportController@ix']);

//Route::group(['as'=>'rca.'], function() {
//Route::resource('/rca/detail', 'RCADetailController');
//});
//Route::group(['as'=>'crq.'], function() {
//Route::resource('/crq/detail', 'CRQDetailController');
//});



//Display Index Page
Route::get('/rca/detail', 'RCADetailController@index');
// Populate Data in Edit Modal Form
Route::get('/rca/detail/{detail}', array('as' => 'rca.detail.get', 'uses' => 'RCADetailController@show'));
//create New Product
Route::post('/rca/detail', 'RCADetailController@store');
// update Existing Product
Route::put('/rca/detail/{detail}', 'RCADetailController@update');
// delete product
Route::delete('/rca/detail/{detail}', 'RCADetailController@destroy');

//Display Index Page
Route::get('/crq/detail', 'CRQDetailController@index');
// Populate Data in Edit Modal Form
Route::get('/crq/detail/{detail}', array('as' => 'crq.detail.get', 'uses' => 'CRQDetailController@show'));
//create New Product
Route::post('/crq/detail', 'CRQDetailController@store');
// update Existing Product
Route::put('/crq/detail/{detail}', 'CRQDetailController@update');
// delete product
Route::delete('/crq/detail/{detail}', 'CRQDetailController@destroy');



Route::get('/setting/user',array('as' => 'setting.user', 'uses' => 'UserController@User'));
Route::get('/setting/user/hapus/{user}',array('as' => 'setting.user.hapus', 'uses' => 'UserController@HapusUser'));
Route::get('/setting/user/detail/{user}',array('as' => 'setting.user.detail', 'uses' => 'UserController@DetailUser'));
Route::get('/setting/user/update/{user}',array('as' => 'setting.user.edit', 'uses' => 'UserController@EditUser'));
Route::post('/setting/user/update/',array('as' => 'setting.user.update', 'uses' => 'UserController@UpdateUser'));
Route::get('/setting/user/tambah/',array('as' => 'setting.user.tambah', 'uses' => 'UserController@TambahUser'));
Route::post('/setting/user/tambah/',array('as' => 'setting.user.tambah.baru', 'uses' => 'UserController@TambahUserBaru'));


Route::get('/setting/role',array('as' => 'setting.role', 'uses' => 'RoleController@Role'));
Route::get('/setting/getrole',array('as' => 'setting.getrole', 'uses' => 'RoleController@getRole'));
Route::get('/setting/role/hapus/{role}',array('as' => 'setting.role.hapus', 'uses' => 'RoleController@HapusRole'));
Route::get('/setting/role/detail/{role}',array('as' => 'setting.role.detail', 'uses' => 'RoleController@DetailRole'));
Route::get('/setting/role/update/{role}',array('as' => 'setting.role.edit', 'uses' => 'RoleController@EditRole'));
Route::post('/setting/role/update/',array('as' => 'setting.role.update', 'uses' => 'RoleController@UpdateRole'));
Route::get('/setting/role/tambah/',array('as' => 'setting.role.tambah', 'uses' => 'RoleController@TambahRole'));
Route::post('/setting/role/tambah/',array('as' => 'setting.role.tambah.baru', 'uses' => 'RoleController@TambahRoleBaru'));


Route::get('/setting/push',array('as' => 'setting.push', 'uses' => 'PushController@index'));
Route::get('/setting/push/{token}',array('as' => 'setting.push.post', 'uses' => 'PushController@post'));

Route::get('/push/test',array('as' => 'setting.push.test', 'uses' => 'PushController@push'));


// Notifications
Route::post('notifications', 'NotificationController@store');
Route::get('notifications', 'NotificationController@index');
Route::patch('notifications/{id}/read', 'NotificationController@markAsRead');
Route::post('notifications/mark-all-read', 'NotificationController@markAllRead');
Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss');

// Push Subscriptions
Route::post('subscriptions', 'PushSubscriptionController@update');
Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');

// status
Route::get('status', 'StatusController@status');
Route::get('status/direct', 'StatusController@getBackhaul');
Route::get('status/sync', 'StatusController@SynchronizeOpenAPI');
Route::get('/status/tambah/',array('as' => 'status.tambah', 'uses' => 'StatusController@TambahStatus'));
Route::post('/status/tambah_baru/',array('as' => 'status.tambah.baru', 'uses' => 'StatusController@TambahStatusBaru'));
Route::get('/status/update/{role}',array('as' => 'status.edit', 'uses' => 'StatusController@EditStatus'));
Route::post('/status/update/',array('as' => 'status.update', 'uses' => 'StatusController@UpdateStatus'));
Route::get('/status/hapus/{role}',array('as' => 'status.hapus', 'uses' => 'StatusController@HapusStatus'));
Route::get('/status/detail/{role}',array('as' => 'status.detail', 'uses' => 'StatusController@DetailStatus'));
Route::get('/status/curl/{role}',array('as' => 'status.curl', 'uses' => 'StatusController@http_request'));
Route::get('/status/oreo/',array('as' => 'status.coba_curl', 'uses' => 'StatusController@oreo'));
Route::get('/status/get_request/',array('as' => 'status.get_request', 'uses' => 'StatusController@get_request'));

// relasi IPBB
Route::get('relasi/valid/{id}', 'RelasiIpbbController@idrow');
Route::get('relasi/validNot/{id}', 'RelasiIpbbController@idrowNot');
Route::get('relasi/makeNe/{id}/{ids}', 'RelasiIpbbController@updateNe');
Route::get('relasi/makeSsp/{id}/{ids}', 'RelasiIpbbController@updateSsp');
Route::get('relasi/makeTie/{id}/{ids}', 'RelasiIpbbController@updateTie');
Route::get('relasi/cekApi', 'RelasiIpbbController@cekApi');
Route::get('relasi/makeSTO/{id}/{ids}', 'RelasiIpbbController@updateSto');


// Route::get('relasi/editNeTransport', 'RelasiIpbbController@editDataNe');

// IPBB NEW
Route::get('ipbb/data/{id}', 'IpbbController@IpbbRegional');
Route::get('ipbb/kota_a/{id}/{var}', 'IpbbController@editKotaA');
Route::get('ipbb/kota_b/{id}/{var}', 'IpbbController@editKotaB');
Route::get('ipbb/router_a/{id}/{var}', 'IpbbController@editRouterA');
Route::get('ipbb/router_b/{id}/{var}', 'IpbbController@editRouterB');
Route::get('ipbb/capacity/{id}/{var}', 'IpbbController@editCapacity');
Route::get('ipbb/layer/{id}/{var}', 'IpbbController@editLayer');
Route::get('ipbb/system/{id}/{var}', 'IpbbController@editSystem');
Route::get('ipbb/transport/{id}/{var}', 'IpbbController@editTransport');

Route::get('ipbb/port_a', 'IpbbController@editPortA');
Route::get('ipbb/port_a', 'IpbbController@editPortA');

Route::post('ipbb/editsPorta', 'IpbbController@editsPortA');
Route::post('ipbb/editsPortb', 'IpbbController@editsPortB');
Route::get('ipbb/editKetBackhaul/{id}/{var}', 'IpbbController@editKetBackhaul');
Route::get('ipbb/editKotaBackhaul/{id}/{var}', 'IpbbController@editKotaBackhaul');

// relasi IPran
Route::get('relasiran/valid/{id}', 'RelasiIpranController@idrow');
Route::get('relasiran/validNot/{id}', 'RelasiIpranController@idrowNot');
Route::get('relasiran/makeSTO/{id}/{ids}', 'RelasiIpranController@updateSto');
Route::get('relasiran/makeNe/{id}/{ids}', 'RelasiIpranController@updateNe');
// Route::post('relasiran/makeNe', 'RelasiIpranController@updateNe');
Route::get('relasiran/makeSsp/{id}/{ids}', 'RelasiIpranController@updateSsp');
Route::get('relasiran/makeTie/{id}/{ids}', 'RelasiIpranController@updateTie');
Route::get('relasiran/cekApi', 'RelasiIpranController@cekApi');
// Route::get('relasi/editNeTransport', 'RelasiIpbbController@editDataNe');

// IPRAN NEW
Route::get('ipran/data/{id}', 'IpranController@IpbbRegional');

Route::get('ipran/kota_a/{id}/{var}', 'IpranController@editKotaA');
Route::get('ipran/kota_b/{id}/{var}', 'IpranController@editKotaB');
Route::get('ipran/router_a/{id}/{var}', 'IpranController@editRouterA');
Route::get('ipran/router_b/{id}/{var}', 'IpranController@editRouterB');
Route::get('ipran/capacity/{id}/{var}', 'IpranController@editCapacity');
Route::get('ipran/layer/{id}/{var}', 'IpranController@editLayer');
Route::get('ipran/system/{id}/{var}', 'IpranController@editSystem');
Route::get('ipran/transport/{id}/{var}', 'IpranController@editTransport');

Route::get('ipran/port_a', 'IpbbController@editPortA');

Route::post('ipran/editsPorta', 'IpranController@editsPortA');
Route::post('ipran/editsPortb', 'IpranController@editsPortB');

// Route::get('ipbb/port_b/{id}/{var}', 'IpbbController@editPortB');
// Route::post('ipbb/port_b/{id}', 'IpbbController@editPortB'); 

Route::post('/inventory/Newipbb/import', array('as' => 'inventory.Newipbb.import', 'uses' => 'IpbbController@PostImportIpbb'));
Route::post('/inventory/Newipbb/import/save', array('as' => 'inventory.Newipbb.import.save', 'uses' => 'IpbbController@SaveImportIpbb'));

Route::get('/inventory/ipbb/detail/delete/{data}', array('as' => 'inventory.ipbb.detail.delete', 'uses' => 'IpbbController@DeleteIpran'));

Route::post('/inventory/Newipran/import/save', array('as' => 'inventory.Newipran.import.save', 'uses' => 'IpranController@SaveImportIpran'));
Route::post('/inventory/Newipran/import', array('as' => 'inventory.Newipran.import', 'uses' => 'IpranController@PostImportIpran'));

// ACCESS DIRECT METRO
Route::get('inventory/accessDirect', array('as' => 'inventory.accessdirect.insertapi', 'uses' => 'AccessDirectController\GetAccessDirectController@InsertApi'));
Route::get('inventory/accessDirect/api/{id}', array('as' => 'inventory.accessdirect.getapi1', 'uses' => 'AccessDirectController\GetAccessDirectController@getapiDirectMetroReg1'));
Route::get('inventory/accessDirect/apiodata', array('as' => 'inventory.accessdirect.apiodata', 'uses' => 'AccessDirectController\ApiAccessDirectController@apiodata'));
Route::get('/inventory/detail/accessdirect/{id}', array('as' => 'inventory.detail.accessdirect', 'uses' => 'AccessDirectController\GetAccessDirectController@DetailDirect'));
Route::get('/inventory/detail/test', array('as' => 'inventory.detail.test', 'uses' => 'AccessDirectController\GetAccessDirectController@test'));

//NEW IX
Route::get('ix/editket/{id}/{var}', 'IxController@editket');
Route::get('ix/editkota/{id}/{var}', 'IxController@editkota');

Route::get('/coba', array('as' => 'coba', 'uses' => 'CobaController@index'));
Route::get('/cobascrap', array('as' => 'cobascrap', 'uses' => 'CobaController@cobaScrap'));
Route::get('/cobascrapran', array('as' => 'cobascrapran', 'uses' => 'CobaController@cobascrapran'));


// NEW ONEIP
Route::get('/inventory/OneIPMPLS/detail/{regional}',array('as' => 'inventory.OneIPMPLS.detail', 'uses' => 'OneipController@DetailOneIPMPLS'));

//Scrapping Ipran
Route::get('ipran/utilran', array('as' => 'getutilran', 'uses' => 'IpranController@getUtilScrapRan'));
Route::get('ipran/getHref', array('as' => 'gethrefdatran', 'uses' => 'IpranController@getHrefDataRandom'));
Route::get('ipran/getRegion', array('as' => 'getutilscrapran', 'uses' => 'IpranController@scrapRegion'));

// Scraping IPBB
Route::get('ipbb/utilpbb', array('as' => 'getutilscrappbb', 'uses' => 'IpbbController@getUtilScrapPbb'));
Route::get('ipbb/getHref', array('as' => 'getutilscrapran', 'uses' => 'IpbbController@getHrefDataJakabaring'));
Route::get('ipbb/getRegion', array('as' => 'getutilscrapran', 'uses' => 'IpbbController@scrapRegion'));

// Route::get('/hdtuto', function() {

//     $crawler = Goutte::request('GET', 'http://nicesnippets.com');

//     // $crawler->filter('.blog-post-item h2 a')->each(function ($node) {
//     $crawler->filter('.col-sm-4 h2 a')->each(function ($node) {

//       dump($node->text());

//     });

// });

// Manifest file (optional if VAPID is used)
Route::get('manifest.json', function () {
    return [
        'name' => config('app.name'),
        'gcm_sender_id' => config('webpush.gcm.sender_id')
    ];
});
