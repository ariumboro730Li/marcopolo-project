--
-- Table structure for table `crq`
--

DROP TABLE IF EXISTS `crq`;
CREATE TABLE `crq` (
  `idactivity` bigint(20) UNSIGNED NOT NULL,
  `activity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_telkom` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_tsel` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mop_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `crq`
--

INSERT INTO `crq` (`idactivity`, `activity`, `reg_telkom`, `reg_tsel`, `mop_file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'aaaaaaa', '', '', 'aaaaaaa', '2018-05-16 08:39:01', '2018-05-16 08:39:01', NULL),
(2, 'aaaaaaa', '', '', 'aaaaaaa', '2018-05-16 08:40:01', '2018-05-16 08:40:01', NULL),
(3, 'aaaaaaa', '', '', 'aaaaaaa', '2018-05-16 08:41:02', '2018-05-16 08:41:02', NULL),
(4, 'Perbaikan Kabel', 'Regional 1', 'Regional 1', NULL, '2018-06-26 08:39:31', '2018-06-26 08:39:31', NULL),
(5, 'Peremajaan Komponen', 'Regional 3', 'Regional 7', NULL, '2018-06-26 08:53:36', '2018-06-26 08:53:36', NULL),
(6, 'Pembaruan Kabel', 'Regional 1', 'Regional 1', NULL, '2018-06-26 08:55:32', '2018-06-26 08:55:32', NULL),
(7, NULL, 'Regional 1', 'Regional 1', NULL, '2018-06-26 09:03:10', '2018-06-26 09:03:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `crq_detail`
--

DROP TABLE IF EXISTS `crq_detail`;
CREATE TABLE `crq_detail` (
  `iddetail` bigint(20) UNSIGNED NOT NULL,
  `lokasi` char(50) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `pic` char(100) NOT NULL,
  `crq` char(50) NOT NULL,
  `status` char(10) NOT NULL,
  `ket` varchar(800) NOT NULL,
  `idactivity` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crq_detail`
--

INSERT INTO `crq_detail` (`iddetail`, `lokasi`, `waktu_mulai`, `waktu_selesai`, `pic`, `crq`, `status`, `ket`, `idactivity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kebun Sawit', '2018-06-26 22:39:00', '2018-06-27 22:39:00', 'Deisna', 'ada', 'Open', 'asadas', 4, '2018-06-26 08:41:08', '2018-06-26 08:41:08', NULL),
(2, 'Bandung', '2018-06-26 22:41:00', '2018-06-29 22:41:00', 'Rivo', 'Ada', 'Open', 'asadas', 4, '2018-06-26 08:41:37', '2018-06-26 08:41:37', NULL),
(3, 'Jatinangor', '2018-06-26 22:53:00', '2018-06-27 22:53:00', 'Deisna', 'Ada', 'Open', 'asadas', 5, '2018-06-26 08:54:00', '2018-06-26 08:54:00', NULL),
(4, 'Kebun Sawit', '2018-06-26 22:54:00', '2018-06-29 22:54:00', 'Rivo', 'Ada', 'Open', 'asadas', 5, '2018-06-26 08:54:45', '2018-06-26 08:54:45', NULL);
