<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataRandomsOcc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_randoms_occ', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("witel", 30)->nullable();
            $table->text("href")->nullable();
            $table->string("class", 100)->nullable();
            $table->string("in_max", 100)->nullable();
            $table->string("in_avg", 100)->nullable();
            $table->string("in_current", 100)->nullable();
            $table->string("out_max", 100)->nullable();
            $table->string("out_avg", 100)->nullable();
            $table->string("out_current", 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_randoms_occ');
    }
}
