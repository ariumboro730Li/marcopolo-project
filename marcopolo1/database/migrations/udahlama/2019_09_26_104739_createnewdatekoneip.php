<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createnewdatekoneip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_datek_oneip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reg_tsel',1)->nullable();
            $table->string('kota_tsel', 100)->nullable();
            $table->string('routername_tsel', 50)->nullable();
            $table->string('port_tsel', 30)->nullable();
            $table->string('reg_telkom', 1)->nullable();
            $table->string('sto_telkom', 30)->nullable();
            $table->string('pe_telkom', 100)->nullable();
            $table->string('port_telkom', 100)->nullable();
            $table->string('description', 191)->nullable();
            $table->mediumInteger('capacity')->nullable();
            $table->double('utilization_cap')->nullable();
            $table->double('utilization_in')->nullable();
            $table->double('utilization_out')->nullable();
            $table->double('utilization_max')->nullable();
            $table->string('layer', 30)->nullable();
            $table->string('system', 30)->nullable();
            $table->string('transport_type', 30)->nullable();
            $table->string('keterangan', 191)->nullable();
            $table->date('date_detected')->nullable();
            $table->date('date_undetected')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_datek_oneip');
    }
}
