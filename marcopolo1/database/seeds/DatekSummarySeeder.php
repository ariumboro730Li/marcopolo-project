<?php

use Illuminate\Database\Seeder;

class DatekSummarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        foreach (range(1, 12) as $loop) {
            DB::table('datek_summary')->insert([
                'month'          	=>  $faker->text(10),
                'year'              =>  $faker->randomLetter,
                'reg_telkom'        =>  $faker->text(10),
                'layer'             =>  $faker->text(10),
                'transport_system'  =>  $faker->text(10),  
                'transport_type'    =>  $faker->text(10),
                'link'              =>  $faker->randomDigit,
                'capacity'          =>  $faker->randomDigit
            ]);
        }
    }
}