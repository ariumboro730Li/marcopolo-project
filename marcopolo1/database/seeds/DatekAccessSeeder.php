<?php

use Illuminate\Database\Seeder;

class DatekAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        foreach (range(1, 5) as $loop) {
            DB::table('datek_access')->insert([
                'reg_tsel'			=>	$faker->unique()->randomDigit,
                'siteid_tsel'		=>	$faker->text(6),
                'sitename_tsel'		=>	$faker->text(40),
                'reg_telkom'		=>	$faker->unique()->randomDigit,
                'sto_gpon_telkom'	=>	$faker->text(20),
                'gpon_gpon_telkom'	=>	$faker->text(20),
                'port_gpon_telkom'	=>	$faker->text(10),
                'ne_radio_telkom'	=>	$faker->text(20),
                'fe_radio_telkom'	=>	$faker->text(20),
                'port_radio_telkom'	=>	$faker->text(10),
                'sto_dm_telkom'		=>	$faker->text(20),
                'me_dm_telkom'		=>	$faker->text(20),
                'port_dm_telkom'	=>	$faker->text(10),
                'capacity'			=>	$faker->randomDigit,
                'system'			=>	$faker->text(10),
                'date_detected'		=>	$faker->date('Y/m/d','now'),
                'date_undetected'	=>	$faker->date(null),
                'status'			=>  $faker->text(10)
            ]);
        }
    }
}