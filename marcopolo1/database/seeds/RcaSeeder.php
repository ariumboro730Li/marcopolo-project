<?php

use Illuminate\Database\Seeder;

class RcaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        foreach (range(1, 5) as $loop) {
            DB::table('rca')->insert([
            	'title'				=>	$faker->sentence(),
                'regional'			=>	$faker->sentence(),
                'date_time'			=>	$faker->dateTime('now',null),
                'duration'			=>	$faker->word(),
                'network_impact'	=>	$faker->text(),
                'service_impact'	=>	$faker->text(),
                'rc_category'		=>	$faker->word(),
                'rc_detail'			=>	$faker->text(),
                'action'			=>	$faker->text(),
                'evidence_file'		=>	$faker->sentence(),
                'topology_file'		=>	$faker->sentence(),
                'improvement_activity'	=>	$faker->sentence(),
                'status'			=>	$faker->word(),
                'tgl_target'		=>	$faker->date('Y/m/d','now'),
                'pic'				=>	$faker->word(),
            ]);
        }
    }
}