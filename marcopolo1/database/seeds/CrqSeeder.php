<?php

use Illuminate\Database\Seeder;

class CrqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        foreach (range(1, 5) as $loop) {
            DB::table('crq')->insert([
                'tgl_pengajuan'		=>	$faker->date('Y/m/d','now'),
                'activity'			=>	$faker->sentence(),
                'lokasi'			=>	$faker->word(),
                'tgl_mulai'			=>	$faker->date('Y/m/d','now'),
                'jam_mulai'			=>	$faker->time('H:i','now'),
                'tgl_selesai'		=>	$faker->date('Y/m/d','now'),
                'jam_selesai'		=>	$faker->time('H:i','now'),
                'durasi'			=>	$faker->word(),
                'impact_service'	=>	$faker->sentence(),
                'pic'				=>	$faker->word()
            ]);
        }
    }
}