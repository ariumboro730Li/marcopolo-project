<?php

use Illuminate\Database\Seeder;

class DatekIpranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        foreach (range(1, 5) as $loop) {
            DB::table('datek_ipran')->insert([
                'reg_tsel'          =>  $faker->unique()->randomDigit,
                'router_end1_tsel'  =>  $faker->text(20),
                'port_end1_tsel'    =>  $faker->text(10),
                'router_end2_tsel'  =>  $faker->text(20),
                'port_end2_tsel'    =>  $faker->text(10),
                'reg_telkom'        =>  $faker->unique()->randomDigit,
                'sto_end1_telkom'   =>  $faker->text(20),
                'metroe_end1_telkom'=>  $faker->text(20),
                'port_end1_telkom'  =>  $faker->text(10),
                'sto_end2_telkom'   =>  $faker->text(20),
                'metroe_end2_telkom'=>  $faker->text(20),
                'port_end2_telkom'  =>  $faker->text(10),
                'capacity'          =>  $faker->randomDigit,
                'layer'             =>  $faker->word,
                'system'            =>  $faker->word,
                'ne_transport_a'    =>  $faker->word,
                'board_transport_a' =>  $faker->word,
                'shelf_transport_a' =>  $faker->word,
                'slot_transport_a'  =>  $faker->word,
                'port_transport_a'  =>  $faker->word,
                'frek_transport_a'  =>  $faker->word,
                'ne_transport_b'    =>  $faker->word,
                'board_transport_b' =>  $faker->word,
                'shelf_transport_b' =>  $faker->word,
                'slot_transport_b'  =>  $faker->word,
                'port_transport_b'  =>  $faker->word,
                'frek_transport_b'  =>  $faker->word,
                'date_detected'		=>	$faker->date('Y/m/d','now'),
                'date_undetected'	=>	$faker->date(null),
                'status'			=>  $faker->text(10)
            ]);
        }
    }
}
