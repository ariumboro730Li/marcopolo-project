<!DOCTYPE html>
<html>

@include('support.head')

<body class="theme-red">

	<div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>

    <div class="overlay"></div>

    @include('navbar.sidebar')

    @yield('content') 

    @include('support.javascript')

    <script>
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('https://marcxopolo.telkom.co.id/service-worker.js').then(function(registration) {
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                }, function(err) {
                    console.log('ServiceWorker registration failed: ', err);
                });
            });
        }
//         //Add this below content to your HTML page, or add the js file to your page at the very top to register service worker
//         if (navigator.serviceWorker.controller) {
//             console.log('[PWA Builder] active service worker found, no need to register')
//         } else {
//
// //Register the ServiceWorker
//             navigator.serviceWorker.register('https://marcxopolo.telkom.co.id/pwabuilder-sw.js', {
//                 scope: './'
//             }).then(function(reg) {
//                 console.log('Service worker has been registered for scope:'+ reg.scope);
//             });
//         }

    </script>

</body>

</html>