@extends('layout')

@section('menu10')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Input - CRQ
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/crq')}}">CRQ</a></li>
                    <li class="active">Input CRQ</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>INPUT CRQ</h2>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="row clearfix">

                            {!! Form::open(['method'=>'POST','action'=>'CRQController@TambahCRQBaru']) !!}
                                <div class="col-md-12">

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="activity">Activity</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="activity" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="reg_tsel">Regional Telkomsel</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control" name="reg_tsel" id="reg_tsel" onchange="regtsel(event)">
                                                    <option value="Regional 1">Regional 1 (Sumbagut)</option>
                                                    <option value="Regional 2">Regional 2 (Sumbagsel)</option>
                                                    <option value="Regional 3">Regional 3 (Jabotabek)</option>
                                                    <option value="Regional 4">Regional 4 (Jabar)</option>
                                                    <option value="Regional 5">Regional 5 (Jateng)</option>
                                                    <option value="Regional 6">Regional 6 (Jatim)</option>
                                                    <option value="Regional 7">Regional 7 (Balnus)</option>
                                                    <option value="Regional 8">Regional 8 (Kalimantan)</option>
                                                    <option value="Regional 9">Regional 9 (Sulawesi)</option>
                                                    <option value="Regional 10">Regional 10 (Sumbagteng)</option>
                                                    <option value="Regional 11">Regional 11 (PUMA)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="reg_telkom">Regional Telkom</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input id="reg_telkom" type="text" class="form-control" name="reg_telkom" value="Regional 1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="activity">Lampiran MOP</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <input type="file" class="fileinput" name="mop_file" title="Browse file..."/>
                                            <span class="help-block">Format file: .doc, .docx, .xls, .xlsx, .pdf </span>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary pull-right">Isi Lokasi</button>
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/crq')}}'">Back</button>

                            {!! Form::close() !!}
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </section>

@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>

    <script>
        function regtsel(e) {
            if (e.target.value == "Regional 1" || e.target.value == "Regional 2" || e.target.value == "Regional 10") {document.getElementById("reg_telkom").value = "Regional 1";}
            else if (e.target.value == "Regional 3") {document.getElementById("reg_telkom").value = "Regional 2";}
            else if (e.target.value == "Regional 4") {document.getElementById("reg_telkom").value = "Regional 3";}
            else if (e.target.value == "Regional 5") {document.getElementById("reg_telkom").value = "Regional 4";}
            else if (e.target.value == "Regional 6" || e.target.value == "Regional 7") {document.getElementById("reg_telkom").value = "Regional 5";}
            else if (e.target.value == "Regional 8") {document.getElementById("reg_telkom").value = "Regional 6";}
            else if (e.target.value == "Regional 9" || e.target.value == 11) {document.getElementById("reg_telkom").value = "Regional 7";}
        }
    </script>
@stop