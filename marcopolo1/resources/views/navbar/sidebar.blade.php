<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <form id="searchForm" name="searchForm" method="post" action="{{URL::Route('inventory.search')}}">
        @csrf
        <input type="text" name="keyword" id="keyword"   placeholder="CARI DI INVENTORY...">
        <input type="submit" style="visibility: hidden;" />
    </form>
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>

<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">ADMIN - CNSO</a>
        </div>
    
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a id="search" href="#" class="js-search" data-close="true"><i class="material-icons">
                search</i></a></li>
                @if(Request::url() === URL::Route('inventory.dashboard'))


                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">error</i>
                        <span class="label-count">5</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">HIGH UTILIZATION LINKS</li>
                        <li class="body">
                            <ul class="menu tasks">
                                <li>
                                    <a href="{{URL::Route('inventory.access.overthreshold')}}">
                                        <h4>
                                            Access
                                            @if(\App\DatekAccess::OverThreshold()>0)<span class="badge bg-pink pull-right">{{\App\DatekAccess::OverThreshold()}}</span>@else<span class="badge bg-pink pull-right">0</span>@endif
                                        </h4>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL::Route('inventory.backhaul.overthreshold')}}">
                                        <h4>
                                            Backhaul
                                            @if(\App\DatekBackhaul::OverThreshold()>0)<span class="badge bg-cyan pull-right">{{\App\DatekBackhaul::OverThreshold()}}</span>@else<span class="badge bg-cyan pull-right">0</span>@endif
                                        </h4>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL::Route('inventory.ipran.overthreshold')}}">
                                        <h4>
                                            IPRAN
                                            @if(\App\DatekIpran::OverThreshold()>0)<span class="badge bg-light-green pull-right">{{\App\DatekIpran::OverThreshold()}}</span>@else<span class="badge bg-light-green pull-right">0</span>@endif
                                        </h4>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL::Route('inventory.ipbb.overthreshold')}}">
                                        <h4>
                                            IPBB
                                            @if(\App\DatekIpbb::OverThreshold()>0)<span class="badge bg-orange pull-right">{{\App\DatekIpbb::OverThreshold()}}</span>@else<span class="badge bg-orange pull-right">0</span>@endif
                                        </h4>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL::Route('inventory.ix.overthreshold')}}">
                                        <h4>
                                            IX
                                            @if(\App\DatekIx::OverThreshold()>0)<span class="badge bg-blue pull-right">{{\App\DatekIx::OverThreshold()}}</span>@else<span class="badge bg-blue pull-right">0</span>@endif
                                        </h4>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<section>
    
    <aside id="leftsidebar" class="sidebar">
        
        <div class="user-info">
            <div class="image">
                <img src="{{asset('images/profile.png')}}" width="64" height="64" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{\Illuminate\Support\Facades\Auth::user()->name}}</div>
                <div class="email">{{\Illuminate\Support\Facades\Auth::user()->ldap}}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{url('/setting/password')}}"><i class="material-icons">person</i>Account</a></li>
                        <li role="seperator" class="divider"></li>
                        <li class="js-sweetalert"><a href="{{url('/logout')}}" data-type="logout"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="menu">
            <ul class="list">
                @if(strcmp(\App\User::getroles("rca"),"on") == 0)
                <li class="header">RCA & Improvement Tracking</li>
                <li @yield('menu1')>
                    <a href="{{url('/rca/dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard RCA</span>
                    </a>
                </li>
                <li @yield('menu2')>
                    <a href="{{url('/rca')}}">
                        <i class="material-icons">assignment</i>
                        <span>RCA</span>
                    </a>
                </li>
                <li @yield('menu3')>
                    <a href="{{url('/rca/tracking')}}">
                        <i class="material-icons">timeline</i>
                        <span>Improvement Tracking</span>
                    </a>
                </li>
                @endif
                {{--{{\App\Role::getAccess("RCA")}}--}}
                    @if(strcmp(\App\User::getroles("inventory"),"on") == 0)
                <li class="header">Inventory Datek </li>
                <li @yield('menu4')>
                    <a href="{{url('/inventory/')}}">
                        <i class="material-icons">assessment</i>
                        <span>Dashboard Inventory</span>
                    </a>
                </li>
                <li @yield('menu5')>
                    <a href="{{url('/inventory/access')}}">
                        <i class="material-icons">dns</i>
                        <span>Access </span>
                    </a>
                </li>
                <li @yield('menu13')>
                    <a href="{{url('/inventory/accessDirectMetro')}}">
                        <i class="material-icons">dns</i>
                        <span>Access Direct Metro </span>
                    </a>
                </li>
                <li @yield('menu6')>
                    <a href="{{url('/inventory/backhaul')}}">
                        <i class="material-icons">dns</i>
                        <span>Backhaul</span>
                    </a>
                </li>
                <li @yield('OneIPMPLS')>
                    <a href="{{url('/inventory/OneIPMPLS')}}">
                        <i class="material-icons">dns</i>
                        <span>OneIPMPLS</span>
                    </a>
                </li>
                <li @yield('menu7')>
                    <a href="{{url('/inventory/ipran')}}">
                        <i class="material-icons">dns</i>
                        <span>IPRAN</span>
                    </a>
                </li>
                <li @yield('menu8')>
                    <a href="{{url('/inventory/ipbb')}}">
                        <i class="material-icons">dns</i>
                        <span>IPBB</span>
                    </a>
                </li>
                <li  @yield('menu9')>
                    <a href="{{url('/inventory/ix')}}">
                        <i class="material-icons">dns</i>
                        <span>IX</span>
                    </a>
                </li>
                    @endif
                    @if(strcmp(\App\User::getroles("crq"),"on") == 0)
                <li class="header">CRQ</li>
                <li @yield('menu10')>
                    <a href="{{url('/crq')}}">
                        <i class="material-icons">note_add</i>
                        <span>CRQ</span>
                    </a>
                </li>
                    @endif
                    @if(strcmp(\App\User::getroles("setting"),"on") == 0)
                <li class="header">Setting</li>
                <li @yield('menu11')>
                    <a href="{{url('/setting/user')}}">
                        <i class="material-icons">group</i>
                        <span>User</span>
                    </a>
                </li>
                <li @yield('menu12')>
                    <a href="{{url('/setting/role')}}">
                        <i class="material-icons">verified_user</i>
                        <span>Access Role</span>
                    </a>
                </li>
                <!-- <li @yield('menu13')>
                    <a href="{{url('/status')}}">
                        <i class="material-icons">verified_user</i>
                        <span>Status</span>
                    </a>
                </li> -->
                    @endif
                {{--<li class="header">Settings</li>--}}
                {{--<li @yield('menu10')>--}}
                    {{--<a href="{{url('/setting/jovice')}}">--}}
                        {{--<i class="material-icons">build</i>--}}
                        {{--<span>API Jovice</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <!-- <li class="header">Setting</li>
                <li>
                    <a href="{{url('/setting/user')}}">
                        <i class="material-icons">account_circle</i>
                        <span>User</span>
                    </a>
                </li> -->

            </ul>
        </div>

        <div class="legal">
            <div class="copyright">
                &copy; 2018 <a href="#">CNSO</a>.
            </div>
        </div>
        
    </aside>
    
</section>