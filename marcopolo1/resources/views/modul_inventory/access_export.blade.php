<table id="table_detail" class="align-center table table-striped table-bordered table-hover dataTable js-exportable">
    <thead>
    <tr>
        <th rowspan="2" class="no">ID</th>
        <th>Telkom</th>
        <th colspan="1">Telkomsel</th>
        <!-- <th colspan="10">Telkom</th> -->

        <th rowspan="2">Capacity (Mbps)</th>
        <th rowspan="2">Util Max</th>
        <th rowspan="2">System</th>
        <th rowspan="2">Date Detected</th>
        {{--<th rowspan="2">Date Undetected</th>--}}
        <th rowspan="2">Status</th>
        <th rowspan="2"></th>
    </tr>
    <tr>
        {{--<th rowspan="1">Regional</th>--}}
        <th rowspan="1">Site ID</th>
        <th rowspan="1">Site Name</th>
    {{--<th rowspan="1">Regional</th>--}}
    <!-- <th colspan="3">GPON</th>
                                            <th colspan="3">Radio</th>
                                            <th colspan="3">Direct Metro</th> -->
    </tr>
    <!-- <tr>
        <th>STO</th>
        <th>GPON</th>
        <th>Port</th>
        <th>NE</th>
        <th>FE</th>
        <th>Port</th>
        <th>STO</th>
        <th>ME</th>
        <th>Port</th>
    </tr> -->
    </thead>
    <tbody>

    <?php $i=0; ?>
    @foreach($access as $acs)
        <?php $i++; ?>
        <tr>
            <td>{{$i}}</td>
            {{--<td>{{$acs['reg_tsel']}}</td>--}}
            <td>{{$acs['siteid_tsel']}}</td>
            <td>{{$acs['sitename_tsel']}}</td>
            {{--<td>{{$acs['reg_telkom']}}</td>--}}
        <!-- <td>{{$acs['sto_gpon_telkom']}}</td>
                                            <td>{{$acs['gpon_gpon_telkom']}}</td>
                                            <td>{{$acs['port_gpon_telkom']}}</td>
                                            <td>{{$acs['ne_radio_telkom']}}</td>
                                            <td>{{$acs['fe_radio_telkom']}}</td>
                                            <td>{{$acs['port_radio_telkom']}}</td>
                                            <td>{{$acs['sto_dm_telkom']}}</td>
                                            <td>{{$acs['me_dm_telkom']}}</td>
                                            <td>{{$acs['port_dm_telkom']}}</td> -->
            <td>{{$acs['capacity']}}</td>
            <td>{{$acs['utilization_max']}}</td>
            <td>{{$acs['system']}}</td>
            <td>{{$acs['date_detected']}}</td>
            {{--<td>{{$acs['date_undetected']}}</td>--}}
            <td>{{$acs['status']}}</td>

        </tr>
    @endforeach
    </tbody>
</table>