<table class="align-center table table-striped table-bordered table-hover dataTable js-exportable">
    <thead>
    <tr>
        <th rowspan="3" class="no">ID</th>
        <th colspan="2">Telkom</th>
        <th colspan="2">Telkomsel</th>
        <!-- <th colspan="7">Telkom</th> -->
        <th rowspan="3">Capacity (Gbps)</th>
        <th rowspan="3">Util Max</th>
        {{--<th rowspan="3">Layer</th>--}}
        <th rowspan="3">System</th>
        <!-- <th colspan="12">Port/Interface Transport</th> -->
        <th rowspan="3">Date Detected</th>
        {{--<th rowspan="3">Date Undetected</th>--}}
        <th rowspan="3">Status</th>
        <th rowspan="3"></th>
    </tr>
    <tr>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="2">Node B</th>
        <th colspan="2">Node A</th>
    {{--<th rowspan="2">Regional</th>--}}
    <!-- <th colspan="6">Transport-A (WDM/FO)</th>
                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
    </tr>
    <tr>
        <th>Router Name</th>
        <th>Port</th>
        <th>Router Name</th>
        <th>Port</th>
        <!-- <th>STO</th>
        <th>Metro-E</th>
        <th>Port</th>
        <th>STO</th>
        <th>Metro-E</th>
        <th>Port</th>
        <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th>
        <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th> -->
    </tr>
    </thead>
    <tbody>

    <?php $i=0; ?>
    @foreach($ipbb as $ib)
        <?php $i++; ?>
        <tr>
            <td>{{$i}}</td>
            {{--<td>{{$ib['reg_tsel']}}</td>--}}
            <td>{{$ib['router_node_b']}}</td>
            <td>{{$ib['port_end2_tsel']}}</td>
            <td>{{$ib['router_node_a']}}</td>
            <td>{{$ib['port_end1_tsel']}}</td>
            {{--<td>{{$ib['reg_telkom']}}</td>--}}
            <td>{{$ib['capacity']}}</td>
            <td>{{$ib['utilization_max']}}</td>
            {{--<td>{{$ib['layer']}}</td>--}}
            <td>{{$ib['system']}}</td>
        <!-- <td>{{$ib['ne_transport_a']}}</td>
                                            <td>{{$ib['board_transport_a']}}</td>
                                            <td>{{$ib['shelf_transport_a']}}</td>
                                            <td>{{$ib['slot_transport_a']}}</td>
                                            <td>{{$ib['port_transport_a']}}</td>
                                            <td>{{$ib['frek_transport_a']}}</td>
                                            <td>{{$ib['ne_transport_b']}}</td>
                                            <td>{{$ib['board_transport_b']}}</td>
                                            <td>{{$ib['shelf_transport_b']}}</td>
                                            <td>{{$ib['slot_transport_b']}}</td>
                                            <td>{{$ib['port_transport_b']}}</td>
                                            <td>{{$ib['frek_transport_b']}}</td> -->
            <td>{{$ib['date_detected']}}</td>
            {{--<td>{{$ib['date_undetected']}}</td>--}}
            <td>{{$ib['status']}}</td>

        </tr>
    @endforeach
    </tbody>
</table>