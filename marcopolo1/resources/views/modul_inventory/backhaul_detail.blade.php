    @extends('layout')

@section('menu6')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
Backhaul Detail Regional - Inventory Datek
@stop

<style>
        .border-0 {
            border:none !important;
            width: 100%;
            background: transparent !important;
            text-align: center;
        }
    </style>
    
    
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/backhaul')}}">Backhaul</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>BACKHAUL - DETAIL REGIONAL {{$regional}}</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.backhaul', $regional)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">

                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 1300px !important">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>TTC</th>
                                            <th>Metro</th>
                                            <th>Port Router A</th>
                                            <th>STO</th>
                                            <th>RAN</th>
                                            <th>Port</th>
                                            <th>Status</th>
                                            <th>Capacity</th>
                                            <th>Utilization</th>
                                            <th>Utilization Max(%)</th>
                                            <th>System</th>
                                            <th>Keterangan</th>
                                            <th>Last Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i=0; ?>
                                    @foreach($backhaul as $bh)
                                        <?php $i++; ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td><input type="text" name="" id="kota{{$bh->idbackhaul}}" class="border-0" value="{{$bh->kota_tsel}}"></td>
                                            <td><input type="hidden" name="" id="metro{{$bh->idbackhaul}}" class="border-0" value="{{$bh->metroe_telkom}}">{{$bh->metroe_telkom}}</td>
                                            <td>{{$bh['port_telkom']}}</td>
                                            <td><input type="text" name="" id="sto{{$bh->idbackhaul}}" class="border-0"  value="{{\App\Metro::STO($bh->metroe_telkom)}}"></td>
                                            <td>{{$bh['routername_tsel']}}</td>
                                            <td>{{$bh['port_tsel']}}</td>
                                            <td>{{$bh['capacity']}}</td>
                                            <td>{{number_format((float)$bh['utilization_cap'], 2, '.', '')}}</td>
                                            <td>{{$bh['system']}}</td>
                                            <td>{{$bh['transport_type']}}</td>
                                            {{-- <td>{{$bh['date_detected']}}</td> --}}
                                            <td>{{$bh['status']}}</td>
                                            <td><input type="text" name="keterangan" class="border-0" id="ket{{$bh['idbackhaul']}}" value="{{$bh['Keterangan']}}"></td>
                                            <td>{{$bh['updated_at']}}</td>
                                            <!-- <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">

                                                        <li><a href="{{URL::Route('inventory.backhaul.show', $bh['idbackhaul'])}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('inventory.backhaul.detail.delete', $bh['idbackhaul'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
    @foreach($backhaul as $bh)
    <script>
        $("#ket{{$bh['idbackhaul']}}").change( function() {
            var id = $(this).val();
            // alert(id);
            $.ajax({
                type: "GET",
                url : "{{ url('/ipbb/editKetBackhaul/') }}/{{$bh['idbackhaul']}}/"+id,
                success : function(res){
                    $("#ket{{$bh['idbackhaul']}}").css({"color": "blue", "font-style": "italic"});
                    setTimeout(function(){ 
                        $("#ket{{$bh['idbackhaul']}}").css({"color": "black", "font-style": "normal"});
                     }, 3000);
                }
            })
        });
        $("#kota{{$bh->idbackhaul}}").change(function() {
                var value = $("#kota{{$bh->idbackhaul}}").val();
                // alert(value);
                $.ajax({
                    type: "GET",
                    url : "{{ url('ipbb/editKotaBackhaul/') }}/{{ $bh->idbackhaul }}/"+value,
                    success : function(response){
                        $("#kota{{$bh->idbackhaul}}").css({"color": "blue", "font-style": "italic"});
                        setTimeout(function(){ 
                            $("#kota{{$bh->idbackhaul}}").css({"color": "black", "font-style": "normal"});
                         }, 3000);
                    }
                })

            })
        $("#sto{{$bh['idbackhaul']}}").change( function() {
                    var id = $(this).val();
                    var metro = $("#metro{{$bh['idbackhaul']}}").val();
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasi/makeSTO/') }}/"+metro+"/"+id,
                        success : function(response){
                            $("#sto{{$bh['idbackhaul']}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#sto{{$bh['idbackhaul']}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);
                        }
                    })
                })

    </script>
    @endforeach
@stop
