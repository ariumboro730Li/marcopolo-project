<table class="align-center table table-striped table-bordered table-hover dataTable js-exportable">
    <thead>
    <tr>
        <th rowspan="3" class="no">ID</th>
        <th colspan="6">Telkom</th>
        <th colspan="4">Telkomsel</th>
        <th rowspan="3">Capacity (Gbps)</th>
        <th rowspan="3">Util Max</th>
        {{--<th rowspan="3">Layer</th>--}}
        <th rowspan="3">System</th>
        <!-- <th colspan="12">Port/Interface Transport</th> -->
        <th rowspan="3">Date Detected</th>
        {{--<th rowspan="3">Date Undetected</th>--}}
        <th rowspan="3">Status</th>
        <!-- <th rowspan="3"></th> -->
    </tr>
    <tr>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="3">End1</th>
        <th colspan="3">End2</th>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="2">End1</th>
        <th colspan="2">End2</th>

        <!-- <th colspan="6">Transport-A (WDM/FO)</th>
        <th colspan="6">Transport-B (WDM/FO)</th> -->
    </tr>
    <tr>
        <th>STO</th>
        <th>Metro-E</th>
        <th>Port</th>
        <th>STO</th>
        <th>Metro-E</th>
        <th>Port</th>
        <th>Router Name</th>
        <th>Port</th>
        <th>Router Name</th>
        <th>Port</th>

        <!-- <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th>
        <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th> -->
    </tr>
    </thead>
    <tbody>

    <?php $i=0; ?>
    @foreach($ipran as $ir)
        <?php $i++; ?>
        <tr>
            <td>{{$i}}</td>
            <td>{{\App\Metro::STO($ir['metroe_end1_telkom'])}}</td>
            <td>{{$ir['metroe_end1_telkom']}}</td>
            <td>{{$ir['port_end1_telkom']}}</td>
            <td>{{\App\Metro::STO($ir['metroe_end2_telkom'])}}</td>
            <td>{{$ir['metroe_end2_telkom']}}</td>
            <td>{{$ir['port_end2_telkom']}}</td>
            {{--<td>{{$ir['reg_tsel']}}</td>--}}
            <td>{{$ir['router_end1_tsel']}}</td>
            <td>{{$ir['port_end1_tsel']}}</td>
            <td>{{$ir['router_end2_tsel']}}</td>
            <td>{{$ir['port_end2_tsel']}}</td>
            {{--<td>{{$ir['reg_telkom']}}</td>--}}

            <td>{{$ir['capacity']}}</td>
            <td>{{$ir['utilization_max']}}</td>
            {{--<td>{{$ir['layer']}}</td>--}}
            <td>{{$ir['system']}}</td>
        <!-- <td>{{$ir['ne_transport_a']}}</td>
                                            <td>{{$ir['board_transport_a']}}</td>
                                            <td>{{$ir['shelf_transport_a']}}</td>
                                            <td>{{$ir['slot_transport_a']}}</td>
                                            <td>{{$ir['port_transport_a']}}</td>
                                            <td>{{$ir['frek_transport_a']}}</td>
                                            <td>{{$ir['ne_transport_b']}}</td>
                                            <td>{{$ir['board_transport_b']}}</td>
                                            <td>{{$ir['shelf_transport_b']}}</td>
                                            <td>{{$ir['slot_transport_b']}}</td>
                                            <td>{{$ir['port_transport_b']}}</td>
                                            <td>{{$ir['frek_transport_b']}}</td> -->
            <td>{{$ir['date_detected']}}</td>
            {{--<td>{{$ir['date_undetected']}}</td>--}}
            <td>{{$ir['status']}}</td>

        </tr>
    @endforeach
    </tbody>
</table>