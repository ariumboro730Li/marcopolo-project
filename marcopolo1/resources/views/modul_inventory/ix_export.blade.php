<table id="detail" class="align-center table table-striped table-bordered table-hover dataTable js-exportable">
    <thead>
    <tr>
        <th rowspan="3" class="no">ID</th>
        <th colspan="3">Telkom</th>
        <th colspan="2">Telkomsel</th>
        <th rowspan="3">Capacity (Gbps)</th>
        <th rowspan="3">Util Max</th>
        {{--<th rowspan="3">Layer</th>--}}
        <th rowspan="3">System</th>
        <!-- <th colspan="12">Port/Interface Transport</th> -->
        <th rowspan="3">Date Detected</th>
        {{--<th rowspan="3">Date Undetected</th>--}}
        <th rowspan="3">Status</th>
        <th rowspan="3"></th>
    </tr>
    <tr>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="3">End1</th>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="2">End1</th>
        <!-- <th colspan="6">Transport-A (WDM/FO)</th>
        <th colspan="6">Transport-B (WDM/FO)</th> -->
    </tr>
    <tr>
        <th>STO</th>
        <th>Metro-E</th>
        <th>Port</th>
        <th>Router Name</th>
        <th>Port</th>
        <!-- <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th>
        <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th> -->
    </tr>
    </thead>
    <tbody>

    <?php $ip=0; ?>
    @foreach($ix as $i)
        <?php $ip++; ?>
        <tr>
            <td>{{$ip}}</td>
            {{--<td>{{$i['reg_telkom']}}</td>--}}
            <td>{{\App\Metro::STO($i['metroe_end1_telkom'])}}</td>
            <td>{{$i['metroe_end1_telkom']}}</td>
            <td>{{$i['port_end1_telkom']}}</td>
            {{--<td>{{$i['reg_tsel']}}</td>--}}
            <td>{{$i['router_end1_tsel']}}</td>
            <td>{{$i['port_end1_tsel']}}</td>
            <td>{{$i['capacity']}}</td>
            <td>{{$i['utilization_max']}}</td>
            {{--<td>{{$i['layer']}}</td>--}}
            <td>{{$i['system']}}</td>
        <!-- <td>{{$i['ne_transport_a']}}</td>
                                            <td>{{$i['board_transport_a']}}</td>
                                            <td>{{$i['shelf_transport_a']}}</td>
                                            <td>{{$i['slot_transport_a']}}</td>
                                            <td>{{$i['port_transport_a']}}</td>
                                            <td>{{$i['frek_transport_a']}}</td>
                                            <td>{{$i['ne_transport_b']}}</td>
                                            <td>{{$i['board_transport_b']}}</td>
                                            <td>{{$i['shelf_transport_b']}}</td>
                                            <td>{{$i['slot_transport_b']}}</td>
                                            <td>{{$i['port_transport_b']}}</td>
                                            <td>{{$i['frek_transport_b']}}</td> -->
            <td>{{$i['date_detected']}}</td>
            {{--<td>{{$i['date_undetected']}}</td>--}}
            <td>{{$i['status']}}</td>
            {{--<td>Date detected</td>--}}
            {{--<td>Date undetected</td>--}}
            {{--<td>Status</td>--}}

        </tr>
    @endforeach
    </tbody>
</table>