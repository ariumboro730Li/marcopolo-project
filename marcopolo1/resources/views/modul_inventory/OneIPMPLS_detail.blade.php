@extends('layout')

@section('OneIPMPLS')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
OneIPMPLS Detail Regional - Inventory Datek
@stop

    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/OneIPMPLS')}}">OneIPMPLS</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>OneIPMPLS - DETAIL REGIONAL {{$regional}}</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.OneIPMPLS', $regional)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">

                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Router A</th>
                                            <th>Port Router A</th>
                                            <th>Lokasi A</th>
                                            <th>Router B</th>
                                            <th>Port Router B</th>
                                            <th>Lokasi B</th>
                                            <th>Status</th>
                                            <th>Capacity</th>
                                            <th>Utilization Max</th>
                                            <th>Utilization Max %</th>
                                            <th>System</th>
                                            <th>Keterangan</th>
                                            <th>Last Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i=0; ?>
                                    @foreach($OneIPMPLS as $one)
                                        <?php $i++; ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{$one->status}}</td>
                                            <td>{{$one->capacity}}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{$one->system}}</td>
                                            <td>{{$one->keterangan}}</td>
                                            <td>{{$one->updated_at}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
