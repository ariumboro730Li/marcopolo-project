@extends('layout')

@section('menu4')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
Search - Inventory Datek
@stop

@section('content')

	<section class="content">
        <div class="container-fluid">
					<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="card">
										<div class="body">
												<h3>Search Result of {{$regi}}</h3>
                        	<ul class="nav nav-tabs tab-nav-right m-t-10" role="tablist">
                                <li role="presentation" class="active"><a href="#access" data-toggle="tab">ACCESS&emsp;<button class="btn btn-success " type="button">{{count($access)}}  </button></a></li>
                                <li role="presentation"><a href="#backhaul" data-toggle="tab">BACKHAUL&emsp;<button class="btn btn-success " type="button">{{count($backhaul)}} </button></a></li>
                                <li role="presentation"><a href="#ipran" data-toggle="tab">IPRAN&emsp;<button class="btn btn-success " type="button">{{count($ipran) + count($ipran2) }} </button></a></li>
                                <li role="presentation"><a href="#ipbb" data-toggle="tab">IPBB&emsp;<button class="btn btn-success " type="button">{{count($ipbb) + count($ipbb2) }} </button></a></li>
                                <li role="presentation"><a href="#ix" data-toggle="tab">IX&emsp;<button class="btn btn-success " type="button">{{count($ix)}} </button></a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="access">

                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">    
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
				                                        <tr>
				                                            <th rowspan="2" class="no">ID</th>
				                                            <th colspan="2">Telkom</th>
				                                            <th colspan="1">Telkomsel</th>
				                                            <!-- <th colspan="10">Telkom</th> -->

				                                            <th rowspan="2">Capacity (Mbps)</th>
				                                            <th rowspan="2">Util Max</th>
				                                            <th rowspan="2">System</th>
				                                            <th rowspan="2">Date Detected</th>
				                                            {{--<th rowspan="2">Date Undetected</th>--}}
				                                            <th rowspan="2">Status</th>
				                                            <th rowspan="2"></th>
				                                        </tr>
				                                        <tr>
				                                            {{--<th rowspan="1">Regional</th>--}}
															<th rowspan="1">Regional</th>
				                                            <th rowspan="1">Site ID</th>
				                                            <th rowspan="1">Site Name</th>
				                                            {{--<th rowspan="1">Regional</th>--}}
				                                            <!-- <th colspan="3">GPON</th>
				                                            <th colspan="3">Radio</th>
				                                            <th colspan="3">Direct Metro</th> -->
				                                        </tr>
				                                        <!-- <tr>
				                                            <th>STO</th>
				                                            <th>GPON</th>
				                                            <th>Port</th>
				                                            <th>NE</th>
				                                            <th>FE</th>
				                                            <th>Port</th>
				                                            <th>STO</th>
				                                            <th>ME</th>
				                                            <th>Port</th>
				                                        </tr> -->
				                                    </thead>
				                                    <tbody>

                                                    <?php $i=0; ?>
													@foreach($access as $acs)
                                                        <?php $i++; ?>
														<tr>
															<td>{{$i}}</td>
															<td>{{$acs['reg_telkom']}}</td>
															<td>{{$acs['siteid_tsel']}}</td>
															<td>{{$acs['sitename_tsel']}}</td>
															{{--<td>{{$acs['reg_telkom']}}</td>--}}
														<!-- <td>{{$acs['sto_gpon_telkom']}}</td>
                                            <td>{{$acs['gpon_gpon_telkom']}}</td>
                                            <td>{{$acs['port_gpon_telkom']}}</td>
                                            <td>{{$acs['ne_radio_telkom']}}</td>
                                            <td>{{$acs['fe_radio_telkom']}}</td>
                                            <td>{{$acs['port_radio_telkom']}}</td>
                                            <td>{{$acs['sto_dm_telkom']}}</td>
                                            <td>{{$acs['me_dm_telkom']}}</td>
                                            <td>{{$acs['port_dm_telkom']}}</td> -->
															<td>{{$acs['capacity']}}</td>
															<td>{{$acs['utilization_max']}}</td>
															<td>{{$acs['system']}}</td>
															<td>{{$acs['date_detected']}}</td>
															{{--<td>{{$acs['date_undetected']}}</td>--}}
															<td>{{$acs['status']}}</td>
															<td class="align-center">
																<div class="btn-group">
																	<a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
																	<ul class="dropdown-menu" role="menu">
																		<li><a href="/inventory/access/edit/{{ $acs['idaccess']}}">Edit</a></li>
																		<li><a href="{{URL::Route('inventory.access.detail.delete', $acs['idaccess'])}}">Delete</a></li>
																	</ul>
																</div>
															</td>
														</tr>
													@endforeach
				                                        	
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div role="tabpanel" class="tab-pane fade" id="backhaul">

                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
																								<th>No</th>
																								<th>TTC</th>
																								<th>Metro</th>
																								<th>Port Router A</th>
																								<th>STO</th>
																								<th>RAN</th>
																								<th>Port</th>
																								<th>Status</th>
																								<th>Capacity</th>
																								<th>Utilization</th>
																								<th>Utilization Max(%)</th>
																								<th>System</th>
																								<th>Keterangan</th>
																								<th>Last Update</th>
																								</thead>
				                                    <tbody>

                                                    <?php $i=0; ?>
													@foreach($backhaul as $bh)
                                                        <?php $i++; ?>
														<tr>
																<td>{{$i}}</td>
																<td>{{$bh->kota_tsel}}</td>
																<td>{{$bh->metroe_telkom}}</td>
																<td>{{$bh['port_telkom']}}</td>
																<td>{{\App\Metro::STO($bh->metroe_telkom)}}</td>
																<td>{{$bh['routername_tsel']}}</td>
																<td>{{$bh['port_tsel']}}</td>
																<td>{{$bh['capacity']}}</td>
																<td>{{number_format((float)$bh['utilization_cap'], 2, '.', '')}}</td>
																<td>{{$bh['system']}}</td>
																<td>{{$bh['transport_type']}}</td>
																{{-- <td>{{$bh['date_detected']}}</td> --}}
																<td>{{$bh['status']}}</td>
																<td>{{$bh['Keterangan']}}</td>
																<td>{{$bh['updated_at']}}</td>
								</tr>
													@endforeach
				                                        
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				          <div role="tabpanel" class="tab-pane fade" id="ipran">
									<button class="btn btn-success btn-lg btn-block waves-effect" type="button"><span class="badge">{{count($ipran)}} & {{count($ipran2) }} </span> DATA DITEMUKAN </button>
									<div class="row clearfix">
											<div class="body">
													<h3>IPRAN DWDM</h3>
													<div class="table-responsive">
															<table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 4010px !important">
																	<thead>
																		<th class="no">ID</th>
																		<th>Link</th>
																		<th>Kota A</th>
																		<th>Router A</th>
																		<th>Port A</th>
																		<th>Kota B</th>
																		<th>Router B</th>
																		<th>Port B</th>
																		<th>STO A</th>
																		<th>NE A</th>
																		<th>Shelf A - Slot A - Port A</th>
																		<th>Tie Line A</th>
																		<th>STO B</th>
																		<th>NE B</th>
																		<th>Shelf B - Slot B - Port B</th>
																		<th>Tie Line B</th> 
																		<th> Status </th>
																		<th> Capacity </th> 
																		<th> Utilization </th> 
																		<th> Utilization In </th> 
																		<th> Utilization Out </th> 
																		<th> Utilization Max </th> 
																		<th> System </th> 
																		<th> Transport Type</th> 
																		<th> Last Update</th> 
																	</thead>
																	<tbody>
																			<?php $ip=0; ?>																								
																		@foreach ($ipran as $ib)
																			<?php $ip++; ?>
																			<tr>
																				<td>{{$ip}}</td>
																				<td><a href="{{$ib['link']}}">{{$ib['link']}}</a></td>
																				<td>{{$ib['kota_router_node_a']}}</td>
																				<td>{{$ib['router_node_a']}}</td>
																				<td>{{$ib['port_tsel_end_a']}}</td>
																				<td>{{$ib['kota_router_node_b']}}</td>
																				<td>{{$ib['router_node_b']}}</td>
																				<td>{{$ib['port_tsel_end_b']}}</td>
																				<?php $id = $ib['idipran']; $RelasiIpbb =  App\RelasiIpran::where('id_ipran',$id); 	?>
																				<td>
																						<table style="width: 100%" class="hop0{{$ib['idiipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input0{{$relasi->id}}">
																												<td>{{\App\Metro::STO($relasi->ne_transport)}}</td>
																										</tr>
																										@endforeach
																						</table>
																				</td>
																				<td>
																						<table style="width: 100%" class="hop1{{$ib['idipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input1{{$relasi->id}}">
																												<td>{{ $relasi->ne_transport }}</td>
																										</tr>
																										@endforeach
																						</table>
																				</td>
																				<td>
																						<table style="width: 100%" class="hop2{{$ib['idipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input2{{$relasi->id}}">
																												<td>{{$relasi->shelf_slot_port}}</td>
																								</tr>
																								@endforeach
																						</table>
																				</td>  
																				<td>                                                
																						<table style="width: 100%" class="hop3{{$ib['idipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input3{{$relasi->id}}">
																												<td>{{$relasi->tie_line}}</td>
																										</tr>
																										@endforeach
																						</table>
																				 </td>  
																				<td>
																						<table style="width: 100%" class="hop0{{$ib['idipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input0{{$relasi->id}}">
																												<td>{{\App\Metro::STO($relasi->ne_transport)}}</td>
																										</tr>
																										@endforeach
																						</table>
																				</td>
																				<td>
																						<table style="width: 100%" class="hop1{{$ib['idipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input1{{$relasi->id}}">
																												<td>{{ $relasi->ne_transport }}</td>
																										</tr>
																										@endforeach
																						</table>
																				</td>
																				<td>
																						<table style="width: 100%" class="hop2{{$ib['idipran']}} RelasiIran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input2{{$relasi->id}}">
																												<td>{{$relasi->shelf_slot_port}}</td>
																								</tr>
																								@endforeach
																						</table>
																				</td>  
																				<td>                                                
																						<table style="width: 100%" class="hop3{{$ib['idipran']}} RelasiIpran">
																										@foreach ($RelasiIpbb->get() as $relasi)
																										<tr class="input3{{$relasi->id}}">
																												<td>{{$relasi->tie_line}}</td>
																										</tr>
																										@endforeach
																						</table>
																				 </td>  
																				 <td>{{$ib['status']}}</td>
																				 <td>{{$ib['capacity']}} G</td>
																				 <td>{{$ib['utilization_cap']}}</td>
																				 <td>{{$ib['utilization_in%']}} %</td>
																				 <td>{{$ib['utilization_out%']}} %</td>
																				 <td>{{$ib['utilization_max%']}} %</td>
																				 <td>{{$ib['system']}}</td>
																				 <td>{{$ib['transport_type']}}</td>
																				 <td>{{$ib['updated_at']}}</td>			
																			</tr>
																		@endforeach
																	</tbody>
															</table>
													</div>
											<div class="body">
													<h3>IPRAN Metro</h3>
													<div class="table-responsive">
															<table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 4010px !important">
																	<thead>
																		<th class="no">ID</th>
																		<th>Link</th>
																		<th>Kota A</th>
																		<th>Router A</th>
																		<th>Port A</th>
																		<th>Kota B</th>
																		<th>Router B</th>
																		<th>Port B</th>
																		<th>STO A</th>
																		<th>NE A</th>
																		<th>Shelf A - Slot A - Port A</th>
																		<th>STO B</th>
																		<th>NE B</th>
																		<th>Shelf B - Slot B - Port B</th>
																		<th> Status </th>
																		<th> Capacity </th> 
																		<th> Utilization </th> 
																		<th> Utilization In </th> 
																		<th> Utilization Out </th> 
																		<th> Utilization Max </th> 
																		<th> System </th> 
																		<th> Transport Type</th> 
																		<th> Last Update</th> 
																	</thead>
																	<tbody>
																			<?php $ip=0; ?>																								
																		@foreach ($ipran2 as $ib)
																		<?php $id = $ib['idipran']; $RelasiIpbb =  App\RelasiIpran::where('id_ipran',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>

																		<?php 

																		if ($RelasiIpbb->count() == 0) {
																				$arr = [
																						'id_ipran' => $id,
																						'row'   => 1,
																						'status'    => 0
																				];
																				$arr2 = [
																						'id_ipran' => $id,
																						'row'   => 2,
																						'status'    => 0
																				];
																				$store = App\RelasiIpran::create($arr);
																				$store = App\RelasiIpran::create($arr2);
																		} ?>

																			<?php $ip++; ?>
																			<tr>
																				<td>{{$ip}}</td>
																				<td><a href="{{$ib['link']}}">{{$ib['link']}}</a></td>
																				<td>{{$ib['kota_router_node_a']}}</td>
																				<td>{{$ib['router_node_a']}}</td>
																				<td>{{$ib['port_tsel_end_a']}}</td>
																				<td>{{$ib['kota_router_node_b']}}</td>
																				<td>{{$ib['router_node_b']}}</td>
																				<td>{{$ib['port_tsel_end_b']}}</td>
																				<?php $id = $ib['idipran']; $RelasiIpbb =  App\RelasiIpran::where('id_ipran',$id)->where('row',1);  ?>
																				@foreach ($RelasiIpbb->get() as $relasi)
																				<td>
																					{{\App\Metro::STO($relasi->ne_transport)}}
																				</td>
																				</form>
																				
																				<td>
																						{{ $relasi->ne_transport }}
																				</td>
																				<td>
																					{{$relasi->shelf_slot_port}}
																				</td>  
																				 @endforeach
																				 <?php $id = $ib['idipran']; $RelasiIpbb =  App\RelasiIpran::where('id_ipran',$id)->where('row',2);  ?>
																				 @foreach ($RelasiIpbb->get() as $relasi)
																				 <td>
																						{{\App\Metro::STO($relasi->ne_transport)}}
																				</td>
																				<td> {{ $relasi->ne_transport }}
																				</td>
																				<td>
																						{{$relasi->shelf_slot_port}}
																				</td>  
																				@endforeach
																							 <td>{{$ib['status']}}</td>
																				 <td>{{$ib['capacity']}} G</td>
																				 <td>{{$ib['utilization_cap']}}</td>
																				 <td>{{$ib['utilization_in%']}} %</td>
																				 <td>{{$ib['utilization_out%']}} %</td>
																				 <td>{{$ib['utilization_max%']}} %</td>
																				 <td>{{$ib['system']}}</td>
																				 <td>{{$ib['transport_type']}}</td>
																				 <td>{{$ib['updated_at']}}</td>			
																			</tr>
																		@endforeach
																	</tbody>
															</table>
													</div>
											</div>
									</div>
							</div>
							</div>

				          <div role="tabpanel" class="tab-pane fade" id="ipbb">
									<button class="btn btn-success btn-lg btn-block waves-effect" type="button"><span class="badge">{{count($ipbb)}} DWDM & {{ count($ipbb2) }} Metro </span>  DATA DITEMUKAN </button>
                                    <div class="row clearfix">
				                        <div class="body">
																		<h3>IPBB DWDM</h3>
				                            <div class="table-responsive">
																				<table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 4010px !important">
				                                    <thead>
																							<th class="no">ID</th>
																							<th>Link</th>
																							<th>Kota A</th>
																							<th>Router A</th>
																							<th>Port A</th>
																							<th>Kota B</th>
																							<th>Router B</th>
																							<th>Port B</th>
																							<th>STO A</th>
																							<th>NE A</th>
																							<th>Shelf A - Slot A - Port A</th>
																							<th>Tie Line A</th>
																							<th>STO B</th>
																							<th>NE B</th>
																							<th>Shelf B - Slot B - Port B</th>
																							<th>Tie Line B</th> 
																							<th> Status </th>
																							<th> Capacity </th> 
																							<th> Utilization </th> 
																							<th> Utilization In </th> 
																							<th> Utilization Out </th> 
																							<th> Utilization Max </th> 
																							<th> System </th> 
																							<th> Transport Type</th> 
																							<th> Last Update</th> 
																						</thead>
				                                    <tbody>
																								<?php $ip=0; ?>																								
																							@foreach ($ipbb as $ib)
																								<?php $ip++; ?>
																								<tr>
																									<td>{{$ip}}</td>
																									<td><a href="{{$ib['link']}}">{{$ib['link']}}</a></td>
																									<td>{{$ib['kota_router_node_a']}}</td>
																									<td>{{$ib['router_node_a']}}</td>
																									<td>{{$ib['port_tsel_end_a']}}</td>
																									<td>{{$ib['kota_router_node_b']}}</td>
																									<td>{{$ib['router_node_b']}}</td>
																									<td>{{$ib['port_tsel_end_b']}}</td>
																									<?php $id = $ib['idipbb']; $RelasiIpbb =  App\RelasiIpbb::where('id_ipbb',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipbb', $id)->orderBy('row', 'desc'); ?>
																									<td>
																											<table style="width: 100%" class="hop0{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input0{{$relasi->id}}">
																																	<td>{{\App\Metro::STO($relasi->ne_transport)}}</td>
																															</tr>
																															@endforeach
																											</table>
																									</td>
																									<td>
																											<table style="width: 100%" class="hop1{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input1{{$relasi->id}}">
																																	<td>{{ $relasi->ne_transport }}</td>
																															</tr>
																															@endforeach
																											</table>
																									</td>
																									<td>
																											<table style="width: 100%" class="hop2{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input2{{$relasi->id}}">
																																	<td>{{$relasi->shelf_slot_port}}</td>
																													</tr>
																													@endforeach
																											</table>
																									</td>  
																									<td>                                                
																											<table style="width: 100%" class="hop3{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input3{{$relasi->id}}">
																																	<td>{{$relasi->tie_line}}</td>
																															</tr>
																															@endforeach
																											</table>
																									 </td>  
																									<td>
																											<table style="width: 100%" class="hop0{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input0{{$relasi->id}}">
																																	<td>{{\App\Metro::STO($relasi->ne_transport)}}</td>
																															</tr>
																															@endforeach
																											</table>
																									</td>
																									<td>
																											<table style="width: 100%" class="hop1{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input1{{$relasi->id}}">
																																	<td>{{ $relasi->ne_transport }}</td>
																															</tr>
																															@endforeach
																											</table>
																									</td>
																									<td>
																											<table style="width: 100%" class="hop2{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input2{{$relasi->id}}">
																																	<td>{{$relasi->shelf_slot_port}}</td>
																													</tr>
																													@endforeach
																											</table>
																									</td>  
																									<td>                                                
																											<table style="width: 100%" class="hop3{{$ib['idipbb']}} RelasiIpbb">
																															@foreach ($RelasiIpbb->get() as $relasi)
																															<tr class="input3{{$relasi->id}}">
																																	<td>{{$relasi->tie_line}}</td>
																															</tr>
																															@endforeach
																											</table>
																									 </td>  
																									 <td>{{$ib['status']}}</td>
																									 <td>{{$ib['capacity']}} G</td>
																									 <td>{{$ib['utilization_cap']}}</td>
																									 <td>{{$ib['utilization_in%']}} %</td>
																									 <td>{{$ib['utilization_out%']}} %</td>
																									 <td>{{$ib['utilization_max%']}} %</td>
																									 <td>{{$ib['system']}}</td>
																									 <td>{{$ib['transport_type']}}</td>
																									 <td>{{$ib['updated_at']}}</td>			
																								</tr>
																							@endforeach
				                                    </tbody>
				                                </table>
				                            </div>
				                        <div class="body">
																		<h3>IPBB Metro</h3>
				                            <div class="table-responsive">
																				<table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 4010px !important">
				                                    <thead>
																							<th class="no">ID</th>
																							<th>Link</th>
																							<th>Kota A</th>
																							<th>Router A</th>
																							<th>Port A</th>
																							<th>Kota B</th>
																							<th>Router B</th>
																							<th>Port B</th>
																							<th>STO A</th>
																							<th>NE A</th>
																							<th>Shelf A - Slot A - Port A</th>
																							<th>STO B</th>
																							<th>NE B</th>
																							<th>Shelf B - Slot B - Port B</th>
																							<th> Status </th>
																							<th> Capacity </th> 
																							<th> Utilization </th> 
																							<th> Utilization In </th> 
																							<th> Utilization Out </th> 
																							<th> Utilization Max </th> 
																							<th> System </th> 
																							<th> Transport Type</th> 
																							<th> Last Update</th> 
																						</thead>
				                                    <tbody>
																								<?php $ip=0; ?>																								
																							@foreach ($ipbb2 as $ib)
																							<?php $id = $ib['idipbb']; $RelasiIpbb =  App\RelasiIpbb::where('id_ipbb',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipbb', $id)->orderBy('row', 'desc'); ?>

																							<?php 
					
																							if ($RelasiIpbb->count() == 0) {
																									$arr = [
																											'id_ipbb' => $id,
																											'row'   => 1,
																											'status'    => 0
																									];
																									$arr2 = [
																											'id_ipbb' => $id,
																											'row'   => 2,
																											'status'    => 0
																									];
																									$store = App\RelasiIpbb::create($arr);
																									$store = App\RelasiIpbb::create($arr2);
																							} ?>
					
																								<?php $ip++; ?>
																								<tr>
																									<td>{{$ip}}</td>
																									<td><a href="{{$ib['link']}}">{{$ib['link']}}</a></td>
																									<td>{{$ib['kota_router_node_a']}}</td>
																									<td>{{$ib['router_node_a']}}</td>
																									<td>{{$ib['port_tsel_end_a']}}</td>
																									<td>{{$ib['kota_router_node_b']}}</td>
																									<td>{{$ib['router_node_b']}}</td>
																									<td>{{$ib['port_tsel_end_b']}}</td>
																									<?php $id = $ib['idipbb']; $RelasiIpbb =  App\RelasiIpbb::where('id_ipbb',$id)->where('row',1);   $relrelan = DB::table('relasi_ipran')->where('id_ipbb', $id)->orderBy('row', 'desc'); ?>
																									@foreach ($RelasiIpbb->get() as $relasi)
																									<td>
																										{{\App\Metro::STO($relasi->ne_transport)}}
																									</td>
																									</form>
																									
																									<td>
																											{{ $relasi->ne_transport }}
																									</td>
																									<td>
																										{{$relasi->shelf_slot_port}}
																									</td>  
																									 @endforeach
																									 <?php $id = $ib['idipbb']; $RelasiIpbb =  App\RelasiIpbb::where('id_ipbb',$id)->where('row',2);  $relrelan = DB::table('relasi_ipran')->where('id_ipbb', $id)->orderBy('row', 'desc'); ?>
																									 @foreach ($RelasiIpbb->get() as $relasi)
																									 <td>
																											{{\App\Metro::STO($relasi->ne_transport)}}
																									</td>
																									<td> {{ $relasi->ne_transport }}
																									</td>
																									<td>
																											{{$relasi->shelf_slot_port}}
																									</td>  
																									@endforeach
																												 <td>{{$ib['status']}}</td>
																									 <td>{{$ib['capacity']}} G</td>
																									 <td>{{$ib['utilization_cap']}}</td>
																									 <td>{{$ib['utilization_in%']}} %</td>
																									 <td>{{$ib['utilization_out%']}} %</td>
																									 <td>{{$ib['utilization_max%']}} %</td>
																									 <td>{{$ib['system']}}</td>
																									 <td>{{$ib['transport_type']}}</td>
																									 <td>{{$ib['updated_at']}}</td>			
																								</tr>
																							@endforeach
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>

				                <div role="tabpanel" class="tab-pane fade" id="ix">
									<button class="btn btn-success btn-lg btn-block waves-effect" type="button"><span class="badge">{{count($ix)}} </span> DATA DITEMUKAN </button>
                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">
																				<table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
																						<thead>
																								<tr>
																										<th class="no">ID</th>
																										<th>EBR</th>
																										<th>Port EBR</th>
																										<th>TTC</th>
																										<th>PE Transit</th>
																										<th>Port</th>
																										<th>STO</th>
																										<th>Status</th>
																										<th>Capacity</th>
																										<th>Utilization Max</th>
																										<th>Utilization Max (%)</th>
																										<th>System</th>
																										<th>Keterangan</th>
																										<th>Last Update</th>
																										<th></th>
																								</tr>
																						</thead>
																						<tbody>
																								<?php $ip= 0; ?>
																								@foreach($ix as $i)
																										<?php $ip++; ?>
																												<tr>
																												<td>{{$ip}}</td>
																												<td>{{$i->reg_tsel}}</td>
																												<td>{{$i->routername_tsel}}</td>
																												<td>{{$i->kota_tsel}}</td>
																												<td>{{$i->pe_telkom}}</td>
																												<td>{{$i->port_tsel}}</td>
																												<td>{{\App\Metro::STO($i->pe_telkom)}}</td>
																												<td>{{$i->status}}</td>
																												<td>{{$i->capacity}}</td>
																												<td> util </td>
																												<td> util </td>
																												<td>{{$i->system}}</td>
																												<td>{{$i->keterangan}}</td>
																												<td>{{$i->updated_at}}</td>
																												<td></td>
																										</tr>
																										@endforeach
																								</tbody>
																								</table>
																						</div>
				                        </div>
				                    </div>
				                </div>

				            </div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop