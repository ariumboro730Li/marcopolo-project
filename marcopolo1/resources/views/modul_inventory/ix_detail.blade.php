@extends('layout')

@section('menu9')
    class="active"
@stop

@section('css')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css'/>
<link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<style>
    .border-0 {
        border: none !important;
        text-align : center;
    }
    .p-2 {
        padding: 5px;
    }
</style>
@stop

@section('title')
IX Detail Regional - Inventory Datek
@stop

    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/ix')}}">IX</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IX - DETAIL REGIONAL {{$regional}}</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.ix', $regional)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
                                    <thead>
                                        <tr>
                                            <th class="no">ID</th>
                                            <th>EBR</th>
                                            <th>Port EBR</th>
                                            <th>TTC</th>
                                            <th>PE Transit</th>
                                            <th>Port</th>
                                            <th>STO</th>
                                            <th>Status</th>
                                            <th>Capacity</th>
                                            <th>Utilization Max</th>
                                            <th>Utilization Max (%)</th>
                                            <th>System</th>
                                            <th>Keterangan</th>
                                            <th>Last Update</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $ip= 0; ?>
                                        @foreach($ix as $i)
                                            <?php $ip++; ?>
                                                <tr>
                                                <td>{{$ip}}</td>
                                                <td>{{$i->reg_tsel}}</td>
                                                <td>{{$i->routername_tsel}}</td>
                                                <td><input type="text" name="" id="kota{{$i->idix}}" class="border-0" value="{{$i->kota_tsel}}"></td>
                                                <td><input type="hidden" name="" id="metro{{$i->idix}}" class="border-0" value="{{$i->pe_telkom}}">{{$i->pe_telkom}}</td>
                                                <td>{{$i->port_tsel}}</td>
                                                <td><input type="text" name="" id="sto{{$i->idix}}" class="border-0"  value="{{\App\Metro::STO($i->pe_telkom)}}"></td>
                                                <td>{{$i->status}}</td>
                                                <td>{{$i->capacity}}</td>
                                                <td> util </td>
                                                <td> util </td>
                                                <td>{{$i->system}}</td>
                                                <td><input type="text" name="" id="ket{{$i->idix}}" class="border-0" value="{{$i->keterangan}}"></td>
                                                <td>{{$i->updated_at}}</td>
                                                <td></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')

    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script> 
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script> 
    @foreach($ix as $i)

        <script>
            $("#ket{{$i->idix}}").change(function() {
                var value = $("#ket{{$i->idix}}").val();
                // alert(value);
                $.ajax({
                    type: "GET",
                    url : "{{ url('ix/editket/') }}/{{ $i->idix }}/"+value,
                    success : function(response){
                        $("#ket{{$i->idix}}").css({"color": "blue", "font-style": "italic"});
                        setTimeout(function(){ 
                            $("#ket{{$i->idix}}").css({"color": "black", "font-style": "normal"});
                         }, 3000);
                    }
                })
            })
            $("#kota{{$i->idix}}").change(function() {
                var value = $("#kota{{$i->idix}}").val();
                // alert(value);
                $.ajax({
                    type: "GET",
                    url : "{{ url('ix/editkota/') }}/{{ $i->idix }}/"+value,
                    success : function(response){
                        $("#kota{{$i->idix}}").css({"color": "blue", "font-style": "italic"});
                        setTimeout(function(){ 
                            $("#kota{{$i->idix}}").css({"color": "black", "font-style": "normal"});
                         }, 3000);
                    }
                })

            })

            $("#sto{{$i->idix}}").change( function() {
                    var id = $(this).val();
                    var metro = $("#metro{{$i->idix}}").val();
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasi/makeSTO/') }}/"+metro+"/"+id,
                        success : function(response){
                            $("#sto{{$i->idix}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#sto{{$i->idix}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);
                        }
                    })
                })

        </script>
    @endforeach

    
@stop
