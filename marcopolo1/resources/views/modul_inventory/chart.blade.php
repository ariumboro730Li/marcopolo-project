<?php 
$tahun1 = date("Y");
$tahun0 = date("Y")-1;
$bulan = date("n");
$bulann = date("n")-1;
?>

<script>

var bulan1 = moment().format("M");
var bulan0;

if (bulan1==1) { bulan1 = "Januari"; bulan0 = "(Oktober)"; } else if (bulan1==2) { bulan1 = "Februari"; bulan0 = "(November)"; } else if (bulan1==3) { bulan1 = "Maret"; bulan0 = "(Desember)"; } else if (bulan1==4) { bulan1 = "April"; bulan0 = "Januari"; } else if (bulan1==5) { bulan1 = "Mei"; bulan0 = "Februari"; } else if (bulan1==6) { bulan1 = "Juni"; bulan0 = "Maret"; } else if (bulan1==7) { bulan1 = "Juli"; bulan0 = "April"; } else if (bulan1==8) { bulan1 = "Agustus"; bulan0 = "Mei"; } else if (bulan1==9) { bulan1 = "September"; bulan0 = "Juni"; } else if (bulan1==10) { bulan1 = "Oktober"; bulan0 = "Juli"; } else if (bulan1==11) { bulan1 = "Agustus"; bulan0 = "Juli"; } else if (bulan1==12) { bulan1 = "Desember"; bulan0 = "September"; }

/* ACCESS LINE */

var configAccLine = {
    data: {
        labels: ["(September)", "(Oktober)", "(November)", "(Desember)", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Utilization",
            data: [{{\App\DatekSummary::AllUtilizationAcc(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(12,$tahun1)}}
            ],
            borderColor: "rgba(233, 30, 99, 1)",
            backgroundColor: "rgba(233, 30, 99, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        },{
            label: "Capacity",
            data: [{{\App\DatekSummary::AllCapacityAcc(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(12,$tahun1)}}
            ],
            borderColor: "rgba(0, 188, 212, 1)",
            backgroundColor: "rgba(0, 188, 212, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(0, 188, 212, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                	min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
}

/* ACCESS BAR */

var configAccBar = {
	data: {
	    labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
	    datasets: [{
	        label: "Capacity",
	        data: [{{\App\DatekSummary::CapacityAcc($bulan,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc($bulan,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc($bulan,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc($bulan,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc($bulan,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc($bulan,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc($bulan,$tahun1,7)}}
	        ],	        
	        backgroundColor: "rgba(0, 188, 212, 0.8)"
	    },
            {
                label: "Utilization",
                data: [{{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc($bulan,$tahun1,7)}}
                ],
                backgroundColor: "rgba(233, 30, 99, 0.8)"
            }
        ]
	},
	options: {
	    responsive : true,
	    legend: false,
	    plugins: {
	        datalabels: {
	            align: 'end',
	            anchor: 'end',
	            backgroundColor: function(context) {
	                return context.dataset.backgroundColor;
	            },
	            borderRadius: 4,
	            color: 'white',
	            font: function(context) {
	                var w = context.chart.width;
	                return {
	                    size: w < 512 ? 12 : 14
	                }
	            },
	            formatter: function(value, context) {
	                return context.chart.data.labels[context.data];
	            }
	        }
	    },
	    scales: {
	        xAxes: [{
	            display: true,
	            offset: true
	        }],
	        yAxes: [{
	            ticks: {
	                beginAtZero: true
	            }
	        }]
	    }
	}
};

/* BACKHAUL LINE */

var configBackLine = {
    data: {
        labels: ["(September)", "(Oktober)", "(November)", "(Desember)", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Utilization",
            data: [{{\App\DatekSummary::AllUtilizationBh(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(12,$tahun1)}}
            ],
            borderColor: "rgba(233, 30, 99, 1)",
            backgroundColor: "rgba(233, 30, 99, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        },{
            label: "Capacity",
            data: [{{\App\DatekSummary::AllCapacityBh(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(12,$tahun1)}}
            ],
            borderColor: "rgba(0, 188, 212, 1)",
            backgroundColor: "rgba(0, 188, 212, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(0, 188, 212, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                	min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
};

/* BACKHAUL BAR */

var configBackBar = {
	data: {
	    labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
	    datasets: [{
	        label: "Capacity",
	        data: [{{\App\DatekSummary::CapacityBh($bulan,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh($bulan,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh($bulan,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh($bulan,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh($bulan,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh($bulan,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh($bulan,$tahun1,7)}}
	        ],
	        backgroundColor: "rgba(0, 188, 212, 0.8)",
	    },
            {
                label: "Utilization",
                data: [{{\App\DatekSummary::UtilizationBh($bulan,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh($bulan,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh($bulan,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh($bulan,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh($bulan,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh($bulan,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh($bulan,$tahun1,7)}}
                ],
                backgroundColor: "rgba(233, 30, 99, 0.8)"
            }
        ]
	},
	options: {
	    responsive : true,
	    legend: false,
	    plugins: {
	        datalabels: {
	            align: 'end',
	            anchor: 'end',
	            backgroundColor: function(context) {
	                return context.dataset.backgroundColor;
	            },
	            borderRadius: 4,
	            color: 'white',
	            font: function(context) {
	                var w = context.chart.width;
	                return {
	                    size: w < 512 ? 12 : 14
	                }
	            },
	            formatter: function(value, context) {
	                return context.chart.data.labels[context.data];
	            }
	        }
	    },
	    scales: {
	        xAxes: [{
	            display: true,
	            offset: true
	        }],
	        yAxes: [{
	            ticks: {
	                beginAtZero: true
	            }
	        }]
	    }
	}
};

/* IPRAN LINE */

var configIpranLine = {
    data: {
        labels: ["(September)", "(Oktober)", "(November)", "(Desember)", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Utilization",
            data: [{{\App\DatekSummary::AllUtilizationIpran(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(12,$tahun1)}}
            ],
            borderColor: "rgba(233, 30, 99, 1)",
            backgroundColor: "rgba(233, 30, 99, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        },{
            label: "Capacity",
            data: [{{\App\DatekSummary::AllCapacityIpran(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(12,$tahun1)}}
            ],
            borderColor: "rgba(0, 188, 212, 1)",
            backgroundColor: "rgba(0, 188, 212, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(0, 188, 212, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                	min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
}

/* IPRAN BAR */

var configIpranBar = {
	data: {
	    labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
	    datasets: [{
	        label: "Capacity",
	        data: [{{\App\DatekSummary::CapacityIpran($bulan,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran($bulan,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran($bulan,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran($bulan,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran($bulan,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran($bulan,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran($bulan,$tahun1,7)}}
	        ],
	        backgroundColor: "rgba(0, 188, 212, 0.8)",
	    },{
            label: "Utilization",
            data: [{{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran($bulan,$tahun1,7)}}
            ],
            backgroundColor: "rgba(233, 30, 99, 0.8)",
        }]
	},
	options: {
	    responsive : true,
	    legend: false,
	    plugins: {
	        datalabels: {
	            align: 'end',
	            anchor: 'end',
	            backgroundColor: function(context) {
	                return context.dataset.backgroundColor;
	            },
	            borderRadius: 4,
	            color: 'white',
	            font: function(context) {
	                var w = context.chart.width;
	                return {
	                    size: w < 512 ? 12 : 14
	                }
	            },
	            formatter: function(value, context) {
	                return context.chart.data.labels[context.data];
	            }
	        }
	    },
	    scales: {
	        xAxes: [{
	            display: true,
	            offset: true
	        }],
	        yAxes: [{
	            ticks: {
	                beginAtZero: true
	            }
	        }]
	    }
	}
};

/* IPBB LINE */

var configIpbbLine = {
    data: {
        labels: ["(September)", "(Oktober)", "(November)", "(Desember)", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Utilization",
            data: [{{\App\DatekSummary::AllUtilizationIx(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(12,$tahun1)}}
            ],
            borderColor: "rgba(233, 30, 99, 1)",
            backgroundColor: "rgba(233, 30, 99, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        },{
            label: "Capacity",
            data: [{{\App\DatekSummary::AllCapacityIpbb(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(12,$tahun1)}}
            ],
            borderColor: "rgba(0, 188, 212, 1)",
            backgroundColor: "rgba(0, 188, 212, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(0, 188, 212, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                	min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
};

/* IPBB BAR */

var configIpbbBar = {
	data: {
	    labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
	    datasets: [{
	        label: "Capacity",
	        data: [{{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb($bulan,$tahun1,7)}}
	        ],
	        backgroundColor: "rgba(0, 188, 212, 0.8)",
	    },{
            label: "Utilization",
            data: [{{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb($bulan,$tahun1,7)}}
            ],
            backgroundColor: "rgba(233, 30, 99, 0.8)",
        }]
	},
	options: {
	    responsive : true,
	    legend: false,
	    plugins: {
	        datalabels: {
	            align: 'end',
	            anchor: 'end',
	            backgroundColor: function(context) {
	                return context.dataset.backgroundColor;
	            },
	            borderRadius: 4,
	            color: 'white',
	            font: function(context) {
	                var w = context.chart.width;
	                return {
	                    size: w < 512 ? 12 : 14
	                }
	            },
	            formatter: function(value, context) {
	                return context.chart.data.labels[context.data];
	            }
	        }
	    },
	    scales: {
	        xAxes: [{
	            display: true,
	            offset: true
	        }],
	        yAxes: [{
	            ticks: {
	                beginAtZero: true
	            }
	        }]
	    }
	}
};

/* ONEIPMPLS LINE */

var configOneIPMPLSLine = {
    data: {
        labels: ["(September)", "(Oktober)", "(November)", "(Desember)", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Utilization",
            data: [{{\App\DatekSummary::AllUtilizationOneIPMPLS(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(12,$tahun1)}}
            ],
            borderColor: "rgba(233, 30, 99, 1)",
            backgroundColor: "rgba(233, 30, 99, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        },{
            label: "Capacity",
            data: [{{\App\DatekSummary::AllCapacityOneIPMPLS(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(12,$tahun1)}}
            ],
            borderColor: "rgba(0, 188, 212, 1)",
            backgroundColor: "rgba(0, 188, 212, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(0, 188, 212, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                    min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
}

/* ONEIPMPLS BAR */

var configOneIPMPLSBar = {
    data: {
        labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
        datasets: [{
            label: "Capacity",
            data: [{{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS($bulan,$tahun1,7)}}
            ],
            backgroundColor: "rgba(0, 188, 212, 0.8)"
        },
            {
                label: "Utilization",
                data: [{{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS($bulan,$tahun1,7)}}
                ],
                backgroundColor: "rgba(233, 30, 99, 0.8)"
            }
        ]
    },
    options: {
        responsive : true,
        legend: false,
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                font: function(context) {
                    var w = context.chart.width;
                    return {
                        size: w < 512 ? 12 : 14
                    }
                },
                formatter: function(value, context) {
                    return context.chart.data.labels[context.data];
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                offset: true
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
};

/* IX LINE */

var configIxLine = {
    data: {
        labels: ["(September)", "(Oktober)", "(November)", "(Desember)", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Utilization",
            data: [{{\App\DatekSummary::AllUtilizationIx(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(12,$tahun1)}}
            ],
            borderColor: "rgba(233, 30, 99, 1)",
            backgroundColor: "rgba(233, 30, 99, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        },{
            label: "Capacity",
            data: [{{\App\DatekSummary::AllCapacityIx(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(12,$tahun1)}}
            ],
            borderColor: "rgba(0, 188, 212, 1)",
            backgroundColor: "rgba(0, 188, 212, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(0, 188, 212, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                	min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
};

/* IX BAR */

var configIxBar = {
	data: {
	    labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
	    datasets: [{
	        label: "Capacity",
	        data: [{{\App\DatekSummary::CapacityIx($bulan,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx($bulan,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx($bulan,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx($bulan,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx($bulan,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx($bulan,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx($bulan,$tahun1,7)}}
	        ],
	        backgroundColor: "rgba(0, 188, 212, 0.8)",
	    },{
            label: "Utilization",
            data: [{{\App\DatekSummary::UtilizationIx($bulan,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx($bulan,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx($bulan,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx($bulan,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx($bulan,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx($bulan,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx($bulan,$tahun1,7)}}
            ],
            backgroundColor: "rgba(233, 30, 99, 0.8)",
        }]
	},
	options: {
	    responsive : true,
	    legend: false,
	    plugins: {
	        datalabels: {
	            align: 'end',
	            anchor: 'end',
	            backgroundColor: function(context) {
	                return context.dataset.backgroundColor;
	            },
	            borderRadius: 4,
	            color: 'white',
	            font: function(context) {
	                var w = context.chart.width;
	                return {
	                    size: w < 512 ? 12 : 14
	                }
	            },
	            formatter: function(value, context) {
	                return context.chart.data.labels[context.data];
	            }
	        }
	    },
	    scales: {
	        xAxes: [{
	            display: true,
	            offset: true
	        }],
	        yAxes: [{
	            ticks: {
	                beginAtZero: true
	            }
	        }]
	    }
	}
};

var ChartJsAccLine = Chart.Line(document.getElementById("line_chart_acc"), configAccLine);
var ChartJsAccBar = Chart.Bar(document.getElementById("bar_chart_acc"), configAccBar);
var ChartJsBackLine = Chart.Line(document.getElementById("line_chart_bh"), configBackLine);
var ChartJsBackBar = Chart.Bar(document.getElementById("bar_chart_bh"), configBackBar);
var ChartJsIpranLine = Chart.Line(document.getElementById("line_chart_ipran"), configIpranLine);
var ChartJsIpranBar = Chart.Bar(document.getElementById("bar_chart_ipran"), configIpranBar);
var ChartJsIpbbLine = Chart.Line(document.getElementById("line_chart_ipbb"), configIpbbLine);
var ChartJsIpbbBar = Chart.Bar(document.getElementById("bar_chart_ipbb"), configIpbbBar);
var ChartJsOneIPMPLSLine = Chart.Line(document.getElementById("line_chart_oneipmpls"), configOneIPMPLSLine);
var ChartJsOneIPMPLSBar = Chart.Bar(document.getElementById("bar_chart_oneipmpls"), configOneIPMPLSBar);
var ChartJsIxLine = Chart.Line(document.getElementById("line_chart_ix"), configIxLine);
var ChartJsIxBar = Chart.Bar(document.getElementById("bar_chart_ix"), configIxBar);

</script>

<script>
    $(function () {
        new Chart(document.getElementById("donut_chart_acc"), getChartJsAcc('donut'));
        new Chart(document.getElementById("donut_chart2_acc"), getChartJsAcc('donut2'));
        /*new Chart(document.getElementById("donut_chart_ipran"), getChartJsIpran('donut'));
        new Chart(document.getElementById("donut_chart2_ipran"), getChartJsIpran('donut2'));
        new Chart(document.getElementById("donut_chart_ipbb"), getChartJsIpbb('donut'));
        new Chart(document.getElementById("donut_chart2_ipbb"), getChartJsIpbb('donut2'));
        new Chart(document.getElementById("donut_chart_oneipmpls"), getChartJsOneIPMPLS('donut'));
        new Chart(document.getElementById("donut_chart2_oneipmpls"), getChartJsOneIPMPLS('donut2'));
        new Chart(document.getElementById("donut_chart_ix"), getChartJsIx('donut'));
        new Chart(document.getElementById("donut_chart2_ix"), getChartJsIx('donut2'));*/
    });

    function getChartJsAcc(type) {
    	var config = null;

    	if (type === 'donut') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["Metro-E", "Radio IP"],
                    datasets: [{
                        data: [{{\App\DatekAccess::TransportSystem("Metro-E")}}, {{\App\DatekAccess::TransportSystem("Radio IP")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        else if (type === 'donut2') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["FO", "Radio"],
                    datasets: [{
                        data: [{{\App\DatekAccess::TransportSystem("Metro-E")}}, {{\App\DatekAccess::TransportSystem("Radio IP")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        return config;
    }

    /*function getChartJsIpran(type) {
        var config = null;

        if (type === 'donut') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["Metro-E", "DWDM", "Direct Core"],
                    datasets: [{
                        data: [{{\App\DatekIpran::TransportSystem("Metro-E")}}, {{\App\DatekIpran::TransportSystem("DWDM")}}, {{\App\DatekIpran::TransportSystem("Direct Core")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(139, 195, 74)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        else if (type === 'donut2') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["SMPCS", "SBCS", "Metro-E", "Jasuka", "JVBB"],
                    datasets: [{
                        data: [{{\App\DatekIpran::TransportSystem("SMPCS")}}, {{\App\DatekIpran::TransportSystem("SBCS")}}, {{\App\DatekIpran::TransportSystem("Metro-E")}}, {{\App\DatekIpran::TransportSystem("Jasuka")}}, {{\App\DatekIpran::TransportSystem("JVBB")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(139, 195, 74)', 'rgb(255, 152, 0)', 'rgb(33, 150, 243)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        return config;
    }

    function getChartJsIpbb(type) {
        var config = null;

        if (type === 'donut') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["DWDM", "Metro"],
                    datasets: [{
                        data: [{{\App\DatekIpbb::TransportSystem("DWDM")}}, {{\App\DatekIpbb::TransportSystem("Metro")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        else if (type === 'donut2') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["SMPCS", "SBCS", "Metro-E", "Jasuka", "JVBB"],
                    datasets: [{
                        data: [{{\App\DatekIpbb::TransportSystem("SMPCS")}}, {{\App\DatekIpbb::TransportSystem("SBCS")}}, {{\App\DatekIpbb::TransportSystem("Metro-E")}}, {{\App\DatekIpbb::TransportSystem("Jasuka")}}, {{\App\DatekIpbb::TransportSystem("JVBB")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(139, 195, 74)', 'rgb(255, 152, 0)', 'rgb(33, 150, 243)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        return config;
    }

    function getChartJsIx(type) {
        var config = null;

        if (type === 'donut') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["DWDM"],
                    datasets: [{
                        data: [{{\App\DatekIx::TransportSystem("DWDM")}}],
                        backgroundColor: ['rgb(233, 30, 99)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        else if (type === 'donut2') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["SMPCS", "SBCS", "Metro-E", "Jasuka", "JVBB"],
                    datasets: [{
                        data: [{{\App\DatekIx::TransportSystem("SMPCS")}}, {{\App\DatekIx::TransportSystem("SBCS")}}, {{\App\DatekIx::TransportSystem("Metro-E")}}, {{\App\DatekIx::TransportSystem("Jasuka")}}, {{\App\DatekIx::TransportSystem("JVBB")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(139, 195, 74)', 'rgb(255, 152, 0)', 'rgb(33, 150, 243)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        return config;
    }

    function getChartJsOneIPMPLS(type) {
        var config = null;

        if (type === 'donut') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["DWDM"],
                    datasets: [{
                        data: [{{\App\DatekOneIPMPLS::TransportSystem("DWDM")}}],
                        backgroundColor: ['rgb(233, 30, 99)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        else if (type === 'donut2') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["SMPCS", "SBCS", "Metro-E", "Jasuka", "JVBB"],
                    datasets: [{
                        data: [{{\App\DatekOneIPMPLS::TransportSystem("SMPCS")}}, {{\App\DatekOneIPMPLS::TransportSystem("SBCS")}}, {{\App\DatekOneIPMPLS::TransportSystem("Metro-E")}}, {{\App\DatekOneIPMPLS::TransportSystem("Jasuka")}}, {{\App\DatekOneIPMPLS::TransportSystem("JVBB")}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(139, 195, 74)', 'rgb(255, 152, 0)', 'rgb(33, 150, 243)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        return config;
    }*/

</script>