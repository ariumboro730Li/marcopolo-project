@extends('layout')

@section('menu5')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
Access Detail Regional - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/access')}}">Access</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>HIGH UTILIZATION ACCESS</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.access', 8)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="table_detail" class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="no">ID</th>
                                            <th colspan="2">Telkom</th>
                                            <th colspan="1">Telkomsel</th>
                                            <!-- <th colspan="10">Telkom</th> -->

                                            <th rowspan="2">Capacity (Mbps)</th>
                                            <th rowspan="2">Util Max</th>
                                            <th rowspan="2">Occupation</th>
                                            <th rowspan="2">System</th>
                                            <th rowspan="2">Date Detected</th>
                                            {{--<th rowspan="2">Date Undetected</th>--}}
                                            <th rowspan="2">Status</th>
                                            {{--<th rowspan="2"></th>--}}
                                        </tr>
                                        <tr>
                                            <th rowspan="1">Regional</th>
                                            <th rowspan="1">Site ID</th>
                                            <th rowspan="1">Site Name</th>
                                            {{--<th rowspan="1">Regional</th>--}}
                                            <!-- <th colspan="3">GPON</th>
                                            <th colspan="3">Radio</th>
                                            <th colspan="3">Direct Metro</th> -->
                                        </tr>
                                        <!-- <tr>
                                            <th>STO</th>
                                            <th>GPON</th>
                                            <th>Port</th>
                                            <th>NE</th>
                                            <th>FE</th>
                                            <th>Port</th>
                                            <th>STO</th>
                                            <th>ME</th>
                                            <th>Port</th>
                                        </tr> -->
                                    </thead>
                                    <tbody>

                                    <?php $i=0; ?>
                                    @foreach($access as $acs)
                                        <?php $i++; ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$acs['reg_telkom']}}</td>
                                            <td>{{$acs['siteid_tsel']}}</td>
                                            <td>{{$acs['sitename_tsel']}}</td>
                                            {{--<td>{{$acs['reg_telkom']}}</td>--}}
                                            <!-- <td>{{$acs['sto_gpon_telkom']}}</td>
                                            <td>{{$acs['gpon_gpon_telkom']}}</td>
                                            <td>{{$acs['port_gpon_telkom']}}</td>
                                            <td>{{$acs['ne_radio_telkom']}}</td>
                                            <td>{{$acs['fe_radio_telkom']}}</td>
                                            <td>{{$acs['port_radio_telkom']}}</td>
                                            <td>{{$acs['sto_dm_telkom']}}</td>
                                            <td>{{$acs['me_dm_telkom']}}</td>
                                            <td>{{$acs['port_dm_telkom']}}</td> -->
                                            <td>{{$acs['capacity']}}</td>
                                            <td>{{number_format($acs['utilization_cap'])}}</td>
                                            <td>{{number_format($acs['utilization_max'])}} %</td>
                                            <td>{{$acs['system']}}</td>
                                            <td>{{$acs['date_detected']}}</td>
                                            {{--<td>{{$acs['date_undetected']}}</td>--}}
                                            <td>{{$acs['status']}}</td>
                                            {{--<td class="align-center">--}}
                                                {{--<div class="btn-group">--}}
                                                    {{--<a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>--}}
                                                    {{--<ul class="dropdown-menu" role="menu">--}}
                                                        {{--<li><a href="/inventory/access/edit/{{ $acs['idaccess']}}">Edit</a></li>--}}
                                                        {{--<li><a href="{{URL::Route('inventory.access.detail.delete', $acs['idaccess'])}}">Delete</a></li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
