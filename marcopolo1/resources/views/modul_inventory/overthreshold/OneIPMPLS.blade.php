@extends('layout')

@section('menu6')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
OneIPMPLS Detail Regional - Inventory Datek
@stop

    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/OneIPMPLS')}}">OneIPMPLS</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>HIGH UTILIZATION BACKHAUL</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.OneIPMPLS', 8)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">

                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
                                    <thead>
                                        <tr>
                                            <th rowspan="3" class="no">ID</th>
                                            <th colspan="4">Telkom</th>
                                            <th colspan="2">Telkomsel</th>
                                            <th rowspan="3">Capacity (Gbps)</th>
                                            <th rowspan="3">Util Max</th>
                                            <th rowspan="3">Occupation</th>
                                            {{--<th rowspan="3">Layer</th>--}}
                                            <th rowspan="3">System</th>
                                            <!-- <th colspan="12">Port/Interface Transport</th> -->
                                            <th rowspan="3">Date Detected</th>
                                            {{--<th rowspan="3">Date Undetected</th>--}}
                                            <th rowspan="3">Status</th>
                                            <!-- <th rowspan="3"></th> -->
                                        </tr>
                                        <tr>
                                            {{--<th rowspan="2">Regional</th>--}}
                                            <th colspan="4">End1</th>
                                            {{--<th rowspan="2">Regional</th>--}}
                                            <th colspan="2">End1</th>

                                            <!-- <th colspan="6">Transport-A (WDM/FO)</th>
                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
                                        </tr>
                                        <tr>
                                            <th>Regional</th>
                                            <th>STO</th>
                                            <th>Metro-E</th>
                                            <th>Port</th>
                                            <th>Router Name</th>
                                            <th>Port</th>

                                            <!-- <th>NE</th>
                                            <th>Board</th>
                                            <th>Shelf</th>
                                            <th>Slot</th>
                                            <th>Port</th>
                                            <th>Frek</th>
                                            <th>NE</th>
                                            <th>Board</th>
                                            <th>Shelf</th>
                                            <th>Slot</th>
                                            <th>Port</th>
                                            <th>Frek</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i=0; ?>
                                    @foreach($OneIPMPLS as $bh)
                                        <?php $i++; ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$bh['reg_telkom']}}</td>
                                            <td>{{\App\Metro::STO($bh['metroe_telkom'])}}</td>
                                            <td>{{$bh['metroe_telkom']}}</td>
                                            <td>{{$bh['port_telkom']}}</td>
                                            {{--<td>{{$bh['reg_tsel']}}</td>--}}
                                            <td>{{$bh['routername_tsel']}}</td>
                                            <td>{{$bh['port_tsel']}}</td>
                                            <td>{{$bh['capacity']}}</td>
                                            <td>{{number_format($bh['utilization_cap'])}}</td>
                                            <td>{{number_format($bh['utilization_max'])}} %</td>
                                            {{--<td>{{$bh['layer']}}</td>--}}
                                            <td>{{$bh['system']}}</td>
                                            <!-- <td>{{$bh['ne_transport_a']}}</td>
                                            <td>{{$bh['board_transport_a']}}</td>
                                            <td>{{$bh['shelf_transport_a']}}</td>
                                            <td>{{$bh['slot_transport_a']}}</td>
                                            <td>{{$bh['port_transport_a']}}</td>
                                            <td>{{$bh['frek_transport_a']}}</td>
                                            <td>{{$bh['ne_transport_b']}}</td>
                                            <td>{{$bh['board_transport_b']}}</td>
                                            <td>{{$bh['shelf_transport_b']}}</td>
                                            <td>{{$bh['slot_transport_b']}}</td>
                                            <td>{{$bh['port_transport_b']}}</td>
                                            <td>{{$bh['frek_transport_b']}}</td> -->
                                            <td>{{$bh['date_detected']}}</td>
                                            {{--<td>{{$bh['date_undetected']}}</td>--}}
                                            <td>{{$bh['status']}}</td>
                                            <!-- <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">

                                                        <li><a href="{{URL::Route('inventory.OneIPMPLS.show', $bh['idOneIPMPLS'])}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('inventory.OneIPMPLS.detail.delete', $bh['idOneIPMPLS'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
