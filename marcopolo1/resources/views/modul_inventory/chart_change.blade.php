<?php 
$tahun1 = date("Y");
$tahun0 = date("Y")-1;
?>

<script>

    function AccessLine()
    {
        reg = document.getElementById("AccessLine").value;

        if (reg == "All") {
            ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityAcc(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(12,$tahun1)}}];
            ChartJsAccLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationAcc(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(12,$tahun1)}}];
            ChartJsAccLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsAccLine.update();
        }
        else if (reg == "R1") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,1)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,1)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 1 Utilization";
  			ChartJsAccLine.update();
        }
        else if (reg == "R2") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,2)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,2)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 2 Utilization";
  			ChartJsAccLine.update();
        }
        else if (reg == "R3") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,3)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,3)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 3 Utilization";
  			ChartJsAccLine.update();
        }
        else if (reg == "R4") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,4)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,4)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 4 Utilization";
  			ChartJsAccLine.update();
        }
        else if (reg == "R5") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,5)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,5)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 5 Utilization";
  			ChartJsAccLine.update();
        }
        else if (reg == "R6") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,6)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,6)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 6 Utilization";
  			ChartJsAccLine.update();
        }
        else if (reg == "R7") {
        	ChartJsAccLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,7)}}];
        	ChartJsAccLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsAccLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,7)}}];
            ChartJsAccLine.data.datasets[0].label = "Regional 7 Utilization";
  			ChartJsAccLine.update();
        }

    }

    function AccessBar()
    {
        bulan = document.getElementById("AccessBar").value;

        if (bulan == "1") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Januari Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Januari Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "2") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Februari Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Februari Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "3") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Maret Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Maret Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "4") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "April Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "April Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "5") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Mei Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Mei Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "6") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Juni Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Juni Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "7") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Juli Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Juli Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "8") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Agustus Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "9") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "September Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "September Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "10") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Oktober Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "11") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "November Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "November Utilization";
  			ChartJsAccBar.update();
        }
        else if (bulan == "12") {
        	ChartJsAccBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,7)}}];
        	ChartJsAccBar.data.datasets[0].label = "Desember Capacity";
            ChartJsAccBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,7)}}];
            ChartJsAccBar.data.datasets[1].label = "Desember Utilization";
  			ChartJsAccBar.update();
        }
    }

    function BackhaulLine()
    {
        reg = document.getElementById("BackhaulLine").value;

        if (reg == "R1") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,1)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,1)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 1 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "R2") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,2)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,2)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 2 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "R3") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,3)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,3)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 3 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "R4") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,4)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,4)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 4 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "R5") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,5)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,5)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 5 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "R6") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,6)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,6)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 6 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "R7") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityBh(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityBh(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityBh(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityBh(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,7)}}];
            ChartJsBackLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,7)}}];
            ChartJsBackLine.data.datasets[0].label = "Regional 7 Utilization";
            ChartJsBackLine.update();
        }
        else if (reg == "All") {
            ChartJsBackLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityBh(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityBh(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityBh(12,$tahun1)}}];
            ChartJsBackLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsBackLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationBh(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationBh(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationBh(12,$tahun1)}}];
            ChartJsBackLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsBackLine.update();
        }
    }

    function BackhaulBar()
    {
        bulan = document.getElementById("BackhaulBar").value;

        if (bulan == "1") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(1,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Januari Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(1,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Januari Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "2") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(2,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Februari Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(2,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Februari Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "3") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(3,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Maret Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(3,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Maret Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "4") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(4,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "April Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(4,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "April Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "5") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(5,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Mei Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(5,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Mei Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "6") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(6,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Juni Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(6,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Juni Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "7") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(7,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Juli Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(7,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Juli Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "8") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(8,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(8,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Agustus Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "9") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(9,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "September Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(9,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "September Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "10") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(10,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(10,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Oktober Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "11") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(11,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "November Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(11,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "November Utilization";
  			ChartJsBackBar.update();
        }
        else if (bulan == "12") {
        	ChartJsBackBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityBh(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityBh(12,$tahun1,7)}}];
        	ChartJsBackBar.data.datasets[0].label = "Desember Capacity";
            ChartJsBackBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationBh(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationBh(12,$tahun1,7)}}];
            ChartJsBackBar.data.datasets[1].label = "Desember Utilization";
  			ChartJsBackBar.update();
        }
    }

    function IpranLine()
    {
        reg = document.getElementById("IpranLine").value;

        if (reg == "R1") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,1)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,1)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 1 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "R2") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,2)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,2)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 2 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "R3") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,3)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,3)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 3 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "R4") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,4)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,4)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 4 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "R5") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,5)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,5)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 5 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "R6") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,6)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,6)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 6 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "R7") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,7)}}];
            ChartJsIpranLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,7)}}];
            ChartJsIpranLine.data.datasets[0].label = "Regional 7 Utilization";
            ChartJsIpranLine.update();
        }
        else if (reg == "All") {
            ChartJsIpranLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityIpran(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpran(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpran(12,$tahun1)}}];
            ChartJsIpranLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsIpranLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationIpran(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpran(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpran(12,$tahun1)}}];
            ChartJsIpranLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsIpranLine.update();
        }
    }

    function IpranBar()
    {
        bulan = document.getElementById("IpranBar").value;

        if (bulan == "1") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(1,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Januari Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(1,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Januari Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "2") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(2,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Februari Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(2,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Februari Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "3") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(3,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Maret Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(3,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Maret Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "4") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(4,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "April Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(4,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "April Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "5") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(5,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Mei Capacity";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "6") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(6,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Juni Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(6,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Juni Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "7") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(7,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Juli Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(7,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Juli Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "8") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(8,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(8,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Agustus Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "9") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(9,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "September Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(9,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "September Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "10") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(10,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(10,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Oktober Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "11") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(11,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "November Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(11,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "November Utilization";
  			ChartJsIpranBar.update();
        }
        else if (bulan == "12") {
        	ChartJsIpranBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpran(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpran(12,$tahun1,7)}}];
        	ChartJsIpranBar.data.datasets[0].label = "Desember Capacity";
            ChartJsIpranBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpran(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpran(12,$tahun1,7)}}];
            ChartJsIpranBar.data.datasets[1].label = "Desember Utilization";
  			ChartJsIpranBar.update();
        }
    }

    function IpbbLine()
    {
        reg = document.getElementById("IpbbLine").value;

        if (reg == "R1") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,1)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,1)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 1 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "R2") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,2)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,2)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 2 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "R3") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,3)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,3)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 3 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "R4") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,4)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,4)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 4 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "R5") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,5)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,5)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 5 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "R6") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,6)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,6)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 6 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "R7") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,7)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,7)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Regional 7 Utilization";
            ChartJsIpbbLine.update();
        }
        else if (reg == "All") {
            ChartJsIpbbLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityIpbb(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityIpbb(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityIpbb(12,$tahun1)}}];
            ChartJsIpbbLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsIpbbLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationIpbb(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIpbb(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIpbb(12,$tahun1)}}];
            ChartJsIpbbLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsIpbbLine.update();
        }
    }

    function IpbbBar()
    {
        bulan = document.getElementById("IpbbBar").value;

        if (bulan == "1") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(1,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Januari Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(1,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Januari Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "2") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(2,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Februari Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(2,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Februari Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "3") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(3,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Maret Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(3,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Maret Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "4") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(4,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "April Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(4,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "April Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "5") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(5,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Mei Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(5,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Mei Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "6") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(6,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Juni Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(6,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Juni Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "7") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(7,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Juli Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(7,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Juli Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "8") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(8,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(8,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Agustus Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "9") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(9,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "September Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(9,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "September Utilization";
        }
        else if (bulan == "10") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(10,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(10,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Oktober Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "11") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(11,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "November Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(11,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "November Utilization";
  			ChartJsIpbbBar.update();
        }
        else if (bulan == "12") {
        	ChartJsIpbbBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIpbb(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityIpbb(12,$tahun1,7)}}];
        	ChartJsIpbbBar.data.datasets[0].label = "Desember Capacity";
            ChartJsIpbbBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIpbb(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIpbb(12,$tahun1,7)}}];
            ChartJsIpbbBar.data.datasets[1].label = "Desember Utilization";
  			ChartJsIpbbBar.update();
        }
    }

    function IxLine()
    {
        reg = document.getElementById("IxLine").value;

        if (reg == "R1") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,1)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,1)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 1 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "R2") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,2)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,2)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 2 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "R3") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,3)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,3)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 3 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "R4") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,4)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,4)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 4 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "R5") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,5)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,5)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 5 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "R6") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,6)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,6)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 6 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "R7") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityIx(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityIx(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityIx(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityIx(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,7)}}];
            ChartJsIxLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,7)}}];
            ChartJsIxLine.data.datasets[0].label = "Regional 7 Utilization";
            ChartJsIxLine.update();
        }
        else if (reg == "All") {
            ChartJsIxLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityIx(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityIx(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityIx(12,$tahun1)}}];
            ChartJsIxLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsIxLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationIx(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationIx(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationIx(12,$tahun1)}}];
            ChartJsIxLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsIxLine.update();
        }
    }

    function IxBar()
    {
        bulan = document.getElementById("IxBar").value;

        if (bulan == "1") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(1,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Januari Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(1,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Januari Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "2") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(2,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Februari Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(2,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Februari Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "3") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(3,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Maret Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(3,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Maret Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "4") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(4,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "April Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(4,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "April Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "5") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(5,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Mei Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(5,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Mei Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "6") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(6,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Juni Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(6,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Juni Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "7") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(7,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Juli Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(7,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Juli Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "8") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(8,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(8,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Agustus Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "9") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(9,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "September Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(9,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "September Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "10") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(10,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(10,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Oktober Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "11") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(11,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "November Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(11,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "November Utilization";
  			ChartJsIxBar.update();
        }
        else if (bulan == "12") {
        	ChartJsIxBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityIx(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityIx(12,$tahun1,7)}}];
        	ChartJsIxBar.data.datasets[0].label = "Desember Capacity";
            ChartJsIxBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationIx(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationIx(12,$tahun1,7)}}];
            ChartJsIxBar.data.datasets[1].label = "Desember Utilization";
  			ChartJsIxBar.update();
        }
    }

    function OneIPMPLSLine()
    {
        reg = document.getElementById("OneIPMPLSLine").value;

        if (reg == "All") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityOneIPMPLS(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityOneIPMPLS(12,$tahun1)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationOneIPMPLS(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationOneIPMPLS(12,$tahun1)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R1") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,1)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,1)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 1 Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R2") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,2)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,2)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 2 Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R3") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,3)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,3)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 3 Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R4") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,4)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,4)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 4 Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R5") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,5)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,5)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 5 Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R6") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,6)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,6)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 6 Utilization";
            ChartJsOneIPMPLSLine.update();
        }
        else if (reg == "R7") {
            ChartJsOneIPMPLSLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,7)}}];
            ChartJsOneIPMPLSLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsOneIPMPLSLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,7)}}];
            ChartJsOneIPMPLSLine.data.datasets[0].label = "Regional 7 Utilization";
            ChartJsOneIPMPLSLine.update();
        }

    }

    function OneIPMPLSBar()
    {
        bulan = document.getElementById("OneIPMPLSBar").value;

        if (bulan == "1") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(1,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Januari Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(1,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Januari Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "2") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(2,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Februari Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(2,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Februari Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "3") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(3,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Maret Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(3,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Maret Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "4") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(4,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "April Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(4,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "April Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "5") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(5,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Mei Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(5,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Mei Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "6") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(6,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Juni Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(6,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Juni Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "7") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(7,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Juli Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(7,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Juli Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "8") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(8,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(8,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Agustus Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "9") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(9,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "September Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(9,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "September Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "10") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(10,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(10,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Oktober Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "11") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(11,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "November Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(11,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "November Utilization";
            ChartJsOneIPMPLSBar.update();
        }
        else if (bulan == "12") {
            ChartJsOneIPMPLSBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityOneIPMPLS(12,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[0].label = "Desember Capacity";
            ChartJsOneIPMPLSBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationOneIPMPLS(12,$tahun1,7)}}];
            ChartJsOneIPMPLSBar.data.datasets[1].label = "Desember Utilization";
            ChartJsOneIPMPLSBar.update();
        }
    }

</script>