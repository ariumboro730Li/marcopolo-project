@extends('layout')

@section('OneIPMPLS')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
    OneIPMPLS Detail Regional - Inventory Datek
@stop


@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/OneIPMPLS')}}">OneIPMPLS</a></li>
                    <li>Detail Regional</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>Import Data</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{url('/inventory/input')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">add</i><span>Import Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                {!! Form::open(['method'=>'POST','action'=>'DatekController@PostImportOneIPMPLS','files'=> true]) !!}
                                <a href="Format.xlsx" class="btn btn-default">
                                        <i class="material-icons">file_download</i><span>
                                            Download Format</span>
                                    </a><br><br>

                                    <!--
                                    Buat sebuah input type file
                                    class pull-left berfungsi agar file input berada di sebelah kiri
                                    -->
                                    <input type="file" name="file" class="pull-left">

                                    <button type="submit" name="/inventory/OneIPMPLS/import" class="btn btn-success btn-sm">
                                        <i class="material-icons">remove_red_eye</i><span> Preview</span>
                                    </button>
                                </form>
                            </div>
                            @if(isset($OneIPMPLS))
                            <div class="table-responsive">
                                {!! Form::open(['method'=>'POST','action'=>'DatekController@SaveImportOneIPMPLS','files'=> true]) !!}
                                @if(isset($file))<input name="file" type="hidden" value="{{$file}}">@endif
                                <table class="align-center table table-striped table-bordered table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th rowspan="3" class="no">ID</th>
                                        <th colspan="3">Telkomsel</th>
                                        <th colspan="4">Telkom</th>
                                        <th rowspan="3">Capacity (G)</th>
                                        <th rowspan="3">Layer</th>
                                        <th rowspan="3">System</th>
                                        <th colspan="12">Port/Interface Transport</th>
                                        <th rowspan="3">Date Detected</th>
                                        <th rowspan="3">Status</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">Regional</th>
                                        <th colspan="2">End1</th>
                                        <th rowspan="2">Regional</th>
                                        <th colspan="3">End1</th>
                                        <th colspan="6">Transport-A (WDM/FO)</th>
                                        <th colspan="6">Transport-B (WDM/FO)</th>
                                    </tr>
                                    <tr>
                                        <th>Router Name</th>
                                        <th>Port</th>
                                        <th>STO</th>
                                        <th>Metro-E</th>
                                        <th>Port</th>
                                        <th>NE</th>
                                        <th>Board</th>
                                        <th>Shelf</th>
                                        <th>Slot</th>
                                        <th>Port</th>
                                        <th>Frek</th>
                                        <th>NE</th>
                                        <th>Board</th>
                                        <th>Shelf</th>
                                        <th>Slot</th>
                                        <th>Port</th>
                                        <th>Frek</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <center>
                                                <a href="{{URL::Route('inventory.OneIPMPLS.import')}}">
                                                    <input type="submit"  class="btn btn-primary btn-block" id="add" value="Import">
                                                </a>
                                            </center>
                                        </td>
                                    </tr>
                                    </tfoot>
                                    <tbody>


                                    @foreach($OneIPMPLS as $bh)
                                        @if($bh[0]!='idOneIPMPLS')
                                        <tr>
                                            <?php $datek = \App\DatekOneIPMPLS::Data($bh[0]); ?>
                                            <td>{{$bh[0]}}</td>
                                            <td>{{$datek->reg_tsel}}</td>
                                            <td>{{$datek->routername_tsel}}</td>
                                            <td>{{$datek->port_tsel}}</td>
                                            <td>{{$datek->reg_telkom}}</td>
                                            <td>{{$datek->sto_telkom}}</td>
                                            <td>{{$datek->metroe_telkom}}</td>
                                            <td>{{$datek->port_telkom}}</td>
                                            <td>{{$datek->capacity}}</td>
                                            <td>{{$datek->layer}}</td>
                                            <td>{{$datek->system}}</td>
                                            {{--<td>{{$bh['reg_tsel']}}</td>--}}
                                            {{--<td>{{$bh['routername_tsel']}}</td>--}}
                                            {{--<td>{{$bh['port_tsel']}}</td>--}}
                                            {{--<td>{{$bh['reg_telkom']}}</td>--}}
                                            {{--<td>{{$bh['sto_telkom']}}</td>--}}
                                            {{--<td>{{$bh['metroe_telkom']}}</td>--}}
                                            {{--<td>{{$bh['port_telkom']}}</td>--}}
                                            {{--<td>{{$bh['capacity']}}</td>--}}
                                            {{--<td>{{$bh['layer']}}</td>--}}
                                            {{--<td>{{$bh['system']}}</td>--}}
                                            <td>{{$bh[11]}}</td>
                                            <td>{{$bh[12]}}</td>
                                            <td>{{$bh[13]}}</td>
                                            <td>{{$bh[14]}}</td>
                                            <td>{{$bh[15]}}</td>
                                            <td>{{$bh[16]}}</td>
                                            <td>{{$bh[17]}}</td>
                                            <td>{{$bh[18]}}</td>
                                            <td>{{$bh[19]}}</td>
                                            <td>{{$bh[20]}}</td>
                                            <td>{{$bh[21]}}</td>
                                            <td>{{$bh[22]}}</td>
                                            {{--<td>{{$bh['ne_transport_a']}}</td>--}}
                                            {{--<td>{{$bh['board_transport_a']}}</td>--}}
                                            {{--<td>{{$bh['shelf_transport_a']}}</td>--}}
                                            {{--<td>{{$bh['slot_transport_a']}}</td>--}}
                                            {{--<td>{{$bh['port_transport_a']}}</td>--}}
                                            {{--<td>{{$bh['frek_transport_a']}}</td>--}}
                                            {{--<td>{{$bh['ne_transport_b']}}</td>--}}
                                            {{--<td>{{$bh['board_transport_b']}}</td>--}}
                                            {{--<td>{{$bh['shelf_transport_b']}}</td>--}}
                                            {{--<td>{{$bh['slot_transport_b']}}</td>--}}
                                            {{--<td>{{$bh['port_transport_b']}}</td>--}}
                                            {{--<td>{{$bh['frek_transport_b']}}</td>--}}
                                            <td>{{\Carbon\Carbon::now()->format('d-m-Y')}}</td>
                                            <td>UP</td>

                                        </tr>
                                        @endif
                                    @endforeach


                                    </tbody>
                                </table>
                                {{Form::close()}}


                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
