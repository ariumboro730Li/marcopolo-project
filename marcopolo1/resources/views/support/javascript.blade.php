<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{asset('plugins/node-waves/waves.js')}}"></script>

<script src="{{asset('plugins/bootstrap-notify/bootstrap-notify.js')}}"></script>
<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/pages/ui/dialogs.js')}}"></script>

<script src="{{asset('js/admin.js')}}"></script>

@yield('js')

<script src="{{asset('js/pages/index.js')}}"></script>
<script src="{{asset('js/demo.js')}}"></script>