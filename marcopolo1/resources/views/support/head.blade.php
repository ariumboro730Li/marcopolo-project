<head>        
  
  <title>@yield('title') | Tsel OC</title>            
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">

  <link href="{{asset('manifest.json')}}" rel="manifest">

  <link rel="shortcut icon" type="image/png" sizes="32x32" href="{{asset('images/favicon-32.png')}}">
  <link rel="shortcut icon" type="image/png" sizes="96x96" href="{{asset('images/favicon-96.png')}}">
  <link rel="shortcut icon" type="image/png" sizes="16x16" href="{{asset('images/favicon-16.png')}}">

  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">  
  <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />
  <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />

  @yield('css')

  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link href="{{asset('css/themes/all-themes.css')}}" rel="stylesheet" />

  <link href="{{asset('css/bs-editable.css')}}" rel="stylesheet" />
  <link href="{{asset('css/select2.css')}}" rel="stylesheet" />

  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="application-name" content="CNOP OC">
  <meta name="apple-mobile-web-app-title" content="CNOP OC">
  <meta name="theme-color" content="#b61c1c">
  <meta name="msapplication-navbutton-color" content="#b61c1c">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="msapplication-starturl" content="/">

</head>
