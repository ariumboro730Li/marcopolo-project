<script>

    $(document).ready(function(){

        //get base URL *********************
        var url = $('#url').val();


        //display modal form for creating new product *********************
        $('#btn_add').click(function(){
            $('#btn-save').val("add");
            $('#frmProducts').trigger("reset");
            $('#myModal').modal('show');
        });



        //display modal form for product EDIT ***************************
        $(document).on('click','.open_modal',function(){
            var product_id = $(this).val();

            // Populate Data in Edit Modal Form
            $.ajax({
                type: "GET",
                url: '/rca/detail' + '/' + product_id,
                success: function (data) {
                    console.log(data);
                    $('#idrca').val(data.idrca);
                    $('#detail_id').val(data.iddetail);
                    $('#activity').val(data.improvement_activity);
                    $('#status').val(data.status);
                    $('#target').val(data.tgl_target);
                    $('#pic').val(data.pic);
                    $('#btn-save').val("update");
                    $('#myModal').modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });



        //create new product / update existing product ***************************
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();
            var formData = {
                idrca: $('#idrca').val(),
                iddetail: $('#detail_id').val(),
                improvement_activity: $('#activity').val(),
                status: $('#status').val(),
                tgl_target: $('#target').val(),
                pic: $('#pic').val(),
            }

            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();
            var type = "POST"; //for creating new resource
            var product_id = $('#detail_id').val();;
            var my_url = '/rca/detail';
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + product_id;
            }
            console.log(formData);
            $.ajax({
                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var product = '<tr id="detail' + data['iddetail'] + '" class="align-center"><td>' + data.improvement_activity + '</td><td>' + data.status + '</td><td>' + data.tgl_target + '</td><td>' + data.pic + '</td>';
                    product += '<td><button class="btn btn-success  open_modal" value="' + data.iddetail + '">Edit</button>';
                    product += ' <button class="btn btn-danger delete-product" value="' + data.iddetail + '">Hapus</button></td></tr>';
                    if (state == "add"){ //if user added a new record
                        $('#detail-list').append(product);
                    }else{ //if user updated an existing record
                        $("#detail" + product_id).replaceWith( product );
                    }
                    $('#frmProducts').trigger("reset");
                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        //delete product and remove it from TABLE list ***************************
        $(document).on('click','.delete-product',function(){
            var product_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: '/rca/detail/' + product_id,
                success: function (data) {
                    console.log(data);
                    $("#detail" + product_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    });
</script>