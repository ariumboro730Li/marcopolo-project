@extends('layout')

@section('menu1')
    class="active"
@stop

@section('css')
    {{--<link href="{{asset('plugins/morrisjs/morris.css')}}" rel="stylesheet" />--}}
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
RCA Dashboard
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>RCA DASHBOARD</h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <h2 class="pull-left no-margin">INCIDENT</h2>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12 m-b-0">
                                    <h4>TOTAL CRITICAL INCIDENT IN A YEAR: {{\App\RCA::TotalIncident()}}</h4>
                                
                                    <div class="progress">
                                        {{-- <div class="progress-bar progress-bar-success" role="progressbar" style="width: {{\App\RCA::TotalRegionalPercentage(1)}}%">
                                            TR1: {{\App\RCA::TotalRegionalPercentage(1)}}%
                                        </div>
                                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" style="width: {{\App\RCA::TotalRegionalPercentage(2)}}%">
                                            TR2: {{\App\RCA::TotalRegionalPercentage(2)}}%
                                        </div>
                                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: {{\App\RCA::TotalRegionalPercentage(3)}}%">
                                            TR3: {{\App\RCA::TotalRegionalPercentage(3)}}%
                                        </div>
                                        <div class="progress-bar progress-bar-success" style="width: {{\App\RCA::TotalRegionalPercentage(4)}}%">
                                            TR4: {{\App\RCA::TotalRegionalPercentage(4)}}%
                                        </div>
                                        <div class="progress-bar progress-bar-success" style="width: {{\App\RCA::TotalRegionalPercentage(5)}}%">
                                            TR5: {{\App\RCA::TotalRegionalPercentage(5)}}%
                                        </div>
                                        <div class="progress-bar progress-bar-success" style="width: {{\App\RCA::TotalRegionalPercentage(6)}}%">
                                            TR6: {{\App\RCA::TotalRegionalPercentage(6)}}%
                                        </div>
                                        <div class="progress-bar progress-bar-success" style="width: {{\App\RCA::TotalRegionalPercentage(7)}}%">
                                            TR7: {{\App\RCA::TotalRegionalPercentage(7)}}%
                                        </div> --}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <h2 class="pull-left no-margin">ROOT CAUSE</h2>
                            </div>
                        </div>
                        <div class="body">
                            <canvas id="donut_chart_rca"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <h2 class="pull-left no-margin">MTTR INCIDENT</h2>
                                {{--<div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">--}}
                                    {{--<select class="form-control show-tick" name="month" id="RcaBar" onchange="RcaBar()">--}}
                                        {{--<option id="mttr1" value="1">Januari</option>--}}
                                        {{--<option id="mttr2" value="2">Februari</option>--}}
                                        {{--<option id="mttr3" value="3">Maret</option>--}}
                                        {{--<option id="mttr4" value="4">April</option>--}}
                                        {{--<option id="mttr5" value="5">Mei</option>--}}
                                        {{--<option id="mttr6" value="6">Juni</option>--}}
                                        {{--<option id="mttr7" value="7">Juli</option>--}}
                                        {{--<option id="mttr8" value="8">Agustus</option>--}}
                                        {{--<option id="mttr9" value="9">September</option>--}}
                                        {{--<option id="mttr10" value="10">Oktober</option>--}}
                                        {{--<option id="mttr11" value="11">November</option>--}}
                                        {{--<option id="mttr12" value="12">Desember</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="body">
                            <canvas id="bar_chart_rca"></canvas>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <h2 class="pull-left no-margin">INCIDENT TREND</h2>
                                {{--<div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">--}}
                                    {{--<select class="form-control show-tick" name="regional" id="RcaLine" onchange="RcaLine()">--}}
                                        {{--<option value="All" selected="true">Nasional</option>--}}
                                        {{--<option value="R1">Regional 1</option>--}}
                                        {{--<option value="R2">Regional 2</option>--}}
                                        {{--<option value="R3">Regional 3</option>--}}
                                        {{--<option value="R4">Regional 4</option>--}}
                                        {{--<option value="R5">Regional 5</option>--}}
                                        {{--<option value="R6">Regional 6</option>--}}
                                        {{--<option value="R7">Regional 7</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="body">
                            <canvas id="line_chart_rca"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <h2 class="pull-left no-margin">IMPROVEMENT ACTIVITY</h2>
                                {{--<div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">--}}
                                    {{--<select class="form-control show-tick" name="month" id="RcaBar2" onchange="RcaBar2()">--}}
                                        {{--<option id="rca1" value="1">Januari</option>--}}
                                        {{--<option id="rca2" value="2">Februari</option>--}}
                                        {{--<option id="rca3" value="3">Maret</option>--}}
                                        {{--<option id="rca4" value="4">April</option>--}}
                                        {{--<option id="rca5" value="5">Mei</option>--}}
                                        {{--<option id="rca6" value="6">Juni</option>--}}
                                        {{--<option id="rca7" value="7">Juli</option>--}}
                                        {{--<option id="rca8" value="8">Agustus</option>--}}
                                        {{--<option id="rca9" value="9">September</option>--}}
                                        {{--<option id="rca10" value="10">Oktober</option>--}}
                                        {{--<option id="rca11" value="11">November</option>--}}
                                        {{--<option id="rca12" value="12">Desember</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="body">
                            <canvas id="bar_chart_rca2"></canvas>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

    <script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('plugins/node-waves/waves.js')}}"></script>
    <script src="{{asset('plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <script src="{{asset('plugins/chartjs/Chart.bundle.js')}}"></script>
    <script src="{{asset('plugins/chartjs/Chart.js')}}"></script>
    <script src="{{asset('plugins/chartjs/Chart.PieceLabel.js')}}"></script>
    <script src="{{asset('plugins/chartjs/chartjs-plugin-datalabels.js')}}"></script>
    <script src="{{asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
        
    <script src="{{asset('js/admin.js')}}"></script>

    {{--<script>--}}
        {{--var bulan = moment().format("M");--}}
        {{--document.getElementById("rca"+bulan).selected = true;--}}
        {{--document.getElementById("mttr"+bulan).selected = true;--}}
    {{--</script>--}}

    @include('modul_rca.chart')
{{--    @include('modul_rca.chart_change')--}}

    <script src="{{asset('js/demo.js')}}"></script>

@stop


@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@stop