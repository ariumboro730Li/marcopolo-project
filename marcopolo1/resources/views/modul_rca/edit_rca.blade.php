@extends('layout')

@section('menu2')
    class="active"
@stop


@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
{{--    <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">--}}
    {{--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>--}}
@stop

@section('title')
Edit RCA
@stop
    
@section('content')
    {{--<!--Import jQuery before export.js-->--}}
    {{--<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>--}}
    {{--<script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>--}}

    {{--<!--Data Table-->--}}
    {{--<script type="text/javascript"  src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>--}}
    {{--<script type="text/javascript"  src="http://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>--}}

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/rca')}}">RCA</a></li>
                    <li class="active">Edit RCA</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>EDIT RCA</h2>
                                </div>
                            </div>
                        </div>

                        {!! Form::open(['method'=>'POST','action'=>'RCAController@UpdateRCA','files'=> true]) !!}

                        <input type="hidden" name="id" value="{{$rca['idrca']}}">

                        <div class="body">
                            <div class="row clearfix">           
                                <div class="col-md-12">                            

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">           
                                                    <input type="text" class="form-control" placeholder="" name="title" value="{{$rca['title']}}"/>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <label class="col-md-2 col-xs-12 form-control-label">Regional</label>
                                        <div class="col-md-2 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">                         
                                                    <select class="form-control show-tick" name="regional">
                                                        <option value="Regional 1" @if($rca['regional']=='Regional 1') selected @endif>Regional 1</option>
                                                        <option value="Regional 2" @if($rca['regional']=='Regional 2') selected @endif>Regional 2</option>
                                                        <option value="Regional 3" @if($rca['regional']=='Regional 3') selected @endif>Regional 3</option>
                                                        <option value="Regional 4" @if($rca['regional']=='Regional 4') selected @endif>Regional 4</option>
                                                        <option value="Regional 5" @if($rca['regional']=='Regional 5') selected @endif>Regional 5</option>
                                                        <option value="Regional 6" @if($rca['regional']=='Regional 6') selected @endif>Regional 6</option>
                                                        <option value="Regional 7" @if($rca['regional']=='Regional 7') selected @endif>Regional 7</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Date & time</label>
                                        <div class="col-md-3 col-xs-12">  
                                            <div class="form-group">
                                                <div class="form-line">                      
                                                    <input type="text" name="date_time_open" id="date1" class="datetimepicker form-control" placeholder="Open" value="{{$rca['date_time_open']}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12">  
                                            <div class="form-group">
                                                <div class="form-line">                      
                                                    <input type="text" name="date_time_close" id="date2" class="datetimepicker form-control" placeholder="Close" value="{{$rca['date_time_close']}}">
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-xs-12 form-control-label">Duration</label>
                                        <div class="col-md-2 col-xs-12">                        
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="" name="duration" value="{{$rca['duration']}}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Network impact</label>
                                        <div class="col-md-10 col-xs-12">                        
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea rows="3" name="network_impact" class="form-control no-resize" placeholder="Type what you want...">{{$rca['network_impact']}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Service Impact</label>
                                        <div class="col-md-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">                        
                                                    <textarea rows="3" name="service_impact" class="form-control no-resize" placeholder="Type what you want...">{{$rca['service_impact']}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Root cause category</label>
                                        <div class="col-md-3 col-xs-12">    
                                            <div class="form-group">
                                                <div class="form-line">                    
                                                    <select class="form-control show-tick" name="rc_category" id="rc_category">
                                                        <option value="NE Problem" @if($rca['rc_category']=='NE Problem') selected @endif>NE Problem</option>
                                                        <option value="Transmission (OSP)" @if($rca['rc_category']=='Transmission (OSP)') selected @endif>Transmission (OSP)</option>
                                                        <option value="Power" @if($rca['rc_category']=='Power') selected @endif>Power</option>
                                                        <option value="Activity" @if($rca['rc_category']=='Activity') selected @endif>Activity</option>
                                                        <option value="Logical Configuration" @if($rca['rc_category']=='Logical Configuration') selected @endif>Logical Configuration</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line" id="data-ne">                    
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="GPON" @if($rca['rc_category_detail']=='GPON') selected @endif>GPON</option>
                                                        <option value="Metro" @if($rca['rc_category_detail']=='Metro') selected @endif>Metro</option>
                                                        <option value="PE" @if($rca['rc_category_detail']=='PE') selected @endif>PE</option>
                                                        <option value="Tera" @if($rca['rc_category_detail']=='Tera') selected @endif>Tera</option>
                                                        <option value="DWDM" @if($rca['rc_category_detail']=='DWDM') selected @endif>DWDM</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-transmission" style="display: none">    
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="FO" @if($rca['rc_category_detail']=='FO') selected @endif>FO</option>
                                                        <option value="Radio" @if($rca['rc_category_detail']=='Radio') selected @endif>Radio</option>
                                                        <option value="Satelit" @if($rca['rc_category_detail']=='Satelit') selected @endif>Satelit</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-power" style="display: none">                    
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="PLN" @if($rca['rc_category_detail']=='PLN') selected @endif>PLN</option>
                                                        <option value="AVR" @if($rca['rc_category_detail']=='AVR') selected @endif>AVR</option>
                                                        <option value="Genset" @if($rca['rc_category_detail']=='Genset') selected @endif>Genset</option>
                                                        <option value="ATS" @if($rca['rc_category_detail']=='ATS') selected @endif>ATS</option>
                                                        <option value="MDP" @if($rca['rc_category_detail']=='MDP') selected @endif>MDP</option>
                                                        <option value="SDP" @if($rca['rc_category_detail']=='SDP') selected @endif>SDP</option>
                                                        <option value="Rectifier" @if($rca['rc_category_detail']=='Rectifier') selected @endif>Rectifier</option>
                                                        <option value="DC PDC" @if($rca['rc_category_detail']=='DC PDC') selected @endif>DC PDB</option>
                                                        <option value="Baterai" @if($rca['rc_category_detail']=='Baterai') selected @endif>Baterai</option>
                                                        <option value="Grounding" @if($rca['rc_category_detail']=='Grounding') selected @endif>Grounding</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-activity" style="display: none">
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="Configuration" @if($rca['rc_category_detail']=='Configuration') selected @endif>Configuration</option>
                                                        <option value="Human Error" @if($rca['rc_category_detail']=='Human Error') selected @endif>Human Error</option>
                                                        <option value="Anomali" @if($rca['rc_category_detail']=='Anomali') selected @endif>Anomali</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-logical_configuration" style="display: none">
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="Bug IOS" @if($rca['rc_category_detail']=='Bug IOS') selected @endif>Bug IOS</option>
                                                        <option value="TIMOS" @if($rca['rc_category_detail']=='TIMOS') selected @endif>TIMOS</option>
                                                        <option value="Configuration" @if($rca['rc_category_detail']=='Configuration') selected @endif>Configuration</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-xs-12 form-control-label">Root cause detail</label>
                                        <div class="col-md-3 col-xs-12">  
                                            <div class="form-group">
                                                <div class="form-line">                      
                                                    <textarea rows="2" name="rc_detail" class="form-control no-resize" placeholder="Type what you want...">{{$rca['rc_detail']}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Action</label>
                                        <div class="col-md-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">                        
                                                    <textarea rows="3" name="action" class="form-control no-resize" placeholder="Type what you want...">{{$rca['action']}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                      <select class="form-control" name="" id="selectpaste">
                                        <option value="0">Choose to paste image</option>
                                        <option value="1">Evidence</option>
                                        <option value="2">Topology</option>
                                      </select>
                                    </div>

                                    <div class="form-group">
                                        <input type="checkbox" name="" id="">
                                        <div id="clickEvidence">
                                            <label class="col-md-2 col-xs-12 form-control-label ">Evidence</label>
                                            <div class="col-md-4 col-xs-12">                        
                                                <div class="form-group">
                                                    <input type="file" class="fileinput" id="evidence_file" name="evidence_file" multiple title="Browse file..."/>
                                                    <span class="help-block">Format file: .jpg, .jpeg, .png</span>
                                                    <div id="div_image1" class="div_image1 my-2 d-flex justify-content-center"></div>
    
                                                </div>
                                            </div>
                                        </div>
                                        <div id="clickTopology">
                                            <label class="col-md-2 col-xs-12 form-control-label">Topology</label>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="file" class="fileinput" name="topology_file" id="topology_file" title="Browse file..."/>
                                                    <span class="help-block">Format file: .jpg, .jpeg, .png</span>
                                                    <div id="div_image2" class="div_image2 my-2 d-flex justify-content-center"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>                                
                        <div class="panel-footer">      
                            {{--<button type="submit" class="btn btn-primary pull-right">Isi Aktivitas</button>--}}
                            {{--<button type="button" class="btn btn-default" onclick="window.location.href='{{url('/rca')}}'">Back</button>--}}
                        </div>

                    </div>

                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>INPUT AKTIVITAS</h2>
                                </div>
                            </div>
                        </div>

                        <div class="body">                 

                        {{--{!! Form::open(['method'=>'POST','action'=>'RCAController@UpdateRCA','id'=>'form','name'=>'form']) !!}--}}
                            {{--<input type="hidden" name="id" value="" id="id">--}}
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Improvement Activity</th>
                                            <th rowspan="2">Status</th>
                                            <th rowspan="2">Target</th>
                                            <th rowspan="2">PIC</th>
                                            <th rowspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="detail-list" name="detail-list">
                                    @foreach(\App\RCADetail::getRCADetailData($rca['idrca']) as $data)
                                        <tr id="detail{{$data['iddetail']}}" class="align-center">
                                            <td>{{$data['improvement_activity']}}</td>
                                            <td>{{$data['status']}}</td>
                                            <td>{{Carbon\Carbon::parse($data['tgl_target'])->format('d-m-Y')}}</td>
                                            <td>{{$data['pic']}}</td>
                                            <td style="width:15%;">
                                                <button type="button" class="btn btn-success open_modal" value="{{$data['iddetail']}}">
                                                Edit
                                                </button>
                                                <button type="button" class="btn btn-danger delete-product" value="{{$data['iddetail']}}">
                                                Hapus
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#editModal" id="btn_add" name="btn_add">Tambah</button>
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/rca')}}'">Back</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Passing BASE URL to AJAX -->
    <input id="url" type="hidden" value="{{ \Request::url() }}">

    <!-- MODAL SECTION -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">RCA Activity</h4>
                </div>
                <div class="modal-body">
                    <form id="frmProducts" name="frmProducts" class="form-horizontal" novalidate="">
                        <input type="hidden" id="idrca" name="idrca" value="{{$rca['idrca']}}">
                        <input type="hidden" id="detail_id" name="detail_id" value="0">
                        <div class="form-group">
                            <label for="inputName" class="col-sm-3 control-label">Improvement Activity</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control has-error" id="activity" name="v" placeholder="Activity" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDetail" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9">
                                <select class="form-control show-tick" name="status" id="status">
                                    <option value="Open">Open</option>
                                    <option value="Close">Close</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDetail" class="col-sm-3 control-label">Target</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="target" name="target" placeholder="target" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDetail" class="col-sm-3 control-label">PIC</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="pic" name="pic" placeholder="pic" value="">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Simpan</button>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="example-modal">--}}
        {{--<div class="modal" id="editModal">--}}
          {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
              {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                  {{--<span aria-hidden="true">&times;</span></button>--}}
                {{--<h4 class="modal-title text-center">Isi Aktivitas RCA</h4>--}}
              {{--</div>--}}
              {{--<div class="modal-body">--}}
                {{--<form id="form" name="form">--}}
                {{--<input type="hidden" name="id" value="" id="id">--}}
                  {{--<div class="form-group">--}}
                    {{--<div class="form-line">--}}
                        {{--<label for="name">Improvement Activity</label>--}}
                        {{--<textarea rows="3" name="activity" id="activity" class="form-control no-resize" placeholder="Aktivitas..."></textarea>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                  {{--<div class="form-group">--}}
                    {{--<div class="form-line">--}}
                        {{--<label for="name">Status</label>--}}
                        {{--<select class="form-control show-tick" name="status" id="status">--}}
                            {{--<option value="Open">Open</option>--}}
                            {{--<option value="Close">Close</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                  {{--<div class="form-group">--}}
                    {{--<div class="form-line">--}}
                        {{--<label for="name">PIC</label>--}}
                        {{--<input type="text" class="form-control" id="pic" placeholder="PIC" name="pic" required>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                  {{--<div class="form-group">--}}
                    {{--<div class="form-line">--}}
                        {{--<label for="email">Target</label>--}}
                        {{--<input type="text" name="target" id="target" class="datepicker form-control" placeholder="Tanggal Target">--}}
                    {{--</div>--}}
                  {{--</div>--}}
                  {{----}}
                  {{--<input type="submit" value="submit" style="display:none;">--}}
                {{--</form>--}}
              {{--</div>--}}
              {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>--}}
                {{--<button type="button" class="btn btn-primary" id="btn-save">Simpan</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}

        {{--<div class="modal" id="deleteModal">--}}
          {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
              {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                  {{--<span aria-hidden="true">&times;</span></button>--}}
                {{--<h4 class="modal-title text-center">Hapus Data</h4>--}}
              {{--</div>--}}
              {{--<div class="modal-body">--}}
                {{--<p>Anda yakin ingin menghapus data?</p>--}}
              {{--</div>--}}
              {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>--}}
                {{--<button type="button" class="btn btn-danger" id="btndelete">Hapus</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>    
    <script src="{{asset('js/select2.js')}}"></script>
    <script src="{{asset('js/pasteimage.js')}}"></script>

    <script>
        $('#date1').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm'});
        $('#date2').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm'});

        $('#rc_category').on('change', function() {
            if (this.value == "NE Problem") {
                document.getElementById('data-ne').style.display = 'block';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Transmission (OSP)") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'block';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Power") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'block';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Activity") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'block';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Logical Configuration") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'block';
            }
        });
    </script>

    {{--<!-- jQuery 2.2.3 -->--}}
    {{--<script src="{{asset('plugins/jquery/jquery-2.2.3.min.js')}}"></script>--}}
    {{--<!-- jQuery UI 1.11.4 -->--}}
    {{--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>--}}

    {{--<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>--}}

    {{--<!-- Slimscroll -->--}}
    {{--<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>--}}
    <script>
        $('#target').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm'});
        /*Document Ready Event*/
        jQuery(document).ready(function () {
                /*Binding Paste Event To The Document*/
                jQuery(document).bind("paste", function (event) {
                    var items = (event.clipboardData || event.originalEvent.clipboardData).items;
                    /*Make Sure Only One File is Copied*/
                    if (items.length != 1) {
                        return;
                    }
                    var item = items[0];
                    /*Verify If The Copied Item is File*/
                    if (item.kind === 'file') {
                        var file = item.getAsFile();
                        var filename = file.name;
                        /*Get File Extension*/
                        var ext = filename.split('.').reverse()[0].toLowerCase();
                        /*Check Image File Extensions*/
                        if (jQuery.inArray(ext, ['jpg', 'png']) > -1) {
                            /*Create FormData Instance*/
                            var data = new FormData();
                            data.append('file', file);
                            /*Request Ajax With File*/
                            request_ajax_file('upload.php', data, file_uploaded);
                        } else {
                            alert('Invalid File');
                        }
                    }
                });
            });

            function file_uploaded(response) {
                /*Process server response*/
            }

            /*Function to Make AJAX Request With File*/
            function request_ajax_file(ajax_url, ajax_data, ajax_callback) {
                jQuery.ajax({
                    url: ajax_url,
                    data: ajax_data,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if (typeof ajax_callback == 'function') {
                            ajax_callback(response);
                        } else if (typeof ajax_callback == 'string') {
                            if (ajax_callback != '') {
                                eval(ajax_callback + '(response)');
                            }
                        }
                    }
                });
            }
    </script>
    <script>

        $("#selectpaste").change(function() {
            var nilai = $(this).val();
            if (nilai == 1) {
                // $(this).css("background", "green");
                $("#clickEvidence").css("color", "blue");
                $("#clickTopology").css("color", "black");

                const form = document.getElementById("new_document_attachment");
                const fileInput = document.getElementById("evidence_file");

                var imagesPreview = function(input, placeToInsertImagePreview) {
                if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img class="" style="width:90px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                    }
                    }
                };
                $('input#evidence_file').on('change', function() {
                    $(".div_image1").html("");
                    imagesPreview(this, '.div_image1');
                });


                window.addEventListener('paste', e => {
                    fileInput.files = e.clipboardData.files;
                });

                function paste1(sr) {
                        $("#div_image1").empty();
                        $("#div_image1").append("<img class='' style='width:90px' src='" + sr + "'>");
                    }
                    
                    $(function() {
                        $.pasteimage(paste1);
                })
            } else if (nilai == 2) {
                                // $(this).css("background", "green");
                $("#clickTopology").css("color", "blue");
                $("#clickEvidence").css("color", "black");

                const fileInput2 = document.getElementById("topology_file");

                var imagesPreview = function(input, placeToInsertImagePreview) {

                if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img class="" style="width:90px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                    }
                    }
                };

                $('input#topology_file').on('change', function() {
                    $(".div_image2").html("");
                    imagesPreview(this, '.div_image2');
                });


                window.addEventListener('paste', e => {
                    fileInput2.files = e.clipboardData.files;
                });


                function paste2(src) {
                        $("#div_image2").empty();
                        $("#div_image2").append("<img class='' style='width:90px' src='" + src + "'>");
                    }
                    
                    $(function() {
                        $.pasteimage(paste2);
                })


            } else {

            }
        });
            



    </script>

    {{--<!-- Scripts -->--}}
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
    {{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
    {{--<script src="{{asset('js/ajaxscript.js')}}"></script>--}}
    {{--<script src="{{asset('js/jquery.min.js')}}"></script>--}}

    <meta name="_token" content="{!! csrf_token() !!}" />
    @include("modul_rca.ajax_rca")
@stop