<html>
<title>Firebase Messaging Demo</title>
<style>
    div {
        margin-bottom: 15px;
    }
</style>
<body>
<div id="token"></div>
<div id="msg"></div>
<div id="notis"></div>
<div id="err"></div>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script>


    MsgElem = document.getElementById("msg")
    TokenElem = document.getElementById("token")
    NotisElem = document.getElementById("notis")
    ErrElem = document.getElementById("err")
    // Initialize Firebase
    // TODO: Replace with your project's customized code snippet
    var config = {
        apiKey: "AAAAwhW1_dk:APA91bHhfWjtYz0w_nlquQXCLnfpewCAj0M__GCS_oMfvN_899D6Gop8rBo7_ppmqJjm5Tk_aTusFunEDYaU-WTU0f_JXWklEEat2_eNaWk7y3TyWK1Pf0cRQtvJE_zzmFtJjyVjroJQ",
        authDomain: "marcxopolo.firebaseapp.com",
        databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
        storageBucket: "<BUCKET>.appspot.com",
        messagingSenderId: "833587903961",
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging
        .requestPermission()
        .then(function () {
            MsgElem.innerHTML = "Notification permission granted."
            console.log("Notification permission granted.");

            // get the token in the form of promise

            return messaging.getToken()
        })
        .then(function(token) {
            <?php $myPhpVar="";?>
            TokenElem.innerHTML = "token is : " + token;


            window.open(
                '/setting/push/' + token,
                '_blank' // <- This is what makes it open in a new window.
            );
            //
            //
            // window.location.href = "/setting/push/" + token;
            {{--{{\App\User::addPush(tkt)}};--}}
        })
        .catch(function (err) {
            ErrElem.innerHTML =  ErrElem.innerHTML + "; " + err
            console.log("Unable to get permission to notify.", err);
        });

    messaging.onMessage(function(payload) {
        console.log("Message received. ", payload);
        NotisElem.innerHTML = NotisElem.innerHTML + JSON.stringify(payload)
    });
</script>

<body>

<?php
//$dummy = "<script>document.write(hasilToken)</script>";
//echo $dummy;
//\App\User::addPush($dummy);
//?>

</body>

</html>
