@extends('layout')

@section('menu11')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet">
@stop

@section('title')
    User Management | Setting
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Setting</li>
                    <li><a href="{{url('setting/user')}}">User Management</a></li>
                    <li class="active">Detail User</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>User</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            @if(! empty($user))
                                {!! Form::open(['method'=>'POST','action'=>'UserController@UpdateUser','class'=>'form-horizontal']) !!}
                                <input type="hidden" name="id" value="{{$user['id']}}"/>
                            @else
                                {!! Form::open(['method'=>'POST','action'=>'UserController@TambahUserBaru','class'=>'form-horizontal']) !!}
                            @endif
                            {{--<form class="form-horizontal">--}}


                            @if($keterangan =="Detail")
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('name',$user['name'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Name']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email">E-Mail</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('email',$user['email'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Email']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="ldap">LDAP</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('ldap',$user['ldap'],['readonly','class'=>'form-control','placeholder'=>'Masukkan LDAP']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            @elseif($keterangan=="Edit")
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('name',$user['name'],['class'=>'form-control','placeholder'=>'Masukkan Nama']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email">E-Mail</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('email',$user['email'],['class'=>'form-control','placeholder'=>'Masukkan Email']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="ldap">LDAP</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('ldap',$user['ldap'],['class'=>'form-control','placeholder'=>'Masukkan LDAP']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="password">Password</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::password('password',['class'=>'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="tipe">Hak Akses</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::select('access',
                                                    \App\Role::getRole()
                                                    ,\App\User::getrole($user['id']),['class'=>'form-control ' ]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect" value="Update" id="update">Perbarui</button>
                                        <button type="reset" class="btn btn-secondary m-t-15 m-l-5 waves-effect"  value="Cancel" id="cancel">Batal</button>
                                    </div>
                                </div>
                            @else
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('name','',['class'=>'form-control','placeholder'=>'Masukkan Name']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email">E-Mail</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Masukkan Email']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="ldap">LDAP</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::text('ldap','',['class'=>'form-control','placeholder'=>'Masukkan LDAP']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="password">Password</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
                                        <button type="reset" class="btn btn-secondary m-t-15 m-l-5 waves-effect">Batal</button>
                                    </div>
                                </div>
                            @endif

                            {!! Form::close() !!}



                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop





{{--@extends('layout')--}}

{{--@section('menu2')--}}
    {{--class="active"--}}
{{--@endsection--}}

{{--@section('head.scripts')--}}

    {{--<link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />--}}
{{--@endsection--}}


{{--@section('content')--}}


    {{--<!-- Horizontal Layout -->--}}
    {{--<div class="row clearfix">--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<h2>--}}
                        {{--User--}}
                    {{--</h2>--}}
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="body">--}}

                    {{--<form class="form-horizontal">--}}


                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="username">Username</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('username',$user['username'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Username']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="email">E-Mail</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('email',$user['email'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Email']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="phone">Phone</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('phone',$user['phone'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Telepon']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="balance">Balance</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('balance',$user['balance'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Balance']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="tipe">Tipe</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::select('tipe',[--}}
                                            {{--'0' => 'Pengguna'--}}
                                            {{--],$user['type'],['readonly','class'=>'form-control show-tick']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}


                {{--</div>--}}


            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Horizontal Layout -->--}}
    {{--<div class="row clearfix">--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<h2>--}}
                        {{--Details--}}
                    {{--</h2>--}}
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="body">--}}

                    {{--<form class="form-horizontal">--}}
                    {{--<div class="row clearfix">--}}
                        {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                            {{--<label for="tipe">Tipe</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-line">--}}
                                    {{--{!! Form::select('tipe',[--}}
                                    {{--'0' => 'Laki Laki',--}}
                                    {{--'1' => 'Perempuan'--}}
                                    {{--],$user->Details()->first()->gender,['readonly','class'=>'form-control show-tick']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="row clearfix">--}}
                        {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                            {{--<label for="alamat">Alamat</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-line">--}}
                                    {{--{!! Form::text('alamat',$user->Details()->first()->alamat,['readonly','class'=>'form-control','placeholder'=>'Masukkan Username']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row clearfix">--}}
                        {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                            {{--<label for="kota">Kota</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-line">--}}
                                    {{--{!! Form::text('kota',$user->Details()->first()->kota,['readonly','class'=>'form-control','placeholder'=>'Masukkan Email']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row clearfix">--}}
                        {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                            {{--<label for="provinsi">Provinsi</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-line">--}}
                                    {{--{!! Form::text('provinsi',$user->Details()->first()->provinsi,['readonly','class'=>'form-control','placeholder'=>'Masukkan Telepon']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row clearfix">--}}
                        {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                            {{--<label for="kodePos">Kode Pos</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-line">--}}
                                    {{--{!! Form::text('kodePos',$user->Details()->first()->kodePos,['readonly','class'=>'form-control','placeholder'=>'Masukkan Balance']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row clearfix">--}}
                        {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                            {{--<label for="tanggalLahir">Tanggal Lahir</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="form-line">--}}
                                    {{--{!! Form::text('tanggalLahir',$user->Details()->first()->tanggalLahir,['readonly','class'=>'form-control','placeholder'=>'Masukkan Balance']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}




                {{--</div>--}}


            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row clearfix">--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<h2>--}}
                        {{--Detail Bank--}}
                    {{--</h2>--}}
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
{{--<!--                --><?php //$products = explode(";", $transaction['description']); ?>--}}

                {{--<div class="body table-responsive">--}}
                    {{--<table class="table">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>#</th>--}}
                            {{--<th>Bank</th>--}}
                            {{--<th>Saldo</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--<?php $i = 0?>--}}
                        {{--@foreach($user->Banks()->get() as $bank)--}}
                            {{--<?php $i++;?>--}}
                            {{--<tr>--}}
                                {{--<td>{{$i}} </td>--}}
                                {{--<td><a href="{{URL::Route('admin.banks.detail', $bank['id'])}}">--}}
                                        {{--{{\App\Banks::Nama($bank['bank'])}}--}}
                                    {{--</a></td>--}}
                                {{--<td> {{$bank['balance']}}</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}


                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}




{{--@endsection--}}