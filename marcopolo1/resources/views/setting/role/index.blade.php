@extends('layout')

@section('menu12')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
    Access Role | Setting
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Setting</li>
                    <li><a href="{{url('/setting/role')}}">Access Role</a></li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>Access Role</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <a href="{{URL::Route('setting.role.tambah')}}">
                                            <input type="button" class="btn btn-primary btn-block" id="add" value="Tambah Role">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Deskripsi</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($role as $data)
                                        <?php $i++ ?>
                                        <tr>
                                            <td class="no align-center">{{ $data['id']}}</td>
                                            <td class="align-center">{{$data['name']}}</td>
                                            <td class="align-center">{{$data['description']}}</td>
                                            <td class="align-center"><a href="{{URL::Route('setting.role.detail', $data['id'])}}"><button type="button" class="btn btn-sm btn-success">Detail</button></a>
                                                <a href="{{URL::Route('setting.role.edit', $data['id'])}}"><button type="button" class="btn btn-sm btn-warning">Update</button></a>
                                                <a href="{{URL::Route('setting.role.hapus', $data['id'])}}"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a>
                                            </td>
                                            {{--<td><a href="{{URL::Route('hapusberkas', $bks['no_file'])}}"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a></td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop


