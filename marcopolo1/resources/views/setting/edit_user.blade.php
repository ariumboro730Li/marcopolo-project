@extends('layout')

@section('title')
Edit User
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/setting/user')}}">User</a></li>
                    <li class="active">Edit User</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>EDIT USER</h2>
                                </div>
                            </div>
                        </div>
                          
                        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="body">                 

                            <div class="row clearfix">           
                                <div class="col-md-12">                            
                                 
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 form-control-label">Full Name</label>
                                            <div class="col-md-9 col-xs-12">  
                                                <div class="form-line form-float">          
                                                    <input type="text" name="full_name" class="form-control" value="Deisna Rahmaningtyas"/>
                                                    <label class="form-label">Full Name</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 form-control-label">Position</label>
                                            <div class="col-md-9 col-xs-12">
                                                <div class="form-line form-float">
                                                    <input type="text" name="position" class="form-control" value="Admin" />
                                                    <label class="form-label">Position</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 form-control-label">Email</label>
                                            <div class="col-md-9 col-xs-12">            
                                                <div class="form-line form-float">
                                                    <input type="email" name="email" class="form-control" value="deisna@telkom.com"/>
                                                    <label class="form-label">Email</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 form-control-label">Phone Number</label>
                                            <div class="col-md-9 col-xs-12">
                                                <div class="form-line form-float">                  
                                                    <input type="phone" class="form-control" name="phone" value="01234567890" />
                                                    <label class="form-label">Phone Number</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 form-control-label">Password</label>
                                            <div class="col-md-9 col-xs-12">
                                                <div class="form-line form-float">
                                                    <input type="password" class="form-control" name ="password" id="password" value="asdasdasd" />
                                                    <label class="form-label">Password</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-xs-12 form-control-label">Photo Profile</label>
                                            <div class="col-md-9 col-xs-12">  
                                                <input type="file" class="fileinput" name="image_file" id="image_file" title="Browse file..."/>
                                                <span class="help-block">Image size: 200x200px</span> 
                                            </div>
                                        </div>

                                    </div>
                                
                                </div>

                            </div>

                        </div>                                
                        <div class="panel-footer">      
                            <input type="submit" class="btn btn-primary pull-right" value="Submit">
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/setting/user')}}'">Back</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
@stop                