﻿// $(function () {
//     new Chart(document.getElementById("line_chart_acc").getContext("2d"), getChartJsAcc('line'));
//     new Chart(document.getElementById("bar_chart_acc").getContext("2d"), getChartJsAcc('bar'));
//     new Chart(document.getElementById("donut_chart_acc").getContext("2d"), getChartJsAcc('donut'));
//     new Chart(document.getElementById("donut_chart2_acc").getContext("2d"), getChartJsAcc('donut2'));
//     new Chart(document.getElementById("line_chart_bh").getContext("2d"), getChartJsBh('line'));
//     new Chart(document.getElementById("bar_chart_bh").getContext("2d"), getChartJsBh('bar'));
//     new Chart(document.getElementById("donut_chart_bh").getContext("2d"), getChartJsBh('donut'));
//     new Chart(document.getElementById("donut_chart2_bh").getContext("2d"), getChartJsBh('donut2'));
//     new Chart(document.getElementById("line_chart_ipran").getContext("2d"), getChartJsIpran('line'));
//     new Chart(document.getElementById("bar_chart_ipran").getContext("2d"), getChartJsIpran('bar'));
//     new Chart(document.getElementById("donut_chart_ipran").getContext("2d"), getChartJsIpran('donut'));
//     new Chart(document.getElementById("donut_chart2_ipran").getContext("2d"), getChartJsIpran('donut2'));
//     new Chart(document.getElementById("line_chart_ipbb").getContext("2d"), getChartJsIpbb('line'));
//     new Chart(document.getElementById("bar_chart_ipbb").getContext("2d"), getChartJsIpbb('bar'));
//     new Chart(document.getElementById("donut_chart_ipbb").getContext("2d"), getChartJsIpbb('donut'));
//     new Chart(document.getElementById("donut_chart2_ipbb").getContext("2d"), getChartJsIpbb('donut2'));
//     new Chart(document.getElementById("line_chart_ix").getContext("2d"), getChartJsIx('line'));
//     new Chart(document.getElementById("bar_chart_ix").getContext("2d"), getChartJsIx('bar'));
//     new Chart(document.getElementById("donut_chart_ix").getContext("2d"), getChartJsIx('donut'));
//     new Chart(document.getElementById("donut_chart2_ix").getContext("2d"), getChartJsIx('donut2'));
// });
//
// function getChartJsAcc(type) {
//     var config = null;
//
//     var bulan = ["January", "February", "March", "April", "May"];
//     /*var data_perbulan = <?php echo $summary; ?>;*/
//
//     if (type === 'line') {
//         config = {
//             type: 'line',
//             data: {
//                 labels: bulan,
//                 datasets: [{
//                     label: "Regional 1",
//                     data: [65, 59, 80, 81, 56],
//                     borderColor: "rgba(0, 188, 212, 1)",
//                     backgroundColor: "rgba(0, 188, 212, 0.5)",
//                     pointBorderColor: "#fff",
//                     pointBackgroundColor: "rgba(0, 188, 212, 0.9)"  ,
//                     pointBorderWidth: 1,
//                     datalabels: {
//                         align: 'start',
//                         anchor: 'start'
//                     }
//                 }/*,
//                 {
//                     label: "Regional 2",
//                     data: [65,59,90,81,56],
//                     backgroundColor: "rgba(220,220,220,0.5)",
//                     borderColor: "rgba(220,220,220,1)",
//                     pointBackgroundColor: "rgba(220,220,220,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//
//                 },
//                 {
//                     label: "Regional 3",
//                     data : [50,68,17,57,24],
//                     backgroundColor: "rgba(200,147,165,0.5)",
//                     borderColor: "rgba(151,187,205,1)",
//                     pointBackgroundColor: "rgba(151,187,205,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false,
//                 plugins: {
//                     datalabels: {
//                         backgroundColor: function(context) {
//                             return context.dataset.backgroundColor;
//                         },
//                         borderRadius: 4,
//                         color: 'white',
//                         formatter: Math.round
//                     }
//                 },
//                 scales: {
//                     yAxes: [{
//                         stacked: true
//                     }]
//                 }
//             }
//         }
//     }
//     else if (type === 'bar') {
//         config = {
//             type: 'bar',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
//                 datasets: [{
//                     label: "January",
//                     data: [65, 59, 80, 81, 56, 55, 53],
//                     backgroundColor: "rgba(0, 188, 212, 0.8)",
//                 }/*,
//                 {
//                     label: "February",
//                     data: [65,59,90,81,56,55,45],
//                     backgroundColor: "rgba(220,220,220,0.8)",
//                     borderColor: "rgba(220,220,220, 1)"
//                 },
//                 {
//                     label: "March",
//                     data: [50,68,17,57,24,96,100],
//                     backgroundColor: "rgba(200,147,165,0.8)",
//                     borderColor: "rgba(200,147,165,1)"
//                 }*/]
//             },
//             options: {
//                 responsive : true,
//                 legend: false,
//                 plugins: {
//                     datalabels: {
//                         align: 'end',
//                         anchor: 'end',
//                         backgroundColor: function(context) {
//                             return context.dataset.backgroundColor;
//                         },
//                         borderRadius: 4,
//                         color: 'white',
//                         font: function(context) {
//                             var w = context.chart.width;
//                             return {
//                                 size: w < 512 ? 12 : 14
//                             }
//                         },
//                         formatter: function(value, context) {
//                             return context.chart.data.labels[context.data];
//                         }
//                     }
//                 },
//                 scales: {
//                     xAxes: [{
//                         display: true,
//                         offset: true
//                     }],
//                     yAxes: [{
//                         ticks: {
//                             beginAtZero: true
//                         }
//                     }]
//                 }
//                 /*plugins: {
//                     datalabels: {
//                         color: 'white',
//                         display: function(context) {
//                             return context.dataset.data[context.dataIndex] > 15;
//                         },
//                         font: {
//                             weight: 'bold'
//                         },
//                         formatter: Math.round
//                     }
//                 },
//                 scales: {
//                     xAxes: [{
//                         stacked: true
//                     }],
//                     yAxes: [{
//                         stacked: true
//                     }]
//                 }*/
//             }
//         }
//     }
//     else if (type === 'donut') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4"],
//                 datasets: [{
//                     data: [23, 28, 21, 28],
//                     backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
//                 }],
//                 datalabels: {
//                     anchor: 'center',
//                     backgroundColor: null,
//                     borderWidth: 0
//                 }
//             },
//             options: {
//                 responsive: true,
//                 legend: {
//                     position: 'left'
//                 },
//                 plugins: {
//                     datalabels: {
//                         backgroundColor: function(context) {
//                             return context.dataset.backgroundColor;
//                         },
//                         borderColor: 'white',
//                         borderRadius: 25,
//                         borderWidth: 2,
//                         color: 'white',
//                         display: function(context) {
//                             var dataset = context.dataset;
//                             var count = dataset.data.length;
//                             var value = dataset.data[context.dataIndex];
//                             return value > count * 1.5;
//                         },
//                         font: {
//                             weight: 'bold'
//                         },
//                         formatter: Math.round
//                     }
//                 }
//             }
//         }
//     }
//     else if (type === 'donut2') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4"],
//                 datasets: [{
//                     data: [23, 28, 21, 28]
//                 }],
//                 datalabels: {
//                     anchor: 'start'
//                 }
//             },
//             options: {
//                 responsive: true,
//                 legend: {
//                     position: 'bottom'
//                 },
//                 plugins: {
//                     datalabels: {
//                         backgroundColor: function(context) {
//                             return context.dataset.backgroundColor;
//                         },
//                         borderColor: 'white',
//                         borderRadius: 25,
//                         borderWidth: 2,
//                         color: 'white',
//                         display: function(context) {
//                             var dataset = context.dataset;
//                             var count = dataset.data.length;
//                             var value = dataset.data[context.dataIndex];
//                             return value > count * 1.5;
//                         },
//                         font: {
//                             weight: 'bold'
//                         },
//                         formatter: Math.round
//                     }
//                 }
//                 /*,
//                 title: {
//                     display: true,
//                     text: 'Doughnut Chart 2'
//                 },
//                 animation: {
//                     animateScale: true,
//                     animateRotate: true
//                 }*/
//             }
//         }
//     }
//     return config;
// }
//
// function getChartJsBh(type) {
//     var config = null;
//
//     if (type === 'line') {
//         config = {
//             type: 'line',
//             data: {
//                 labels: ["January", "February", "March", "April", "May"],
//                 datasets: [{
//                     label: "Regional 1",
//                     data: [28,48,40,19,96],
//                     borderColor: "rgba(0, 188, 212, 1)",
//                     backgroundColor: "rgba(0, 188, 212, 0.5)",
//                     pointBorderColor: "#fff",
//                     pointBackgroundColor: "rgba(0, 188, 212, 0.9)"  ,
//                     pointBorderWidth: 1
//                 }/*,
//                 {
//                     label: "Regional 2",
//                     data: [65,59,90,81,56],
//                     backgroundColor: "rgba(220,220,220,0.5)",
//                     borderColor: "rgba(220,220,220,1)",
//                     pointBackgroundColor: "rgba(220,220,220,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//
//                 },
//                 {
//                     label: "Regional 3",
//                     data : [50,68,17,57,24],
//                     backgroundColor: "rgba(200,147,165,0.5)",
//                     borderColor: "rgba(151,187,205,1)",
//                     pointBackgroundColor: "rgba(151,187,205,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'bar') {
//         config = {
//             type: 'bar',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
//                 datasets: [{
//                     label: "January",
//                     data: [65, 59, 80, 81, 56, 55, 53],
//                     backgroundColor: "rgba(0, 188, 212, 0.8)",
//                 }/*,
//                 {
//                     label: "February",
//                     data: [65,59,90,81,56,55,45],
//                     backgroundColor: "rgba(220,220,220,0.8)",
//                     borderColor: "rgba(220,220,220, 1)"
//                 },
//                 {
//                     label: "March",
//                     data: [50,68,17,57,24,96,100],
//                     backgroundColor: "rgba(200,147,165,0.8)",
//                     borderColor: "rgba(200,147,165,1)"
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81],
//                     backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)']
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut2') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81]
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     return config;
// }
//
// function getChartJsIpran(type) {
//     var config = null;
//
//     if (type === 'line') {
//         config = {
//             type: 'line',
//             data: {
//                 labels: ["January", "February", "March", "April", "May"],
//                 datasets: [{
//                     label: "Regional 1",
//                     data: [28,48,40,19,96],
//                     borderColor: "rgba(0, 188, 212, 1)",
//                     backgroundColor: "rgba(0, 188, 212, 0.5)",
//                     pointBorderColor: "#fff",
//                     pointBackgroundColor: "rgba(0, 188, 212, 0.9)"  ,
//                     pointBorderWidth: 1
//                 }/*,
//                 {
//                     label: "Regional 2",
//                     data: [65,59,90,81,56],
//                     backgroundColor: "rgba(220,220,220,0.5)",
//                     borderColor: "rgba(220,220,220,1)",
//                     pointBackgroundColor: "rgba(220,220,220,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//
//                 },
//                 {
//                     label: "Regional 3",
//                     data : [50,68,17,57,24],
//                     backgroundColor: "rgba(200,147,165,0.5)",
//                     borderColor: "rgba(151,187,205,1)",
//                     pointBackgroundColor: "rgba(151,187,205,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'bar') {
//         config = {
//             type: 'bar',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
//                 datasets: [{
//                     label: "January",
//                     data: [65, 59, 80, 81, 56, 55, 53],
//                     backgroundColor: "rgba(0, 188, 212, 0.8)",
//                 }/*,
//                 {
//                     label: "February",
//                     data: [65,59,90,81,56,55,45],
//                     backgroundColor: "rgba(220,220,220,0.8)",
//                     borderColor: "rgba(220,220,220, 1)"
//                 },
//                 {
//                     label: "March",
//                     data: [50,68,17,57,24,96,100],
//                     backgroundColor: "rgba(200,147,165,0.8)",
//                     borderColor: "rgba(200,147,165,1)"
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81],
//                     backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)']
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut2') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81]
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     return config;
// }
// function getChartJsIpbb(type) {
//     var config = null;
//
//     if (type === 'line') {
//         config = {
//             type: 'line',
//             data: {
//                 labels: ["January", "February", "March", "April", "May"],
//                 datasets: [{
//                     label: "Regional 1",
//                     data: [28,48,40,19,96],
//                     borderColor: "rgba(0, 188, 212, 1)",
//                     backgroundColor: "rgba(0, 188, 212, 0.5)",
//                     pointBorderColor: "#fff",
//                     pointBackgroundColor: "rgba(0, 188, 212, 0.9)"  ,
//                     pointBorderWidth: 1
//                 }/*,
//                 {
//                     label: "Regional 2",
//                     data: [65,59,90,81,56],
//                     backgroundColor: "rgba(220,220,220,0.5)",
//                     borderColor: "rgba(220,220,220,1)",
//                     pointBackgroundColor: "rgba(220,220,220,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//
//                 },
//                 {
//                     label: "Regional 3",
//                     data : [50,68,17,57,24],
//                     backgroundColor: "rgba(200,147,165,0.5)",
//                     borderColor: "rgba(151,187,205,1)",
//                     pointBackgroundColor: "rgba(151,187,205,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'bar') {
//         config = {
//             type: 'bar',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
//                 datasets: [{
//                     label: "January",
//                     data: [65, 59, 80, 81, 56, 55, 53],
//                     backgroundColor: "rgba(0, 188, 212, 0.8)",
//                 }/*,
//                 {
//                     label: "February",
//                     data: [65,59,90,81,56,55,45],
//                     backgroundColor: "rgba(220,220,220,0.8)",
//                     borderColor: "rgba(220,220,220, 1)"
//                 },
//                 {
//                     label: "March",
//                     data: [50,68,17,57,24,96,100],
//                     backgroundColor: "rgba(200,147,165,0.8)",
//                     borderColor: "rgba(200,147,165,1)"
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81],
//                     backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)']
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut2') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81]
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     return config;
// }
// function getChartJsIx(type) {
//     var config = null;
//
//     if (type === 'line') {
//         config = {
//             type: 'line',
//             data: {
//                 labels: ["January", "February", "March", "April", "May"],
//                 datasets: [{
//                     label: "Regional 1",
//                     data: [28,48,40,19,96],
//                     borderColor: "rgba(0, 188, 212, 1)",
//                     backgroundColor: "rgba(0, 188, 212, 0.5)",
//                     pointBorderColor: "#fff",
//                     pointBackgroundColor: "rgba(0, 188, 212, 0.9)"  ,
//                     pointBorderWidth: 1
//                 }/*,
//                 {
//                     label: "Regional 2",
//                     data: [65,59,90,81,56],
//                     backgroundColor: "rgba(220,220,220,0.5)",
//                     borderColor: "rgba(220,220,220,1)",
//                     pointBackgroundColor: "rgba(220,220,220,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//
//                 },
//                 {
//                     label: "Regional 3",
//                     data : [50,68,17,57,24],
//                     backgroundColor: "rgba(200,147,165,0.5)",
//                     borderColor: "rgba(151,187,205,1)",
//                     pointBackgroundColor: "rgba(151,187,205,1)",
//                     pointBorderColor: "#fff",
//                     pointBorderWidth: 1
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'bar') {
//         config = {
//             type: 'bar',
//             data: {
//                 labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
//                 datasets: [{
//                     label: "January",
//                     data: [65, 59, 80, 81, 56, 55, 53],
//                     backgroundColor: "rgba(0, 188, 212, 0.8)",
//                 }/*,
//                 {
//                     label: "February",
//                     data: [65,59,90,81,56,55,45],
//                     backgroundColor: "rgba(220,220,220,0.8)",
//                     borderColor: "rgba(220,220,220, 1)"
//                 },
//                 {
//                     label: "March",
//                     data: [50,68,17,57,24,96,100],
//                     backgroundColor: "rgba(200,147,165,0.8)",
//                     borderColor: "rgba(200,147,165,1)"
//                 }*/]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81],
//                     backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)']
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     else if (type === 'donut2') {
//         config = {
//             type: 'doughnut',
//             data: {
//                 labels: ["1", "2", "3", "4"],
//                 datasets: [{
//                     data: [65, 59, 80, 81]
//                 }]
//             },
//             options: {
//                 responsive: true,
//                 legend: false
//             }
//         }
//     }
//     return config;
// }
//
// /*Chart.pluginService.register({
//   beforeDraw: function(chart) {
//     var width = chart.chart.width,
//         height = chart.chart.height,
//         ctx = chart.chart.ctx;
//
//     ctx.restore();
//     var fontSize = (height / 114).toFixed(2);
//     ctx.font = fontSize + "em sans-serif";
//     ctx.textBaseline = "middle";
//
//     var text = "75%",
//         textX = Math.round((width - ctx.measureText(text).width) / 2),
//         textY = height / 2;
//
//     ctx.fillText(text, textX, textY);
//     ctx.save();
//   }
// });*/