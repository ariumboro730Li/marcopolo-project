/*importScripts('/cache-polyfill.js');*/

var filesToCache = [];

self.addEventListener('install', function(e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
        caches.open('oso-pwa').then(function(cache) {
            return cache.addAll(/*[
                '/',
                '/rca',
                '/crq',
                '/inventory'
            ]*/filesToCache);
        })
    );
});

self.addEventListener('fetch', function(event) {

    console.log(event.request.url);

    event.respondWith(

        caches.match(event.request).then(function(response) {

            return response || fetch(event.request);

        })

    );

});