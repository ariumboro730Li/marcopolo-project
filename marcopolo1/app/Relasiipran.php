<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Relasiipran extends Model
{
    // use SoftDeletes;
    protected $table = 'transport_ipran';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id_ipran', 'row', 'status', 'ne_transport', 'shelf_slot_port', 'tie_line', 'sto'
    ];

    
    public static function NeTransportCount($id)
    {
        $Relasiipran =  Relasiipran::where('id_ipran',$id)->count();
        if(isset($Relasiipran))
        {
            // return $Relasiipran->ne_transport;
            return $Relasiipran;
        }
        else{
            return " ";
        }

    }

    public static function BoardTransportCount($id)
    {
        $Relasiipran =  Relasiipran::where('id_ipran',$id)->count();
        if(isset($Relasiipran))
        {
            // return $Relasiipran->ne_transport;
            return $Relasiipran;
        }
        else{
            return " ";
        }

    }

}
