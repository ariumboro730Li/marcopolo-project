<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataJakabaringOcc extends Model
{
    protected $table = 'data_jakabaring_occ';
	protected $primaryKey = 'id';

    protected $fillable = [
        "witel", "href", "ifname", "in_max", "in_avg", "in_current", "out_max", "out_avg", "out_current"
        // "witel", "ref", "max_in", "max_out", "avg_in", "avg_out", "cur_in", "cur_out"
    ];
}
