<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataJakabaring extends Model
{
    protected $table = 'data_jakabaring';
	protected $primaryKey = 'id';

    protected $fillable = [
        'witel', 'region', 'data_bb', 'href', 'treg',
        // 'witel', 'region', 'treg',
    ];
}
