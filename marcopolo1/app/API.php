<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class API extends Model
{
    protected $table = 'apilink';
    protected $primaryKey = 'id';
}
