<?php

namespace App\Exports;

use App\DatekAccess;
use App\DatekBackhaul;
use App\DatekIpbb;
use App\DatekIpran;
use App\DatekIx;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InventoryExport implements FromView
{
    public function __construct(string $layer,int $regional)
    {
        $this->layer = $layer;
        $this->regional = $regional;
    }

    public function view(): View
    {
        if($this->layer=="access")
        {
            if($this->regional==0)
            {
                return view('modul_inventory.access_export', [
                    'access' => DatekAccess::all()
                ]);
            }
            else if($this->regional==8)
            {
                return view('modul_inventory.access_export', [
                    'access' => DatekAccess::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get()
                ]);
            }
            else{
                return view('modul_inventory.access_export', [
                    'access' => DatekAccess::where('reg_telkom',$this->regional)->get()
                ]);
            }
        }
        else if($this->layer=="backhaul")
        {
            if($this->regional==0) {
                return view('modul_inventory.backhaul_export', [
                    'backhaul' => DatekBackhaul::all()
                ]);
            }
            else if($this->regional==8)
            {
                return view('modul_inventory.backhaul_export', [
                    'backhaul' => DatekBackhaul::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get()
                ]);
            }
            else{
                return view('modul_inventory.backhaul_export', [
                    'backhaul' => DatekBackhaul::where('reg_telkom',$this->regional)->get()
                ]);
            }
        }
        else if($this->layer=="OneIPMPLS")
        {
            if($this->regional==0) {
                return view('modul_inventory.OneIPMPLS_export', [
                    'OneIPMPLS' => DatekBackhaul::all()
                ]);
            }
            else if($this->regional==8)
            {
                return view('modul_inventory.OneIPMPLS_export', [
                    'OneIPMPLS' => DatekBackhaul::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get()
                ]);
            }
            else{
                return view('modul_inventory.OneIPMPLS_export', [
                    'OneIPMPLS' => DatekBackhaul::where('reg_telkom',$this->regional)->get()
                ]);
            }
        }
        else if($this->layer=="ipbb")
        {
            if($this->regional==0) {
                return view('modul_inventory.ipbb_export', [
                    'ipbb' => DatekIpbb::all()
                ]);
            }
            else if($this->regional==8)
            {
                return view('modul_inventory.ipbb_export', [
                    'ipbb' => DatekIpbb::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get()
                ]);
            }
            else{
                return view('modul_inventory.ipbb_export', [
                    'ipbb' => DatekIpbb::where('reg_telkom',$this->regional)->get()
                ]);
            }
        }

        else if($this->layer=="ipran")
        {
            if($this->regional==0) {
                return view('modul_inventory.ipran_export', [
                    'ipran' => DatekIpran::all()
                ]);
            }
            else if($this->regional==8)
            {
                return view('modul_inventory.ipran_export', [
                    'ipran' => DatekIpran::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get()
                ]);
            }
            else{
                return view('modul_inventory.ipran_export', [
                    'ipran' => DatekIpran::where('reg_telkom',$this->regional)->get()
                ]);
            }
        }
        else if($this->layer=="ix")
        {
            if($this->regional==0) {
                return view('modul_inventory.ix_export', [
                    'ix' => DatekIx::all()
                ]);
            }
            else if($this->regional==8)
            {
                return view('modul_inventory.ix_export', [
                    'ix' => DatekIx::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get()
                ]);
            }
            else{
                return view('modul_inventory.ix_export', [
                    'ix' => DatekIx::where('reg_telkom',$this->regional)->get()
                ]);
            }
        }

    }
}

//implements FromCollection
//{
//    public function collection()
//    {
//        return DatekIx::all();
//    }
//}

