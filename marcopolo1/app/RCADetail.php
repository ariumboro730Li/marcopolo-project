<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RCADetail extends Model
{
    protected $table = 'rca_detail';
    protected $primaryKey = 'iddetail';

    protected $fillable = [
        'iddetail', 'idrca','improvement_activity','status','tgl_target','pic'
    ];

    public function title()
    {
        return $this->hasOne('App\RCA','idrca','idrca');
    }

    public static function getRCADetailData($idactivity)
    {
        return RCADetail::where('idrca',$idactivity)->get();
    }
}
