<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatekIpran extends Model
{
    use SoftDeletes;
    protected $table = 'datek_ipran';
	protected $primaryKey = 'idipran';

    protected $fillable = [
        'reg_tsel','router_end1_tsel','port_end1_tsel','router_end2_tsel','port_end2_tsel','reg_telkom','sto_end1_telkom','metroe_end1_telkom','port_end1_telkom','sto_end2_telkom','metroe_end2_telkom','port_end2_telkom','description','capacity','layer','system','ne_transport_a','board_transport_a','shelf_transport_a','slot_transport_a','port_transport_a','frek_transport_a','ne_transport_b','board_transport_b','shelf_transport_b','slot_transport_b','port_transport_b','frek_transport_b'
    ];

    public static function Link($regional)
    {
        return DatekIpran::where('reg_telkom',$regional)->whereNull('date_undetected')->count();
    }
    public static function LinkWithUndetected($regional)
    {
        return DatekIpran::where('reg_telkom',$regional)->count();
    }
    public static function Capacity($regional)
    {
        return DatekIpran::where('reg_telkom',$regional)->whereNull('date_undetected')->sum('capacity');
    }
    public static function CapacityWithUndetected($regional)
    {
        return DatekIpran::where('reg_telkom',$regional)->sum('capacity');
    }
    public static function AllLink()
    {
        return DatekIpran::whereNull('date_undetected')->count();
    }
    public static function AllLinkWithUndetected()
    {
        return DatekIpran::all()->count();
    }
    public static function AllCapacity()
    {
        return DatekIpran::whereNull('date_undetected')->sum('capacity');
    }
    public static function AllCapacityWithUndetected()
    {
        return DatekIpran::all()->sum('capacity');
    }
    public static function AllUtilization()
    {
        return DatekIpran::whereNull('date_undetected')->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function AllUtilizationCap()
    {
        return DatekIpran::whereNull('date_undetected')->whereNotNull('utilization_max')->sum('utilization_cap');
    }
    public static function Utilization($regional)
    {
        return DatekIpran::whereNull('date_undetected')->where('reg_telkom',$regional)->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function UtilizationCap($regional)
    {
        return DatekIpran::whereNull('date_undetected')->where('reg_telkom',$regional)->whereNotNull('utilization_max')->sum('utilization_cap');
    }
    public static function Utilization2()
    {
        return DatekIpran::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_max');
    }
    public static function UtilizationCap2()
    {
        return DatekIpran::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_cap');
    }
    public static function TransportSystem($system)
    {
        return DatekIpran::where('system',$system)->count();
    }
    public static function TransportType($system)
    {
        return DatekIpran::where('system',$system)->count();
    }

    public static function OverThreshold()
    {
        return DatekIpran::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->count();
    }

    public static function AllOverThreshold()
    {
        return DatekIpran::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get();
    }

    public static function FiveOverThreshold()
    {
        return DatekIpran::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->take(5)->get();
    }
}
