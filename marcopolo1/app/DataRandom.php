<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataRandom extends Model
{
    protected $table = 'data_random';
	protected $primaryKey = 'id';

    protected $fillable = [
        'witel', 'region', 'data_ran', 'href', 'treg',
        // 'witel', 'region', 'treg',
    ];
}
