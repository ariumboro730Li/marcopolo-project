<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewDatekOneip;
use App\DatekOneIPMPLS;
use App\Metro;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Redirect;


class OneipController extends Controller
{
    public function DetailOneIPMPLS($regional)
    {
        if($regional==0)
        {
            $ix = DatekOneIPMPLS::get();
        }
        else {
            $ix = DatekOneIPMPLS::where('reg_telkom', $regional)->get();
        }
        return view('modul_inventory.OneIPMPLS_detail')->with('OneIPMPLS',$ix)->with('regional',$regional);
    }

}
