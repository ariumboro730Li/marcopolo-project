<?php

namespace App\Http\Controllers;

use App\API;
use App\Status;
use App\User;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;


class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
            $this->middleware('auth');
        }
    
    public function status(Request $request)
    {
        $status = Status::all();

        return view('modul_status.status')->with('status',$status);
        // return view('modul_status.status');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */

    function http_request($url){
        // persiapkan curl
        $ch = curl_init(); 
    
        // set url 
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // set user agent    
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    
        // return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    
        // $output contains the output string 
        $output = curl_exec($ch); 
    
        // tutup curl 
        curl_close($ch);      
    
        // mengembalikan hasil curl
        return $output;
    }    

    function coba_curl() {

        // https://api.github.com/users/petanikode
        // $profile = $this->http_request("https://musikdewa.com/nasabah/data");
        // $profile = $this->http_request("https://api.github.com/users/petanikode");
        // $profile = $this->http_request("http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 1'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        // ubah string JSON menjadi array
        $profil = json_decode($profile, TRUE);
        
        // echo json_decode($profile, TRUE);


        // $data = array('node_id' => 1, 'avatar_url' => 2);
        // $input = Status::only($data);
        // $store = Status::create($input);

        echo "<pre>";
        print_r($profil);
        echo "</pre>";

        // $input = Status::all('node_id', 'avatar_url');
    }

    public function get_request() {
        $curl = curl_init();

        curl_setopt_array($curl, array(

        // CURLOPT_URL => "https://api.github.com/users/petanikode",
        CURLOPT_URL => "https://vishavjeet.com",

        CURLOPT_RETURNTRANSFER => true,

        CURLOPT_ENCODING => "",

        CURLOPT_TIMEOUT => 30000,

        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

        CURLOPT_CUSTOMREQUEST => "GET",

        CURLOPT_HTTPHEADER => array(

        // Set Here Your Requesred Headers

        'Content-Type: application/json',

        ),

        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

        echo "cURL Error #:" . $err;

        } else {

        print_r(json_decode($response));

        }
    }
        
        
    public function EditStatus($idstatus)
    {
        $status = Status::where('id',$idstatus)->first();
        $keterangan = "Edit";

        return view('modul_status.detail')->with('status',$status)->with('keterangan',$keterangan);
    }

    public function DetailStatus($idstatus)
    {
        $status = Status::where('id',$idstatus)->first();
        $keterangan = "Detail";
        return view('modul_status.detail')->with('status',$status)->with('keterangan',$keterangan);
    }

    public function UpdateStatus(Request $request)
    {
        $input = $request->all();
        $status = Status::where('id',$input['id'])->first();
        $status->status_name = $input['status_name'];
        $status->nilai = $input['nilai'];

        $status->save();

        return Redirect('status') ;

    }

    public function DetailRole($idStatus)
    {
        $role = Status::where('id',$idStatus)->first();
        $keterangan = "Detail";
        return view('setting.role.detail')->with('role',$role)->with('keterangan',$keterangan);
    }


    public function HapusStatus($idstatus)
    {

        Status::destroy($idstatus);
        return Redirect::back();
    }

    public function TambahStatus()
    {
        $keterangan = null;
        return view('modul_status.detail')->with('keterangan',$keterangan);
    }

    public function TambahStatusBaru(Request $request)
    {
        $input = $request->all();
        $status = new Status;
        $status->status_name = $input['status_name'];
        $status->nilai = $input['nilai'];
        $status->save();

        return Redirect('status') ;
    }

    public function SynchronizeOpenAPI()
    {
        $apis = API::all();
        $json = file_get_contents($apis);
        echo json_encode($json);

        foreach ($apis as $datum)
        {
            echo $datum->url."<br>";
            $url = $datum->url;
            $client = new Client();
            $res = $client->request('GET', $url, [
                'auth' => ['940156', 'No5423676;']
            ]);

            $area = json_decode($res->getBody(), true);
            foreach ($area['d']['results'] as $data) {
                $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
                $data['Name'] = str_replace("Ex","",$data['Name']);
                $data['Name'] = str_replace("Ag","",$data['Name']);
                $data['Name'] = str_replace("lag-","",$data['Name']);
                $data['Name'] = str_replace("GigabitEthernet","",$data['Name']);
                $data['Name'] = str_replace("Gi.","",$data['Name']);
                $data['Name'] = str_replace("Eth-Trunk","",$data['Name']);
                if(isset($openapi))
                {
                    $openapi['utilization'] = end($data['portmfs']['results'])['Value'];
                    // $openapi['layer']=$apis['layer'];
                    // $openapi['reg_tsel']=$data['reg_tsel'];
                    // $openapi['reg_telkom']=$data['reg_telkom'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." Update"."<br>";
                }
                else
                {

                    $openapi = new OpenAPI();
                    $openapi['name']=$data['device']['Name'];
                    $openapi['port']=$data['Name'];
                    // $openapi['layer']=$apis['layer'];
                    // $openapi['reg_tsel']=$data['reg_tsel'];
                    // $openapi['reg_telkom']=$data['reg_telkom'];

                    $openapi['utilization']=end($data['portmfs']['results'])['Value'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." New"."<br>";
                }

                echo $data['device']['Name']." ".$data['Name']." ";
                echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
            }
            echo "<br>";
        }


        $command = new CommandHistory();
        $command->name = "Get OpenAPI";
        $command->type = "GET";
        $command->save();
    }

    public static function oreo()
    {
        $ldap_username = Setting::where('key','ldap_id')->first()->value;
        $ldap_password = Setting::where('key','ldap_password')->first()->value;
        // REGIONAL1 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID,%20aggregate(portmfs(im_Utilization%20with%20max%20as%20Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower(%27telkomsel%20region:reg-01(sumbagut):backhaul%27),%20tolower(groups/GroupPathLocation))%20eq%20true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username, $ldap_password]
        ]);
            

        echo json_encode($res->getBody(), true);
            


    
    //     $area = json_decode($res->getBody(), true);
    //     foreach ($area['d']['results'] as $data)
    //     {
    //         $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
    //         if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
    //         {
    //             $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
    //             $backhaul->save();
    //         }
    // //
    // //            echo $data['device']['Name']." ";
    //            echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
        // }
    }
}
