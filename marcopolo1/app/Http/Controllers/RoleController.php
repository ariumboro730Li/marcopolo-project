<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RoleController extends Controller
{
    public function __construct()
{
        $this->middleware('auth');
    }

    public function Role(Request $request)
    {
//        $request->user()->authorizeRoles(['pengguna', 'admin']);
        $role = Role::all();

        return view('setting.role.index')->with('role',$role);
    }


    public function getRole()
    {
//        $request->user()->authorizeRoles(['pengguna', 'admin']);


        $user = User::with('roles')->where('ldap','940156')->get();

        dd($user[0]['roles'][0]['name']);

//
//        $role = Role::getRole();
//
//        dd($role);

    }

    public function HapusRole($idrole)
    {

        Role::destroy($idrole);

        return Redirect::back();
    }
    public function DetailRole($idrole)
    {
        $role = Role::where('id',$idrole)->first();
        $keterangan = "Detail";
        return view('setting.role.detail')->with('role',$role)->with('keterangan',$keterangan);
    }
    public function EditRole($idrole)
    {
        $role = Role::where('id',$idrole)->first();
        $keterangan = "Edit";

        return view('setting.role.detail')->with('role',$role)->with('keterangan',$keterangan);
    }
    public function UpdateRole(Request $request)
    {
        $input = $request->all();
        $role = Role::where('id',$input['id'])->first();
        $role->name = $input['name'];
        $role->description = $input['description'];
        if(isset($input['rca']))
            $role->rca = $input['rca'];
        else
            $role->rca = "off";

        if(isset($input['inventory']))
        $role->inventory = $input['inventory'];
        else
            $role->inventory = "off";

        if(isset($input['crq']))
        $role->crq = $input['crq'];
        else
            $role->crq = "off";

        if(isset($input['setting']))
        $role->setting = $input['setting'];
        else
            $role->setting = "off";

        $role->save();

        //return Redirect::back() ;
        return Redirect('setting/role') ;
    }
    public function TambahRole()
    {
        $keterangan = null;
        return view('setting.role.detail')->with('keterangan',$keterangan);
    }
    public function TambahRoleBaru(Request $request)
    {
        $input = $request->all();
        $role = new Role;
        $role->name = $input['name'];
        $role->description = $input['description'];
        $role->save();

        return Redirect('setting/role') ;
    }
}
