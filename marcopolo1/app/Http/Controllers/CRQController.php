<?php

namespace App\Http\Controllers;

use App\CRQ;
use App\CRQDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Validator;

class CRQController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $crq = CRQ::all()->sortByDesc("created_at");
        $crqtoday = CRQDetail::whereDate('tgl_mulai','<=', date("Y-m-d"))
            ->whereDate('tgl_selesai','>=', date("Y-m-d"))
            ->where('jam_selesai', '>=', (new \DateTime())->format('H:00'))
            ->orderBy("created_at", 'DESC')
            ->get();
        $crqupcoming = CRQDetail::whereDate('tgl_mulai','>', date("Y-m-d"))
            ->orderBy("created_at", 'DESC')
            ->get();
        return view('modul_crq.list_crq')
            ->with('crq',$crq)
            ->with('crqtoday',$crqtoday)
            ->with('crqupcoming',$crqupcoming);
    }

    public function HapusCRQ($idcrq)
    {
        CRQ::destroy($idcrq);

        return Redirect::back();
    }

    public function EditCRQ($idcrq)
    {
        $crq = CRQ::find($idcrq);
        $keterangan = "Edit";

        return view('modul_crq.edit_crq')->with('crq',$crq)->with('keterangan',$keterangan);
    }

    public function UpdateCRQ(Request $request)
    {
        $input           = $request->all();
        $crq             = CRQ::find($input['idcrq']);
        $crq->activity   = $input['activity'];
        $crq->reg_telkom = $input['reg_telkom'];
        $crq->reg_tsel   = $input['reg_tsel'];
        if($request->file('mop_file') == "")
        {
            $crq->mop_file = $crq->mop_file ;
        }
        else
        {
            $file       = $request->file('mop_file');
            $ext   = $file->getClientOriginalName();
            $request->file('mop_file')->move("images/", $input['id'].'_mop_file'.$ext);
            $crq->mop_file = $input['id'].'_mop_file'.$ext;
        }
        $crq->save();

        foreach ($request->input("addmore") as $key => $value) {
            $id = $value['iddetail'];
            $lokasi = $value['lokasi'];
            if ($lokasi == "") {
                # code...
            } else {
            //  $request->validate([
            //     'addmore.*.lokasi'      => 'required',
            //     'addmore.*.in_date'     => 'required',
            //     'addmore.*.end_date'    => 'required',
            //     'addmore.*.in_hour'     => 'required',
            //     'addmore.*.end_hour'    => 'required',
            //     'addmore.*.pic'         => 'required',
            //     'addmore.*.crq'         => 'required',
            //     'addmore.*.status'      => 'required',
            //     'addmore.*.keterangan'  => 'required',   
            // ]);

                if ($id == 0) {
                    # code...
                    $azz = [
                        "lokasi"        => $value['lokasi'],
                        "tgl_mulai"     => $value['in_date'],
                        "tgl_selesai"   => $value['end_date'],
                        "jam_mulai"     => $value['in_hour'],
                        "jam_selesai"   => $value['end_hour'],
                        "pic"           => $value['pic'],
                        "crq"           => $value['crq'],
                        "status"        => $value['status'],
                        "ket"           => $value['keterangan'],
                        'idactivity'    => $input['idcrq'],
                    ];
                    CRQDetail::create($azz);
                } else {
                    $azz = [
                        "lokasi"        => $value['lokasi'],
                        "tgl_mulai"     => $value['in_date'],
                        "tgl_selesai"   => $value['end_date'],
                        "jam_mulai"     => $value['in_hour'],
                        "jam_selesai"   => $value['end_hour'],
                        "pic"           => $value['pic'],
                        "crq"           => $value['crq'],
                        "status"        => $value['status'],
                        "ket"           => $value['keterangan'],
                        'idactivity'    => $input['idcrq'],
                    ];
                    CRQDetail::where("iddetail", $id)->update($azz);
                }
            }
            
            
        }
        

        return Redirect(URL::Route('crq.index'));
    }

    public function EditDetailCRQ($iddetail)
    {
        $crqdetail = CRQDetail::find($iddetail);
        $keterangan = "Edit";

        return view('modul_crq.edit_crq_detail')->with('crqdetail',$crqdetail)->with('keterangan',$keterangan);
    }
    public function UpdateDetailCRQ(Request $request)
    {
        $input = $request->all();
        $crq_detail = CRQDetail::find($input['iddetail']);
        $crq_detail->lokasi = $input['lokasi'];
        $crq_detail->tgl_mulai = $input['tgl_mulai'];
        $crq_detail->jam_mulai = $input['jam_mulai'];
        $crq_detail->tgl_selesai = $input['tgl_selesai'];
        $crq_detail->jam_selesai = $input['jam_selesai'];
        $crq_detail->pic = $input['pic'];
        $crq_detail->crq = $input['crq'];
        $crq_detail->status = $input['status'];
        $crq_detail->ket = $input['ket'];
        $crq_detail->save();

        return Redirect(URL::Route('crq.index'));
    }

    public function TambahCRQ()
    {
        $keterangan = null;

        return view('modul_crq.input_crq')->with('keterangan',$keterangan);
    }

    public function TambahCRQDetail($idactivity)
    {
        $keterangan = null;

        // return view('modul_crq.input_crq_detail')->with('idactivity',$idactivity);
    }
    public function TambahCRQBaru(Request $request)
    {
        $input = $request->all();
        $crq = new CRQ();

        $crq->activity = $input['activity'];
        $crq->reg_telkom = $input['reg_telkom'];
        $crq->reg_tsel = $input['reg_tsel'];
        if($request->file('mop_file') == "")
        {
            $crq->mop_file = $crq->mop_file ;
        }
        else
        {
            $file       = $request->file('mop_file');
            $ext   = $file->getClientOriginalName();
            $request->file('mop_file')->move("images/", $input['id'].'_mop_file'.$ext);
            $crq->mop_file = $input['id'].'_mop_file'.$ext;
        }
        $crq->save();

        $crq = CRQ::find($crq->idactivity);
        $keterangan = "Edit";


        // return view('modul_crq.edit_crq')->with('crq',$crq)->with('keterangan',$keterangan);
        return Redirect(URL::Route('crq.tambahlokasi',$crq->idactivity)) ;

    }

    public function TambahCRQBaruDetail(Request $request)
    {
        $input = $request->all();
        $crq_detail = new CRQDetail();

        $crq_detail->lokasi = $input['lokasi'];
        // $crq_detail->tgl_mulai = $input['tgl_mulai'];
        // $crq_detail->jam_mulai = $input['jam_mulai'];
        // $crq_detail->tgl_selesai = $input['tgl_selesai'];
        // $crq_detail->jam_selesai = $input['jam_selesai'];
        $crq_detail->pic = $input['pic'];
        $crq_detail->crq = $input['crq'];
        $crq_detail->status = $input['status'];
        $crq_detail->ket = $input['ket'];
        $crq_detail->idactivity = $input['idactivity'];

        $crq_detail->save();

        return Redirect(URL::Route('crq.index',$crq_detail->iddetail)) ;
    }

    public function HapusDetailCRQ($idcrqdetail)
    {
        CRQDetail::destroy($idcrqdetail);

        return Redirect::back();
    }


    public function addMore()
    {
        return view("addMore");
    }


    public function addMorePost(Request $request)
    {
        $validator = $request->validate([
            'addmore.*.name' => 'required',
            'addmore.*.qty' => 'required',
            'addmore.*.price' => 'required',
        ]);

        $messages = $validator->errors();
        $messages->add('My Custom Message Here');

        foreach ($request->input("addmore") as $key => $value) {
            $azz = [
                "lokasi" => $value['name'],
                "waktu_mulai" => $value['qty'],
                "waktu_selesai" => $value['price'],
            ];
            CRQDetail::create($azz);
        }
    
        return back()->with('success', 'Record Created Successfully.')->withErrors($messages);
    }
}
