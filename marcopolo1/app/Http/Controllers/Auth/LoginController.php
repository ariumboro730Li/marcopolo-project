<?php

namespace App\Http\Controllers\Auth;

use App\Classes\LDAP;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/inventory';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }

//    public function authenticate(Request $request)
//    {
//        dd($request);
////        $credentials = $request->only('email', 'password');
////
////        if (Auth::attempt($credentials)) {
////            // Authentication passed...
////            return redirect()->intended('dashboard');
////        }
//    }
//
//    public function login(Request $request)
//    {
//        $input = $request->all();
//        dd($input);
//    }

//    protected function validateLogin(Request $request)
//    {
//
//
//        $input = $request->all();
//        //dd($input['email']." ".$input['password']);
//        $ldap = new LDAP();
//        //$result = $ldap->auth("940156","No5423676..");
//        $result = $ldap->auth($input['ldap'],$input['password']);
//
//        if ($result==1){
//            $user = User::where('ldap',$input['ldap'])->first();
//            if (!isset($user))
//            {
//                $hasil =$ldap->info($input['ldap']);
//                $role = Role::where('name', 'Staf')->first();
//                $user = new User();
//                $user->ldap =  $input['ldap'];
//                $user->email =  $hasil[0]['mail'][0];
//                $user->password =  Hash::make($input['password']);
//                $user->name =  $hasil[0]['sn'][0];
//                $user->save();
//                $user->roles()->attach($role);
//
//            }
//            $this->validate($request, [
//                $this->username() => 'required|string',
//                'password' => 'required|string',
//            ]);
//        }
//        else
//        {
//            return false;
//        }
//
//
//
//    }

    public function login(Request $request)
    {
        $input = $request->all();
        //dd($input['email']." ".$input['password']);
        // $ldap = new LDAP();
        // //$result = $ldap->auth("940156","No5423676..");
        // $result = $ldap->auth($input['ldap'],$input['password']);

        // if ($result==1){
        //     $user = User::where('ldap',$input['ldap'])->first();
        //     if (!isset($user))
        //     {
        //         $hasil = $ldap->info($input['ldap']);
        //         $role = Role::where('name', 'Staf')->first();
        //         $user = new User();
        //         $user->ldap =  $input['ldap'];
        //         $user->email =  $hasil[0]['mail'][0];
        //         $user->password =  Hash::make($input['password']);
        //         $user->name =  $hasil[0]['sn'][0];
        //         $user->save();
        //         $user->roles()->attach($role);

        //     }
//            $this->validate($request, [
//                $this->username() => 'required|string',
//                'password' => 'required|string',
//            ]);
                    $user = User::where('ldap',$input['ldap'])->first();
        if ($user) {
            \Auth::login($user);
        }
        else
        {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => Lang::get('auth.failed'),
                ]);
        }

//        // Check validation
//        $this->validate($request, [
//            'mobile_no' => 'required|regex:/[0-9]{10}/|digits:10',
//        ]);
//
//        // Get user record
//        $user = User::where('mobile_no', $request->get('mobile_no'))->first();
//
//        // Check Condition Mobile No. Found or Not
//        if($request->get('mobile_no') != $user->mobile_no) {
//            \Session::put('errors', 'Your mobile number not match in our system..!!');
//            return back();
//        }

        // Set Auth Details


        // Redirect home page
        return redirect()->route('inventory.dashboard');
    }


    public function username()
    {
        return 'ldap';
    }

//    public function authenticate(Request $request)
//    {
//        $credentials = $request->only('email', 'password');
//
//        dd($credentials);
//
//        if (Auth::attempt($credentials)) {
//            // Authentication passed...
//            return redirect()->intended('dashboard');
//        }
//    }


}
