<?php

namespace App\Http\Controllers\AccessDirectController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AccessDirectController\ApiAccessDirectController;
use App\AccessDirectMetro;
use Illuminate\Support\Facades\Storage;


class GetAccessDirectController extends Controller
{

    public function __construct()
    {
        // parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->api = new ApiAccessDirectController();
    }

    public function index() {
        return view('modul_inventory.directmetro');    
    }


    public function getapiDirectMetroReg1($val)
    {
            // $val = '';
            $response = $this->api->apiDirectMetroReg1($val);
            $data = json_decode($response);
            try {
                \DB::beginTransaction();
                $dbase = AccessDirectMetro::count();
                $dbases = $dbase;
                if (!empty($data->d)) {
                    foreach($data->d->results as $key) {
                        foreach($key->portmfs->results as $row ) {
                            if ($row->max_im_Utilization == null) {

                                $site_id   = preg_match_all("/[A-Z]{3}[0-9]{3}/", $key->Description, $matches);
                                    foreach ($matches[0] as $data) {
                                        $cekId = AccessDirectMetro::where('siteid_tsel', $data)->where('port_dm_telkom', $key->Name)->count();
                                        // echo $data." - ".$key->Name." - ".$cekId."<br>";
                                        if ($cekId >= 1) {
                                            $cekupdate = AccessDirectMetro::where('siteid_tsel', $data)->where('port_dm_telkom', $key->Name)->first();
                                            $azz = [
                                                'utilization'    => $row->Value,
                                                'utilization_cap' => $key->SpeedIn * $row->Value,
                                                'capacity'        => $key->SpeedIn,  
                                                'updated_at'      => date("Y:m:d H:i:s"),
                                                'update_count'    => $cekupdate->update_count + 1,
                                            ];
                                            AccessDirectMetro::where("siteid_tsel", $data)->where('port_dm_telkom', $key->Name)->update($azz);

                                        } else {
                                            $variable = strstr($key->Description, $data, false);
                                            $var = substr($variable,7);
                                            $okelah = preg_match("/AKSES_/", $key->Description);
                                            if ($okelah) {
                                                $azz = [
                                                    'utilization'    => $row->Value,
                                                    'utilization_cap' => $key->SpeedIn * $row->Value,
                                                    'siteid_tsel'     => $data,
                                                    'sitename_tsel'   => $var,
                                                    'regional_telkom' => $val,
                                                    'me_dm_telkom'    => $key->device->Name,
                                                    'port_dm_telkom'  => $key->Name,
                                                    'capacity'        => $key->SpeedIn,  
                                                    'description'     => $key->Description,
                                                    'created_at'      => date("Y:m:d H:i:s"),
                                                    // 'updated_at'      => date("Y:m:d H:i:s"),
    
                                                ];
                
                                                $store = AccessDirectMetro::create($azz);
                                            } else {
                                                // echo "Tidak masuk - ";
                                            }
                                       
                                        } 
                                    }
                                }
                            }
                            
                        }
                        
                }
                
                $dbase2 =  AccessDirectMetro::count();
                $dbases2 = $dbase2;
                $dbases3 = $dbases2 - $dbases;
                $dbases4 = $dbases3 + $dbases;

                echo "Access Direct Metro - Regional ".$val."<br>";
                echo "Data lama : ".$dbases; echo "<br>";
                echo "Penambahan Data Baru : ".$dbases3; echo "<br>";
                echo "Total Data : ".$dbases4;

    
                \DB::commit();
            } catch(\Exception $e) {
                \DB::rollback();
                $error = $e->getMessage()." ".$e->getFile()." ".$e->getLine();    
                return $error;
            }
        }


        public function DetailDirect($regional)
        {
            if($regional==0)
            {
                $ix = AccessDirectMetro::get();
            }
            else {
                $ix = AccessDirectMetro::where('regional_telkom', $regional)->get();
            }
            return view('modul_inventory.directmetro')->with('direct',$ix)->with('regional',$regional);
        }

        public function test(){
            // $key = 'AKSES_ME-D1-BTGA/2/1/11_TO_NODE-B_TSEL_PBI001-NEW-BETUNG_eks_SKY003';
            // preg_match_all("/[A-Z]{3}[0-9]{3}/", $key, $matches);
            // preg_match("/[A-Z]{3}[0-9]{3}/", $key, $matchs);

            // print_r($matches);
            // echo "<br>";
            // print_r($matchs);

            // $contents = Storage::get('itungan Gila.xls');
            return Storage::download('filex');

        }

}
