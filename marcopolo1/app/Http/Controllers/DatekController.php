<?php

namespace App\Http\Controllers;

use App\API;
use App\Classes\LDAP;
use App\CommandHistory;
use App\DatekAccess;
use App\DatekBackhaul;
use App\DatekIpbb;
use App\NewDatekIpbb;
use App\NewDatekIpran;
use App\NewDatekIx;
use App\DatekIpran;
use App\DatekIx;
use App\DatekOneIPMPLS;
use App\DatekSummary;
use App\Metro;
use App\MetroLokasi;
use App\OpenAPI;
use App\Setting;
use App\UtilAccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use GuzzleHttp\Client;


class DatekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul_inventory.dashboard');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowBackhaul($id)
    {
        $backhaul = DatekBackhaul::find($id);
        return view('modul_inventory.edit_inventory')->with('layer','backhaul')->with('data',$backhaul);
    }

    public function ImportBackhaul()
    {

        return view('modul_inventory.import')->with('layer','backhaul');
    }

    public function PostImportBackhaul(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
            //$ip = ; // Ambil IP Address dari User
            $nama_file_baru = 'backhaul_'.Carbon::now()->format('d-m-Y').'.xlsx';

            // Cek apakah terdapat file data.xlsx pada folder tmp
            if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
                unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

            $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
            $tmp_file = $request->file('file')->getRealPath();

            // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
            //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                // Upload file yang dipilih ke folder tmp
                // dan rename file tersebut menjadi data{ip_address}.xlsx
                // {ip_address} diganti jadi ip address user yang ada di variabel $ip
                // Contoh nama file setelah di rename : data127.0.0.1.xlsx
                move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
                $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
                $sheetData = $spreadsheet->getActiveSheet()->toArray();
                //dd($sheetData);


        return view('modul_inventory.import')->with('layer','backhaul')->with('backhaul',$sheetData)->with('file',$nama_file_baru);
    }

    public function SaveImportBackhaul(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0]!='idbackhaul')
            {
                $datek = DatekBackhaul::find($data[0]);
                $datek->ne_transport_a=$data[11];
                $datek->board_transport_a=$data[12];
                $datek->shelf_transport_a=$data[13];
                $datek->slot_transport_a=$data[14];
                $datek->port_transport_a=$data[15];
                $datek->frek_transport_a=$data[16];
                $datek->ne_transport_b=$data[17];
                $datek->board_transport_b=$data[18];
                $datek->shelf_transport_b=$data[19];
                $datek->slot_transport_b=$data[20];
                $datek->port_transport_b=$data[21];
                $datek->frek_transport_b=$data[22];
                $datek->save();
            }
        }

        return Redirect(URL::Route('inventory.backhaul')) ;

    }

    public function ShowOneIPMPLS($id)
    {
        $backhaul = DatekOneIPMPLS::find($id);
        return view('modul_inventory.edit_inventory')->with('layer','OneIPMPLS')->with('data',$backhaul);
    }

    public function ImportOneIPMPLS()
    {

        return view('modul_inventory.import')->with('layer','OneIPMPLS');
    }

    public function PostImportOneIPMPLS(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'OneIPMPLS_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);


        return view('modul_inventory.import')->with('layer','OneIPMPLS')->with('OneIPMPLS',$sheetData)->with('file',$nama_file_baru);
    }

    public function SaveImportOneIPMPLS(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0]!='idOneIPMPLS')
            {
                $datek = DatekOneIPMPLS::find($data[0]);
                $datek->ne_transport_a=$data[11];
                $datek->board_transport_a=$data[12];
                $datek->shelf_transport_a=$data[13];
                $datek->slot_transport_a=$data[14];
                $datek->port_transport_a=$data[15];
                $datek->frek_transport_a=$data[16];
                $datek->ne_transport_b=$data[17];
                $datek->board_transport_b=$data[18];
                $datek->shelf_transport_b=$data[19];
                $datek->slot_transport_b=$data[20];
                $datek->port_transport_b=$data[21];
                $datek->frek_transport_b=$data[22];
                $datek->save();
            }
        }

        return Redirect(URL::Route('inventory.OneIPMPLS')) ;

    }


    public function ShowIpran($id)
    {
        $ipran = DatekIpran::find($id);
        return view('modul_inventory.edit_inventory')->with('layer','ipran')->with('data',$ipran);
    }

    public function ShowIpbb($id)
    {
        $ipbb = DatekIpbb::find($id);
        return view('modul_inventory.edit_ipbb')->with('layer','ipbb')->with('data',$ipbb);
    }

    public function ShowIx($id)
    {
        $ix = DatekIx::find($id);
        return view('modul_inventory.edit_ix')->with('layer','ix')->with('data',$ix);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        if($input['layer']=='backhaul')
        {
            $backhaul = DatekBackhaul::find($input['id']);
            $backhaul->ne_transport_a = $input['ne_transport_a'];
            $backhaul->board_transport_a = $input['board_transport_a'];
            $backhaul->shelf_transport_a = $input['shelf_transport_a'];
            $backhaul->slot_transport_a = $input['slot_transport_a'];
            $backhaul->port_transport_a = $input['port_transport_a'];
            $backhaul->frek_transport_a = $input['frek_transport_a'];
            $backhaul->ne_transport_b = $input['ne_transport_b'];
            $backhaul->board_transport_b = $input['board_transport_b'];
            $backhaul->shelf_transport_b = $input['shelf_transport_b'];
            $backhaul->slot_transport_b = $input['slot_transport_b'];
            $backhaul->port_transport_b = $input['port_transport_b'];
            $backhaul->frek_transport_b = $input['frek_transport_b'];
            $backhaul->save();
            return Redirect(URL::Route('inventory.backhaul.detail', $input['reg'])) ;
        }
        if($input['layer']=='OneIPMPLS')
        {
            $backhaul = DatekOneIPMPLS::find($input['id']);
            $backhaul->ne_transport_a = $input['ne_transport_a'];
            $backhaul->board_transport_a = $input['board_transport_a'];
            $backhaul->shelf_transport_a = $input['shelf_transport_a'];
            $backhaul->slot_transport_a = $input['slot_transport_a'];
            $backhaul->port_transport_a = $input['port_transport_a'];
            $backhaul->frek_transport_a = $input['frek_transport_a'];
            $backhaul->ne_transport_b = $input['ne_transport_b'];
            $backhaul->board_transport_b = $input['board_transport_b'];
            $backhaul->shelf_transport_b = $input['shelf_transport_b'];
            $backhaul->slot_transport_b = $input['slot_transport_b'];
            $backhaul->port_transport_b = $input['port_transport_b'];
            $backhaul->frek_transport_b = $input['frek_transport_b'];
            $backhaul->save();
            return Redirect(URL::Route('inventory.OneIPMPLS.detail', $input['reg'])) ;
        }
        else if($input['layer']=='ipran')
        {
            $ipran = DatekIpran::find($input['id']);
            $ipran->ne_transport_a = $input['ne_transport_a'];
            $ipran->board_transport_a = $input['board_transport_a'];
            $ipran->shelf_transport_a = $input['shelf_transport_a'];
            $ipran->slot_transport_a = $input['slot_transport_a'];
            $ipran->port_transport_a = $input['port_transport_a'];
            $ipran->frek_transport_a = $input['frek_transport_a'];
            $ipran->ne_transport_b = $input['ne_transport_b'];
            $ipran->board_transport_b = $input['board_transport_b'];
            $ipran->shelf_transport_b = $input['shelf_transport_b'];
            $ipran->slot_transport_b = $input['slot_transport_b'];
            $ipran->port_transport_b = $input['port_transport_b'];
            $ipran->frek_transport_b = $input['frek_transport_b'];
            $ipran->save();
            return Redirect(URL::Route('inventory.ipran.detail', $input['reg'])) ;
        }
        else if($input['layer']=='ipbb')
        {
            $ipbb = DatekIpbb::find($input['id']);
            $ipbb->ne_transport_a = $input['ne_transport_a'];
            $ipbb->board_transport_a = $input['board_transport_a'];
            $ipbb->shelf_transport_a = $input['shelf_transport_a'];
            $ipbb->slot_transport_a = $input['slot_transport_a'];
            $ipbb->port_transport_a = $input['port_transport_a'];
            $ipbb->frek_transport_a = $input['frek_transport_a'];
            $ipbb->ne_transport_b = $input['ne_transport_b'];
            $ipbb->board_transport_b = $input['board_transport_b'];
            $ipbb->shelf_transport_b = $input['shelf_transport_b'];
            $ipbb->slot_transport_b = $input['slot_transport_b'];
            $ipbb->port_transport_b = $input['port_transport_b'];
            $ipbb->frek_transport_b = $input['frek_transport_b'];
            $ipbb->save();
            return Redirect(URL::Route('inventory.ipbb.detail', $input['reg'])) ;
        }
        else if($input['layer']=='ix')
        {
            $ix = DatekIx::find($input['id']);
            $ix->ne_transport_a = $input['ne_transport_a'];
            $ix->board_transport_a = $input['board_transport_a'];
            $ix->shelf_transport_a = $input['shelf_transport_a'];
            $ix->slot_transport_a = $input['slot_transport_a'];
            $ix->port_transport_a = $input['port_transport_a'];
            $ix->frek_transport_a = $input['frek_transport_a'];
            $ix->ne_transport_b = $input['ne_transport_b'];
            $ix->board_transport_b = $input['board_transport_b'];
            $ix->shelf_transport_b = $input['shelf_transport_b'];
            $ix->slot_transport_b = $input['slot_transport_b'];
            $ix->port_transport_b = $input['port_transport_b'];
            $ix->frek_transport_b = $input['frek_transport_b'];
            $ix->save();
            return Redirect(URL::Route('inventory.ix.detail', $input['reg'])) ;
        }
        
    }

    public function updateAccess(Request $request)
    {
        $input = $request->all();
        $access = DatekAccess::find($input['id']);
        $access->reg_tsel = $input['reg_tsel'];
        $access->siteid_tsel = $input['siteid_tsel'];
        $access->sitename_tsel = $input['sitename_tsel'];
        $access->reg_telkom = $input['reg_telkom'];
        $access->capacity = $input['capacity'];
        $access->system = $input['system'];
        $access->save();
        return Redirect(URL::Route('inventory.access.detail', $input['reg_telkom'])) ;
    }

    public function updateIPBB(Request $request)
    {
        $input = $request->all();
        $ipbb = DatekIpbb::find($input['id']);
        $ipbb->reg_tsel = $input['reg_tsel'];
        $ipbb->router_node_a = $input['router_node_a'];
        $ipbb->port_end1_tsel = $input['port_end1_tsel'];
        $ipbb->router_node_b = $input['router_node_b'];
        $ipbb->port_end2_tsel = $input['port_end2_tsel'];
        $ipbb->reg_telkom = $input['reg_telkom'];
        $ipbb->capacity = $input['capacity'];
        $ipbb->system = $input['system'];

        $ipbb->ne_transport_a = $input['ne_transport_a'];
        $ipbb->board_transport_a = $input['board_transport_a'];
        $ipbb->shelf_transport_a = $input['shelf_transport_a'];
        $ipbb->slot_transport_a = $input['slot_transport_a'];
        $ipbb->port_transport_a = $input['port_transport_a'];
        $ipbb->frek_transport_a = $input['frek_transport_a'];
        $ipbb->ne_transport_b = $input['ne_transport_b'];
        $ipbb->board_transport_b = $input['board_transport_b'];
        $ipbb->shelf_transport_b = $input['shelf_transport_b'];
        $ipbb->slot_transport_b = $input['slot_transport_b'];
        $ipbb->port_transport_b = $input['port_transport_b'];
        $ipbb->frek_transport_b = $input['frek_transport_b'];
        $ipbb->save();
        return Redirect(URL::Route('inventory.ipbb.detail', $input['reg_telkom'])) ;
    }

    public function updateIX(Request $request)
    {
        $input = $request->all();
        $ix = DatekIx::find($input['id']);
        $ix->reg_tsel = $input['reg_tsel'];
        $ix->router_end1_tsel = $input['router_end1_tsel'];
        $ix->port_end1_tsel = $input['port_end1_tsel'];
        $ix->capacity = $input['capacity'];
        $ix->reg_telkom = $input['reg_telkom'];
        $ix->sto_end1_telkom = $input['sto_end1_telkom'];
        $ix->metroe_end1_telkom = $input['metroe_end1_telkom'];
        $ix->port_end1_telkom = $input['port_end1_telkom'];
        $ix->system = $input['system'];

        $ix->ne_transport_a = $input['ne_transport_a'];
        $ix->board_transport_a = $input['board_transport_a'];
        $ix->shelf_transport_a = $input['shelf_transport_a'];
        $ix->slot_transport_a = $input['slot_transport_a'];
        $ix->port_transport_a = $input['port_transport_a'];
        $ix->frek_transport_a = $input['frek_transport_a'];
        $ix->ne_transport_b = $input['ne_transport_b'];
        $ix->board_transport_b = $input['board_transport_b'];
        $ix->shelf_transport_b = $input['shelf_transport_b'];
        $ix->slot_transport_b = $input['slot_transport_b'];
        $ix->port_transport_b = $input['port_transport_b'];
        $ix->frek_transport_b = $input['frek_transport_b'];
        $ix->save();
        return Redirect(URL::Route('inventory.ix.detail', $input['reg_telkom'])) ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function Access()
    {
        return view('modul_inventory.access');
    }

    public function AccessDirectMetro()
    {
        return view('modul_inventory.accessDirectMetro');
    }

    public function ShowAccess($id)
    {
        $access = DatekAccess::find($id);
        return view('modul_inventory.edit_access')->with('layer','access')->with('data',$access);
    }

    public function DetailAccess($regional)
    {
        if($regional==0)
        {
            $access = DatekAccess::whereNull('date_undetected')->get();
        }
        else {
            $access = DatekAccess::where('reg_telkom', $regional)->get();
        }

        return view('modul_inventory.access_detail')->with('access',$access)->with('regional',$regional);
    }

    public function ImportAccess()
    {

        return view('modul_inventory.access_import')->with('layer','access');
    }

    public function PostImportAccess(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'access_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //  dd($sheetData);


        return view('modul_inventory.access_import')->with('layer','access')->with('access',$sheetData)->with('file',$nama_file_baru);
    }

    public function SaveImportAccess(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {

            if($data[1]!='siteid_tsel')
            {
                $datek = new DatekAccess();
                $datek->reg_tsel=$data[0];
                $datek->siteid_tsel=$data[1];
                $datek->sitename_tsel=$data[2];
                $datek->reg_telkom=$data[3];
                $datek->sto_gpon_telkom=$data[4];
                $datek->gpon_gpon_telkom=$data[5];
                $datek->port_gpon_telkom=$data[6];
                $datek->ne_radio_telkom=$data[7];
                $datek->fe_radio_telkom=$data[8];
                $datek->port_radio_telkom=$data[9];
                $datek->sto_dm_telkom=$data[10];
                $datek->me_dm_telkom=$data[11];
                $datek->port_dm_telkom=$data[12];
                $datek->capacity=$data[13];
                $datek->system=$data[14];
                // $datek->date_detected=$data[15];
                // $datek->date_undetected=$data[16];
                $datek->status=$data[17];
                $datek->save();
            }
        }

        return Redirect(URL::Route('inventory.access')) ;

    }

    public function DeleteAccess($id)
    {
        DatekAccess::destroy($id);

        return Redirect::back();
    }

    public function Backhaul()
    {
    
        // $date = date("Y:m:d 6:30:00");
        // $dates = date("Y:m:d H:i:s");
        // if ($date != $dates)  {
        //     $this->getBackhaul();
        // } else {
            
        // }

        return view('modul_inventory.backhaul');
    
    }

    public function DetailBackhaul($regional)
    {
        if($regional==0)
        {
            $ix = DatekBackhaul::whereNull('date_undetected')->get();
        }
        else {
            $ix = DatekBackhaul::where('reg_telkom', $regional)->get();
        }
        
        return view('modul_inventory.backhaul_detail')->with('backhaul',$ix)->with('regional',$regional);
    }

    public function DeleteBackhaul($id)
    {
        DatekBackhaul::destroy($id);

        return Redirect::back();
    }



    public function OneIPMPLS()
    {

        return view('modul_inventory.OneIPMPLS');
    }

    public function OneIPMPLSS()
    {

        return view('modul_inventory.OneIPMPLS');
    }

    // public function DetailOneIPMPLS($regional)
    // {
    //     if($regional==0)
    //     {
    //         $ix = DatekOneIPMPLS::whereNull('date_undetected')->get();
    //     }
    //     else {
    //         $ix = DatekOneIPMPLS::where('reg_telkom', $regional)->get();
    //     }
    //     return view('modul_inventory.OneIPMPLS_detail')->with('OneIPMPLS',$ix)->with('regional',$regional);
    // }

    public function DeleteOneIPMPLS($id)
    {
        DatekOneIPMPLS::destroy($id);

        return Redirect::back();
    }



    public function Ipran()
    {
        return view('modul_inventory.ipran');
    }

    public function ImportIpran()
    {

        return view('modul_inventory.ipran_import')->with('layer','ipran');
    }

    public function PostImportIpran(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'ipran_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);


        return view('modul_inventory.ipran_import')->with('layer','ipran')->with('ipran',$sheetData)->with('file',$nama_file_baru);
    }

    public function SaveImportIpran(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0]!='vcid')
            {
                $datek = new DatekIpran();
                $datek->vcid=$data[0];
                $datek->reg_tsel=$data[1];
                $datek->kota_router_end_1=$data[2];
                $datek->router_end1_tsel=$data[3];
                $datek->port_end1_tsel=$data[4];
                $datek->kota_router_end_2=$data[5];
                $datek->router_end2_tsel=$data[6];
                $datek->port_end2_tsel=$data[7];
                $datek->reg_telkom=$data[8];
                $datek->sto_end1_telkom=$data[9];
                $datek->metroe_end1_telkom=$data[10];
                $datek->port_end1_telkom=$data[11];
                $datek->sto_end2_telkom=$data[12];
                $datek->metroe_end2_telkom=$data[13];
                $datek->port_end2_telkom=$data[14];
                $datek->description=$data[15];
                $datek->capacity=$data[16];
                $datek->layer=$data[17];
                $datek->system=$data[18];
                $datek->transport_type=$data[19];
                $datek->ne_transport_a=$data[20];
                $datek->board_transport_a=$data[21];
                $datek->shelf_transport_a=$data[22];
                $datek->slot_transport_a=$data[23];
                $datek->port_transport_a=$data[24];
                $datek->frek_transport_a=$data[25];
                $datek->ne_transport_b=$data[26];
                $datek->board_transport_b=$data[27];
                $datek->shelf_transport_b=$data[28];
                $datek->slot_transport_b=$data[29];
                $datek->port_transport_b=$data[30];
                $datek->frek_transport_b=$data[31];
                $datek->date_detected=$data[32];
                $datek->date_undetected=$data[33];
                $datek->status=$data[34];
                $datek->utilization_max=$data[35];
                $datek->link=router_end1_tsel." to ".$datek->router_end2_tsel;
                $datek->save();
            }
        }

        return Redirect(URL::Route('inventory.ipran')) ;

    }

    public function DetailIpran($regional)
    {
        if($regional==0)
        {
            $ix = DatekIpran::whereNull('date_undetected')->get();
        }
        else {
            $ix = DatekIpran::where('reg_telkom', $regional)->get();
        }
        return view('modul_inventory.ipran_detail')->with('ipran',$ix)->with('regional',$regional);
    }

    public function DeleteIpran($id)
    {
        DatekIpran::destroy($id);

        return Redirect::back();
    }

    public function Ipbb()
    {
        return view('modul_inventory.ipbb');
    }

    public function ImportIpbb()
    {

        return view('modul_inventory.ipbb_import')->with('layer','ipbb');
    }


    public function PostImportIpbb(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'ipbb_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);


        return view('modul_inventory.ipbb_import')->with('layer','ipbb')->with('ipbb',$sheetData)->with('file',$nama_file_baru);
    }

    public function SaveImportIpbb(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0]!='reg_tsel')
            {
                $datek = new DatekIpbb();
                $datek->reg_tsel=$data[0];
                $datek->router_node_a=$data[1];
                $datek->port_end1_tsel=$data[2];
                $datek->router_node_b=$data[3];
                $datek->port_end2_tsel=$data[4];
                $datek->reg_telkom=$data[5];
                $datek->capacity=$data[6];
                $datek->layer=$data[7];
                $datek->system=$data[8];
                $datek->ne_transport_a=$data[9];
                $datek->board_transport_a=$data[10];
                $datek->shelf_transport_a=$data[11];
                $datek->slot_transport_a=$data[12];
                $datek->port_transport_a=$data[13];
                $datek->frek_transport_a=$data[14];
                $datek->ne_transport_b=$data[15];
                $datek->board_transport_b=$data[16];
                $datek->shelf_transport_b=$data[17];
                $datek->slot_transport_b=$data[18];
                $datek->port_transport_b=$data[19];
                $datek->frek_transport_b=$data[20];
                $datek->date_detected=$data[21];
                $datek->date_undetected=$data[22];
                $datek->status=$data[23];
                $datek->utilization_max=$data[24];
                $datek->kota_router_node_a=$data[25];
                $datek->kota_router_node_b=$data[26];
                $datek->transport_type=$data[27];
                $datek->save();
            }
        }

        return Redirect(URL::Route('inventory.ipbb')) ;

    }

    public function ililIpbb($regional)
    {
        if($regional==0)
        {
            $ix = DatekIpbb::whereNull('date_undetected')->get();
        }
        else {
            $ix = DatekIpbb::where('reg_telkom', $regional)->get();

        }
        return view('modul_inventory.ipbb_detail')->with('ipbb',$ix)->with('regional',$regional);
    }

    public function DeleteIpbb($id)
    {
        DatekIpbb::destroy($id);

        return Redirect::back();
    }

    public function Ix()
    {
        return view('modul_inventory.ix');
    }

    public function ImportIx()
    {

        return view('modul_inventory.ix_import')->with('layer','ix');
    }

    public function PostImportIx(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'ix_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();


        return view('modul_inventory.ix_import')->with('layer','ix')->with('ix',$sheetData)->with('file',$nama_file_baru);
    }

    public function SaveImportIx(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0]!='reg_tsel')
            {
                $datek = new DatekIx();
                $datek->reg_tsel=$data[0];
                $datek->router_end1_tsel=$data[1];
                $datek->port_end1_tsel=$data[2];
                $datek->reg_telkom=$data[3];
                $datek->sto_end1_telkom=$data[4];
                $datek->metroe_end1_telkom=$data[5];
                $datek->port_end1_telkom=$data[6];
                $datek->capacity=$data[7];
                $datek->layer=$data[8];
                $datek->system=$data[9];
//                $datek->ne_transport_a=$data[10];
//                $datek->board_transport_a=$data[11];
//                $datek->shelf_transport_a=$data[12];
//                $datek->slot_transport_a=$data[13];
//                $datek->port_transport_a=$data[14];
//                $datek->frek_transport_a=$data[15];
//                $datek->ne_transport_b=$data[16];
//                $datek->board_transport_b=$data[17];
//                $datek->shelf_transport_b=$data[18];
//                $datek->slot_transport_b=$data[19];
//                $datek->port_transport_b=$data[20];
//                $datek->frek_transport_b=$data[21];
                $datek->date_detected=$data[10];
                $datek->date_undetected=$data[11];
                $datek->status=$data[12];
                $datek->save();
            }
        }

        return Redirect(URL::Route('inventory.ix')) ;

    }

    public function DetailIx($regional)
    {
        if($regional==0)
        {
            $ix = DatekIx::whereNull('date_undetected')->get();
        }
        else {
            $ix = DatekIx::where('reg_telkom',$regional)->get();
        }

        return view('modul_inventory.ix_detail')->with('ix',$ix)->with('regional',$regional);
    }

    public function DeleteIx($id)
    {
        DatekIx::destroy($id);

        return Redirect::back();
    }

    public function Dashboard()
    {
        return view('modul_inventory.dashboard');
    }

    public function importFileIntoDB(Request $request){
        if($request->hasFile('backhaul')){
            $path = $request->file('backhaul')->getRealPath();
            $data = Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $key => $value) {
                    $arr[] = ['TREG' => $value->reg_telkom, 'NEAREND' => $value->metroe_telkom];
                }
                if(!empty($arr)){
                    DB::table('datek_backhaul')->insert($arr);
                    dd('Insert Record successfully.');
                }
            }
        }
        dd('Request data does not have any files to import.');
    }


    public function getBackhaul()
    {
        // $jsonurl = "http://10.62.164.166/api/jovice/get/backhaulnew";
       
        $jsonurl = "http://localhost/toko-online/api/direct";

        $json = file_get_contents($jsonurl);
        echo json_encode($json);
        // $bitcoin = json_decode($json);
        // echo count($bitcoin)."\n";
        // //return response()->json($bitcoin);
        // $cap = 0;
        // foreach (json_decode($json, true) as $area)
        // {
        //     $area['capacity'] = str_replace("G","",$area['capacity']);
        //     $cap = $cap+$area['capacity'];
        //     $area['mi_name'] = str_replace("Ex","",$area['mi_name']);
        //     $area['mi_name'] = str_replace("Ag","",$area['mi_name']);
        //     $area['mi_name'] = str_replace("lag-","",$area['mi_name']);
        //     $area['mi_name'] = str_replace("Gi","",$area['mi_name']);
        //     //print_r($area); // this is your area from json response
        //     $backhaul = DatekBackhaul::where('metroe_telkom',$area['no_name'])->where('port_telkom',$area['mi_name'])->first();
        //     if (isset($backhaul))
        //     {
        //         $backhaul->routername_tsel = $area['routername_tsel'];
        //         $backhaul->port_tsel = $area['port_tsel'];
        //         $backhaul->description = $area['mi_description'];
        //         $backhaul->status = $area['status'];
        //         $backhaul->capacity = $area['capacity'];
        //         $backhaul->date_undetected = null;
        //         $backhaul->tglcheck=Carbon::now();
        //         //$backhaul->date_detected = date("Y-m-d");
        //         $backhaul->save();
        //     }
        //     else
        //     {
        //         $backhaul = new DatekBackhaul();
        //         $backhaul->reg_tsel = $area['regional'];
        //         $backhaul->reg_telkom = $area['regional'];
        //         $backhaul->routername_tsel = $area['routername_tsel'];
        //         $backhaul->metroe_telkom = $area['no_name'];
        //         $backhaul->port_tsel = $area['port_tsel'];
        //         $backhaul->port_telkom = $area['mi_name'];
        //         $backhaul->status = $area['status'];
        //         $backhaul->capacity = $area['capacity'];
        //         $backhaul->layer = "BACKHAUL";
        //         $backhaul->system = "METRO-E";
        //         $backhaul->date_detected = date("Y-m-d");
        //         $backhaul->description = $area['mi_description'];
        //         $backhaul->tglcheck=Carbon::now();
        //         $backhaul->save();
        //     }
        // }

        // echo 'Jumlah Capacity '.$cap;
        // $command = new CommandHistory();
        // $command->name = "Get Backhaul";
        // $command->type = "GET";
        // $command->save();

        // return "done";
        //         // echo json_encode($json);

    }

    public function getOneip()
    {

        $ldap_username = Setting::where('key','ldap_id')->first()->value;
        $ldap_password = Setting::where('key','ldap_password')->first()->value;

        // REGIONAL1
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R1'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);


        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            if ((strpos($data['Name'], '.') === false) )
            {
                if(( strpos($data['Name'], "Cisco") === false))
                {
                $openapi = DatekOneIPMPLS::where('reg_telkom',1)
                    ->where('metroe_telkom',$data['device']['Name'])
                    ->where('port_telkom',$data['Name'])->first();
                if(isset($openapi))
                {
                    $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
                    echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
                        ." ".(end($data['portmfs']['results'])['Value'])."<br>";
                    $openapi->save();
                }
                else
                {
                    $openapi = new DatekOneIPMPLS();
                    $openapi['metroe_telkom']=$data['device']['Name'];
                    $openapi['port_telkom']=$data['Name'];
                    $openapi['reg_telkom']=1;
                    $openapi['description']=$data['Description'];
                    $parts = explode('E', $data['SpeedIn']);
                    if(isset($parts[1])) {
                        $parts[0]=str_replace(".","",$parts[0]);
                        if ($parts[1] == 10) {
                            $openapi['capacity'] = $parts[0] * 1;
                        } else if ($parts[1] == 11) {
                            $openapi['capacity'] = $parts[0] * 10;
                        } else {
                            $openapi['capacity'] = $parts[0]/10;
                        }
                    }
                    else
                    {
//                            $parts = explode('.', $data['SpeedIn']);
                        $openapi['capacity'] = $parts[0];
                    }
                    $openapi['layer']="OneIPMPLS";
                    $openapi['transport_type']="DWDM";
                    if($data['OperStatus']==1){
                        $openapi['status']="UP";
                    }
                    else
                    {$openapi['status']="DOWN";}
                    $openapi['utilization_max']=(end($data['portmfs']['results'])['Value']);
                    $openapi['date_detected']=Carbon::now();
                    echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
                    $openapi->save();
                }
            }
            }
        }

        echo "DONE Fetch OneIPMPLS Regional 1";

        $command = new CommandHistory();
        $command->name = "Fetch OneIPMPLS Regional 1";
        $command->type = "FETCH";
        $command->save();

//        // REGIONAL2
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R2'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',2)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=2;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 2";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 2";
//        $command->type = "FETCH";
//        $command->save();
//
//        // REGIONAL3
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R3'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',3)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=3;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 3";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 3";
//        $command->type = "FETCH";
//        $command->save();


//        // REGIONAL4
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R4'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('ae'), tolower(Name)) eq true)) and (OperStatus ne 2) and (length(tolower(Description)) ne 0))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',4)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=4;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 4";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 4";
//        $command->type = "FETCH";
//        $command->save();
//


//        // REGIONAL5
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R5'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('ae'), tolower(Name)) eq true)) and (OperStatus ne 2) and (length(tolower(Description)) ne 0))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',5)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=5;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 5";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 5";
//        $command->type = "FETCH";
//        $command->save();


//        // REGIONAL6
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R6'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',6)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=6;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('.0E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//
//
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 100;
//                            } else {
//                                $openapi['capacity'] = $parts[0];
//                            }
//                        }
//                        else
//                        {
//                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 6";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 6";
//        $command->type = "FETCH";
//        $command->save();
////
//        // REGIONAL7
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R7'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',7)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=7;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('.0E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//
//
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 100;
//                            } else {
//                                $openapi['capacity'] = $parts[0];
//                            }
//                        }
//                        else
//                        {
//                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 7";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 7";
//        $command->type = "FETCH";
//        $command->save();

    }


    public function getIpran()
    {

        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            ]
        ];

        $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
        //$file = file_get_contents('http://www.example.com/', false, $context);


        $jsonurl = "http://10.62.164.166/api/jovice/get/ipran";
        $json = file_get_contents($jsonurl, false, $context);
        $bitcoin = json_decode($json);
        echo count($bitcoin)."<br>";

        //return response()->json($bitcoin);
        $f=0;
        $cap=0;
        foreach (json_decode($json, true) as $area)
        {
            $cap = $cap+$area['capacity'];
            //$area['capacity'] = str_replace("G","",$area['capacity']);
            $f++;
            echo $f." \n";
//            print_r($area); // this is your area from json response



            $area['portfarend'] = str_replace("Ex","",$area['portfarend']);
            $area['portfarend'] = str_replace("Ag","",$area['portfarend']);
            $area['portfarend'] = str_replace("Gi.","",$area['portfarend']);
            $area['portfarend'] = str_replace("Gi","",$area['portfarend']);
            $area['portnearend'] = str_replace("Ex","",$area['portnearend']);
            $area['portnearend'] = str_replace("Ag","",$area['portnearend']);
            $area['portnearend'] = str_replace("Gi","",$area['portnearend']);
            $area['portnearend'] = str_replace("Gi.","",$area['portnearend']);


            $ipran = DatekIpran::where('vcid',  $area['vcid'])->get();
//            $ipran = DatekIpran::where('vcid',  $area['vcid'])
//                ->where('metroe_end1_telkom',  $area['nearend'])
//                ->where('metroe_end2_telkom',  $area['farend'])->first();
            if (count($ipran)>0)
            {

                $found=0;
                foreach ($ipran as $datum)
                {
                    echo $datum['metroe_end1_telkom'];
                    if(($area['nearend']==$datum['metroe_end1_telkom']||$area['nearend']==$datum['metroe_end2_telkom'])
                    && ($area['farend']==$datum['metroe_end1_telkom']||$area['farend']==$datum['metroe_end2_telkom'])
                    )
                    {
                        $found=1;
                        $datum->status = $area['status'];
                        $datum->capacity = $area['capacity'];
                        $datum->date_undetected = null;
                        $datum->tglcheck=Carbon::now();
                        $datum->description = $area['description'];
                        //$ipran->date_detected = date("Y-m-d");
                        $datum->save();
                    }
                }

                if($found==0)
                {
                    echo $area['nearend'];
                    $dataipran = new DatekIpran();
                    $dataipran->vcid = $area['vcid'];
                    $dataipran->reg_tsel = $area['reg'];
                    $dataipran->reg_telkom = $area['reg'];
                    $dataipran->router_end1_tsel = $area['router_end1_tsel'];
                    $dataipran->metroe_end1_telkom = $area['nearend'];
                    $dataipran->port_end1_tsel = $area['port_end1_tsel'];
                    $dataipran->port_end1_telkom = $area['portnearend'];
                    $dataipran->router_end2_tsel = $area['router_end2_tsel'];
                    $dataipran->metroe_end2_telkom = $area['farend'];
                    $dataipran->port_end2_tsel = $area['port_end2_tsel'];
                    $dataipran->port_end2_telkom = $area['portfarend'];
                    $dataipran->status = $area['status'];
                    $dataipran->capacity = $area['capacity'];
                    $dataipran->description = $area['description'];
                    $dataipran->layer = "IPRAN";
                    $dataipran->system = "METRO-E";
                    $dataipran->date_detected = date("Y-m-d");
                    $dataipran->save();
                }
            }
            else
            {
                echo $area['nearend'];
                $ipran = new DatekIpran();
                $ipran->vcid = $area['vcid'];
                $ipran->reg_tsel = $area['reg'];
                $ipran->reg_telkom = $area['reg'];
                $ipran->router_end1_tsel = $area['router_end1_tsel'];
                $ipran->metroe_end1_telkom = $area['nearend'];
                $ipran->port_end1_tsel = $area['port_end1_tsel'];
                $ipran->port_end1_telkom = $area['portnearend'];
                $ipran->router_end2_tsel = $area['router_end2_tsel'];
                $ipran->metroe_end2_telkom = $area['farend'];
                $ipran->port_end2_tsel = $area['port_end2_tsel'];
                $ipran->port_end2_telkom = $area['portfarend'];
                $ipran->status = $area['status'];
                $ipran->capacity = $area['capacity'];
                $ipran->description = $area['description'];
                $ipran->layer = "IPRAN";
                $ipran->system = "METRO-E";
                $ipran->date_detected = date("Y-m-d");
                $ipran->save();
            }

            echo "<br>";
        }

        echo 'Jumlah Capacity '.$cap;
        $command = new CommandHistory();
        $command->name = "Get IPRAN";
        $command->type = "GET";
        $command->save();

        return "done";
    }

    public function getIX()
    {

        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        // Open the file using the HTTP headers set above
        //$file = file_get_contents('http://www.example.com/', false, $context);


        $jsonurl = "http://10.62.164.166/api/jovice/get/ix";
        $json = file_get_contents($jsonurl, false, $context);
        $bitcoin = json_decode($json);
        echo count($bitcoin)."<br>";
        //return response()->json($bitcoin);
        foreach (json_decode($json, true) as $area)
        {
            //print_r($area); // this is your area from json response
            $ix = DatekIx::where('metroe_end1_telkom',$area['metroe_end1_telkom'])->where('port_end1_telkom',$area['port_end1_telkom'])->first();
            if (isset($ix))
            {

                $ix->status = $area['status'];
                $ix->capacity = $area['speed'];
                $ix->description = $area['description'];
                $ix->date_undetected = null;
                $ix->tglcheck=Carbon::now();
                //$ipran->date_detected = date("Y-m-d");
                $ix->save();
            }
            else
            {
                $ix = new DatekIx();
                $ix->reg_tsel = $area['aw_ag'];
                $ix->reg_telkom = $area['aw_ag'];
                $ix->router_end1_tsel = $area['router_end1_tsel'];
                $ix->metroe_end1_telkom = $area['metroe_end1_telkom'];
                $ix->port_end1_tsel = $area['port_end1_tsel'];
                $ix->port_end1_telkom = $area['port_end1_telkom'];
                $ix->status = $area['status'];
                $ix->capacity = $area['capacity'];
                $ix->layer = "IX";
                $ix->system = "Direct Core";
                $ix->date_detected = date("Y-m-d");
                $ix->save();
            }
//            echo $area['router_end1_tsel']." ".$area['port_end1_tsel']."<br>";
            echo $area['metroe_end1_telkom']." ".$area['port_end1_telkom']."<br>";
        }


        $command = new CommandHistory();
        $command->name = "Get IX";
        $command->type = "GET";
        $command->save();


        return "done";
    }

    public function getIPBB()
    {

        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            ]
        ];

        $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
        //$file = file_get_contents('http://www.example.com/', false, $context);


        $jsonurl = "http://10.62.164.166/api/jovice/get/ipbb";
        $json = file_get_contents($jsonurl, false, $context);
        $bitcoin = json_decode($json);
        //return response()->json($bitcoin);
        foreach (json_decode($json, true) as $area)
        {
            //print_r($area); // this is your area from json response
            $ipbb = DatekIpbb::where('router_node_a',$area['nearend'])->where('router_node_b',$area['farend'])->first();
            if (isset($ipbb))
            {

                $ipbb->status = $area['status'];
                $ipbb->capacity = $area['capacity'];
                $ipbb->description = $area['description'];
                $ipbb->system = "DWDM";
                $ipbb->date_undetected = null;
                //$ipran->date_detected = date("Y-m-d");
                $ipbb->save();
            }
            else
            {
                $datek = new DatekIpbb();
                $datek->reg_tsel=$area['reg'];
                $datek->router_node_a=$area['nearend'];
                $datek->port_end1_tsel=$area['portnearend'];
                $datek->router_node_b=$area['farend'];
                $datek->port_end2_tsel=$area['portfarend'];
                $datek->reg_telkom=$area['reg'];
                $datek->capacity=$area['capacity'];
                $datek->layer="IPBB";
                $datek->system="DWDM";
                $datek->date_detected=date("Y-m-d");
                $datek->status=$area['status'];
                $datek->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Get IPBB";
        $command->type = "GET";
        $command->save();

        return "done";
    }


    public function getSummary()
    {

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n"))
                ->where('year',date("Y"))
                ->where('Layer',"Access")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekAccess::Link($x);
                $data->capacity = DatekAccess::Capacity($x)*0.001;
                $data->utilization = DatekAccess::Utilization($x)*0.001;
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n");
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "Access";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekAccess::Link($x)*0.001;
                $summary->capacity = DatekAccess::Capacity($x)*0.001;
                $summary->utilization = DatekAccess::Utilization($x)*0.001;
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n"))
                ->where('year',date("Y"))
                ->where('Layer',"Backhaul")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekBackhaul::Link($x);
                $data->capacity = DatekBackhaul::Capacity($x);
                $data->utilization = DatekBackhaul::Utilization($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n");
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "Backhaul";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekBackhaul::Link($x);
                $summary->capacity = DatekBackhaul::Capacity($x);
                $summary->utilization = DatekBackhaul::Utilization($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n"))
                ->where('year',date("Y"))
                ->where('Layer',"IPRAN")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekIpran::Link($x);
                $data->capacity = DatekIpran::Capacity($x);
                $data->utilization = DatekIpran::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n");
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "IPRAN";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekIpran::Link($x);
                $summary->capacity = DatekIpran::Capacity($x);
                $summary->utilization = DatekIpran::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n"))
                ->where('year',date("Y"))
                ->where('Layer',"IPBB")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekIpbb::Link($x);
                $data->capacity = DatekIpbb::Capacity($x);
                $data->utilization = DatekIpbb::Utilization($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n");
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "IPBB";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekIpbb::Link($x);
                $summary->capacity = DatekIpbb::Capacity($x);
                $summary->utilization = DatekIpbb::Utilization($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }



        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n"))
                ->where('year',date("Y"))
                ->where('Layer',"OneIPMPLS")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekOneIPMPLS::Link($x);
                $data->capacity = DatekOneIPMPLS::Capacity($x);
                $data->utilization = DatekOneIPMPLS::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n");
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "OneIPMPLS";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekOneIPMPLS::Link($x);
                $summary->capacity = DatekOneIPMPLS::Capacity($x);
                $summary->utilization = DatekOneIPMPLS::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }



        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n"))
                ->where('year',date("Y"))
                ->where('Layer',"IX")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekIx::Link($x);
                $data->capacity = DatekIx::Capacity($x);
                $data->utilization = DatekIx::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n");
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "IX";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekIx::Link($x);
                $summary->capacity = DatekIx::Capacity($x);
                $summary->utilization = DatekIx::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Calculate Summary";
        $command->type = "CALCULATE";
        $command->save();

        return "done";
    }


    public function getumax()
    {

        $access = DatekAccess::all();
        foreach ($access as $data)
        {

            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];

            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];

            }
            if($data['utilization_max']!=null && $data['capacity'] != null)
            {

            $data['utilization_cap']=$data['utilization_max']*$data['capacity'];
            $data->save();
            }
        }

        $backhaul = DatekBackhaul::all();
        foreach ($backhaul as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
            if($data['utilization_max']!=null && $data['capacity'] != null)
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity'];
                $data->save();
            }
        }

        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_max'];
                $data->save();
            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
            if($data['utilization_max']!=null && $data['capacity'] != null)
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity'];
                $data->save();
            }
        }

        $ipbb = DatekIpbb::all();
        foreach ($ipbb as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
            if($data['utilization_max']!=null && $data['capacity'] != null)
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity'];
                $data->save();
            }
        }

        $ix = DatekIx::all();
        foreach ($ix as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
            if($data['utilization_max']!=null && $data['capacity'] != null)
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity'];
                $data->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Calculate Utilization Max";
        $command->type = "CALCULATE";
        $command->save();

        return "done";
    }

    public function getUndetected()
    {
//
//        $access = DatekAccess::all();
//        foreach ($access as $data)
//        {
//
//            if(!$data['updated_at']->isToday())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//        }


        $backhaul = DatekBackhaul::whereDate('tglcheck','<>'. Carbon::today()->toDateString())
            ->orWhereNull('tglcheck')->get();
        foreach ($backhaul as $data)
        {
                $data['date_undetected']=Carbon::now();
                $data->save();
        }


        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            if(Carbon::parse($data['tglcheck'])->toDateString()!=Carbon::now()->toDateString())
            {
                $data['date_undetected']=Carbon::now();
                $data->save();
            }
            else{
                $data['date_undetected']=null;
                $data->save();
            }

        }

//        $ipran = DatekIpran::all();
//        foreach ($ipran as $data)
//        {
//            if(!$data['tglcheck']==Carbon::now()->toDateString())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//            else{
//                $data['date_undetected']=null;
//                $data->save();
//            }
//
//        }

//        $ipbb = DatekIpbb::all();
//        foreach ($ipbb as $data)
//        {
//            if(!$data['updated_at']->isToday())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//        }

//        $ix = DatekIx::all();
//        foreach ($ix as $data)
//        {
//            if(!$data['updated_at']->toDateString()==Carbon::now()->toDateString())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//            else{
//                $data['date_undetected']=null;
//                $data->save();
//            }
//
//        }

        $command = new CommandHistory();
        $command->name = "Check Undetected";
        $command->type = "CHECK";
        $command->save();

        return "done";
    }

    public function SynchronizeCities()
    {


        $backhaul = DatekBackhaul::all();
        foreach ($backhaul as $data)
        {

            $kota = Metro::where('host_name','like', '%'.$data->metroe_telkom.'%')->first();
            if(isset($kota))
            {
            $data->sto_telkom = $kota->lokasi;
            $data->save();
            }

            $kota=null;

        }

        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            $kota1 = Metro::where('host_name','like', '%'.$data->metroe_end1_telkom.'%')->first();
            if(isset($kota1)) {
                $data->sto_end1_telkom = $kota1->lokasi;
                $data->save();
            }
            $kota2 = Metro::where('host_name','like', '%'.$data->metroe_end2_telkom.'%')->first();
            if(isset($kota2)) {
                $data->sto_end2_telkom = $kota2->lokasi;
                $data->save();
            }
            $kota1=null;
            $kota2=null;
        }

//        $ipbb = DatekIpbb::all();
//        foreach ($ipbb as $data)
//        {
//            $kota1 = Metro::where('host_name',$data->metroe_end1_telkom)->get();
//            $data->kota_router_end_1 = $kota1->lokasi;
//            $kota2 = Metro::where('host_name',$data->metroe_end2_telkom)->get();
//            $data->kota_router_end_2 = $kota2->lokasi;
//            $data->save();
//        }

        $ix = DatekIx::all();
        foreach ($ix as $data)
        {

            $kota = Metro::where('host_name','like', '%'.$data->metroe_end1_telkom.'%')->first();
            if(isset($kota)) {
                $data->sto_end1_telkom = $kota->lokasi;
                $data->save();
            }
            $kota=null;

        }

        $command = new CommandHistory();
        $command->name = "Synchronize City";
        $command->type = "SYNCHRONIZE";
        $command->save();

        return "done";
    }

    public function SynchronizeOpenAPI()
    {
        $apis = API::all();
        foreach ($apis as $datum)
        {
            echo $datum->url."<br>";
            $url = $datum->url;
            $client = new Client();
            $res = $client->request('GET', $url, [
                'auth' => ['940156', 'No5423676;']
            ]);

            $area = json_decode($res->getBody(), true);
            foreach ($area['d']['results'] as $data) {
                $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
                $data['Name'] = str_replace("Ex","",$data['Name']);
                $data['Name'] = str_replace("Ag","",$data['Name']);
                $data['Name'] = str_replace("lag-","",$data['Name']);
                $data['Name'] = str_replace("GigabitEthernet","",$data['Name']);
                $data['Name'] = str_replace("Gi.","",$data['Name']);
                $data['Name'] = str_replace("Eth-Trunk","",$data['Name']);
                if(isset($openapi))
                {
                    $openapi['utilization'] = end($data['portmfs']['results'])['Value'];
                    // $openapi['layer']=$apis['layer'];
                    // $openapi['reg_tsel']=$data['reg_tsel'];
                    // $openapi['reg_telkom']=$data['reg_telkom'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." Update"."<br>";
                }
                else
                {

                    $openapi = new OpenAPI();
                    $openapi['name']=$data['device']['Name'];
                    $openapi['port']=$data['Name'];
                    // $openapi['layer']=$apis['layer'];
                    // $openapi['reg_tsel']=$data['reg_tsel'];
                    // $openapi['reg_telkom']=$data['reg_telkom'];

                    $openapi['utilization']=end($data['portmfs']['results'])['Value'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." New"."<br>";
                }

                echo $data['device']['Name']." ".$data['Name']." ";
                echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
            }
            echo "<br>";
        }


        $command = new CommandHistory();
        $command->name = "Get OpenAPI";
        $command->type = "GET";
        $command->save();
    }


    public function getucap()
    {
        $access = DatekAccess::all();
        foreach ($access as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null  )
            {

                $data['utilization_cap']=($data['utilization_max']/$data['capacity']);
                $data->save();
            }
        }

        $backhaul = DatekBackhaul::all();
        foreach ($backhaul as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null  )
            {

                $data['utilization_cap']=($data['utilization_max']/$data['capacity']);
                $data->save();
            }
        }

        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null  )
            {

                $data['utilization_cap']=($data['utilization_max']*$data['capacity']*0.01);
                $data->save();
            }
        }

        $ipbb = DatekIpbb::all();
        foreach ($ipbb as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null  )
            {

                $data['utilization_cap']=($data['utilization_max']/$data['capacity']);
                $data->save();
            }
        }



        $OneIPMPLS = DatekOneIPMPLS::all();
        foreach ($OneIPMPLS as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null  )
            {

                $data['utilization_cap']=($data['utilization_max']*$data['capacity']*0.01);
                $data->save();
            }
        }

        $ix = DatekIx::all();
        foreach ($ix as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null  )
            {

                $data['utilization_cap']=($data['utilization_max']*$data['capacity']*0.01);
                $data->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Calculate Utilization Capacity";
        $command->type = "CALCULATE";
        $command->save();

        return "done";
    }

    public function getucapsummary()
    {
        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',4)
                ->where('year',date("Y"))
                ->where('Layer',"IPBB")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->utilization = DatekIpbb::UtilizationCap($x);
                //$data->capacity = DatekIpbb::Capacity($x);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Calculate Utilization Capacity Summary";
        $command->type = "CALCULATE";
        $command->save();

        return "done";

    }

    public function getUtilAccess()
    {

        $access = DatekAccess::all();
        foreach ($access as $data)
        {

            if(isset($data['me_dm_telkom'])&&isset($data['port_dm_telkom']))
            {
            $utilaccess = UtilAccess::where('device_name',$data['me_dm_telkom'])->where('port',$data['port_dm_telkom'])->first();
            //echo "<br>".$data['me_dm_telkom']." util : ".$utilaccess['util-in'];
            $data['utilization_in']=$utilaccess['util-in'];
            $data['utilization_out']=$utilaccess['util-out'];
            $data->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Get Utilization Access";
        $command->type = "GET";
        $command->save();

        return "done";
    }

    public static function AccessOverThreshold()
    {

        $access = DatekAccess::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();

        return view('modul_inventory.overthreshold.access')->with('access',$access)->with('regional',' ');
    }

    public static function BackhaulOverThreshold()
    {
        $backhaul = DatekBackhaul::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();

        return view('modul_inventory.overthreshold.backhaul')->with('backhaul',$backhaul)->with('regional',' ');
    }


    public static function OneIPMPLSOverThreshold()
    {
        $backhaul = DatekOneIPMPLS::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();

        return view('modul_inventory.overthreshold.OneIPMPLS')->with('OneIPMPLS',$backhaul)->with('regional',' ');
    }

    public static function IpranOverThreshold()
    {
        $ipran = DatekIpran::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();

        return view('modul_inventory.overthreshold.ipran')->with('ipran',$ipran)->with('regional',' ');
    }

    public static function IpbbOverThreshold()
    {
        $ipbb = DatekIpbb::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();

        return view('modul_inventory.overthreshold.ipbb')->with('ipbb',$ipbb)->with('regional',' ');
    }

    public static function IxOverThreshold()
    {
        $ix = DatekIx::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();
        return view('modul_inventory.overthreshold.ix')->with('ix',$ix)->with('regional',' ');
    }

    public static function Search(Request $request)
    {
        $input = $request->all();

        // Kata Kunci yg dicari
        $keyword = $input['keyword'];

            $access = DatekAccess::where('siteid_tsel','like', '%'.$keyword.'%')
                ->orwhere('sitename_tsel','like', '%'.$keyword.'%')
                ->whereNull('date_undetected')->get();
                
            // mencari STO backhaul di table data_lokasi_ne
            $backhaulsto = MetroLokasi::where('lokasi',$keyword);
            if ($backhaulsto->count() != 0) {
                // jika hasil query ada maka 
                foreach ($backhaulsto->get() as $val) {
                    $regi[] =  $val['host_name'];
                } 

                // mencari description yg sesuai keyword
                $backhaul = DatekBackhaul::where('description','like', '%'.$keyword.'%')
                // mencari metroe yg sesuai keyword STO
                ->whereIn("metroe_telkom",  array(implode(', ', $regi)))
                ->whereNull('date_undetected')->get();
    
            }else {                    
                // mencari description yg sesuai keyword
                $backhaul = DatekBackhaul::where('description','like', '%'.$keyword.'%')
                ->whereNull('date_undetected')->get();
            }
            
            // untuk mencari ipran DWDM
            // mencari kota a, router a, kota b, router b yg sesuai keyword
            $ipran1 = NewDatekIpran::where('kota_router_node_a','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->orwhere('router_node_a','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->orwhere('kota_router_node_b','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->orwhere('router_node_b','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->get();
            
            // untuk mencari ipran metro
            // mencari kota a, router a, kota b, router b yg sesuai keyword
            $ipran2 = NewDatekIpran::where('kota_router_node_a','like', '%'.$keyword.'%')->where("transport_type", "metro")
            ->orwhere('router_node_a','like', '%'.$keyword.'%')->where("transport_type", "metro")
            ->orwhere('kota_router_node_b','like', '%'.$keyword.'%')->where("transport_type", "metro")
            ->orwhere('router_node_b','like', '%'.$keyword.'%')->where("transport_type", "metro")
            ->get();
            
            // untuk mencari ipbb dwdm
            // mencari kota a, router a, kota b, router b yg sesuai keyword
            $ipbb1 = NewDatekIpbb::where('kota_router_node_a','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->orwhere('router_node_a','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->orwhere('kota_router_node_b','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->orwhere('router_node_b','like', '%'.$keyword.'%')->where("transport_type", "dwdm")
            ->get();
    
            // untuk mencari ipbb metro
            // mencari kota a, router a, kota b, router b yg sesuai keyword
            $ipbb2 = NewDatekIpbb::where('kota_router_node_a','like', '%'.$keyword.'%')->where("transport_type", "metro")
                ->orwhere('router_node_a','like', '%'.$keyword.'%')->where("transport_type", "metro")
                ->orwhere('kota_router_node_b','like', '%'.$keyword.'%')->where("transport_type", "metro")
                ->orwhere('router_node_b','like', '%'.$keyword.'%')->where("transport_type", "metro")
                ->get();
    
    
                // mencari STO Ix di table data_lokasi_ne
                $ixsto = MetroLokasi::where('lokasi',$keyword);
                if ($ixsto->count() != 0) {
                    // jika hasil query ada maka 
                    foreach($ixsto->get() as $item  => $r){ 
                        $e[] = '"'.$r['host_name'].'"';
                    }

                                        
                    // mencari routername_tsel yg sesuai keyword
                    $ix = NewDatekIx::where('routername_tsel','like', '%'.$keyword.'%')
                    // mencari pe_telkom yg sesuai keyword STO Ix
                    ->orwhereIn('pe_telkom', array(implode(', ', $e)))
                    ->orwhere('description','like', '%'.$keyword.'%')->get();
                }else {                   
                        // mencari routername_tsel dan description yg sesuai keyword 
                        $ix = NewDatekIx::where('routername_tsel','like', '%'.$keyword.'%')
                        ->orwhere('description','like', '%'.$keyword.'%')->get();
                }

    
                
            return view('modul_inventory.inventory_search')
                ->with('access',$access)
                ->with('backhaul',$backhaul)
                ->with('ipran',$ipran1)
                ->with('ipran2',$ipran2)
                ->with('ipbb',$ipbb1)
                ->with('ipbb2',$ipbb2)
                ->with('ix',$ix)
                ->with('regional',' ')
                ->with('regi',$keyword);


            // $ixso = MetroLokasi::where('lokasi',$keyword);
            // foreach($ixso->get() as $item  => $r){ 
            //     $a[] = '"'.$r['host_name'].'"';
            // }

            // //  $b = implode(', ', $a);
            // // //  $b = explode(',', $b);
            // // //  $b = array("CES-D1-DRIA", "ME-D1-DRIA", "ME2-D1-DRIA", "PE-D1-DRI", "PE-D1-DRI-SPEEDY", "PE-D1-DRI-SS", "PE-D1-DRI-TRANSIT", "PE-D1-DRI-VPN", "t-d1-dri");
            // //  echo $b;
            // //  echo "<br>";

            // $ix = NewDatekIx::whereIn('pe_telkom',  [$a])->count();
            // // $ix = NewDatekIx::whereIn('pe_telkom', array("CES-D1-DRIA", "ME-D1-DRIA", "ME2-D1-DRIA", "PE-D1-DRI", "PE-D1-DRI-SPEEDY", "PE-D1-DRI-SS", "PE-D1-DRI-TRANSIT", "PE-D1-DRI-VPN", "t-d1-dri"))->count();

            // echo $ix." Hasil";



    }

    public static function GetApiUtil()
    {
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 1'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
//        dd(end($area['d']['results'][0]['portmfs']['results'])['Value']);
        foreach ($area['d']['results'] as $data)
        {
            echo $data['device']['Name']." ";
            echo $data['Name']." ";
            echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
        }




    }

    public static function SaveApiUtil()
    {

//        // REGIONAL1
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 1'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => ['940156', 'No5423676..']
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            $openapi = OpenAPI::where('regional',1)
//                ->where('name',$data['device']['Name'])
//                ->where('port',$data['Name'])->first();
//            if(isset($openapi))
//            {
//                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//            else
//            {
//                $openapi = new OpenAPI();
//                $openapi['name']=$data['device']['Name'];
//                $openapi['port']=$data['Name'];
//                $openapi['regional']=1;
//                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//
//        }
//
//        // REGIONAL2
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=8h&bh=Sun-Sat 19:00-23:00&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 2'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => ['940156', 'No5423676..']
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            $openapi = OpenAPI::where('regional',2)
//                ->where('name',$data['device']['Name'])
//                ->where('port',$data['Name'])->first();
//            if(isset($openapi))
//            {
//                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//            else
//            {
//                $openapi = new OpenAPI();
//                $openapi['name']=$data['device']['Name'];
//                $openapi['port']=$data['Name'];
//                $openapi['regional']=2;
//                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//
//        }
//
//        // REGIONAL3
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 3'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => ['940156', 'No5423676..']
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            $openapi = OpenAPI::where('regional',3)
//                ->where('name',$data['device']['Name'])
//                ->where('port',$data['Name'])->first();
//            if(isset($openapi))
//            {
//                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//            else
//            {
//                $openapi = new OpenAPI();
//                $openapi['name']=$data['device']['Name'];
//                $openapi['port']=$data['Name'];
//                $openapi['regional']=3;
//                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//
//        }

//        // REGIONAL4
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 4'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => ['940156', 'No5423676..']
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            $openapi = OpenAPI::where('regional',4)
//                ->where('name',$data['device']['Name'])
//                ->where('port',$data['Name'])->first();
//            if(isset($openapi))
//            {
//                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//            else
//            {
//                $openapi = new OpenAPI();
//                $openapi['name']=$data['device']['Name'];
//                $openapi['port']=$data['Name'];
//                $openapi['regional']=4;
//                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//
//        }
//
//        // REGIONAL5
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 5'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => ['940156', 'No5423676..']
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            $openapi = OpenAPI::where('regional',5)
//                ->where('name',$data['device']['Name'])
//                ->where('port',$data['Name'])->first();
//            if(isset($openapi))
//            {
//                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//            else
//            {
//                $openapi = new OpenAPI();
//                $openapi['name']=$data['device']['Name'];
//                $openapi['port']=$data['Name'];
//                $openapi['regional']=5;
//                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
//                $openapi->save();
//            }
//
//        }

        // REGIONAL6
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 6'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);


        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('regional',6)
                ->where('name',$data['device']['Name'])
                ->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['regional']=6;
                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }

        // REGIONAL7
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1h&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 7'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);


        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('regional',7)
                ->where('name',$data['device']['Name'])
                ->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['regional']=7;
                $openapi['utilization']=(number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }


        return "DONE";

    }

    public static function GetUtilBackhaul()
    {
        $ldap_username = Setting::where('key','ldap_id')->first()->value;
        $ldap_password = Setting::where('key','ldap_password')->first()->value;
        // REGIONAL1 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID,%20aggregate(portmfs(im_Utilization%20with%20max%20as%20Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower(%27telkomsel%20region:reg-01(sumbagut):backhaul%27),%20tolower(groups/GroupPathLocation))%20eq%20true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username, $ldap_password]
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
//
//            echo $data['device']['Name']." ";
//            echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
        }

        // REGIONAL2 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-02(sumbagsel):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL3 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-03(jabo):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 4 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-04(jabar):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 5 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-05(jateng):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if(isset($backhaul))
            {
                if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
                {
                    $backhaul->utilization_max=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                    $backhaul->save();
                }
            }

        }

        // REGIONAL 6 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-06(jatim):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul->utilization_max=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 7 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-07(BALNUS):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if(isset($backhaul)) {
                if ((0.01 * number_format(end($data['portmfs']['results'])['Value'])) > 0) {
                    $backhaul->utilization_max = (0.01 * number_format(end($data['portmfs']['results'])['Value']));
                    $backhaul->save();
                }
            }
        }

        // REGIONAL 8 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-08(KALIMANTAN):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul->utilization_max=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 9 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-09(SULAWESI):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul->utilization_max=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 10 (TSEL):
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('TELKOMSEL REGION:reg-10(sumbagteng):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul->utilization_max=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 11 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-11(puma):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if(isset($backhaul))
            {
                if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
                {
                    $backhaul->utilization_max=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                    $backhaul->save();
                }
            }

        }

        $command = new CommandHistory();
        $command->name = "Get Utilization Backhaul";
        $command->type = "GET";
        $command->save();

    }

    public static function GetUtilOpenAPI()
    {
        // REGIONAL1
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 1'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
//            $backhaul = DatekBackhaul::where('metroe_telkom', $data['device']['Name'])->first();
//            if ((0.01 * number_format(end($data['portmfs']['results'])['Value'])) > 0) {
//                $backhaul['utilization_max'] = (0.01 * number_format(end($data['portmfs']['results'])['Value']));
//                $backhaul->save();
//            }
//
//            echo $data['device']['Name']." ".$data['Name']." ";
//            echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
        }


        // REGIONAL2
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 2'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }

        // REGIONAL3
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 3'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }

        // REGIONAL4
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 4'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }

        // REGIONAL5
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 5'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }


        // REGIONAL6
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 6'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }

        // REGIONAL7
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&&resolution=RATE&period=4h&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 7'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('IPRAN'), tolower(Description)) eq true) or (substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('RAN-TSEL'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['940156', 'No5423676..']
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['utilization']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $openapi->save();
            }

        }


        echo "done";
    }

    public static function SynchronizeUtil()
    {
//        // Access
//        $data = DatekAccess::all();
//        foreach ($data as $datum)
//        {
//            $openapi = OpenAPI::where('name',$datum['me_dm_telkom'])
//                ->where('port',$datum['port_dm_telkom'])
//                ->first();
//            if(isset($openapi))
//            {
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//        }

//        // Backhaul
//        $data = DatekBackhaul::all();
//        foreach ($data as $datum)
//        {
//            $openapi = OpenAPI::where('name','like', '%'.$datum['metroe_telkom'].'%')
//                ->where('port','like', '%'.$datum['port_telkom'].'%')
//                ->first();
//            echo $datum['metroe_telkom']." ".$datum['port_telkom']." ".$openapi."<br>";
////            if(isset($openapi))
////            {
////                $datum['utilization_max'] = $openapi['utilization'];
////                $datum->save();
////            }
//        }

//        $data = DatekBackhaul::whereNotNull('metroe_telkom')->get();
//        foreach ($data as $datum) {
//            $datum['port_telkom'] = str_replace("Ex","",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("Ag","lag-",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("lag-","Eth-Trunk",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_telkom']);
////            echo $datum['metroe_telkom']." ".$datum['port_telkom']." ";
//            $openapi = OpenAPI::where('name', $datum['metroe_telkom'])
//                ->where('port', $datum['port_telkom'])
//                ->first();
//            if (isset($openapi)) {
////                echo $openapi['utilization']." ";
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
////            echo "<br>";
//        }

//        // IPBB
//        $data = DatekIpbb::all();
//        foreach ($data as $datum)
//        {
//            $openapi = OpenAPI::where('name','like', '%'.$data['metroe_telkom'].'%')
//                ->where('port','like', '%'.$data['port_telkom'].'%')
//                ->first();
//            if(isset($openapi))
//            {
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//        }

//
//         IPRAN
//        $data = DatekIpran::all();
//        foreach ($data as $datum)
//        {
//            $openapi = OpenAPI::where('name','like', '%'.$datum['metroe_end2_telkom'].'%')
//                ->where('port','like', '%'.$datum['port_end2_telkom'].'%')
//                ->first();
//            echo $datum['metroe_end2_telkom']." ".$datum['port_end2_telkom']." ".$openapi."<br>";
////            $datum->port_end2_telkom = str_replace("Ex","",$datum['port_end2_telkom']);
////            $datum->port_end2_telkom = str_replace("Ag","lag-",$datum['port_end2_telkom']);
////            $datum->port_end2_telkom = str_replace("GigabitEthernetgabitEthernet","GigabitEthernet",$datum['port_end2_telkom']);
////            $datum->save();
//            if(isset($openapi))
//            {
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//        }

        // IX
//        $data = DatekIx::all();
//        foreach ($data as $datum)
//        {
//            $openapi = OpenAPI::where('name','like', '%'.$datum['metroe_end1_telkom'].'%')
//                ->where('port','like', '%'.$datum['port_end1_telkom'].'%')
//                ->first();
//            if(isset($openapi))
//            {
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//        }


        $data = DatekIx::whereNotNull('metroe_end1_telkom')->get();
        foreach ($data as $datum) {
            $port = str_replace("Ex","",$datum['port_end1_telkom']);
            $port = str_replace("Ag","lag-",$datum['port_end1_telkom']);
            $port = str_replace("lag-","Eth-Trunk",$datum['port_end1_telkom']);
            $port = str_replace("Gi","GigabitEthernetgabitEthernet",$datum['port_end1_telkom']);
            $openapi = OpenAPI::where('name', $datum['metroe_end1_telkom'])
                ->where('port', 'like', '%' .$port. '%')
                ->first();
            echo $datum['metroe_end1_telkom']." ".$port." ".$openapi."<br>";
            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
                $datum['utilization_max'] = $openapi['utilization'];
                $datum->save();
            }
//            echo "<br>";
        }


        $command = new CommandHistory();
        $command->name = "Synchronize IX";
        $command->type = "SYNCHRONIZE";
        $command->save();
    }

    public static function CheckPort()
    {
        // Access
        $data = DatekAccess::whereNotNull('me_dm_telkom')->get();
        foreach ($data as $datum) {
//            $datum['port_end2_telkom'] = str_replace("Ex","",$datum['port_end2_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Ag","lag-",$datum['port_end2_telkom']);
            echo $datum['me_dm_telkom']." ".$datum['port_dm_telkom']." ";
            $openapi = OpenAPI::where('name', $datum['me_dm_telkom'])
                ->where('port','like', '%'.$datum['port_dm_telkom'].'%')
                ->first();
            if (isset($openapi)) {
                echo $openapi['utilization']." ";
                $datum['utilization_max'] = $openapi['utilization'];
                $datum->save();
            }
            echo "<br>";
        }

//        // Backhaul
//        $data = DatekBackhaul::whereNotNull('metroe_telkom')->get();
//        foreach ($data as $datum) {
//            $datum['port_telkom'] = str_replace("Ex","",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("Ag","lag-",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_telkom']);
//            echo $datum['metroe_telkom']." ".$datum['port_telkom']." ";
//            $openapi = OpenAPI::where('name', $datum['metroe_telkom'])
//                ->where('port', $datum['port_telkom'])
//                ->first();
//            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//            echo "<br>";
//        }


//
//        // IPRAN
//        $data = DatekIpran::whereNotNull('metroe_end2_telkom')->get();
//        foreach ($data as $datum) {
//            $datum['port_end2_telkom'] = str_replace("Ex","",$datum['port_end2_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Ag","lag-",$datum['port_end2_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_end2_telkom']);
//            echo $datum['metroe_end2_telkom']." ".$datum['port_end2_telkom']." ";
//            $openapi = OpenAPI::where('name', $datum['metroe_end2_telkom'])
//                ->where('port', $datum['port_end2_telkom'])
//                ->first();
//            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//            echo "<br>";
//        }

//        // IPBB
//        $data = DatekIpbb::whereNotNull('router_node_a')->get();
//        foreach ($data as $datum) {
//            $datum['port_telkom'] = str_replace("Ex","",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("Ag","lag-",$datum['port_telkom']);
//            echo $datum['router_node_a']." ".$datum['port_end1_tsel']." ";
//            $openapi = OpenAPI::where('name', $datum['router_node_a'])
//                ->where('port', $datum['port_end1_tsel'])
//                ->first();
//            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//            echo "<br>";
//        }

//        // IX
//        $data = DatekIx::whereNotNull('metroe_end1_telkom')->get();
//        foreach ($data as $datum) {
//            $datum['port_telkom'] = str_replace("Ex","",$datum['port_telkom']);
//            $datum['port_telkom'] = str_replace("Ag","lag-",$datum['port_telkom']);
//            echo $datum['metroe_end1_telkom']." ".$datum['port_end1_telkom']." ";
//            $openapi = OpenAPI::where('name', $datum['metroe_end1_telkom'])
//                ->where('port', $datum['port_end1_telkom'])
//                ->first();
//            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
//                $datum['utilization_max'] = $openapi['utilization'];
//                $datum->save();
//            }
//            echo "<br>";
//        }

    }



    public static function CleanDuplicate()
    {

        $backhaul = DatekBackhaul::where('reg_telkom','7')->get();
        foreach ($backhaul as $datum)
        {
            echo $datum->metroe_telkom." ".$datum->port_telkom."<br>";
            $cari = DatekBackhaul::where('reg_telkom','7')
                ->where('idbackhaul',"!=",$datum->idbackhaul)
                ->where('metroe_telkom',$datum->metroe_telkom)
                ->where('port_telkom',$datum->port_telkom)
                ->get();
            foreach ($cari as $hasil)
            {
                echo $hasil;
                $hasil->delete();
            }
            echo "";
        }

        echo "done";
    }

    public static function CheckAPILink()
    {

        $apis = API::where('layer','backhaul')->get();
        foreach ($apis as $datum)
        {
            echo $datum->url."<br>";
            $url = $datum->url;
            $client = new Client();
            $res = $client->request('GET', $url, [
                'auth' => ['940156', 'No5423676..']
            ]);

            $area = json_decode($res->getBody(), true);
            foreach ($area['d']['results'] as $data) {
                $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
                $data['Name'] = str_replace("Ex","",$data['Name']);
                $data['Name'] = str_replace("Ag","",$data['Name']);
                $data['Name']= str_replace("lag-","",$data['Name']);
                $data['Name'] = str_replace("GigabitEthernet","",$data['Name']);
                $data['Name'] = str_replace("Eth-Trunk","",$data['Name']);
                if(isset($openapi))
                {
                    $openapi['utilization'] = end($data['portmfs']['results'])['Value'];
                    $openapi['layer']="ix";
                    $openapi['reg_tsel']=$datum['reg_tsel'];
                    $openapi['reg_telkom']=$datum['reg_telkom'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." Update"."<br>";
                }
                else
                {

                    $openapi = new OpenAPI();
                    $openapi['name']=$data['device']['Name'];
                    $openapi['port']=$data['Name'];
                    $openapi['layer']="ix";
                    $openapi['reg_tsel']=$datum['reg_tsel'];
                    $openapi['reg_telkom']=$datum['reg_telkom'];

                    $openapi['utilization']=end($data['portmfs']['results'])['Value'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." New"."<br>";
                }

            echo $data['device']['Name']." ".$data['Name']." ";
            echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
            }
            echo "<br>";
        }

//        $apis = API::where('layer','ix')->get();
//        foreach ($apis as $datum)
//        {
//            echo $datum->url."<br>";
//            $url = $datum->url;
//            $client = new Client();
//            $res = $client->request('GET', $url, [
//                'auth' => ['940156', 'No5423676..']
//            ]);
//
//            $area = json_decode($res->getBody(), true);
//            foreach ($area['d']['results'] as $data) {
//                $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
//                if(isset($openapi))
//                {
//                    $openapi['utilization'] = end($data['portmfs']['results'])['Value'];
//                    $openapi['layer']="ix";
//                    $openapi['reg_tsel']=$datum['reg_tsel'];
//                    $openapi['reg_telkom']=$datum['reg_telkom'];
//                    if(isset($openapi['utilization']))
//                    {
//                        $openapi->save();
//                        echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." Update"."<br>";
//                    }
//
//
//                }
//                else
//                {
//                    $openapi = new OpenAPI();
//                    $openapi['name']=$data['device']['Name'];
//                    $openapi['port']=$data['Name'];
//                    $openapi['layer']="ix";
//                    $openapi['reg_tsel']=$datum['reg_tsel'];
//                    $openapi['reg_telkom']=$datum['reg_telkom'];
//
//                    $openapi['utilization']=end($data['portmfs']['results'])['Value'];
//                    if(isset(end($data['portmfs']['results'])['Value']))
//                    {
//                        $openapi->save();
//                        echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results']['Value'])." New"."<br>";
//                    }
//
//
//                }
//
//                echo $data['device']['Name']." ".$data['Name']." ";
//                //echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
//            }
//            echo "<br>";
//        }

        echo "done";
    }

    public static function CheckLDAP()
    {
        $ldap = new LDAP();
        $result = $ldap->auth("940156","No5423676..");

        return $result;
    }

    public static function CheckSetting()
    {
        $ldap = Setting::where('key','ldap_id')->first()->value;
        $password = Setting::where('key','ldap_password')->first()->value;

        return $ldap." & ".$password;
    }

    public static function EditMetroeEnd1TelkomIx($id, $param)
    {
        DB::table('datek_ix')->where('idix',$id)->update([
            'metroe_end1_telkom' => $param,
            'updated_at'         => date("Y:m:d H:i:s")
        ]);       
        $data = "Sukses Perubahan";  
        echo json_encode($data);
    }

    public static function EditRouterEnd1TelkomIx($id, $param)
    {

        $dataK = DB::table('datek_ix')->where('idix',$id)->update([
            'router_end1_tsel' => $param,
            'updated_at'       => date("Y:m:d H:i:s")
        ]);       

        if($dataK == true) {
            $data = "Sukses Perubahan";  
        } else {
            $data = "Gagal Perubahan";  
        }

        echo json_encode($data);
    }
}
