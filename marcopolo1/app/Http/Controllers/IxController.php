<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewDatekIx;
use App\Metro;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Redirect;

class IxController extends Controller
{
    public function DetailIx($regional)
    {
        if($regional==0)
        {
            $Ix = NewDatekIx::get();
        }
        else {
            $Ix= NewDatekIx::where('reg_tsel', $regional)->get();

        }
        return view('modul_inventory.ix_detail')->with('ix', $Ix)->with('regional',$regional);
    }

    public function editket($id, $var){
        $update = NewDatekIx::find($id)->update([
            'keterangan'    => $var,
        ]);
        $resp = $update;
        return 1;

    }
    public function editkota($id, $var){
        $update = NewDatekIx::find($id)->update([
            'kota_tsel'    => $var,
        ]);
        $resp = $update;
        return 1;

    }
    public function updateSto($id, $value) {
        $olala  = trim($value);
        $update = Metro::where('host_name', $id)->update([
            'lokasi'        => $olala,
        ]);
        $resp = $update;
        return $resp;
    }

}
