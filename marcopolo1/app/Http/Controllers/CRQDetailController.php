<?php

namespace App\Http\Controllers;

use App\CRQDetail;
use Illuminate\Http\Request;

class CRQDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crqdetails = CRQDetail::all();
        return view('index')->with('rcadetails', $crqdetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $crqdetail = new CRQDetail();
        $crqdetail->idactivity = $request->idactivity;
        $crqdetail->lokasi = $request->lokasi;
        $crqdetail->tgl_mulai = $request->tgl_mulai;
        $crqdetail->jam_mulai = $request->jam_mulai;
        $crqdetail->tgl_selesai = $request->tgl_selesai;
        $crqdetail->jam_selesai = $request->jam_selesai;
        $crqdetail->pic = $request->pic;
        $crqdetail->crq = $request->crq;
        $crqdetail->status = $request->status;
        $crqdetail->ket = $request->ket;
        $crqdetail->save();
        $crqdetail->iddetail = $crqdetail->id ;
        $datum = CRQDetail::where('created_at',$crqdetail->created_at)->first();
        return response()->json($datum);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($detail_id)
    {
        $crqdetail = CRQDetail::where('iddetail',$detail_id)->first();
        return response()->json($crqdetail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $detail_id)
    {
        $crqdetail = CRQDetail::find($detail_id);
        $crqdetail->lokasi = $request->lokasi;
        $crqdetail->tgl_mulai = $request->tgl_mulai;
        $crqdetail->jam_mulai = $request->jam_mulai;
        $crqdetail->tgl_selesai = $request->tgl_selesai;
        $crqdetail->jam_selesai = $request->jam_selesai;
        $crqdetail->pic = $request->pic;
        $crqdetail->crq = $request->crq;
        $crqdetail->status = $request->status;
        $crqdetail->ket = $request->ket;
        $crqdetail->save();
        return response()->json($crqdetail);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($detail_id)
    {
        $crqdetail = CRQDetail::destroy($detail_id);
        return response()->json($crqdetail);
    }
}
