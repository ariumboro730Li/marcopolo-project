<?php

namespace App\Http\Controllers;

use App\Classes\LDAP;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function User()
    {
        //$user = User::whereHas('roles', function($q){$q->whereIn('name', ['656777']);})->get();
        $user = User::all();
        return view('setting.user.index')->with('user',$user);
    }
    public function HapusUser($iduser)
    {

        User::destroy($iduser);

        return Redirect::back();
    }
    public function DetailUser($iduser)
    {
        $user = User::where('id',$iduser)->first();
        $keterangan = "Detail";
        return view('setting.user.detail')->with('user',$user)->with('keterangan',$keterangan);
    }
    public function EditUser($iduser)
    {
        $user = User::where('id',$iduser)->first();
        $keterangan = "Edit";

        return view('setting.user.detail')->with('user',$user)->with('keterangan',$keterangan);
    }
    public function UpdateUser(Request $request)
    {
        $input = $request->all();
        $role = Role::where('id', $input['access'])->first();
        $user = User::where('id',$input['id'])->first();
        //$user->id = $input['id'];
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->ldap = $input['ldap'];
        if(isset($input['password']))
            $user->password = Hash::make($input['password']);
        $user->save();
        $user->roles()->detach();
        $user->roles()->attach($role);

        //return Redirect::back() ;
        return Redirect('setting/user') ;
    }
    public function TambahUser()
    {
        $keterangan = null;


        return view('setting.user.detail')->with('keterangan',$keterangan);
    }
    public function TambahUserBaru(Request $request)
    {
        $input = $request->all();
        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->ldap = $input['ldap'];
        $user->save();

        return Redirect('setting/user') ;
    }


    public function LoginLDAP(Request $request)
    {
//        $this->validate($request, [
//            'email' => 'required|string',
//            'password' => 'required|string',
//        ]);

//        return Redirect('setting/user') ;

//
        $input = $request->all();
//
//        dd($input);
//
        $ldap = new LDAP();
//        //$result = $ldap->auth("940156","No5423676..");
//        $result = $ldap->auth($input['email'],$input['password']);
//
//        if ($result==1)
        $hasil =$ldap->info("940156");
        return dd($hasil[0]);
//        else
//        return $result;


        //return $ldap->info("940156")." ".$result;
        //$user = User::whereHas('roles', function($q){$q->whereIn('name', ['656777']);})->get();
//        $user = User::all();
//        return view('setting.user.index')->with('user',$user);
    }
}
