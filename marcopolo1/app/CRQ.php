<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CRQ extends Model
{
    use SoftDeletes;
    protected $table = 'crq';
	protected $primaryKey = 'idactivity';

	protected $fillable = [
        'idactivity', 'activity', 'reg_telkom', 'reg_tsel', 'impact_service'
    ];

    public function detail()
    {
        return $this->hasMany('App\CRQDetail');
    }

    public static function getCRQData($idactivity)
    {
        return CRQ::find($idactivity);
    }
}