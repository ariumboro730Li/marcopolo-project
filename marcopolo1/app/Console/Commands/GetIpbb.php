<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekIpbb;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetIpbb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Ipbb:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            ]
        ];

        $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
        //$file = file_get_contents('http://www.example.com/', false, $context);


        $jsonurl = "http://10.62.164.166/api/get/ipbb";
        $json = file_get_contents($jsonurl, false, $context);
        $bitcoin = json_decode($json);
        //return response()->json($bitcoin);
        foreach (json_decode($json, true) as $area)
        {
            //print_r($area); // this is your area from json response
            $ipbb = DatekIpbb::where('router_node_a',$area['nearend'])->where('router_node_b',$area['farend'])->first();
            if (isset($ipbb))
            {

                $ipbb->status = $area['status'];
                $ipbb->capacity = $area['capacity'];
                $ipbb->system = "DWDM";
                $ipbb->date_undetected = null;
                $ipbb->tglcheck=Carbon::now();
                //$ipran->date_detected = date("Y-m-d");
                $ipbb->save();
            }
            else
            {
                $datek = new DatekIpbb();
                $datek->reg_tsel=$area['reg'];
                $datek->router_node_a=$area['nearend'];
                $datek->port_end1_tsel=$area['portnearend'];
                $datek->router_node_b=$area['farend'];
                $datek->port_end2_tsel=$area['portfarend'];
                $datek->reg_telkom=$area['reg'];
                $datek->capacity=$area['capacity'];
                $datek->layer="IPBB";
                $datek->system="DWDM";
                $datek->date_detected=date("Y-m-d");
                $datek->status=$area['status'];
                $datek->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Get IPBB";
        $command->type = "GET";
        $command->save();
    }
}
