<?php

namespace App\Console\Commands;

use App\CommandHistory;
use Illuminate\Console\Command;
use App\DatekAccess;
use App\DatekBackhaul;
use App\DatekIpbb;
use App\DatekIpran;
use App\DatekIx;
use App\DatekSummary;
use Carbon\Carbon;

class GetUndetected extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Undetected:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $access = DatekAccess::all();
//        foreach ($access as $data)
//        {
//
//            if(!$data['updated_at']->isToday())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//        }
//
//        $backhaul = DatekBackhaul::where('tglcheck', '!=', Carbon::today())->get;
//        foreach ($backhaul as $data)
//        {
//            if(!Carbon::parse($data['tglcheck'])->toDateString()==Carbon::now()->toDateString())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//            else{
//                $data['date_undetected']=null;
//                $data->save();
//            }
//        }

        $backhaul = DatekBackhaul::whereDate('tglcheck','<>'. Carbon::today()->toDateString())
            ->orWhereNull('tglcheck')->get();
        foreach ($backhaul as $data)
        {
            $data['date_undetected']=Carbon::now();
            $data->save();
        }

        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            if(!Carbon::parse($data['tglcheck'])->toDateString()==Carbon::now()->toDateString())
            {
                $data['date_undetected']=Carbon::now();
                $data->save();
            }
            else{
                $data['date_undetected']=null;
                $data->save();
            }

        }

//        $ipbb = DatekIpbb::all();
//        foreach ($ipbb as $data)
//        {
//            if(!$data['updated_at']->isToday())
//            {
//                $data['date_undetected']=Carbon::now();
//                $data->save();
//            }
//        }

        $ix = DatekIx::all();
        foreach ($ix as $data)
        {
            if(!$data['updated_at']->toDateString()==Carbon::now()->toDateString())
            {
                $data['date_undetected']=Carbon::now();
                $data->save();
            }
            else{
                $data['date_undetected']=null;
                $data->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Check Undetected";
        $command->type = "CHECK";
        $command->save();

    }
}
