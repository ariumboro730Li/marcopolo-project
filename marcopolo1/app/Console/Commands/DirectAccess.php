<?php

namespace App\Console\Commands;

use App\Http\Controllers\AccessDirectController\GetAccessDirectController;
use Illuminate\Console\Command;

class DirectAccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DirectAccess:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch data API Access Direct Metro';

    /**
     * Create a new command instance.
     *P
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->api = new GetAccessDirectController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $this->api->getapiDirectMetroReg1(1);
            $this->api->getapiDirectMetroReg1(2);
            $this->api->getapiDirectMetroReg1(3);
            $this->api->getapiDirectMetroReg1(6);
            $this->api->getapiDirectMetroReg1(7);
    }
}
