<?php

namespace App\Console\Commands;

use App\CommandHistory;
use Illuminate\Console\Command;

class GetBackhaulUtil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BackhaulUtil:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ldap_username = Setting::where('key','ldap_id')->first()->value;
        $ldap_password = Setting::where('key','ldap_password')->first()->value;
        // REGIONAL1 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID,%20aggregate(portmfs(im_Utilization%20with%20max%20as%20Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower(%27telkomsel%20region:reg-01(sumbagut):backhaul%27),%20tolower(groups/GroupPathLocation))%20eq%20true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username, $ldap_password]
        ]);

        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
//
//            echo $data['device']['Name']." ";
//            echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
        }

        // REGIONAL2 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-02(sumbagsel):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL3 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-03(jabo):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 4 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-04(jabar):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 5 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-05(jateng):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 6 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-06(jatim):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 7 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-07(BALNUS):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 8 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-08(KALIMANTAN):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 9 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-09(SULAWESI):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 10 (TSEL):
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('TELKOMSEL REGION:reg-10(sumbagteng):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        // REGIONAL 11 (TSEL)
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-11(puma):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);
        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data)
        {
            $backhaul = DatekBackhaul::where('metroe_telkom',$data['device']['Name'])->first();
            if((0.01*number_format(end($data['portmfs']['results'])['Value']))>0)
            {
                $backhaul['utilization_max']=(0.01*number_format(end($data['portmfs']['results'])['Value']));
                $backhaul->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Get Utilization Backhaul";
        $command->type = "GET";
        $command->save();
    }
}
