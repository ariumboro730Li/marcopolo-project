<?php

namespace App\Console\Commands;

use App\CommandHistory;
use Illuminate\Console\Command;
use App\Http\Controllers\IpbbController;

class GrabRegionIpbb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'IpbbRegion:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'untuk mengambil data region href dan mrtg IPBB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        
        // Memanggil Controller IPBB
        $this->get_region = new IpbbController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // memanggil update fungsi update region Jakabaring
        $this->get_region->scrapRegion();

        // memanggil fungsi update href & mrtg jakabaring
        $this->get_region->getHrefDataJakabaring();

        $command = new CommandHistory();
        $command->name = "Grab Region, href & mrtg jakabaring";
        $command->type = "GET";
        $command->save();

    }
}
