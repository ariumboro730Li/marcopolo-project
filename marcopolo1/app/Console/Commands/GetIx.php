<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekIx;
use App\NewDatekIx;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetIx extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Ix:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            ]
        ];

        $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
        //$file = file_get_contents('http://www.example.com/', false, $context);


        $jsonurl = "http://10.62.164.166/api/jovice/get/ix";
        $json = file_get_contents($jsonurl, false, $context);
        $bitcoin = json_decode($json);
        //return response()->json($bitcoin);
        // foreach (json_decode($json, true) as $area)
        // {
        //     //print_r($area); // this is your area from json response
        //     $ix = DatekIx::where('router_end1_tsel',$area['router_end1_tsel'])->where('port_end1_tsel',$area['port_end1_tsel'])->first();
        //     if (isset($ix))
        //     {

        //         $ix->status = $area['status'];
        //         $ix->capacity = $area['speed'];
        //         $ix->date_undetected = null;
        //         $ix->description = $area['description'];
        //         $ix->tglcheck=Carbon::now();
        //         //$ipran->date_detected = date("Y-m-d");
        //         $ix->save();
        //     }
        //     else
        //     {
        //         $ix = new DatekIx();
        //         $ix->reg_tsel = $area['aw_ag'];
        //         $ix->reg_telkom = $area['aw_ag'];
        //         $ix->router_end1_tsel = $area['router_end1_tsel'];
        //         $ix->metroe_end1_telkom = $area['metroe_end1_telkom'];
        //         $ix->port_end1_tsel = $area['port_end1_tsel'];
        //         $ix->port_end1_telkom = $area['port_end1_telkom'];
        //         $ix->status = $area['status'];
        //         $ix->capacity = $area['capacity'];
        //         $ix->layer = "IX";
        //         $ix->system = "Direct Core";
        //         $ix->date_detected = date("Y-m-d");
        //         $ix->description = $area['description'];
        //         $ix->save();
        //     }
        // }

        foreach (json_decode($json, true) as $area)
        {
            //print_r($area); // this is your area from json response
            $ix = NewDatekIx::where('routername_tsel',$area['router_end1_tsel'])->where('port_tsel',$area['port_end1_tsel'])->first();
            if (isset($ix))
            {

                $ix->status = $area['status'];
                $ix->capacity = $area['speed'];
                $ix->date_undetected = null;
                $ix->description = $area['description'];
                // $ix->date_detected = Carbon::now();
                //$ipran->date_detected = date("Y-m-d");
                $ix->save();
            }
            else
            {
                $ix = new NewDatekIx();
                $ix->reg_tsel = $area['aw_ag'];
                $ix->reg_telkom = $area['aw_ag'];
                $ix->routername_tsel = $area['router_end1_tsel'];
                $ix->pe_telkom = $area['metroe_end1_telkom'];
                $ix->port_tsel = $area['port_end1_tsel'];
                $ix->port_telkom = $area['port_end1_telkom'];
                $ix->status = $area['status'];
                $ix->capacity = $area['capacity'];
                $ix->layer = "IX";
                $ix->system = "Direct Core";
                $ix->date_detected = date("Y-m-d");
                $ix->description = $area['description'];
                $ix->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Get IX";
        $command->type = "GET";
        $command->save();
    }
}
