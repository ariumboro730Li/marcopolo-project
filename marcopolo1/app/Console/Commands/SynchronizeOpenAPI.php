<?php

namespace App\Console\Commands;

use App\API;
use App\CommandHistory;
use App\OpenAPI;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class SynchronizeOpenAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Synchronize:OpenAPI';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $apis = API::all();
        foreach ($apis as $datum)
        {
            echo $datum->url."<br>";
            $url = $datum->url;
            $client = new Client();
            $res = $client->request('GET', $url, [
                'auth' => ['940156', 'No5423676..']
            ]);

            $area = json_decode($res->getBody(), true);
            foreach ($area['d']['results'] as $data) {
                $openapi = OpenAPI::where('name',$data['device']['Name'])->where('port',$data['Name'])->first();
                $data['Name'] = str_replace("Ex","",$data['Name']);
                $data['Name'] = str_replace("Ag","",$data['Name']);
                $data['Name']= str_replace("lag-","",$data['Name']);
                $data['Name'] = str_replace("GigabitEthernet","",$data['Name']);
                $data['Name'] = str_replace("Gi.","",$data['Name']);
                $data['Name'] = str_replace("Eth-Trunk","",$data['Name']);
                if(isset($openapi))
                {
                    $openapi['utilization'] = end($data['portmfs']['results'])['Value'];
                    $openapi['layer']=$apis['layer'];
                    $openapi['reg_tsel']=$data['reg_tsel'];
                    $openapi['reg_telkom']=$data['reg_telkom'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." Update"."<br>";
                }
                else
                {

                    $openapi = new OpenAPI();
                    $openapi['name']=$data['device']['Name'];
                    $openapi['port']=$data['Name'];
                    $openapi['layer']=$apis['layer'];
                    $openapi['reg_tsel']=$data['reg_tsel'];
                    $openapi['reg_telkom']=$data['reg_telkom'];

                    $openapi['utilization']=end($data['portmfs']['results'])['Value'];
                    if(isset($openapi['utilization']))
                    {
                        $openapi->save();
                    }

                    echo $openapi['name']." ".$openapi['port']." ".end($data['portmfs']['results'])['Value']." New"."<br>";
                }

                echo $data['device']['Name']." ".$data['Name']." ";
                echo (0.01*number_format(end($data['portmfs']['results'])['Value']))."<br> ";
            }
            echo "<br>";


        }


        $command = new CommandHistory();
        $command->name = "Get OpenAPI";
        $command->type = "GET";
        $command->save();

    }



}
