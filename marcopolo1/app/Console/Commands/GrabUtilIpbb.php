<?php

namespace App\Console\Commands;

use App\CommandHistory;
use Illuminate\Console\Command;
use App\Http\Controllers\IpbbController;

class GrabUtilIpbb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UtilIpbb:grab';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk mengambil data utilization Ipbb';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        // Memanggil Controller IPBB
        $this->get_util = new IpbbController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Memanggil fungsi update Util Ipbb
        $this->get_util->getUtilScrapPbb();

        $command = new CommandHistory();
        $command->name = "Grab Util IPBB";
        $command->type = "GET";
        $command->save();

    }
}
