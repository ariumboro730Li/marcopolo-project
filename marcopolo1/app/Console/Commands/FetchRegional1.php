<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\OpenAPI;
use App\Setting;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class FetchRegional1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Regional1:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ldap_username = Setting::where('key','ldap_id')->first()->value;
        $ldap_password = Setting::where('key','ldap_password')->first()->value;

        //return $ldap->value." ".$password->value;
        // REGIONAL1
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=8h&bh=Sun-Sat 19:00-23:00&$"."expand=device,portmfs&$"."select=Name,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('metro:metro divre 1'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('TSEL'), tolower(Description)) eq true) or (substringof(tolower('IPRAN'), tolower(Description)) eq true)))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);


        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            $openapi = OpenAPI::where('regional',1)
                ->where('name',$data['device']['Name'])
                ->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (end($data['portmfs']['results'])['Value']);
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['regional']=1;
                $openapi['utilization']=(end($data['portmfs']['results'])['Value']);
                $openapi->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Fetch OpenAPI Regional 1";
        $command->type = "FETCH";
        $command->save();

        // REGIONAL1
        $url = "http://10.62.166.182:8581/odata/api/interfaces?&resolution=HOUR&period=1d&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,SpeedIn,SpeedOut,device/Name,portmfs/im_Utilization&$"."filter=((substringof(tolower('telkomsel region:reg-01(sumbagut):backhaul'), tolower(groups/GroupPathLocation)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);


        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {

            $data['Name'] = str_replace("Ex","",$data['Name']);
            $data['Name'] = str_replace("Ag","",$data['Name']);
            $data['Name'] = str_replace("lag-","",$data['Name']);
            $data['Name'] = str_replace("GigabitEthernet","",$data['Name']);
            $data['Name'] = str_replace("Gi.","",$data['Name']);
            $data['Name'] = str_replace("Eth-Trunk","",$data['Name']);


            $openapi = OpenAPI::where('regional',1)
                ->where('name',$data['device']['Name'])
                ->where('port',$data['Name'])->first();
            if(isset($openapi))
            {
                $openapi['utilization'] = (end($data['portmfs']['results'])['Value']);
                $openapi->save();
            }
            else
            {
                $openapi = new OpenAPI();
                $openapi['name']=$data['device']['Name'];
                $openapi['port']=$data['Name'];
                $openapi['regional']=1;
                $openapi['utilization']=(end($data['portmfs']['results'])['Value']);
                $openapi->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Fetch OpenAPI Regional 1";
        $command->type = "FETCH";
        $command->save();
    }
}
