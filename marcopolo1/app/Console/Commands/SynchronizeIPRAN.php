<?php

namespace App\Console\Commands;

use App\DatekIpran;
use App\OpenAPI;
use Illuminate\Console\Command;

class SynchronizeIPRAN extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Synchronize:IPRAN';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
                // IPRAN
        $data = DatekIpran::whereNotNull('metroe_end2_telkom')->get();
        foreach ($data as $datum) {
//            $datum['port_end1_telkom'] = str_replace("Ex","",$datum['port_end1_telkom']);
//            $datum['port_end1_telkom'] = str_replace("Ag","lag-",$datum['port_end1_telkom']);
//            $datum['port_end1_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_end1_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Ex","",$datum['port_end2_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Ag","lag-",$datum['port_end2_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_end2_telkom']);
            //$datum['port_end2_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_end2_telkom']);
            echo $datum['metroe_end2_telkom']." ".$datum['port_end2_telkom']." ";
            $openapi = OpenAPI::where('name', $datum['metroe_end2_telkom'])
                ->where('port','like', '%' .  $datum['port_end2_telkom']. '%')
                ->first();
            if (isset($openapi)) {
                echo $openapi['utilization']." ";
                $datum['utilization_max'] = $openapi['utilization'];
                $datum->save();
            }
            echo "<br>";
        }


        $command = new CommandHistory();
        $command->name = "Synchronize IPRAN";
        $command->type = "SYNCHRONIZE";
        $command->save();
    }
}
