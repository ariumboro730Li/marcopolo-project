<?php

namespace App\Console;

use App\Console\Commands\SynchronizeOpenAPI;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\BackupDatabase::class,
        Commands\FetchRegional1::class,
        Commands\FetchRegional2::class,
        Commands\FetchRegional3::class,
        Commands\FetchRegional4::class,
        Commands\FetchRegional5::class,
        Commands\FetchRegional6::class,
        Commands\FetchRegional7::class,
        Commands\GetBackhaul::class,
        Commands\GetBackhaulUtil::class,
        Commands\GetIpbb::class,
        Commands\GetIpran::class,
        Commands\GetIx::class,
        Commands\GetOneIPMPLS::class,
        Commands\GetSummary::class,
        Commands\GetUcap::class,
        Commands\GetUmax::class,
        Commands\GetUndetected::class,
        Commands\GetUtilAccess::class,
        Commands\SynchronizeAccess::class,
        Commands\SynchronizeBackhaul::class,
        Commands\SynchronizeIPRAN::class,
        Commands\SynchronizeIPBB::class,
        Commands\SynchronizeIX::class,
        SynchronizeOpenAPI::class
    ];

    /** 
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    // scheduler data direct access metro setiap menit
    // $schedule->command('DirectAccess:get')->everyMinute();
    // $schedule->command('Backhaul:Get')->everyMinute();
    // $schedule->command('BackhaulUtil:Get')->everyMinute();
    // $schedule->command('Ix:Get')->everyMinute();
    // $schedule->command('Synchronize:IX')->everyMinute();
    // $schedule->command('OneIPMPLS:Get')->everyMinute();
    // $schedule->command('IpbbRegion:get')->everyFiveMinutes();

    // scheduler Util Ipbb setiap 15 menit
    $schedule->command('UtilIpbb:grab')->everyFifteenMinutes();

    // Scheduler Region dan Href & Mrtg Jakabaring setiap sebulan sekali
    $schedule->command('IpbbRegion:get')->monthlyOn(11, '16:15');



    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
