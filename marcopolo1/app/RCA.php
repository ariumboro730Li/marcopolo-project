<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RCA extends Model
{
    use SoftDeletes;
    protected $table = 'rca';
	protected $primaryKey = 'idrca';

    public function detail()
    {
        return $this->hasMany('App\RCADetail','idrca','idrca');
    }

    public static function getRCAData($idactivity)
    {
        return RCA::find($idactivity);
    }


    public static function TotalRegional($regional)
    {
        return 2*$regional;
    }

    public static function TotalIncident()
    {
        return 70;
    }

    public static function RootCauseMetro()
    {
        return 26451;
    }

    public static function RootCauseRadio()
    {
        return 15787;
    }
}
