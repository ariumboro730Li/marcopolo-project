<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CRQDetail extends Model
{
    use SoftDeletes;
    protected $table = 'crq_detail';
	protected $primaryKey = 'iddetail';

	protected $fillable = [
        'iddetail', 'lokasi','waktu_mulai','waktu_selesai', 'tgl_mulai', 'tgl_selesai', 'jam_mulai', 'jam_selesai', 'pic','crq','status','ket','idactivity'
    ];

    public function activity()
    {
        return $this->hasOne('App\CRQ');
    }

    public static function getCRQDetailData($idactivity)
    {
        return CRQDetail::where('idactivity',$idactivity)->get();
    }
}