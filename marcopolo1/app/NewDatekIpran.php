<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NewDatekIpran extends Model
{
        use SoftDeletes;
        protected $table = 'new_datek_ipran';
        protected $primaryKey = 'idipran';
    
        protected $fillable = [
            'idipran', 'link', 'reg_tsel_a', 'kota_router_node_a', 'router_node_a', 'port_tsel_end_a', 'reg_tsel_b', 'kota_router_node_b', 'router_node_b', 'port_tsel_end_b', 'reg_telkom_a', 'capacity', 'utilization_cap', 'utilization_in%', 'utilization_out%', 'utilization_max%', 'layer', 'system', 'transport_type' 
        ];
        
        public static function Link($regional)
        {
            return NewDatekIpran::where('reg_tsel_a',$regional)->count();
        }
        public static function Capacity($regional)
        {
            return NewDatekIpran::where('reg_tsel_a',$regional)->sum('capacity');
        }
        public static function UtilizationCap($regional)
        {
            return NewDatekIpran::where('reg_tsel_a',$regional)->where('utilization_max%')->sum('utilization_cap');
        }
        public static function AllLink()
        {
            return NewDatekIpran::count();
        }
        public static function AllCapacity()
        {
            return NewDatekIpran::sum('capacity');
        }    
        public static function AllLinkWithUndetected()
        {
            return NewDatekIpran::all()->count();
        }
        public static function AllUtilizationCap()
        {
            return NewDatekIpran::whereNotNull('utilization_max%')->sum('utilization_cap');
        }
    
    
    
    
}
