<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class RelasiIpbb extends Model
{
    // use SoftDeletes;
    protected $table = 'transport_ipbb';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id_ipbb', 'row', 'status', 'ne_transport', 'shelf_slot_port', 'tie_line'
    ];

    
    public static function NeTransportCount($id)
    {
        $RelasiIpbb =  RelasiIpbb::where('id_ipbb',$id)->count();
        if(isset($RelasiIpbb))
        {
            // return $RelasiIpbb->ne_transport;
            return $RelasiIpbb;
        }
        else{
            return " ";
        }

    }

    public static function BoardTransportCount($id)
    {
        $RelasiIpbb =  RelasiIpbb::where('id_ipbb',$id)->count();
        if(isset($RelasiIpbb))
        {
            // return $RelasiIpbb->ne_transport;
            return $RelasiIpbb;
        }
        else{
            return " ";
        }

    }

}
