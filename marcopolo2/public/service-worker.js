importScripts('https://unpkg.com/workbox-sw@0.0.2/build/importScripts/workbox-sw.dev.v0.0.2.js');
importScripts('https://unpkg.com/workbox-runtime-caching@1.3.0/build/importScripts/workbox-runtime-caching.prod.v1.3.0.js');
importScripts('https://unpkg.com/workbox-routing@1.3.0/build/importScripts/workbox-routing.prod.v1.3.0.js');

const assetRoute = new workbox.routing.RegExpRoute({
    regExp: new RegExp('^https://marcxopolo.telkom.co.id/*'),
    handler: new workbox.runtimeCaching.NetworkFirst()
});

const router = new workbox.routing.Router();
//router.addFetchListener();
router.registerRoutes({routes: [assetRoute]});
router.setDefaultHandler({
    handler: new workbox.runtimeCaching.NetworkFirst()
});


// (function() {
//     'use strict';
//
//     var CACHE_NAME = 'static-cache';
//     var urlsToCache = [
//     '/',
//     '/css/style.css',
//     '/css/themes/all-themes.css',
//     '/css/bs-editable.css',
//     '/css/select2.css',
//     '/plugins/bootstrap/css/bootstrap.css',
//     '/plugins/jquery/jquery.min.js',
//     '/plugins/bootstrap/js/bootstrap.js',
//     '/plugins/bootstrap-select/js/bootstrap-select.js',
//     '/plugins/jquery-slimscroll/jquery.slimscroll.js',
//     '/js/admin.js',
//     '/js/pages/index.js',
//     '/js/demo.js',
//     '/images/deisna.png'
//     ];
//
//     self.addEventListener('install', function(event) {
//     event.waitUntil(
//         caches.open(CACHE_NAME)
//         .then(function(cache) {
//         return cache.addAll(urlsToCache);
//         })
//     );
//     });
//
//     self.addEventListener('activate', function(e) {
//         console.log('[ServiceWorker] Activate');
//         e.waitUntil(
//             caches.keys().then(function(keyList) {
//               return Promise.all(keyList.map(function(key) {
//                 if (key !== CACHE_NAME) {
//                   console.log('[ServiceWorker] Removing old cache', key);
//                   return caches.delete(key);
//                 }
//               }));
//             })
//         );
//         return self.clients.claim();
//     });
//
//     self.addEventListener('fetch', function(event) {
//         console.log('[ServiceWorker] Fetch', event.request.url);
//         event.respondWith(
//         caches.match(event.request)
//         .then(function(response) {
//             return response || fetchAndCache(event.request);
//         })
//         );
//     });
//
//     function fetchAndCache(url) {
//         return fetch(url)
//         .then(function(response) {
//         // Check if we received a valid response
//         if (!response.ok) {
//             throw Error(response.statusText);
//         }
//         return caches.open(CACHE_NAME)
//         .then(function(cache) {
//             cache.put(url, response.clone());
//             return response;
//         });
//         })
//         .catch(function(error) {
//         console.log('Request failed:', error);
//         // You could return a custom offline 404 page here
//         });
//     }
//
// })();