// Create a graph object
var graph = {

// Define Nodes
nodes: [
    { id: "r1", label: "Reg_1", type: 'image', url: "https://img.icons8.com/nolan/64/000000/radio-tower.png", x: -4, y: 1, size: 3, color: '#008cc2' },
    { id: "r2", label: "Reg_2", x: -2, y: 4.7, size: 3, color: '#008cc2' },
    { id: "r3", label: "Reg_3", x: -1.4, y: 5, size: 3, color: '#008cc2' },
    { id: "r4", label: "Reg_4", x: -0.8, y: 5.3, size: 3, color: '#008cc2' },
    { id: "r5", label: "Reg_5", x: 0.3, y: 5.5, size: 3, color: '#008cc2' },
    { id: "r6", label: "Reg_6", x: 1.3, y: 5.3, size: 3, color: '#008cc2' },
    { id: "r7", label: "Reg_7", x: 2.5, y: 5.3, size: 3, color: '#008cc2' },
    { id: "r8", label: "Reg_8", x: 1, y: 2.7, size: 3, color: '#008cc2' },
    { id: "r9", label: "Reg_9", x: 2.5, y: 3, size: 3, color: '#008cc2' },
    { id: "r10", label: "Reg_10", x: -2.3, y: 3.2, size: 3, color: '#008cc2' },
    { id: "r11", label: "Reg_11", x: 5.2, y: 2.7, size: 3, color: '#008cc2' }
  ],

// Define Edges for Node routes
edges: [
    { id: "e0", source: "r2", target: "r1", color: '#FF0000', type:'line', size:2},
    { id: "e1", source: "r3", target: "r2", color: '#FF0000', type:'line', size:2},
    { id: "e2", source: "r5", target: "r4", color: '#FF0000', type:'line', size:2},
    { id: "e3", source: "r6", target: "r5", color: '#FF0000', type:'line', size:2},
    { id: "e4", source: "r7", target: "r5", color: '#FF0000', type:'line', size:2},
    { id: "e5", source: "r8", target: "r6", color: '#FF0000', type:'line', size:2},
    { id: "e6", source: "r9", target: "r7", color: '#FF0000', type:'line', size:2},
    { id: "e7", source: "r10", target: "r1", color: '#FF0000', type:'line', size:2},
    { id: "e8", source: "r11", target: "r7", color: '#FF0000', type:'line', size:2}
  ]
}
console.log(graph);
// Initialise sigma:
var s = new sigma(
  {
    renderer: {
      container: document.getElementById('graph-container'),
      type: 'canvas'
    },
    settings: {
     minEdgeSize: 0.1,
     maxEdgeSize: 2,
     minNodeSize: 1,
     maxNodeSize: 8,
    }
  }
);

// Load the graph in sigma
s.graph.read(graph);
// Ask sigma to draw it
s.refresh();
