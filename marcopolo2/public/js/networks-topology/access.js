sigma.utils.pkg('sigma.canvas.nodes');

// Create a graph object
var g = {

  // Define Nodes
  nodes: [
      { id: "n1", label: "Node_1", type: 'topology', image: {clip: 3, scale: 3, url: 'https://img.icons8.com/nolan/64/000000/radio-tower.png'}, x: 0, y: 0.15, size: 5, color: '#008cc2' },
      { id: "n2", label: "Node_2", type: 'topology', image: {clip: 3, scale: 3, url: 'https://img.icons8.com/nolan/64/000000/radio-tower.png'}, x: 0.5, y: 0, size: 5, color: '#008cc2' },
      { id: "n3", label: "Node_3", type: 'topology', image: {clip: 3, scale: 3, url: 'https://img.icons8.com/nolan/64/000000/radio-tower.png'}, x: 0.5, y: 0.3, size: 5, color: '#008cc2' },
      { id: "n4", label: "Node_4", type: 'topology', image: {clip: 3, scale: 3, url: 'https://img.icons8.com/nolan/64/000000/radio-tower.png'}, x: 1, y: 0, size: 5, color: '#008cc2' }
    ],
  // Define Edges for Node routes
  edges: [
      { id: "e1", source: "n1", target: "n2", color: '#FF0000', type:'dashed', size:2},
      { id: "e2", source: "n1", target: "n3", color: '#FF0000', type:'line', size:2},
      { id: "e3", source: "n2", target: "n4", color: '#FF0000', type:'dashed', size:2}
    ]
  }

  // console.log(g.nodes);
  
// declare new node shapes
sigma.canvas.labels.topology = function(node, context, settings) {
  // declarations
  var prefix = settings('prefix') || '';
  var size = node[prefix + 'size'];
  var nodeX = node[prefix + 'x'];
  var nodeY = node[prefix + 'y'];
  var textWidth;
  // define settings
  context.fillStyle = node.textColor;
  context.lineWidth = size * 0.5;
  context.font = '400 ' + size + 'px AvenirNext';
  context.textAlign = 'center';
  context.fillText(node.label, nodeX, nodeY + size * 3);
  // measure text width
  textWidth = context.measureText(node.label).width
  node.labelWidth = textWidth; // important for clicks
};

s = new sigma({
  graph: g,
  renderer: {
    // IMPORTANT:
    // This works only with the canvas renderer, so the
    // renderer type set as "canvas" is necessary here.
    container: document.getElementById('graph-container'),
    type: 'canvas'
  },
  settings: {
    minNodeSize: 2,
    maxNodeSize: 16,
    textAlign: 'center'
  }
});
CustomShapes.init(s);
s.refresh();
s.bind('clickNode', function(e) {
  window.location.href = "http://marcxopolo.tes/inventory/access?site_id=" + e.data.node.id;
  //console.log(e)
})
