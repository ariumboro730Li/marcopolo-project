$(function () {
    new Chart(document.getElementById("line_chart").getContext("2d"), getChartJs('line'));
    new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
    new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
    new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
});

function getChartJs(type) {
    var config = null;

    if (type === 'line') {
        config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May"],
                datasets: [{
                    label: "Regional 1",
                    data: [28,48,40,19,96],
                    borderColor: "rgba(0, 188, 212, 1)",
                    backgroundColor: "rgba(0, 188, 212, 0.5)",
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "rgba(0, 188, 212, 0.9)"  ,
                    pointBorderWidth: 1
                }/*,
                {
                    label: "Regional 2",
                    data: [65,59,90,81,56],
                    backgroundColor: "rgba(220,220,220,0.5)",
                    borderColor: "rgba(220,220,220,1)",
                    pointBackgroundColor: "rgba(220,220,220,1)",
                    pointBorderColor: "#fff",
                    pointBorderWidth: 1

                },
                {
                    label: "Regional 3",
                    data : [50,68,17,57,24],
                    backgroundColor: "rgba(200,147,165,0.5)",
                    borderColor: "rgba(151,187,205,1)",
                    pointBackgroundColor: "rgba(151,187,205,1)",
                    pointBorderColor: "#fff",
                    pointBorderWidth: 1
                }*/]
            },
            options: {
                responsive: true,
                legend: false
            }
        }
    }
    else if (type === 'bar') {
        config = {
            type: 'bar',
            data: {
                labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
                datasets: [{
                    label: "January",
                    data: [65, 59, 80, 81, 56, 55, 53],
                    backgroundColor: "rgba(0, 188, 212, 0.8)",
                }/*,
                {
                    label: "February",
                    data: [65,59,90,81,56,55,45],
                    backgroundColor: "rgba(220,220,220,0.8)",
                    borderColor: "rgba(220,220,220, 1)"
                },
                {
                    label: "March",
                    data: [50,68,17,57,24,96,100],
                    backgroundColor: "rgba(200,147,165,0.8)",
                    borderColor: "rgba(200,147,165,1)"
                }*/]
            },
            options: {
                responsive: true,
                legend: false
            }
        }
    }
    else if (type === 'radar') {
        config = {
            type: 'radar',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "My First dataset",
                    data: [65, 25, 90, 81, 56, 55, 40],
                    borderColor: 'rgba(0, 188, 212, 0.8)',
                    backgroundColor: 'rgba(0, 188, 212, 0.5)',
                    pointBorderColor: 'rgba(0, 188, 212, 0)',
                    pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                    pointBorderWidth: 1
                }, {
                        label: "My Second dataset",
                        data: [72, 48, 40, 19, 96, 27, 100],
                        borderColor: 'rgba(233, 30, 99, 0.8)',
                        backgroundColor: 'rgba(233, 30, 99, 0.5)',
                        pointBorderColor: 'rgba(233, 30, 99, 0)',
                        pointBackgroundColor: 'rgba(233, 30, 99, 0.8)',
                        pointBorderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                legend: false
            }
        }
    }
    else if (type === 'pie') {
        config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [225, 50, 100, 40],
                    backgroundColor: [
                        "rgb(233, 30, 99)",
                        "rgb(255, 193, 7)",
                        "rgb(0, 188, 212)",
                        "rgb(139, 195, 74)"
                    ],
                }],
                labels: [
                    "Pink",
                    "Amber",
                    "Cyan",
                    "Light Green"
                ]
            },
            options: {
                responsive: true,
                legend: false
            }
        }
    }
    return config;
}