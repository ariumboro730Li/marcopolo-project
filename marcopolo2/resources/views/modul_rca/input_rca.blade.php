@extends('layout')

@section('menu2')
    class="active"
@stop


@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Input - RCA
@stop
    
@section('content')

    <section class="content" id="inputrca">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/rca')}}">RCA</a></li>
                    <li class="active">Input RCA</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>INPUT RCA</h2>
                                </div>
                            </div>
                        </div>

                        {!! Form::open(['method'=>'POST','action'=>'RCAController@TambahRCABaru','files'=> true]) !!}

                        <div class="body">                 

                            <div class="row clearfix">           
                                <div class="col-md-12">                            

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!!Form::text('title', '', array('class' =>"form-control", 'placeholder'=>"Title"))!!}
                                                    <!-- <input type="text" class="form-control" placeholder="" name="title" /> -->
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-xs-12 form-control-label">Regional</label>
                                        <div class="col-md-2 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                {!!Form::select('regional', array('Regional 1' => 'Regional 1', 'Regional 2' => 'Regional 2', 'Regional 3' => 'Regional 3', 'Regional 4' => 'Regional 4', 'Regional 5' => 'Regional 5', 'Regional 6' => 'Regional 6', 'Regional 7' => 'Regional 7'))!!}
                                                <!-- <select class="form-control show-tick" name="regional">
                                                        <option value="">-- Regional --</option>
                                                        <option value="Regional 1">Regional 1</option>
                                                        <option value="Regional 2">Regional 2</option>
                                                        <option value="Regional 3">Regional 3</option>
                                                        <option value="Regional 4">Regional 4</option>
                                                        <option value="Regional 5">Regional 5</option>
                                                        <option value="Regional 6">Regional 6</option>
                                                        <option value="Regional 7">Regional 7</option>
                                                    </select> -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">

                                        <label class="col-md-2 col-xs-12 form-control-label">Date & time</label>
                                        <div class="col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!!Form::text('date_time_open', null, ['class' => 'datetimepicker form-control', 'id'=>'date1', 'placeholder'=> 'Open'])!!}
                                                                         
                                                   <!--  <input type="text" id="date1" class="datetimepicker form-control" placeholder="Choose date & time..." name="date_time"> -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                {!!Form::text('date_time_close', null, ['class' => 'datetimepicker form-control', 'id'=>'date2', 'placeholder'=> 'Close'])!!}

                                                <!--  <input type="text" id="date1" class="datetimepicker form-control" placeholder="Choose date & time..." name="date_time"> -->
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-xs-12 form-control-label">Duration</label>
                                        <div class="col-md-2 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    {!!Form::text('duration', '', array('class' =>"form-control", 'placeholder'=>""))!!}
                                                    <!-- <input type="text" class="form-control" placeholder="" name="duration" />
                                                    } -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Network impact</label>
                                        <div class="col-md-10 col-xs-12">                        
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea rows="3" name="network_impact" class="form-control no-resize" placeholder="Type what you want..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Service Impact</label>
                                        <div class="col-md-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">                        
                                                    <textarea rows="3" name="service_impact" class="form-control no-resize" placeholder="Type what you want..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Root cause category</label>
                                        <div class="col-md-3 col-xs-12">    
                                            <div class="form-group">
                                                <div class="form-line">                    
                                                    <select class="form-control show-tick" name="rc_category" id="rc_category">
                                                        <option value="NE Problem" selected>NE Problem</option>
                                                        <option value="Transmission (OSP)">Transmission (OSP)</option>
                                                        <option value="Power">Power</option>
                                                        <option value="Activity">Activity</option>
                                                        <option value="Logical Configuration">Logical Configuration</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line" id="data-ne">                    
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="GPON">GPON</option>
                                                        <option value="Metro">Metro</option>
                                                        <option value="PE">PE</option>
                                                        <option value="Tera">Tera</option>
                                                        <option value="DWDM">DWDM</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-transmission" style="display: none">    
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="FO">FO</option>
                                                        <option value="Radio">Radio</option>
                                                        <option value="Satelit">Satelit</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-power" style="display: none">                    
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="PLN">PLN</option>
                                                        <option value="AVR">AVR</option>
                                                        <option value="Genset">Genset</option>
                                                        <option value="ATS">ATS</option>
                                                        <option value="MDP">MDP</option>
                                                        <option value="SDP">SDP</option>
                                                        <option value="Rectifier">Rectifier</option>
                                                        <option value="DC PDC">DC PDB</option>
                                                        <option value="Baterai">Baterai</option>
                                                        <option value="Grounding">Grounding</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-activity" style="display: none">
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="Configuration">Configuration</option>
                                                        <option value="Human Error">Human Error</option>
                                                        <option value="Anomali">Anomali</option>
                                                    </select>
                                                </div>
                                                <div class="form-line" id="data-logical_configuration" style="display: none">
                                                    <select class="form-control show-tick" name="rc_category_detail">
                                                        <option value="Bug IOS">Bug IOS</option>
                                                        <option value="TIMOS">TIMOS</option>
                                                        <option value="Configuration">Configuration</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-xs-12 form-control-label">Root cause detail</label>
                                        <div class="col-md-3 col-xs-12">  
                                            <div class="form-group">
                                                <div class="form-line">                      
                                                    <textarea rows="2" name="rc_detail" class="form-control no-resize" placeholder="Type what you want..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Action</label>
                                        <div class="col-md-10 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">                        
                                                    <textarea rows="3" name="action" class="form-control no-resize" placeholder="Type what you want..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-xs-12 form-control-label">Evidence</label>
                                        <div class="col-md-4 col-xs-12">                        
                                            <div class="form-group">
                                                <input type="file" class="fileinput" name="evidence_file" title="Browse file..."/>
                                                <span class="help-block">Format file: .jpg, .jpeg, .png</span>
                                            </div>
                                        </div>

                                        <label class="col-md-2 col-xs-12 form-control-label">Topology</label>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <input type="file" class="fileinput" name="topology_file" title="Browse file..."/>
                                                <span class="help-block">Format file: .jpg, .jpeg, .png</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>                                
                        <div class="panel-footer">      
                            <button type="submit" class="btn btn-primary pull-right">Input Improvement Activity</button>
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/rca')}}'">Back</button>
                        </div>
                        <!-- </form> -->
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </section>
@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('js/select2.js')}}"></script>

    <script>
        $('#date1').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm'});
        $('#date2').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm'});

        $('#rc_category').on('change', function() {
            if (this.value == "NE Problem") {
                document.getElementById('data-ne').style.display = 'block';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Transmission (OSP)") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'block';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Power") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'block';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Activity") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'block';
                document.getElementById('data-logical_configuration').style.display = 'none';
            }
            else if (this.value == "Logical Configuration") {
                document.getElementById('data-ne').style.display = 'none';
                document.getElementById('data-transmission').style.display = 'none';
                document.getElementById('data-power').style.display = 'none';
                document.getElementById('data-activity').style.display = 'none';
                document.getElementById('data-logical_configuration').style.display = 'block';
            }
        });
    </script>
@stop