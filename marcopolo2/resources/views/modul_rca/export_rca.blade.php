
                                <table class="table table-bordered table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Title</th>
                                            <th>Regional</th>
                                            <th>Date and time</th>
                                            <th>Duration</th>
                                            <th>Root cause category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($data as $dt)
                                        <?php $i++ ?>
                                        <tr>
                                            <td class="align-center">{{ $i }}</td>
                                            <td>{{$dt['title']}}</td>
                                            <td>{{$dt['regional']}}</td>
                                            <td>{{$dt['date_time']}}</td>
                                            <td>{{$dt['duration']}}</td>
                                            <td>{{$dt['rc_category']}}</td>
                                            <
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>