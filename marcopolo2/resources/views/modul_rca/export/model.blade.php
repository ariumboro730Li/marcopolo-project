<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <title>{{$data->title}}</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css" media="all">

    html{overflow-y:scroll;} /* Forces a scrollbar when the viewport is larger than the websites content - CSS3 */

    body{margin:0; padding:0; font-size:13px; font-family:Georgia, "Times New Roman", Times, serif; color:#919191; background-color:#232323;}

    .clear:after{content:"."; display:block; height:0; clear:both; visibility:hidden; line-height:0;}
    .clear{display:block; clear:both;}
    html[xmlns] .clear{display:block;}
    * html .clear{height:1%;}

    a{outline:none; text-decoration:none;}

    code{font-weight:normal; font-style:normal; font-family:Georgia, "Times New Roman", Times, serif;}

    .fl_left{float:left;}
    .fl_right{float:right;}

    img{margin:0; padding:0; border:none; line-height:normal; vertical-align:middle;}
    .imgholder, .imgl, .imgr{padding:4px; border:1px solid #D6D6D6; text-align:center;}
    .imgl{float:left; margin:0 15px 15px 0; clear:left;}
    .imgr{float:right; margin:0 0 15px 15px; clear:right;}

    /*----------------------------------------------HTML 5 Overrides-------------------------------------*/

    address, article, aside, figcaption, figure, footer, header, nav, section{display:block; margin:0; padding:0;}

    q{display:block; padding:0 10px 8px 10px; color:#979797; background-color:#ECECEC; font-style:italic; line-height:normal;}
    q:before{content:'� '; font-size:26px;}
    q:after{content:' �'; font-size:26px; line-height:0;}

    /* ----------------------------------------------Wrapper-------------------------------------*/

    div.wrapper{display:block; width:100%; margin:10px; padding:10px; text-align:left;}

    .row1, .row1 a{color:#C0BAB6; background-color:#333333;}
    .row2{color:#979797; background-color:#FFFFFF;}
    .row2 a{color:#FF9900; background-color:#FFFFFF;}
    /*.row3, .row3 a{color:#919191; background-color:#232323;}*/

    /*----------------------------------------------Generalise-------------------------------------*/

    #header, #container, #footer{display:block; width:960px; margin:0 auto;}

    nav ul{margin:0; padding:0; list-style:none;}

    h1, h2, h3, h4, h5, h6{margin:0; padding:0; font-size:20px; font-weight:normal; font-style:normal; line-height:normal;}

    address{font-style:normal;}

    blockquote, q{display:block; padding:8px 10px; color:#979797; background-color:#ECECEC; font-style:italic; line-height:normal;}
    blockquote:before, q:before{content:'� '; font-size:26px;}
    blockquote:after, q:after{content:' �'; font-size:26px; line-height:0;}

    form, fieldset, legend{margin:0; padding:0; border:none;}
    legend{display:none;}
    input, textarea, select{font-size:12px; font-family:Georgia,"Times New Roman",Times,serif;}

    .one_third, .two_third, .three_third{display:block; float:left; margin:0 30px 0 0;}
    .one_third{width:300px;}
    .two_third{width:630px;}
    .three_third{width:960px; float:none; margin-right:0; clear:both;}
    .lastbox{margin-right:0;}

    /*----------------------------------------------Header-------------------------------------*/

    #header{}

    #header #hgroup{float:left; padding:20px 0;}
    #header #hgroup h1, #header #hgroup h2{font-weight:normal; text-transform:none;}
    #header #hgroup h1{font-size:24px;}
    #header #hgroup h2{font-size:13px;}

    #header form{display:block; width:290px; float:right; margin:40px 0 0 0; padding:0;}
    #header form input{display:block; float:left; width:200px; margin:0; padding:5px; color:#C0BAB6; background-color:#232323; border:1px solid #666666;}
    #header form #sf_submit{display:block; float:right; width:70px; font-size:12px; font-weight:bold; text-transform:uppercase; color:#FFFFFF; background-color:#FF9900; border:none; cursor:pointer;}

    #header nav{display:block; width:100%; margin:0; padding:20px 0; border-bottom:1px solid #DEDEDE;}
    #header nav ul{}
    #header nav li{display:inline; margin-right:25px; text-transform:uppercase;}
    #header nav li.last{margin-right:0;}
    #header nav li a{}
    #header nav li a:hover{color:#FF9900; background-color:#333333;}

    /*----------------------------------------------Content Area-------------------------------------*/

    #container{padding:30px 0;}
    #container section{display:block; width:100%; margin:0 0 30px 0; padding:0;}
    #container .last{margin:0;}
    #container .more{text-align:right;}

    /* ------Slider-----*/

    #container #slider{}
    #container #slider figure{}
    #container #slider figure img{float:left; width:600px; height:300px;}
    #container #slider figure figcaption{display:block; float:right; width:200px; height:auto; padding:20px; color:#989898; background-color:#333333; line-height:1.8em;}
    #container #slider figure figcaption a{color:#FF9900; background-color:#333333;}
    #container #slider figure h2{padding-bottom:8px; border-bottom:1px solid #DEDEDE;}
    #container #slider figure footer{}

    /* ------Main Content-----*/

    #container #homepage{display:block; width:100%; line-height:1.6em;}

    #container #homepage .services{margin:0; font-size:12px;}
    #container #homepage .services article{}
    #container #homepage .services article h2{margin-bottom:10px; text-transform:uppercase; font-size:14px; font-weight:bold;}
    #container #homepage .services article img{float:left; padding:4px; border:1px solid #D6D6D6;}
    #container #homepage .services article p{display:block; float:right; width:200px; margin:0; padding:0;}
    #container #homepage .services article footer{padding-top:10px; clear:both;}

    #container #homepage .spacer{display:block; width:100%; height:30px; margin:0; padding:0; clear:both;}

    /*----------------------------------------------Footer-------------------------------------*/

    #footer{padding:20px 0;}
    #footer p{margin:0; padding:0;}
  </style>

  {{--<link rel="stylesheet" type="text/css" media="all" href="{{asset('css/export.css')}}">--}}
</head>
<body>

<div class="wrapper row1">
  <header id="header" class="clear">

    <div id="hgroup">
      <h1><a href="#">{{$data->title}}</a></h1>
      <h2>{{$data->regional}} {{$data->date_time_open}} to {{$data->date_time_close}}  {{$data->duration}}</h2>
    </div>

  </header>
</div>
<!-- content -->
<div class="wrapper row2">
  <div id="container" class="clear">
    <!-- Slider -->
    <section id="slider" class="clear">

      <figure>
        <div class="container">
        <div class="row">
          <div class="col-md-3">
            <img src="{{asset('/images/rca/'.$data->evidence_file)}}" alt="" style="margin:0;padding:0;width:33%;height: 100%;float:left;margin-right:15px;">
          </div>
          <div class="col-md-3">
            <img src="{{asset('/images/rca/'.$data->topology_file)}}" alt="" style="margin:0;padding:0;width:33%;height: 100%;float:left;">
          </div>
        </div>
        </div>

        <figcaption>
          <h2>Improvement Plans</h2>
          @foreach(\App\RCADetail::getRCADetailData($data->idrca) as $activity)
          <li>{{$activity->improvement_activity}} </li>
          <li>by {{$activity->pic}}</li>
          @endforeach
        </figcaption>

      </figure>
    </section>
    <!-- Content -->
    <div id="homepage">
      <!-- ########################################################################################## -->
      <section class="clear">
        <article class="one_third">

          <h2>Network Impact</h2>
          <?php $network_impact = explode(PHP_EOL, $data->network_impact); ?>

          @foreach($network_impact as $impact)
          <li>{{$impact}} </li>
          @endforeach

          <h2 style="margin-top: 30px;">Service Impact</h2>

          <p>{{$data->service_impact}}</p>

        </article>
        <article class="two_third lastbox">
          <figure><img src="images/demo/630x300.gif" alt=""></figure>
          <h2>Root Cause</h2>
          <p>({{$data->rc_category}}) {{$data->rc_detail}}</p>
          <h2>Action</h2>
          <?php $actions = explode(PHP_EOL,$data->action); ?>
          @foreach($actions as $action)
          <li>{{$action}}</li>
          @endforeach
        </article>
      </section>
    </div>
  </div>
</div>

<div class="wrapper row3">
  <footer id="footer" class="clear">
    <p class="fl_left">Copyright © <script>document.write(new Date().getFullYear());</script> All Rights Reserved - marcxopolo.telkom.co.id</p>
  </footer>
</div>
</body>
</html>
