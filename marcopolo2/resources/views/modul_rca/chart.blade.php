<?php 
$tahun1 = date("Y");
$tahun0 = date("Y")-1;
$bulan = date("n");
$bulann = date("n")-1;
?>

<script>

var bulan1 = moment().format("M");
var bulan0;

if (bulan1==1) { bulan1 = "Januari"; bulan0 = "(Oktober)"; } else if (bulan1==2) { bulan1 = "Februari"; bulan0 = "(November)"; } else if (bulan1==3) { bulan1 = "Maret"; bulan0 = "(Desember)"; } else if (bulan1==4) { bulan1 = "April"; bulan0 = "Januari"; } else if (bulan1==5) { bulan1 = "Mei"; bulan0 = "Februari"; } else if (bulan1==6) { bulan1 = "Juni"; bulan0 = "Maret"; } else if (bulan1==7) { bulan1 = "Juli"; bulan0 = "April"; } else if (bulan1==8) { bulan1 = "Agustus"; bulan0 = "Mei"; } else if (bulan1==9) { bulan1 = "September"; bulan0 = "Juni"; } else if (bulan1==10) { bulan1 = "Oktober"; bulan0 = "Juli"; } else if (bulan1==11) { bulan1 = "Agustus"; bulan0 = "Juli"; } else if (bulan1==12) { bulan1 = "Desember"; bulan0 = "September"; }

/* ACCESS LINE */

var configRcaLine = {
    data: {
        labels: [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [{
            label: "Incidents",
            data: [{{\App\RCA::TotalIncidentMonth(1)}},{{\App\RCA::TotalIncidentMonth(2)}},{{\App\RCA::TotalIncidentMonth(3)}},{{\App\RCA::TotalIncidentMonth(4)}},{{\App\RCA::TotalIncidentMonth(5)}},{{\App\RCA::TotalIncidentMonth(6)}},{{\App\RCA::TotalIncidentMonth(7)}},{{\App\RCA::TotalIncidentMonth(8)}},{{\App\RCA::TotalIncidentMonth(9)}},{{\App\RCA::TotalIncidentMonth(10)}},{{\App\RCA::TotalIncidentMonth(11)}},{{\App\RCA::TotalIncidentMonth(12)}}
            ],
            borderColor: "rgba(41, 128, 185,1)",
            backgroundColor: "rgba(52, 152, 219, 0.5)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(233, 30, 99, 0.9)",
            pointBorderWidth: 1,
            datalabels: {
                align: 'end',
                anchor: 'start'
            }
        }]
    },
    options: {
        responsive: true,
        legend: false,
        plugins: {
            datalabels: {
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                formatter: Math.round
            }
        },
        scales: {
            yAxes: [{
                stacked: false
            }],
            xAxes: [{
                ticks: {
                	min: bulan0,
                    max: bulan1
                }
            }]
        }
    }
}

/* RCA BAR */

var configRcaBar = {
	data: {
	    labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
	    datasets: [{
	        label: "Duration < 4 jam",
	        data: [{{\App\RCA::TotalRegionalDurationUnder(1)}},{{\App\RCA::TotalRegionalDurationUnder(2)}},{{\App\RCA::TotalRegionalDurationUnder(3)}},{{\App\RCA::TotalRegionalDurationUnder(4)}},{{\App\RCA::TotalRegionalDurationUnder(5)}},{{\App\RCA::TotalRegionalDurationUnder(6)}},{{\App\RCA::TotalRegionalDurationUnder(7)}}
	        ],
	        backgroundColor: "rgba(0, 188, 212, 0.8)"
	    },
            {
                label: "Duration > 4 jam",
                data: [{{\App\RCA::TotalRegionalDurationOver(1)}},{{\App\RCA::TotalRegionalDurationOver(2)}},{{\App\RCA::TotalRegionalDurationOver(3)}},{{\App\RCA::TotalRegionalDurationOver(4)}},{{\App\RCA::TotalRegionalDurationOver(5)}},{{\App\RCA::TotalRegionalDurationOver(6)}},{{\App\RCA::TotalRegionalDurationOver(7)}}
                ],
                backgroundColor: "rgba(233, 30, 99, 0.8)"
            }
        ]
	},
	options: {
	    responsive : true,
	    legend: false,
	    plugins: {
	        datalabels: {
	            align: 'end',
	            anchor: 'end',
	            backgroundColor: function(context) {
	                return context.dataset.backgroundColor;
	            },
	            borderRadius: 4,
	            color: 'white',
	            font: function(context) {
	                var w = context.chart.width;
	                return {
	                    size: w < 512 ? 12 : 14
	                }
	            },
	            formatter: function(value, context) {
	                return context.chart.data.labels[context.data];
	            }
	        }
	    },
	    scales: {
	        xAxes: [{
	            display: true,
	            offset: true
	        }],
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
	    }
	}
}

var configRcaBar2 = {
    data: {
        labels: ["R1", "R2", "R3", "R4", "R5", "R6", "R7"],
        datasets: [{
            label: "Close",
            data: [{{\App\RCA::TotalRegionalClose(1)}},{{\App\RCA::TotalRegionalClose(2)}},{{\App\RCA::TotalRegionalClose(3)}},{{\App\RCA::TotalRegionalClose(4)}},{{\App\RCA::TotalRegionalClose(5)}},{{\App\RCA::TotalRegionalClose(6)}},{{\App\RCA::TotalRegionalClose(7)}}
            ],          
            backgroundColor: "rgba(41, 128, 185,0.8)"
        },
            {
                label: "Open",
                data: [{{\App\RCA::TotalRegionalOpen(1)}},{{\App\RCA::TotalRegionalOpen(2)}},{{\App\RCA::TotalRegionalOpen(3)}},{{\App\RCA::TotalRegionalOpen(4)}},{{\App\RCA::TotalRegionalOpen(5)}},{{\App\RCA::TotalRegionalOpen(6)}},{{\App\RCA::TotalRegionalOpen(7)}}
                ],
                backgroundColor: "rgba(192, 57, 43,0.8)"
            }
        ]
    },
    options: {
        responsive : true,
        legend: false,
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                backgroundColor: function(context) {
                    return context.dataset.backgroundColor;
                },
                borderRadius: 4,
                color: 'white',
                font: function(context) {
                    var w = context.chart.width;
                    return {
                        size: w < 512 ? 12 : 14
                    }
                },
                formatter: function(value, context) {
                    return context.chart.data.labels[context.data];
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                offset: true
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}

var ChartJsRcaLine = Chart.Line(document.getElementById("line_chart_rca"), configRcaLine);
var ChartJsRcaBar = Chart.Bar(document.getElementById("bar_chart_rca"), configRcaBar);
var ChartJsRcaBar2 = Chart.Bar(document.getElementById("bar_chart_rca2"), configRcaBar2);

</script>

<script>
    $(function () {
        new Chart(document.getElementById("donut_chart_rca"), getChartJsRca('donut'));
    });

    function getChartJsRca(type) {
        var config = null;

        if (type === 'donut') {
            config = {
                type: 'doughnut',
                data: {
                    labels: ["NE Problem", "Transmission","Power", "Activity", "Logical Configuration"],
                    datasets: [{
                        data: [{{\App\RCA::RootCauseNEProblem()}}, {{\App\RCA::RootCauseTranmission()}},{{\App\RCA::RootCausePower()}},{{\App\RCA::RootCauseActivity()}},{{\App\RCA::RootCauseLogicalConfiguration()}}],
                        backgroundColor: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)','rgb(243, 156, 18)', 'rgb(39, 174, 96)','rgb(44, 62, 80)'],
                    }],
                    datalabels: {
                        anchor: 'center',
                        backgroundColor: null,
                        borderWidth: 0
                    }
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'left'
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function(context) {
                                return context.dataset.backgroundColor;
                            },
                            borderColor: 'white',
                            borderRadius: 25,
                            borderWidth: 2,
                            color: 'white',
                            display: function(context) {
                                var dataset = context.dataset;
                                var count = dataset.data.length;
                                var value = dataset.data[context.dataIndex];
                                return value > count * 1.5;
                            },
                            font: {
                                weight: 'bold'
                            },
                            formatter: Math.round
                        }
                    }
                }
            }
        }
        return config;
    }






</script>

