@extends('layout')

@section('menu3')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
Improvement Tracking - RCA
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/rca')}}">RCA</a></li>
                    <li class="active">Improvement Tracking</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IMPROVEMENT TRACKING</h2>
                                </div>
                            </div>
                        </div>
                      
                        <div class="body">                 

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Improvement Activity</th>
                                            <th>Status</th>
                                            <th>Target</th>
                                            <th>PIC</th>
                                            <th>Aging</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($rca as $data)
                                        <?php $i++ ?>

                                        @foreach(\App\RCADetail::getRCADetailData($data['idrca']) as $detail)
<!--                                            --><?php
//                                            $startTimeStamp = strtotime($data['date_time_open']);
//                                            $endTimeStamp = strtotime($detail['tgl_target']);
//
//                                            $timeDiff = abs($endTimeStamp - $startTimeStamp);
//
//                                            if($timeDiff!=0)
//                                                {
//                                            $numberDays = $timeDiff/86400;  // 86400 seconds in one day
//
//                                            // and you might want to convert to integer
//                                            $numberDays = intval($numberDays);
//                                            $datehariini = strtotime(date("Y-m-d"));
//
//
//                                            $timeDiff = abs($datehariini - $startTimeStamp);
//                                            if($timeDiff!=0)
//                                                {
//                                            $selisih = $timeDiff/86400;  // 86400 seconds in one day
//
//                                            // and you might want to convert to integer
//                                            $selisih = intval($selisih);
//                                            if($numberDays!=0)
//                                            $nilai= 100-(($selisih/$numberDays)*100);
//
//                                                }
//                                                }
//                                                else
//                                                    {
//                                                        $numberDays=1;
//                                                        $selisih=0;
//                                                    }
//                                            ?>

                                            <?php
//                                            $date_time_open = strtotime($data['date_time_open']);
                                            $dates = explode(' ', $data['date_time_open']);
                                            $date_time_open = $dates[0];
                                            $date_time_open = strtotime($date_time_open);
                                            $target = strtotime($detail['tgl_target']);
                                            $now = strtotime(date("Y-m-d"));


                                            $x = $now - $date_time_open;
                                            $x = round($x / (60 * 60 * 24));
                                            $y = $target - $date_time_open;
                                            $y = round($y / (60 * 60 * 24));


                                            if($x!= 0 && $y !=0)
                                            $nilai = round(($x/$y)*100);
                                            else
                                                $nilai=101;
                                            ?>
                                        <tr class="align-center">
                                            <td>{{ $i }}</td>
                                            <td>{{$detail['improvement_activity']}}</td>
                                            <td class="align-center">
                                                @if($detail['status']=='Open')<h5 class="no-margin"><span class="label label-success">Open</span></h5>
                                                @else<h5 class="no-margin"><span class="label label-danger">Close</span></h5>@endif
                                            </td>
                                            <td>{{$detail['tgl_target']}}</td>
                                            <td>{{$detail['pic']}}</td>
                                            <td class="aging">
                                                <div class="progress">
                                                    <div class="progress-bar @if($nilai<=100)progress-bar-success @else progress-bar-danger @endif progress-bar-striped active" role="progressbar" aria-valuenow="{{$nilai}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$nilai}}%" title="{{$nilai}} hari">{{$x}} / {{$y}} hari</div>
                                                </div>
                                            </td>
                                            <td><a href="{{URL::Route('rca.edit',$data['idrca'])}}" class="btn btn-info">Update</a></td>
                                        </tr>
                                        @endforeach
                                        @endforeach


                                    </tbody>
                                </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop