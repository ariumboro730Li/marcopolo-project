<?php 
$tahun1 = date("Y");
$tahun0 = date("Y")-1;
?>

<script>

    function RcaLine()
    {
        reg = document.getElementById("RcaLine").value;

        if (reg == "All") {
            ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::AllCapacityAcc(9,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(10,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(11,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(12,$tahun0)}}, {{\App\DatekSummary::AllCapacityAcc(1,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(2,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(3,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(4,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(5,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(6,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(7,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(8,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(9,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(10,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(11,$tahun1)}}, {{\App\DatekSummary::AllCapacityAcc(12,$tahun1)}}];
            ChartJsRcaLine.data.datasets[1].label = "Nasional Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::AllUtilizationAcc(9,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(10,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(11,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(12,$tahun0)}}, {{\App\DatekSummary::AllUtilizationAcc(1,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(2,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(3,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(4,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(5,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(6,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(7,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(8,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(9,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(10,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(11,$tahun1)}}, {{\App\DatekSummary::AllUtilizationAcc(12,$tahun1)}}];
            ChartJsRcaLine.data.datasets[0].label = "Nasional Utilization";
            ChartJsRcaLine.update();
        }
        else if (reg == "R1") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,1)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,1)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 1 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,1)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,1)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 1 Utilization";
  			ChartJsRcaLine.update();
        }
        else if (reg == "R2") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,2)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,2)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 2 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,2)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,2)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 2 Utilization";
  			ChartJsRcaLine.update();
        }
        else if (reg == "R3") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,3)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,3)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 3 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,3)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,3)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 3 Utilization";
  			ChartJsRcaLine.update();
        }
        else if (reg == "R4") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,4)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,4)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 4 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,4)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,4)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 4 Utilization";
  			ChartJsRcaLine.update();
        }
        else if (reg == "R5") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,5)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,5)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 5 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,5)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,5)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 5 Utilization";
  			ChartJsRcaLine.update();
        }
        else if (reg == "R6") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,6)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,6)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 6 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,6)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,6)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 6 Utilization";
  			ChartJsRcaLine.update();
        }
        else if (reg == "R7") {
        	ChartJsRcaLine.data.datasets[1].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun0,7)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,7)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,7)}}];
        	ChartJsRcaLine.data.datasets[1].label = "Regional 7 Capacity";
            ChartJsRcaLine.data.datasets[0].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun0,7)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,7)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,7)}}];
            ChartJsRcaLine.data.datasets[0].label = "Regional 7 Utilization";
  			ChartJsRcaLine.update();
        }

    }

    function RcaBar()
    {
        bulan = document.getElementById("RcaBar").value;

        if (bulan == "1") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Januari Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Januari Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "2") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Februari Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Februari Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "3") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Maret Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Maret Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "4") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "April Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "April Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "5") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Mei Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Mei Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "6") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Juni Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Juni Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "7") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Juli Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Juli Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "8") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Agustus Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Agustus Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "9") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "September Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "September Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "10") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Oktober Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Oktober Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "11") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "November Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "November Utilization";
  			ChartJsRcaBar.update();
        }
        else if (bulan == "12") {
        	ChartJsRcaBar.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,7)}}];
        	ChartJsRcaBar.data.datasets[0].label = "Desember Capacity";
            ChartJsRcaBar.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,7)}}];
            ChartJsRcaBar.data.datasets[1].label = "Desember Utilization";
  			ChartJsRcaBar.update();
        }
    }

    function RcaBar2()
    {
        bulan = document.getElementById("RcaBar2").value;

        if (bulan == "1") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(1,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(1,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Januari Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(1,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(1,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Januari Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "2") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(2,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(2,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Februari Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(2,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(2,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Februari Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "3") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(3,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(3,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Maret Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(3,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(3,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Maret Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "4") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(4,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(4,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "April Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(4,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(4,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "April Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "5") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(5,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(5,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Mei Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(5,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(5,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Mei Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "6") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(6,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(6,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Juni Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(6,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(6,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Juni Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "7") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(7,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(7,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Juli Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(7,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(7,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Juli Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "8") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(8,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(8,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Agustus Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(8,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(8,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Agustus Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "9") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(9,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(9,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "September Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(9,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(9,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "September Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "10") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(10,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(10,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Oktober Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(10,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(10,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Oktober Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "11") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(11,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(11,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "November Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(11,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(11,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "November Utilization";
            ChartJsRcaBar2.update();
        }
        else if (bulan == "12") {
            ChartJsRcaBar2.data.datasets[0].data = [{{\App\DatekSummary::CapacityAcc(12,$tahun1,1)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,2)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,3)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,4)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,5)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,6)}}, {{\App\DatekSummary::CapacityAcc(12,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[0].label = "Desember Capacity";
            ChartJsRcaBar2.data.datasets[1].data = [{{\App\DatekSummary::UtilizationAcc(12,$tahun1,1)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,2)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,3)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,4)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,5)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,6)}}, {{\App\DatekSummary::UtilizationAcc(12,$tahun1,7)}}];
            ChartJsRcaBar2.data.datasets[1].label = "Desember Utilization";
            ChartJsRcaBar2.update();
        }
    }

</script>