@extends('layout')

@section('menu2')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
RCA
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li class="active">RCA</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>LIST RCA</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('rca.tambah')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">add</i><span>Input RCA</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">                 

                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-condensed table-hover dataTable js-basic-example">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Title</th>
                                            <th>Summary (Done/Total)</th>
                                            <th>Regional</th>
                                            <th>Date and time</th>
                                            <th>Duration</th>
                                            <th>Root cause category</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i = 0 ?>
                                        @foreach($rca as $data)
					<?php $count = App\RCADetail::where('idrca', $data->idrca)->where('status', 'close')->count(); ?>
                                        <?php $count1 = App\RCADetail::where('idrca', $data->idrca)->count(); ?>
                                            <?php $i++ ?>
                                        <tr>
                                            <td class="align-center">{{ $i }}</td>
                                            <td>{{$data['title']}}</td>
					    <td>{{ $count }}/{{ $count1}}</td>
                                            <td>{{$data['regional']}}</td>
                                            <td>{{\Carbon\Carbon::parse($data['date_time_open'])->format('d-m-Y H:i')}}</td>
                                            <td>W-{{\Carbon\Carbon::parse($data['date_time_open'])->format('W')}}</td>
                                            <td>{{$data['duration']}}</td>
                                            <td>{{$data['rc_category']}}</td>
                                            <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::Route('rca.edit',$data['idrca'])}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('rca.hapus',$data['idrca'])}}">Delete</a></li>
                                                        <li><a href="{{URL::Route('rca.view.data',$data['idrca'])}}">View</a></li>
                                                        <li><a href="{{URL::Route('rca.export.data',$data['idrca'])}}">Export</a></li>
                                                        <!-- <li><a href="{{URL::Route('rca.hapus',$data['idrca'])}}">Export</a></li> -->
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop