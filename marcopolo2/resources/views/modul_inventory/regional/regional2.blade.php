@extends('layout')

@section('menu4')
    class="active"
@stop

@section('title')
Regional 2 - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">Regional 2</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>Regional 2</h2>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="{{URL::Route('inventory.access.detail', 2)}}">Access</a></td>
                                            <td>{{\App\DatekAccess::Link(2)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(2)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{URL::Route('inventory.backhaul.detail', 2)}}">Backhaul</a></td>
                                            <td>{{\App\DatekBackhaul::Link(2)}}</td>
                                            <td>{{\App\DatekBackhaul::Capacity(2)}}</td>
                                        </tr>
                                        <tr>                                            
                                            <td><a href="{{URL::Route('inventory.ipran.detail', 2)}}">IPRAN</a></td>
                                            <td>{{\App\DatekIpran::Link(2)}}</td>
                                            <td>{{\App\DatekIpran::Capacity(2)}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{URL::Route('inventory.ipbb.detail', 2)}}">IPBB</a></td>
                                            <td>{{\App\DatekIpbb::Link(2)}}</td>
                                            <td>{{\App\DatekIpbb::Capacity(2)}}</td>
                                        </tr>
                                        <tr>
                                            <td><a href="{{URL::Route('inventory.ix.detail', 2)}}">IX</a></td>
                                            <td>{{\App\DatekIx::Link(2)}}</td>
                                            <td>{{\App\DatekIx::Capacity(2)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop