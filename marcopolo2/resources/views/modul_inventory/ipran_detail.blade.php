@extends('layout')

@section('menu7')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

@stop

@section('title')
ipran Detail Regional - Inventory Datek
@stop


    
@section('content')
<style>
    .border-0 {
        border:none !important;
        width: 100%;
        background: transparent !important;
        text-align: center;
    }
</style>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/ipran')}}">Ipran</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IPRAN - DETAIL REGIONAL {{$regional}}</h2>
                                    {{-- <h3 id="chtext">Olalala</h3>
                                    <textarea name="" hidden id="textch" cols="30" rows="10"></textarea> --}}
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.ipran', $regional)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <h3>IPRAN DWDM</h3>
                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 4010px !important">
                                    <thead class="text-center">
                                        <tr>
                                            <th class="no">ID</th>
                                            <th>Link</th>
                                            <th>Kota A</th>
                                            <th>Router A</th>
                                            <th>Port A</th>
                                            <th>Kota B</th>
                                            <th>Router B</th>
                                            <th>Port B</th>
                                            <th></th> 
                                            <th>STO A</th>
                                            <th>NE A</th>
                                            <th>Shelf A - Slot A - Port A</th>
                                            <th>Tie Line A</th>
                                            <th>STO B</th>
                                            <th>NE B</th>
                                            <th>Shelf B - Slot B - Port B</th>
                                            <th>Tie Line B</th> 
                                            <th> Status </th>
                                            <th> Capacity </th> 
                                            <th> Utilization </th> 
                                            <th> Utilization In </th> 
                                            <th> Utilization Out </th> 
                                            <th> Utilization Max </th> 
                                            <th> System </th> 
                                            <th> Transport Type</th> 
                                            <th> Last Update</th> 
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0;?>
                                    @foreach($ipran_dwdm as $ib)
                                    <?php $id = $ib['idipran'];  $Relasiipran =  App\Relasiipran::where('id_ipran',$id); ?>
                                        <?php $i++; ?>
                                            <tr>
                                            <td>{{$i}}</td>
                                            <td><a href="http://">{{$ib['link']}}</a></td>
                                            <td><input type="text" name="kota_a" class="border-0" id="kota_a{{$ib['idipran']}}" value="{{$ib['kota_router_node_a']}}"> </td>
                                            <td><input type="text" name="router_a" class="border-0" id="router_a{{$ib['idipran']}}" value="{{$ib['router_node_a']}}"></td>
                                            
                                            <td><form id="port_a{{$ib['idipran']}}form">
                                                    @csrf
                                                <input name="id" hidden value="{{$ib['idipran']}}" id="port_a{{$ib['idipran']}}id">
                                                <input type="text" name="port_a" class="border-0" id="port_a{{$ib['idipran']}}" value="{{$ib['port_tsel_end_a']}}">
                                            </form></td>

                                            <td><input type="text" name="kota_b" class="border-0" id="kota_b{{$ib['idipran']}}" value="{{$ib['kota_router_node_b']}}"></td>
                                            <td><input type="text" name="router_b" class="border-0" id="router_b{{$ib['idipran']}}" value="{{$ib['router_node_b']}}"></td>
                                            <td><form id="port_b{{$ib['idipran']}}form">
                                                    @csrf
                                                <input name="id" hidden value="{{$ib['idipran']}}" id="port_b{{$ib['idipran']}}id">
                                                <input type="text" name="port_b" class="border-0" id="port_b{{$ib['idipran']}}" value="{{$ib['port_tsel_end_b']}}">
                                            </form></td>
                                            <td>
                                                <a name="" id="addHop{{$ib['idipran']}}" class="btn btn-primary" href="#" role="button">Add Hop</a>
                                            </td>    
                                            {{-- <td>sto A</td> --}}
                                            <?php $id = $ib['idipran']; $Relasiipran =  App\Relasiipran::where('id_ipran',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>
                                            @foreach ($Relasiipran->get() as $relasi)
                                            <form action="" id="frmRelasiipran{{$relasi->id}}">
                                            @endforeach
                                                <input autocomplete=off type="hidden" name="id" id="" value="{{$ib['idipran']}}">
                                            <?php if ($Relasiipran->count() == 0) { ?>
                                                
                                                <td>
                                                    <table style="width: 100%" class="hop0{{$ib['idipran']}} Relasiipran">
                                                            {{-- <tr>
                                                                    <td></td>
                                                        </tr> --}}
                                                    </table>
                                                </td>  
                                                <td>
                                                    <table style="width: 100%" class="hop1{{$ib['idipran']}} Relasiipran">
                                                            {{-- <tr>
                                                                    <td></td>
                                                        </tr> --}}
                                                    </table>
                                                </td>  
                                                <td>
                                                    <table style="width: 100%" class="hop2{{$ib['idipran']}} Relasiipran">
                                                        {{-- <tr>
                                                                <td></td>
                                                            </tr> --}}
                                                    </table>
                                                </td>  
                                                <td>
                                                    <table style="width: 100%" class="hop3{{$ib['idipran']}} Relasiipran">
                                                            {{-- <tr>
                                                                    <td></td>
                                                            </tr> --}}
                                                    </table>
                                                </td>  
                                           <?php  } else { ?>
                                            <?php $id = $ib['idipran']; $Relasiipran =  App\Relasiipran::where('id_ipran',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>
                                            <td>
                                                <table style="width: 100%" class="hop0{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input0{{$relasi->id}}">
                                                            <td><a name="" id="removeHop{{$relasi->id}}" class="label label-danger" href="javascript:void(0)" role="button"> - </a></td>    
                                                            <td><input class="border-0" type="text" class="border-0" name="sto" id="sto{{$relasi->id}}" value="{{\App\Metro::STO($relasi->ne_transport)}}"></td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                            </td>
                                            <td>
                                                <table style="width: 100%" class="hop1{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input1{{$relasi->id}}">
                                                            <td><input autocomplete=off type="text" class="border-0" name="ne_transport" id="ne_transport{{$relasi->id}}" value="{{ $relasi->ne_transport }}"></td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                            </td>
                                            <td>
                                                <table style="width: 100%" class="hop2{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input2{{$relasi->id}}">
                                                            <td><input autocomplete=off type="text" class="border-0" name="shelf_slot_port" id="ssp{{$relasi->id}}" value="{{$relasi->shelf_slot_port}}" ></td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </td>  
                                            <td>                                                
                                                <table style="width: 100%" class="hop3{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input3{{$relasi->id}}">
                                                            <td><input autocomplete=off type="text" class="border-0" name="tie_line_transport" id="tieline{{$relasi->id}}" value="{{$relasi->tie_line}}"></td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                             </td>  
                                             <?php } ?>
                                            </form>
                                            {{-- <td>sto B</td> --}}
                                            <?php 
                                            // $id = $ib['idipran']; 
                                            // $Relasiipran =  App\Relasiipran::where('id_ipran',$id)->whereNotIn('row', [1]);  
                                            // $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc');
                                             ?>
                                            <?php $id = $ib['idipran']; $Relasiipran =  App\Relasiipran::where('id_ipran',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>
                                            <td>
                                                <table style="width: 100%" class="hop7{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input0{{$relasi->id}}">
                                                               <td> 
                                                                   <input type="text" class="border-0" name="sto" id="sto{{$relasi->id}}b" value="{{\App\Metro::STO($relasi->ne_transport)}}">
                                                               </td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                            </td>

                                            <td>
                                                <table style="width: 100%" class="hop4{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input1{{$relasi->id}}">
                                                            <td><input class="border-0" disabled  id="ne_transport{{$relasi->id}}b" value="{{ $relasi->ne_transport }}"></td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                            </td>
                                            <td>
                                                <table style="width: 100%" class="hop5{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input2{{$relasi->id}}">
                                                            <td><input class="border-0" disabled id="ssp{{$relasi->id}}b" value="{{$relasi->shelf_slot_port}}"></td>

                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </td>  
                                            <td>                                                
                                                <table style="width: 100%" class="hop6{{$ib['idipran']}} Relasiipran">
                                                        @foreach ($Relasiipran->get() as $relasi)
                                                        <tr class="input3{{$relasi->id}}">
                                                            <td><input class="border-0" disabled id="tieline{{$relasi->id}}b" value="{{$relasi->tie_line}}"></td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                             </td>  
                                             <td>{{$ib['status']}}</td>
                                            <td><input type="number" name="" id="capacity{{$ib['idipran']}}"  class="border-0" style="width:23%" value="{{$ib['capacity']}}"> G</td>
                                            <td>{{$ib['utilization_cap']}}</td>
                                            <td>{{$ib['utilization_in%']}}</td>
                                            <td>{{$ib['utilization_out%']}}</td>
                                            <td>{{$ib['utilization_max%']}}</td>
                                            <td><input type="text" name="" class="border-0" id="system{{$ib['idipran']}}" value="{{$ib['system']}}"></td>
                                            <td>{{$ib['transport_type']}}</td>
                                            <td>{{$ib['updated_at']}}</td>
                                            <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::Route('inventory.ipran.detail.delete', $ib['idipran'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="body">
                            <h3>IPRAN Metro</h3>
                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 3110px !important">
                                    <thead class="text-center">
                                        <tr>
                                            <th class="no">ID</th>
                                            <th>Link</th>
                                            <th>Kota A</th>
                                            <th>Router A</th>
                                            <th>Port A</th>
                                            <th>Kota B</th>
                                            <th>Router B</th>
                                            <th>Port B</th>
                                            <th>STO A</th>
                                            <th>Metro A</th>
                                            <th>Port A</th>
                                            <th>STO B</th>
                                            <th>Metro B</th>
                                            <th>Port B</th>
                                            <th>Status </th>
                                            <th>Capacity (G) </th> 
                                            <th>Utilization </th> 
                                            <th>Current In </th> 
                                            <th>Current Out </th> 
                                            <th>Utilization Max </th> 
                                            <th>System </th> 
                                            <th>Transport Type</th> 
                                            <th>Last Update</th> 
                                            <th></th> 
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0;?>
                                    @foreach($ipran_metro as $ib)
                                                            
                                    <?php $id = $ib['idipran']; $Relasiipran =  App\Relasiipran::where('id_ipran',$id);  $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>

                                    <?php 

                                    if ($Relasiipran->count() == 0) {
                                        $arr = [
                                            'id_ipran' => $id,
                                            'row'   => 1,
                                            'status'    => 0
                                        ];
                                        $arr2 = [
                                            'id_ipran' => $id,
                                            'row'   => 2,
                                            'status'    => 0
                                        ];
                                        $store = App\Relasiipran::create($arr);
                                        $store = App\Relasiipran::create($arr2);
                                    } ?>

                                    <?php $id = $ib['idipran'];  $Relasiipran =  App\Relasiipran::where('id_ipran',$id); ?>
                                        <?php $i++; ?>
                                        <tr>
                                            <td> {{ $ib['idipran'] }}</td>
                                            <td>{{$ib['link']}}</td>
                                            <td><input type="text" name="kota_a" class="border-0" id="kota_a{{$ib['idipran']}}" value="{{$ib['kota_router_node_a']}}"> </td>
                                            <td><input type="text" name="router_a" class="border-0" id="router_a{{$ib['idipran']}}" value="{{$ib['router_node_a']}}"></td>
                                            <td><form id="port_a{{$ib['idipran']}}form">
                                                    @csrf
                                                <input name="id" hidden value="{{$ib['idipran']}}" id="port_a{{$ib['idipran']}}id">
                                                <input type="text" name="port_a" class="border-0" id="port_a{{$ib['idipran']}}" value="{{$ib['port_tsel_end_a']}}">
                                            </form></td>

                                            <td><input type="text" name="kota_b" class="border-0" id="kota_b{{$ib['idipran']}}" value="{{$ib['kota_router_node_b']}}"></td>
                                            <td><input type="text" name="router_b" class="border-0" id="router_b{{$ib['idipran']}}" value="{{$ib['router_node_b']}}"></td>
                                            <td><form id="port_b{{$ib['idipran']}}form">
                                                    @csrf
                                                <input name="id" hidden value="{{$ib['idipran']}}" id="port_b{{$ib['idipran']}}id">
                                                <input type="text" name="port_b" class="border-0" id="port_b{{$ib['idipran']}}" value="{{$ib['port_tsel_end_b']}}">
                                            </form></td>
                                            <?php $id = $ib['idipran']; $Relasiipran =  App\Relasiipran::where('id_ipran',$id)->where('row',1);   $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>
                                            @foreach ($Relasiipran->get() as $relasi)
                                            <td>
                                                <input class="border-0" type="text" class="border-0" name="sto" id="sto{{$relasi->id}}" value="{{\App\Metro::STO($relasi->ne_transport)}}">
                                            </td>
                                            </form>
                                            
                                            <td>
                                                <input autocomplete=off type="text" class="border-0" name="ne_transport" id="ne_transport{{$relasi->id}}" value="{{ $relasi->ne_transport }}">
                                            </td>
                                            <td>
                                                <input autocomplete=off type="text" class="border-0" name="shelf_slot_port" id="ssp{{$relasi->id}}" value="{{$relasi->shelf_slot_port}}" >
                                            </td>  
                                             @endforeach
                                             <?php $id = $ib['idipran']; $Relasiipran =  App\Relasiipran::where('id_ipran',$id)->where('row',2);  $relrelan = DB::table('relasi_ipran')->where('id_ipran', $id)->orderBy('row', 'desc'); ?>
                                             @foreach ($Relasiipran->get() as $relasi)
                                             <td>
                                                    <input class="border-0" type="text" class="border-0" name="sto" id="sto{{$relasi->id}}" value="{{\App\Metro::STO($relasi->ne_transport)}}">
                                            </td>
                                            <td>
                                                <input class="border-0"  id="ne_transport{{$relasi->id}}" value="{{ $relasi->ne_transport }}">
                                            </td>
                                            <td>
                                                <input class="border-0" id="ssp{{$relasi->id}}" value="{{$relasi->shelf_slot_port}}">
                                            </td>  
                                            @endforeach
                                            <td>{{$ib['status']}}</td>
                                            <td><input type="number" name="" id="capacity{{$ib['idipran']}}"  class="border-0" style="width:35%" value="{{$ib['capacity']}}"></td>
                                            <td>{{$ib['utilization_cap']}}</td>
                                            <td>{{$ib['utilization_in%']}}</td>
                                            <td>{{$ib['utilization_out%']}}</td>
                                            <td>{{$ib['utilization_max%']}}</td>
                                            <td><input type="text" name="" class="border-0" id="system{{$ib['idipran']}}" value="{{$ib['system']}}"></td>
                                            <td>{{$ib['transport_type']}}</td>
                                            <td>{{$ib['updated_at']}}</td>
                                            <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::Route('inventory.ipran.detail.delete', $ib['idipran'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
    
    @foreach($ipran as $ib)
    <script>
    $(document).ready(function() {
            var tableRelasiipran = $('Relasiipran').dataTable();
            var max_fields      = 1000; //maximum input boxes allowed
            var add_button      = $("#addHop{{$ib['idipran']}}"); //Add button ID
            var wrapper0   		= $(".hop0{{$ib['idipran']}}"); //Fields wrapper
            var wrapper7   		= $(".hop7{{$ib['idipran']}}"); //Fields wrapper
            var wrapper1   		= $(".hop1{{$ib['idipran']}}"); //Fields wrapper
            var wrapper2   		= $(".hop2{{$ib['idipran']}}"); //Fields wrapper
            var wrapper3   		= $(".hop3{{$ib['idipran']}}"); //Fields wrapper
            var wrapper4        = $("#removeHop");
            var wrapper4   		= $(".hop4{{$ib['idipran']}}"); //Fields wrapper
            var wrapper5   		= $(".hop5{{$ib['idipran']}}"); //Fields wrapper
            var wrapper6   		= $(".hop6{{$ib['idipran']}}"); //Fields wrapper
            var x = 1; //initlal text box count

            $(add_button).click(function(e){ //on add input button click
              e.preventDefault();
                var value = "{{$ib['idipran']}}";
                $.ajax({
                    type: 'GET',
                    url: '{{ url("relasiran/valid") }}/'+value,
                    dataType: 'json',
                    success : function (response){
                        var fer = response;
                        console.log(response)
                        if(x < max_fields){ //max input box allowed
                            x++; //text box increment
                            $(wrapper0).append('<tr class="tambahan0'+response+'"><td><a name="" class="removeHop'+response+' label label-danger" href="javascript:void(0)" role="button"> - </a></td><td><input autocomplete=off type="text" class="border-0" name="sto" id="sto'+response+'" value=" - "></td></tr>');
                            $(wrapper1).append('<tr class="tambahan1'+response+'"><td><input autocomplete=off type="text" class="border-0" name="ne_transport" id="ne_transport'+response+'" value=" - "></td></tr>');
                            $(wrapper2).append('<tr class="tambahan2'+response+'"><td><input autocomplete=off class="border-0 type="text" name="" id="ssp'+response+'" value=" - "></td></tr>');
                            $(wrapper3).append('<tr class="tambahan3'+response+'"><td><input autocomplete=off class="border-0 type="text" name="" id="tieline'+response+'"  value=" - "></td></tr>');
                            $(wrapper4).append('<tr class="tambahan1'+response+'"><td><input value="-" class="border-0" disabled id="ne_transport'+response+'b"></td></tr>');
                            $(wrapper5).append('<tr class="tambahan2'+response+'"><td><input value="-" class="border-0" disabled id="ssp'+response+'b"></td></tr>');
                            $(wrapper6).append('<tr class="tambahan3'+response+'"><td><input value="-" class="border-0" disabled id="tieline'+response+'b"></td></tr>');
                            $(wrapper7).append('<tr class="tambahan0'+response+'"><td><input value="-" class="border-0" disabled id="sto'+response+'b"></td></tr>');
                        }  

                        $(".removeHop"+response).on("click", function(){ //user click on remove text
                        // e.preventDefault(); 
                        $(".tambahan0"+response).remove();
                        $(".tambahan1"+response).remove();
                        $(".tambahan2"+response).remove();
                        $(".tambahan3"+response).remove();
                        $.ajax({
                            type: 'GET',
                            url: '{{ url("relasiran/validNot") }}/'+response,
                            dataType: 'json',
                            success : function (response){
                                console.log(response)
                            }
                        })
                    })

                    $("#sto"+response).change( function() {
                        var id = $(this).val();
                        var ne = $("#ne_transport"+response).val();
                        $("#sto"+response+"b").val(id)
                        $.ajax({
                            type: "GET",
                            url : "{{ url('relasi/makeSTO/') }}/"+ne+"/"+id,
                            success : function(res){
                                $("#sto"+response).css({"color": "blue", "font-style": "italic"});
                                setTimeout(function(){ 
                                    $("#sto"+response).css({"color": "black", "font-style": "normal"});
                                }, 3000);
                            }
                        })
                    })

                    $("#ne_transport"+response).change(function() {
                        var id = $(this).val();
                        var ne = $(this).val();
                        $("#ne_transport"+response+"b").val(id)
                        $.ajax({
                            type: "GET",
                            url : "{{ url('relasiran/makeNe/') }}/"+response+"/"+id,
                            success : function(resp){
                                $("#ne_transport"+response).css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#ne_transport"+response).css({"color": "black", "font-style": "normal"});
                             }, 3000);

                             $("#sto"+response).val(resp);
                             $("#sto"+response+"b").val(resp);
                            }
                        })
                    }) 
                    $("#ssp"+response).change( function() {
                        var id = $(this).val();
                        $("#ssp"+response+"b").val(id)
                        $.ajax({
                            type: "GET",
                            url : "{{ url('relasiran/makeSsp/') }}/"+response+"/"+id,
                            success : function(resp){
                             $("#ssp"+response).css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#ssp"+response).css({"color": "black", "font-style": "normal"});
                             }, 3000);
                            }
                        })
                    })
                    $("#tieline"+response).change( function() {
                    var id = $(this).val();
                    $("#tieline"+response+"b").val(id)
                        $.ajax({
                            type: "GET",
                            url : "{{ url('relasiran/makeTie/') }}/"+response+"/"+id,
                            success : function(resp){
                                $("#tieline"+response).css({"color": "blue", "font-style": "italic"});
                                setTimeout(function(){ 
                                    $("#tieline"+response).css({"color": "black", "font-style": "normal"});
                                }, 3000);

                            }
                        })
                    })
                                        
                    }
                })
            });

        });
        </script>
        <?php $id = $ib['idipran'];  $Relasiipran =  App\Relasiipran::where('id_ipran',$id); ?>
        <script>
                $("#kota_a{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/kota_a/') }}/{{$id}}/"+id,
                        success : function(response){

                            $("#kota_a{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#kota_a{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });
                $("#kota_b{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/kota_b/') }}/{{$id}}/"+id,
                        success : function(response){

                            $("#kota_b{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#kota_b{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });
                $("#router_a{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/router_a/') }}/{{$id}}/"+id,
                        success : function(response){

                            $("#router_a{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#router_a{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });
                $("#router_b{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/router_b/') }}/{{$id}}/"+id,
                        success : function(response){

                            $("#router_b{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#router_b{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });
                $("#capacity{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/capacity/') }}/{{$id}}/"+id,
                        success : function(response){
                            $("#capacity{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#capacity{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });

                $("#layer{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/layer/') }}/{{$id}}/"+id,
                        success : function(response){
                            $("#layer{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#layer{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });

                $("#system{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/system/') }}/{{$id}}/"+id,
                        success : function(response){
                            $("#system{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#system{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });

                $("#transport{{$id}}").change( function() {
                    var id = $(this).val();
                    // alert(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('ipran/transport/') }}/{{$id}}/"+id,
                        success : function(response){
                            $("#transport{{$id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#transport{{$id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                        }
                    })

                });

                $("#port_a{{$id}}").change( function() {
                        var data = $("#port_a{{$id}}form").serialize();
                        // alert(data);
                        $.ajax({
                            url : "{{ url('ipran/editsPorta')}}",
                            type: "POST",
                            data: data,
                            success : function(response){
                                $("#port_a{{$id}}").css({"color": "blue", "font-style": "italic"});
                                setTimeout(function(){ 
                                    $("#port_a{{$id}}").css({"color": "black", "font-style": "normal"});
                                 }, 3000);
    
                            }
                        })
    
                    });

                $("#port_b{{$id}}").change( function() {
                        var data = $("#port_b{{$id}}form").serialize();
                        // alert(data);
                        $.ajax({
                            url : "{{ url('ipran/editsPortb')}}",
                            type: "POST",
                            data: data,
                            success : function(response){
                                $("#port_b{{$id}}").css({"color": "blue", "font-style": "italic"});
                                setTimeout(function(){ 
                                    $("#port_b{{$id}}").css({"color": "black", "font-style": "normal"});
                                 }, 3000);
    
                            }
                        })
    
                    });

        </script>
        @foreach ($Relasiipran->get() as $relasi)
        <script>

                $("#sto{{$relasi->id}}").change( function() {
                    var id = $(this).val();
                    var ne = $("#ne_transport{{$relasi->id}}").val();
                    $("#sto{{$relasi->id}}b").val(id)
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasi/makeSTO/') }}/"+ne+"/"+id,
                        success : function(response){
                            $("#sto{{$relasi->id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#sto{{$relasi->id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);
                        }
                    })
                })

                $("#ne_transport{{$relasi->id}}").change( function() {
                    var id = $(this).val();
                    $("#ne_transport{{$relasi->id}}b").val(id)
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasi/makeNe/') }}/{{$relasi->id}}/"+id,
                        success : function(response){

                            $("#ne_transport{{$relasi->id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#ne_transport{{$relasi->id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);

                             $("#ne_transport{{$relasi->id}}").val(id);
                             $("#sto{{$relasi->id}}").val(response)
                             $("#sto{{$relasi->id}}b").val(response)

                        }
                    })
                })
                $("#ssp{{$relasi->id}}").change( function() {
                    var id = $(this).val();
                    $("#ssp{{$relasi->id}}b").val(id);
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasiran/makeSsp/') }}/{{$relasi->id}}/"+id,
                        success : function(response){
                            $("#ssp{{$relasi->id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#ssp{{$relasi->id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);
                        }
                    })
                })
                $("#tieline{{$relasi->id}}").change( function() {
                    var id = $(this).val();
                    $("#tieline{{$relasi->id}}b").val(id) 
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasiran/makeTie/') }}/{{$relasi->id}}/"+id,
                        success : function(response){
                            $("#tieline{{$relasi->id}}").css({"color": "blue", "font-style": "italic"});
                            setTimeout(function(){ 
                                $("#tieline{{$relasi->id}}").css({"color": "black", "font-style": "normal"});
                             }, 3000);
                        }
                    })
                })
                $("#removeHop{{$relasi->id}}").on("click", function(){ //user click on remove text
                        // e.preventDefault(); 
                        $(".input7{{$relasi->id}}").remove();
                        $(".input0{{$relasi->id}}").remove();
                        $(".input1{{$relasi->id}}").remove();
                        $(".input2{{$relasi->id}}").remove();
                        $(".input3{{$relasi->id}}").remove();
                        $.ajax({
                            type: 'GET',
                            url: '{{ url("relasiran/validNot/$relasi->id") }}',
                            dataType: 'json',
                            success : function (response){
                                console.log(response)
                            }
                        })

                    })

      </script>
        @endforeach
      @endforeach
@stop
    
