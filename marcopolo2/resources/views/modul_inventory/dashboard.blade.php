@extends('layout-db')

@section('menu4')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/jvectormap/jqvmap.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/morrisjs/morris.css')}}" rel="stylesheet" />
@stop

@section('title')
Inventory Dashboard
@stop

@section('content')

    <section id="counters" class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>INVENTORY DASHBOARD <span>{{date("d M Y")}} (Gbps)</span></h2>
                <!-- <a href="<?php //echo asset('storage/file.txt') ?>">ooooo</a> -->
            </div>

            <div class="row clearfix">

                <div class="col-md-12 topologi">
                    <img src="{{asset('images/topologi.png')}}">
                </div>

                <div class="col-md-12">
                    <div class="col-lg-20 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box custom bg-pink">
                            <div class="icon">
                                <i class="material-icons">dns</i>
                            </div>
                            <div class="content">
                                <div class="text">ACCESS</div>
                                <div class="text links">{{\App\DatekAccess::AllLink()}} Links</div>
                                @if(\App\DatekAccess::AllCapacity()>0)
                                    <div class="number count-to" data-from="0" data-to="{{ceil(\App\DatekAccess::AllCapacity()/1000)}}" data-speed="1000" data-fresh-interval="20">{{ceil(\App\DatekAccess::AllCapacity()/1000)}}G</div>
                                @else
                                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0 G</div>
                                @endif
                            </div>
                        </div>
                        <div class="progress">
                            @if(\App\DatekAccess::AllCapacity()>0)
                                <div class="progress-bar bg-pink progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: {{(\App\DatekAccess::AllUtilizationCap()/\App\DatekAccess::AllCapacity())*100}}%">
                                </div>
                                <span class="util-text">{{number_format(\App\DatekAccess::AllUtilizationCap()/1000)}} G ({{number_format((\App\DatekAccess::AllUtilizationCap()/\App\DatekAccess::AllCapacity())*100)}}%)</span>
                            @else
                                <div class="progress-bar bg-pink progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                </div>
                                <span class="util-text">0 G (0%)</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-20 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box custom bg-cyan">
                            <div class="icon">
                                <i class="material-icons">dns</i>
                            </div>
                            <div class="content">
                                <div class="text">BACKHAUL</div>
                                <div class="text links">{{\App\DatekBackhaul::AllLink()}} Links</div>
                                @if(\App\DatekBackhaul::AllCapacity()>0)
                                    <div class="number count-to" data-from="0" data-to="{{\App\DatekBackhaul::AllCapacity()}}" data-speed="1000" data-fresh-interval="20">{{\App\DatekBackhaul::AllCapacity()}} G</div>
                                @else
                                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0 G</div>
                                @endif
                            </div>
                        </div>
                        <div class="progress">
                            @if(\App\DatekBackhaul::AllCapacity()>0)
                                <div class="progress-bar bg-cyan progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{(\App\DatekBackhaul::AllUtilizationCap()/\App\DatekBackhaul::AllCapacity())*100}}%">
                                </div>
                                <span class="util-text">{{number_format(\App\DatekBackhaul::AllUtilizationCap())}} G ({{number_format((\App\DatekBackhaul::AllUtilizationCap()/\App\DatekBackhaul::AllCapacity())*100)}}%)</span>
                            @else
                                <div class="progress-bar bg-cyan progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                </div>
                                <span class="util-text">0 G (0%)</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-40 col-md-3 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="info-box custom bg-light-green" style="height:60px;margin-bottom:5px;">
                                    <div class="icon" style="width:40px;">
                                        <i class="material-icons" style="font-size:30px;line-height:60px;">dns</i>
                                    </div>
                                    <div class="content">
                                        <div class="text">IPRAN - {{\App\DatekIpran::AllLink()}} Links</div>
                                        <div class="text links">
                                        @if(\App\DatekIpran::AllCapacity()>0)
                                            <div class="number count-to" data-from="0" data-to="{{\App\DatekIpran::AllCapacity()}}" data-speed="1000" data-fresh-interval="20">{{\App\DatekIpran::AllCapacity()}} G</div>
                                        @else
                                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0 G</div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box custom bg-orange" style="height:60px;margin-bottom:5px;">
                                    <div class="icon" style="width:40px;">
                                        <i class="material-icons" style="font-size:30px;line-height:60px;">dns</i>
                                    </div>
                                    <div class="content">
                                        <div class="text">IPBB - {{\App\DatekIpbb::AllLink()}} Links</div>
                                        <div class="text links">
                                        @if(\App\DatekIpbb::AllCapacity()>0)
                                            <div class="number count-to" data-from="0" data-to="{{\App\DatekIpbb::AllCapacity()}}" data-speed="1000" data-fresh-interval="20">{{\App\DatekIpbb::AllCapacity()}} G</div>
                                        @else
                                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0 G</div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="info-box custom bg-purple" style="height:55px;">
                                    <div class="icon" style="width:40px;">
                                        <i class="material-icons" style="font-size:30px;line-height:55px;">dns</i>
                                    </div>
                                    <div class="content">
                                        <div class="text">OneIPMPLS - {{\App\DatekIpran::AllLink() + \App\DatekIpbb::AllLink()}} Links</div>
                                        <div class="text links">
                                        @if(\App\DatekIpran::AllCapacity() + \App\DatekIpbb::AllCapacity() > 0)
                                            <div class="number count-to" data-from="0" data-to="{{\App\DatekIpran::AllCapacity() + \App\DatekIpbb::AllCapacity()}}" data-speed="1000" data-fresh-interval="20">{{\App\DatekIpbb::AllCapacity()}} G</div>
                                        @else
                                            <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0 G</div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="progress">
                                            @if(\App\DatekIpran::AllCapacity()>0)
                                                <div class="progress-bar bg-light-green progress-bar-striped active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: {{(\App\DatekIpran::AllUtilizationCap()/\App\DatekIpran::AllCapacity())*100}}%">
                                                </div>
                                                <span class="util-text">{{number_format(\App\DatekIpran::AllUtilizationCap())}} G ({{number_format((\App\DatekIpran::AllUtilizationCap()/\App\DatekIpran::AllCapacity())*100)}}%)</span>
                                            @else
                                                <div class="progress-bar bg-light-green progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                </div>
                                                <span class="util-text">0 G (0%)</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="progress">
                                            @if(\App\DatekIpbb::AllCapacity()>0)
                                                <div class="progress-bar bg-orange progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{(\App\DatekIpbb::AllUtilizationCap()/\App\DatekIpbb::AllCapacity())*100}}%">
                                                </div>
                                                <span class="util-text">{{number_format(\App\DatekIpbb::AllUtilizationCap())}} G ({{number_format((\App\DatekIpbb::AllUtilizationCap()/\App\DatekIpbb::AllCapacity())*100)}}%)</span>
                                            @else
                                                <div class="progress-bar bg-orange progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                </div>
                                                <span class="util-text">0 G (0%)</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-20 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box custom bg-blue">
                            <div class="icon">
                                <i class="material-icons">dns</i>
                            </div>
                            <div class="content">
                                <div class="text">IX</div>
                                <div class="text links">{{\App\DatekIx::AllLink()}} Links</div>
                                @if(\App\DatekIx::AllCapacity()>0)
                                    <div class="number count-to" data-from="0" data-to="{{\App\DatekIx::AllCapacity()}}" data-speed="1000" data-fresh-interval="20">{{\App\DatekIx::AllCapacity()}} G</div>
                                @else
                                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20">0 G</div>
                                @endif
                            </div>
                        </div>
                        <div class="progress">
                            @if(\App\DatekIx::AllCapacity()>0)
                                <div class="progress-bar bg-blue progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{(\App\DatekIx::AllUtilizationCap()/\App\DatekIx::AllCapacity())*100}}%">
                                </div>
                                <span class="util-text">{{number_format(\App\DatekIx::AllUtilizationCap())}} G ({{number_format((\App\DatekIx::AllUtilizationCap()/\App\DatekIx::AllCapacity())*100)}}%)</span>
                            @else
                                <div class="progress-bar bg-blue progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                </div>
                                <span class="util-text">0 G (0%)</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">

                            <h4 class="align-center m-b-10">VISUALISASI PETA</h4>

                            <div id="vmap">
                                <div id="reg1" class="jqvmap-label">REGIONAL 1<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(1)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(1)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(1)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(1)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(1)}}</div>
                                <div id="reg2" class="jqvmap-label">REGIONAL 2<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(2)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(2)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(2)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(2)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(2)}}</div>
                                <div id="reg3" class="jqvmap-label">REGIONAL 3<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(3)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(3)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(3)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(3)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(3)}}</div>
                                <div id="reg4" class="jqvmap-label">REGIONAL 4<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(4)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(4)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(4)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(4)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(4)}}</div>
                                <div id="reg5" class="jqvmap-label">REGIONAL 5<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(5)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(5)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(5)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(5)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(5)}}</div>
                                <div id="reg6" class="jqvmap-label">REGIONAL 6<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(6)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(6)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(6)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(6)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(6)}}</div>
                                <div id="reg7" class="jqvmap-label">REGIONAL 7<br><br>Access Cap: {{ceil(\App\DatekAccess::Capacity(7)/1000)}}<br>Backhaul Cap: {{\App\DatekBackhaul::Capacity(7)}}<br>IPRAN Cap: {{\App\DatekIpran::Capacity(7)}}<br>IPBB Cap: {{\App\DatekIpbb::Capacity(7)}}<br>OneIPMPLS Cap: 0<br>IX Cap: {{\App\DatekIx::Capacity(7)}}</div>
                            </div>

                            <ul class="nav nav-tabs tab-nav-right m-t-10" role="tablist">
                                <li role="presentation" class="active"><a href="#access" data-toggle="tab">ACCESS</a></li>
                                <li role="presentation"><a href="#backhaul" data-toggle="tab">BACKHAUL</a></li>
                                <li role="presentation"><a href="#ipran" data-toggle="tab">IPRAN</a></li>
                                <li role="presentation"><a href="#ipbb" data-toggle="tab">IPBB</a></li>
                                <li role="presentation"><a href="#oneipmpls" data-toggle="tab">OneIPMPLS</a></li>
                                <li role="presentation"><a href="#ix" data-toggle="tab">IX</a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="access">
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">GROWTH TREND (Gbps)</h2>
                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="regional" id="AccessLine" onchange="AccessLine()">
                                                                <option value="All" selected="true">Nasional</option>
                                                                <option value="R1">Regional 1</option>
                                                                <option value="R2">Regional 2</option>
                                                                <option value="R3">Regional 3</option>
                                                                <option value="R4">Regional 4</option>
                                                                <option value="R5">Regional 5</option>
                                                                <option value="R6">Regional 6</option>
                                                                <option value="R7">Regional 7</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="line_chart_acc"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">ALL TREG (Gbps)</h2>
                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="month" id="AccessBar" onchange="AccessBar()">
                                                                <option id="acc1" value="1">Januari</option>
                                                                <option id="acc2" value="2">Februari</option>
                                                                <option id="acc3" value="3">Maret</option>
                                                                <option id="acc4" value="4">April</option>
                                                                <option id="acc5" value="5">Mei</option>
                                                                <option id="acc6" value="6">Juni</option>
                                                                <option id="acc7" value="7">Juli</option>
                                                                <option id="acc8" value="8">Agustus</option>
                                                                <option id="acc9" value="9">September</option>
                                                                <option id="acc10" value="10">Oktober</option>
                                                                <option id="acc11" value="11">November</option>
                                                                <option id="acc12" value="12">Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="bar_chart_acc"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT TYPE</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart_acc"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT SYSTEM</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart2_acc"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card no-margin">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">5 HIGHEST UTILIZATION</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Regional</th>
                                                                <th>Site ID</th>
                                                                <th>Capacity</th>
                                                                <th>Utilization</th>
                                                                <th>Occupation</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i=0; ?>
                                                        @foreach(\App\DatekAccess::FiveOverThreshold() as $data)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <td>{{$data['reg_telkom']}}</td>
                                                                <td>{{$data['siteid_tsel']}}</td>
                                                                <td>{{$data['capacity']}}</td>
                                                                <td>{{number_format($data['utilization_cap'],2,",",".")}}</td>
                                                                <td>{{number_format($data['utilization_max'])}} %</td>
                                                            </tr>

                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div style="text-align:right">
                                                        <a href="{{URL::Route('inventory.access.overthreshold')}}" class="btn btn-info">
                                                            <span>View more...</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="backhaul">
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY GROWTH TREND</h2>
                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="regional" id="BackhaulLine" onchange="BackhaulLine()">
                                                                <option value="All" selected="true">Nasional</option>
                                                                <option value="R1">Regional 1</option>
                                                                <option value="R2">Regional 2</option>
                                                                <option value="R3">Regional 3</option>
                                                                <option value="R4">Regional 4</option>
                                                                <option value="R5">Regional 5</option>
                                                                <option value="R6">Regional 6</option>
                                                                <option value="R7">Regional 7</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="line_chart_bh"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY ALL TREG</h2>
                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="month"  id="BackhaulBar" onchange="BackhaulBar()">
                                                                <option id="bh1" value="1">Januari</option>
                                                                <option id="bh2" value="2">Februari</option>
                                                                <option id="bh3" value="3">Maret</option>
                                                                <option id="bh4" value="4">April</option>
                                                                <option id="bh5" value="5">Mei</option>
                                                                <option id="bh6" value="6">Juni</option>
                                                                <option id="bh7" value="7">Juli</option>
                                                                <option id="bh8" value="8">Agustus</option>
                                                                <option id="bh9" value="9">September</option>
                                                                <option id="bh10" value="10">Oktober</option>
                                                                <option id="bh11" value="11">November</option>
                                                                <option id="bh12" value="12">Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="bar_chart_bh"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card no-margin">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">5 HIGHEST UTILIZATION</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Regional</th>
                                                                <th>Metro</th>
                                                                <th>Port</th>
                                                                <th>Capacity</th>
                                                                <th>Utilization</th>
                                                                <th>Occupation</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php $i=0; ?>
                                                            @foreach(\App\DatekBackhaul::FiveOverThreshold() as $data)
                                                                <?php $i++; ?>
                                                                <tr>
                                                                    <td>{{$data['reg_telkom']}}</td>
                                                                    <td>{{$data['metroe_telkom']}}</td>
                                                                    <td>{{$data['port_telkom']}}</td>
                                                                    <td>{{$data['capacity']}}</td>
                                                                    <td>{{number_format($data['utilization_cap'],2,",",".")}}</td>
                                                                    <td>{{number_format($data['utilization_max'])}} %</td>
                                                                </tr>

                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div style="text-align:right">
                                                        <a href="{{URL::Route('inventory.backhaul.overthreshold')}}" class="btn btn-info">
                                                            <span>View more...</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="ipran">
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">

                                                        <h2 class="pull-left">CAPACITY GROWTH TREND</h2>

                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="regional"  id="IpranLine" onchange="IpranLine()">
                                                                <option value="All" selected="true">Nasional</option>
                                                                <option value="R1">Regional 1</option>
                                                                <option value="R2">Regional 2</option>
                                                                <option value="R3">Regional 3</option>
                                                                <option value="R4">Regional 4</option>
                                                                <option value="R5">Regional 5</option>
                                                                <option value="R6">Regional 6</option>
                                                                <option value="R7">Regional 7</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="line_chart_ipran"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY ALL TREG</h2>

                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="month"  id="IpranBar" onchange="IpranBar()">
                                                                <option id="ipran1" value="1">Januari</option>
                                                                <option id="ipran2" value="2">Februari</option>
                                                                <option id="ipran3" value="3">Maret</option>
                                                                <option id="ipran4" value="4">April</option>
                                                                <option id="ipran5" value="5">Mei</option>
                                                                <option id="ipran6" value="6">Juni</option>
                                                                <option id="ipran7" value="7">Juli</option>
                                                                <option id="ipran8" value="8">Agustus</option>
                                                                <option id="ipran9" value="9">September</option>
                                                                <option id="ipran10" value="10">Oktober</option>
                                                                <option id="ipran11" value="11">November</option>
                                                                <option id="ipran12" value="12">Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="bar_chart_ipran"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">

                                                        <h2 class="pull-left">TRANSPORT TYPE</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart_ipran"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT SYSTEM</h2>
                                                    </div>
                                                </div>
                                                <div class="body">

                                                    <canvas id="donut_chart2_ipran"></canvas>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card no-margin">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">5 HIGHEST UTILIZATION</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Regional</th>
                                                                <th>Nearend</th>
                                                                <th>Port</th>
                                                                <th>Farend</th>
                                                                <th>Port</th>
                                                                <th>Capacity</th>
                                                                <th>Utilization</th>
                                                                <th>Occupation</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i=0; ?>
                                                        @foreach(\App\DatekIpran::FiveOverThreshold() as $data)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <td>{{$data['reg_telkom']}}</td>
                                                                <td>{{$data['metroe_end1_telkom']}}</td>
                                                                <td>{{$data['port_end1_telkom']}}</td>
                                                                <td>{{$data['metroe_end2_telkom']}}</td>
                                                                <td>{{$data['port_end2_telkom']}}</td>
                                                                <td>{{$data['capacity']}}</td>
                                                                <td>{{number_format($data['utilization_cap'],2,",",".")}}</td>
                                                                <td>{{number_format($data['utilization_max'])}} %</td>
                                                            </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                    <div style="text-align:right">
                                                        <a href="{{URL::Route('inventory.ipran.overthreshold')}}" class="btn btn-info">
                                                            <span>View more...</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="ipbb">
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY GROWTH TREND</h2>
                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="regional" id="IpbbLine" onchange="IpbbLine()">
                                                                <option value="All" selected="true">Nasional</option>
                                                                <option value="R1">Regional 1</option>
                                                                <option value="R2">Regional 2</option>
                                                                <option value="R3">Regional 3</option>
                                                                <option value="R4">Regional 4</option>
                                                                <option value="R5">Regional 5</option>
                                                                <option value="R6">Regional 6</option>
                                                                <option value="R7">Regional 7</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="line_chart_ipbb"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY ALL TREG</h2>
                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="month"  id="IpbbBar" onchange="IpbbBar()">
                                                                <option id="ipbb1" value="1">Januari</option>
                                                                <option id="ipbb2" value="2">Februari</option>
                                                                <option id="ipbb3" value="3">Maret</option>
                                                                <option id="ipbb4" value="4">April</option>
                                                                <option id="ipbb5" value="5">Mei</option>
                                                                <option id="ipbb6" value="6">Juni</option>
                                                                <option id="ipbb7" value="7">Juli</option>
                                                                <option id="ipbb8" value="8">Agustus</option>
                                                                <option id="ipbb9" value="9">September</option>
                                                                <option id="ipbb10" value="10">Oktober</option>
                                                                <option id="ipbb11" value="11">November</option>
                                                                <option id="ipbb12" value="12">Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="bar_chart_ipbb"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT TYPE</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart_ipbb"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT SYSTEM</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart2_ipbb"></canvas>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card no-margin">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">5 HIGHEST UTILIZATION</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Regional</th>
                                                                <th>Nearend</th>
                                                                <th>Port</th>
                                                                <th>Farend</th>
                                                                <th>Port</th>
                                                                <th>Capacity</th>
                                                                <th>Utilization</th>
                                                                <th>Occupation</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i=0; ?>
                                                        @foreach(\App\DatekIpbb::FiveOverThreshold() as $data)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <td>{{$data['reg_telkom']}}</td>
                                                                <td>{{$data['router_node_a']}}</td>
                                                                <td>{{$data['port_end1_tsel']}}</td>
                                                                <td>{{$data['router_node_b']}}</td>
                                                                <td>{{$data['port_end2_tsel']}}</td>
                                                                <td>{{$data['capacity']}}</td>
                                                                <td>{{number_format($data['utilization_cap'],2,",",".")}}</td>
                                                                <td>{{number_format($data['utilization_max'])}} %</td>
                                                            </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                    <div style="text-align:right">
                                                        <a href="{{URL::Route('inventory.ipbb.overthreshold')}}" class="btn btn-info">
                                                            <span>View more...</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="ix">
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY GROWTH TREND</h2>

                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="regional"  id="IxLine" onchange="IxLine()">
                                                                <option value="All" selected="true">Nasional</option>
                                                                <option value="R1">Regional 1</option>
                                                                <option value="R2">Regional 2</option>
                                                                <option value="R3">Regional 3</option>
                                                                <option value="R4">Regional 4</option>
                                                                <option value="R5">Regional 5</option>
                                                                <option value="R6">Regional 6</option>
                                                                <option value="R7">Regional 7</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">

                                                    <canvas id="line_chart_ix"></canvas>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">

                                                        <h2 class="pull-left">CAPACITY ALL TREG</h2>

                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="month"  id="IxBar" onchange="IxBar()">
                                                                <option id="ix1" value="1">Januari</option>
                                                                <option id="ix2" value="2">Februari</option>
                                                                <option id="ix3" value="3">Maret</option>
                                                                <option id="ix4" value="4">April</option>
                                                                <option id="ix5" value="5">Mei</option>
                                                                <option id="ix6" value="6">Juni</option>
                                                                <option id="ix7" value="7">Juli</option>
                                                                <option id="ix8" value="8">Agustus</option>
                                                                <option id="ix9" value="9">September</option>
                                                                <option id="ix10" value="10">Oktober</option>
                                                                <option id="ix11" value="11">November</option>
                                                                <option id="ix12" value="12">Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="bar_chart_ix"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT TYPE</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart_ix" ></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT SYSTEM</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart2_ix"></canvas>

                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card no-margin">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">5 HIGHEST UTILIZATION</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Regional</th>
                                                                <th>Metro</th>
                                                                <th>Port</th>
                                                                <th>Capacity</th>
                                                                <th>Utilization</th>
                                                                <th>Occupation</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i=0; ?>
                                                        @foreach(\App\DatekIx::FiveOverThreshold() as $data)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <td>{{number_format($data['reg_telkom'])}}</td>
                                                                <td>{{number_format($data['metroe_end1_telkom'])}}</td>
                                                                <td>{{number_format($data['port_end1_telkom'])}}</td>
                                                                <td>{{number_format($data['capacity'])}}</td>
                                                                <td>{{number_format($data['utilization_cap'],2,",",".")}}</td>
                                                                <td>{{number_format($data['utilization_max'])}} %</td>
                                                            </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                    <div style="text-align:right">
                                                        <a href="{{URL::Route('inventory.ix.overthreshold')}}" class="btn btn-info">
                                                            <span>View more...</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="oneipmpls">
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">CAPACITY GROWTH TREND</h2>

                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="regional"  id="OneIPMPLSLine" onchange="OneIPMPLSLine()">
                                                                <option value="All" selected="true">Nasional</option>
                                                                <option value="R1">Regional 1</option>
                                                                <option value="R2">Regional 2</option>
                                                                <option value="R3">Regional 3</option>
                                                                <option value="R4">Regional 4</option>
                                                                <option value="R5">Regional 5</option>
                                                                <option value="R6">Regional 6</option>
                                                                <option value="R7">Regional 7</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">

                                                    <canvas id="line_chart_oneipmpls"></canvas>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">

                                                        <h2 class="pull-left">CAPACITY ALL TREG</h2>

                                                        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 pull-right no-margin menu-chart">
                                                            <select class="form-control show-tick" name="month"  id="OneIPMPLSBar" onchange="OneIPMPLSBar()">
                                                                <option id="ix1" value="1">Januari</option>
                                                                <option id="ix2" value="2">Februari</option>
                                                                <option id="ix3" value="3">Maret</option>
                                                                <option id="ix4" value="4">April</option>
                                                                <option id="ix5" value="5">Mei</option>
                                                                <option id="ix6" value="6">Juni</option>
                                                                <option id="ix7" value="7">Juli</option>
                                                                <option id="ix8" value="8">Agustus</option>
                                                                <option id="ix9" value="9">September</option>
                                                                <option id="ix10" value="10">Oktober</option>
                                                                <option id="ix11" value="11">November</option>
                                                                <option id="ix12" value="12">Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="bar_chart_oneipmpls"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT TYPE</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart_oneipmpls"></canvas>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">TRANSPORT SYSTEM</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <canvas id="donut_chart2_oneipmpls"></canvas>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="card no-margin">
                                                <div class="header">
                                                    <div class="row clearfix">
                                                        <h2 class="pull-left">5 HIGHEST UTILIZATION</h2>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Regional</th>
                                                            <th>Metro</th>
                                                            <th>Port</th>
                                                            <th>Capacity</th>
                                                            <th>Utilization</th>
                                                            <th>Occupation</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i=0; ?>
                                                        @foreach(\App\DatekOneIPMPLS::FiveOverThreshold() as $data)
                                                            <?php $i++; ?>
                                                            <tr>
                                                                <td>{{number_format($data['reg_telkom'])}}</td>
                                                                <td>{{number_format($data['metroe_end1_telkom'])}}</td>
                                                                <td>{{number_format($data['port_end1_telkom'])}}</td>
                                                                <td>{{number_format($data['capacity'])}}</td>
                                                                <td>{{number_format($data['utilization_cap'],2,",",".")}}</td>
                                                                <td>{{number_format($data['utilization_max'])}} %</td>
                                                            </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                    <div style="text-align:right">
                                                        <a href="{{URL::Route('inventory.OneIPMPLS.overthreshold')}}" class="btn btn-info">
                                                            <span>View more...</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

    <script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('plugins/node-waves/waves.js')}}"></script>
    <script src="{{asset('plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <script src="{{asset('plugins/chartjs/Chart.bundle.js')}}"></script>
    <script src="{{asset('plugins/chartjs/Chart.PieceLabel.js')}}"></script>
    <script src="{{asset('plugins/chartjs/chartjs-plugin-datalabels.js')}}"></script>
    <script src="{{asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>

    <script src="{{asset('plugins/jvectormap/jquery.vmap.js')}}"></script>

    <script src="{{asset('js/admin.js')}}"></script>

    <script>
        var bulan = moment().format("M");
        document.getElementById("acc"+bulan).selected = true;
        document.getElementById("bh"+bulan).selected = true;
        document.getElementById("ipran"+bulan).selected = true;
        document.getElementById("ipbb"+bulan).selected = true;
        document.getElementById("ix"+bulan).selected = true;
    </script>

    @include('modul_inventory.chart')
    @include('modul_inventory.map')
    @include('modul_inventory.chart_change')

    <script src="{{asset('js/pages/widgets/infobox/infobox-2.js')}}"></script>

    <script src="{{asset('js/demo.js')}}"></script>

    <script>
                $('.number').countTo({
                    onComplete: function() {
                        $(this).append(" G");
                    }
                });

    </script>

@stop
