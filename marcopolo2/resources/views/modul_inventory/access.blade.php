@extends('layout')

@section('menu5')
    class="active"
@stop

@section('title')
Access - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">Access</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>ACCESS</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.access.import')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_upload</i><span>Import Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-6">
                                    <div id="topologi">
                                        <div id="graph-container"></div>
                                    </div>
                                    <script src="{{asset('plugins/sigma.js/sigma.renderers.customShapes/shape-topology.js')}}"></script>
                                    <script src="{{asset('plugins/sigma.js/sigma.renderers.customShapes/sigma.renderers.customShapes.js')}}"></script>
                                    <script src="{{asset('plugins/sigma.js/sigma.renderers.customEdgeShapes/sigma.canvas.edges.dashed.js')}}"></script>
                                    <script src="{{asset('js/networks-topology/access.js')}}"></script>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                            <th>UtilizationCap</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 1)}}">Regional 1</a></td>
                                            <td>{{\App\DatekAccess::Linss(1)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(1)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(1)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 2)}}">Regional 2</a></td>
                                            <td>{{\App\DatekAccess::Linss(2)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(2)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(2)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 3)}}">Regional 3</a></td>
                                            <td>{{\App\DatekAccess::Linss(3)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(3)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(3)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 4)}}">Regional 4</a></td>
                                            <td>{{\App\DatekAccess::Linss(4)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(4)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(4)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 5)}}">Regional 5</a></td>
                                            <td>{{\App\DatekAccess::Linss(5)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(5)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(5)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 6)}}">Regional 6</a></td>
                                            <td>{{\App\DatekAccess::Linss(6)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(6)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(6)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 7)}}">Regional 7</a></td>
                                            <td>{{\App\DatekAccess::Linss(7)}}</td>
                                            <td>{{ceil(\App\DatekAccess::Capacity(7)/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::UtilizationCap(7)/1000)}}</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td><a href="{{URL::Route('inventory.access.detail', 0)}}">Nasional</a></td>
                                            <td>{{\App\DatekAccess::AllLink()}}</td>
                                            <td>{{ceil(\App\DatekAccess::AllCapacity()/1000)}}</td>
                                            <td>{{number_format(\App\DatekAccess::AllUtilizationCap()/1000)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop