@extends('layout')

@section('menu8')
    class="active"
@stop

@section('css')
    {{-- <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet"> --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

@stop

@section('title')
IPBB Detail Regional - Inventory Datek
@stop

<style>
    .border-0 {
        border:none;
    }
</style>

    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/ipbb')}}">IPBB</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IPBB - DETAIL REGIONAL {{$regional}}</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.ipbb', $regional)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <div class="alert alert-primary" hidden role="alert">
                                    Harap Tunggu
                                </div>
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" id="table_id" style="width: 100% !important">
                                    <thead class="text-center">
                                        <tr>
                                            <th class="no">ID</th>
                                            <th>Link</th>
                                            <th>Kota</th>
                                            <th>Router A</th>
                                            <th>Port</th>
                                            <th>Kota</th>
                                            <th>Router B</th>
                                            <th>Port</th>
                                            <th>NE</th>
                                            <th>Shelf / Slot /Port</th>
                                            <th>Tie Line A</th>
                                            {{-- <th>NE</th>
                                            <th>Shelf / Slot / Port</th>
                                            <th>Tie Line B</th>  --}}
                                            <th></th> 
                                            <th></th> 
                                            <th> Capacity </th> 
                                            <th> Utilization </th> 
                                            <th> Utilization In </th> 
                                            <th> Utilization Out </th> 
                                            <th> Utilization Max </th> 
                                            <th> Layer </th> 
                                            <th> System </th> 
                                            <th> Transport Type</th> 
                                            <th> Last Update</th> 
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@stop

@section('js')
    {{-- <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>--}}
    {{-- <script src="{{asset('js/pages/tables/jqueype="ry-datatable.js')}}"></script> --}}
    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    
    <script>
        $(document).ready(function() {
            tablepbb = $('#table_id').DataTable({
            ajax: {
                // "url": '{{ url("ipbb/data/4") }}'
                "url":"http://localhost/marcopolo/public/ipbb/data/4",
                "type": "GET",
                "data": {}
            },
            "language": {
                "emptyTable": "Tidak Ada Data Tersimpan"
            },
            "paging": true,
            "destroy": true,
            "pageLength": 10,
            "lengthMenu": [[3, 5, 10, 25, -1], [3, 5, 10, 25, "All"]],
            order: [],
                "columns": [
                    {data: 'id'},                   
                    {data: 'link'},                   
                    {data: 'kota_a'},                   
                    {data: 'router_a'},                   
                    {data: 'port_1'},                   
                    {data: 'kota_b'},                   
                    {data: 'router_b'},                   
                    {data: 'port_2'},                   
                    {data: 'id'},                   
                    {data: 'id'},                   
                    {data: 'id'},                   
                ],
            rowCallback: function(nRow, aData, iDisplayIndex) { 
                $('td:eq(8)', nRow).html("<table id='tablene'><tr><td>10009</td></tr><tr><td>10009</td></tr></table>");
            }
          });
        
        //     tableNe = $('#tablene').DataTable({
        //     "paging": true,
        //     "destroy": true,
        //     "pageLength": 10,
        //     "lengthMenu": [[3, 5, 10, 25, -1], [3, 5, 10, 25, "All"]],
        //     order: [],
        //         "rows": [
        //             {data: 1},                   
        //         ],

        //     rowCallback: function(nRow, aData, iDisplayIndex) { 
        //     }
        //   });
        
        });
    </script>

    @foreach($ipbb as $ib)

    <script>
    $(document).ready(function() {
            var tableRelasiIpbb = $('relasiIpbb').dataTable();
            var max_fields      = 10; //maximum input boxes allowed
            var add_button      = $("#addHop{{$ib['idipbb']}}"); //Add button ID
            var wrapper1   		= $(".hop1{{$ib['idipbb']}}"); //Fields wrapper
            var wrapper2   		= $(".hop2{{$ib['idipbb']}}"); //Fields wrapper
            var wrapper3   		= $(".hop3{{$ib['idipbb']}}"); //Fields wrapper

            var x = 1; //initlal text box count

            $(add_button).click(function(e){ //on add input button click
              e.preventDefault();
                var value = "{{$ib['idipbb']}}";
                $.ajax({
                    type: 'GET',
                    url: '{{ url("relasi/valid") }}/'+value,
                    dataType: 'json',
                    success : function (response){
                        var fer = response;
                        console.log(response)
                        if(x < max_fields){ //max input box allowed
                            x++; //text box increment
                            $(wrapper1).append('<tr id="tambahan1{{$ib['idipbb']}}"><td><input type="text" name="" id="'+response+'" value="'+response+'" style="width:20px; border:none"></td><td><input type="text" name="ne_transport" id="ne_transport'+response+'" value="Ne Transport '+response+'"></td><</tr>');
                            $(wrapper2).append('<tr id="tambahan2{{$ib['idipbb']}}"><td><input type="text" name="" id="" value="Shelf Transport '+response+'"></td></tr>');
                            $(wrapper3).append('<tr id="tambahan3{{$ib['idipbb']}}"><td><input type="text" name="" id="" value=" tambahn "></td></tr>');
                            $("#body").load(location.href + " #body");
                        }        
                    }
                })
                    // $("#ne_transport"+fer).change( function() {
                    // var id = $(this).val();
                    //     $.ajax({
                    //         type: "GET",
                    //         url : "{{ url('relasi/makeNe/') }}/"+fer+"/"+id,
                    //         success : function(response){
                    //             alert(response);
                    //         }
                    //     })
                    // })
            });

            var x = 0; //initlal text box count
            $("#removeHop{{$ib['idipbb']}}").on("click", function(e){ //user click on remove text
            e.preventDefault(); 
                $("#tambahan1{{$ib['idipbb']}}").remove(); x--;
                $("#tambahan2{{$ib['idipbb']}}").remove(); x--;
                $("#tambahan3{{$ib['idipbb']}}").remove(); x--;
                var value = "{{$ib['idipbb']}}";
                $.ajax({
                    type: 'GET',
                    url: '{{ url("relasi/validNot") }}/'+value,
                    dataType: 'json',
                    success : function (response){
                        console.log(response)
                        // $(".relasiIpbb").load(location.href + " .relasiIpbb");
                    }
                })
            })
        });
        </script>
            <?php $id = $ib['idipbb'];  $RelasiIpbb =  App\RelasiIpbb::where('id_ipbb',$id); ?>
        @foreach ($RelasiIpbb->get() as $relasi)
        <script>

                $("#ne_transport{{$relasi->id}}").change( function() {
                    var id = $(this).val();
                    $.ajax({
                        type: "GET",
                        url : "{{ url('relasi/makeNe/') }}/{{$relasi->id}}/"+id,
                        success : function(response){
                            alert(response);
                        }
                    })
                })
      </script>
        @endforeach
      @endforeach
@stop
    
