@extends('layout')

@section('menu8')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
IPBB Detail Regional - Inventory Datek
@stop

    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/ipbb')}}">IPBB</a></li>
                    <li class="active">Detail Regional</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IPBB - DETAIL REGIONAL {{$regional}}</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.export.ipbb', $regional)}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_download</i><span>Export Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 130%">
                                    <thead>
                                        <tr>
                                            <th rowspan="3" class="no">ID</th>
                                            <th colspan="3">Telkom</th>
                                            <th colspan="3">Telkomsel</th>
                                            <!-- <th colspan="7">Telkom</th> -->
                                            <th rowspan="3">Capacity (Gbps)</th>
                                            <th rowspan="3">Util Max</th>
                                            {{--<th rowspan="3">Layer</th>--}}
                                            <th rowspan="3">System</th>
                                            <!-- <th colspan="12">Port/Interface Transport</th> -->
                                            <th rowspan="3">Date Detected</th>
                                            {{-- <th rowspan="3">Date Undetected</th> --}}
                                            <th rowspan="3">Status</th>
                                            <th rowspan="3">Transport Type</th>
                                            {{-- <th rowspan="3"></th> --}}
                                        </tr>
                                        <tr>
                                            {{--<th rowspan="2">Regional</th>--}}
                                            <th colspan="3">Node B</th>
                                            <th colspan="3">Node A</th>
                                            {{--<th rowspan="2">Regional</th>--}}
                                            <!-- <th colspan="6">Transport-A (WDM/FO)</th>
                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
                                        </tr>
                                        <tr>
                                            <th>Kota</th>
                                            <th>Router Name</th>
                                            <th>Port</th>
                                            <th>Kota</th>
                                            <th>Router Name</th>
                                            <th>Port</th>
                                            <!-- <th>STO</th>
                                            <th>Metro-E</th>
                                            <th>Port</th>
                                            <th>STO</th>
                                            <th>Metro-E</th>
                                            <th>Port</th>-->
                                            <th>NE</th>
                                            <th>Board</th>
                                            <th class="col-4">Shelf / Slot /Port</th>
                                            <th>Frek</th>
                                            <th>NE</th>
                                            <th>Board</th>
                                            <th>Shelf / Slot / Port</th>
                                            <th>Frek</th> 
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i=0;?>
                                    @foreach($ipbb as $ib)
                                    
                                    <?php $id = $ib['idipbb'];  $RelasiIpbb =  App\RelasiIpbb::where('id_ipbb',$id); ?>
                                    @if ($RelasiIpbb->count() == 0)
                                    <tr>
                                        {{-- <td>{{$i}}</td> --}}
                                        <td>{{$ib['idipbb']}}</td>
                                        {{--<td>{{$ib['reg_tsel']}}</td>--}}
                                        <td>{{$ib['kota_router_node_a']}}</td>
                                        <td>{{$ib['router_node_b']}}</td>
                                        <td>{{$ib['port_end2_tsel']}}</td>
                                        <td>{{$ib['kota_router_node_b']}}</td>
                                        <td>{{$ib['router_node_a']}}</td>
                                        <td>{{$ib['port_end1_tsel']}}</td>
                                        {{--<td>{{$ib['reg_telkom']}}</td>--}}
                                        <td>{{$ib['capacity']}}</td>
                                        <td>{{number_format((float)$ib['utilization_cap'], 2, '.', '')}}</td>
                                        {{--<td>{{$ib['layer']}}</td>--}}
                                            <td>{{$ib['system']}}</td>
                                            {{--<td>@if($ib['system']=="METRO-E") FO--}}
                                                {{--@elseif($ib['system']=="DWDM") FO--}}
                                                {{--@elseif($ib['system']=="IDR") Satelit--}}
                                                {{--@endif</td>--}}
                                        <td>{{$ib['date_undetected']}}</td>
                                        <td>{{$ib['date_undetected']}}</td>
                                        <td>{{$ib['date_undetected']}}</td>
                                        {{-- <td>{{$ib['ne_transport_a']}}</td> --}}
                                         <td>{{\App\RelasiIpbb::NeTransportCount($ib['idipbb'])}}</td>
                                         <td>{{\App\RelasiIpbb::BoardTransportCount($ib['idipbb'])}}</td>
                                         <td>{{\App\RelasiIpbb::NeTransportCount($ib['idipbb'])}} / {{\App\RelasiIpbb::BoardTransportCount($ib['idipbb'])}} / {{\App\RelasiIpbb::BoardTransportCount($ib['idipbb'])}}</td>
                                         {{-- <td>{{$ib['shelf_transport_a']}} / {{$ib['slot_transport_a']}} / {{$ib['port_transport_a']}}</td> --}}
                                         <td>{{$ib['frek_transport_a']}}</td>
                                        <td>{{$ib['ne_transport_b']}}</td>
                                        <td>{{$ib['board_transport_b']}}</td>
                                        <td>{{$ib['shelf_transport_b']}} / {{$ib['slot_transport_b']}} / {{$ib['port_transport_b']}}</td>
                                        <td>{{$ib['frek_transport_b']}}</td>
                                        <td>{{$ib['date_detected']}}</td>
                                        <td>{{$ib['status']}}</td>
                                            <td>{{$ib['transport_type']}}</td>
                                        <td class="align-center">
                                            <div class="btn-group">
                                                <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="/inventory/ipbb/edit/{{$ib['idipbb']}}">Edit</a></li>
                                                    <li><a href="{{URL::Route('inventory.ipbb.detail.delete', $ib['idipbb'])}}">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>

                                    @endif
                                     @foreach ($RelasiIpbb->get() as $relasi)
                                        <?php $i++; ?>
                                            <tr>
                                            {{-- <td>{{$i}}</td> --}}
                                            <td>{{$ib['idipbb']}}</td>
                                            {{--<td>{{$ib['reg_tsel']}}</td>--}}
                                            <td>{{$ib['kota_router_node_a']}}</td>
                                            <td>{{$ib['router_node_b']}}</td>
                                            <td>{{$ib['port_end2_tsel']}}</td>
                                            <td>{{$ib['kota_router_node_b']}}</td>
                                            <td>{{$ib['router_node_a']}}</td>
                                            <td>{{$ib['port_end1_tsel']}}</td>
                                            {{--<td>{{$ib['reg_telkom']}}</td>--}}
                                            <td>{{$ib['capacity']}}</td>
                                            <td>{{number_format((float)$ib['utilization_cap'], 2, '.', '')}}</td>
                                            {{--<td>{{$ib['layer']}}</td>--}}
                                                <td>{{$ib['system']}}</td>
                                                {{--<td>@if($ib['system']=="METRO-E") FO--}}
                                                    {{--@elseif($ib['system']=="DWDM") FO--}}
                                                    {{--@elseif($ib['system']=="IDR") Satelit--}}
                                                    {{--@endif</td>--}}
                                            <td>{{$ib['date_undetected']}}</td>
                                            <td>{{$ib['date_undetected']}}</td>
                                            <td>{{$ib['date_undetected']}}</td>
                                            {{-- <td>{{$ib['ne_transport_a']}}</td> --}}
                                            {{-- <td>{{\App\RelasiIpbb::NeTransportCount($ib['idipbb'])}}</td> --}}
                                            <td>{{$relasi->ne_transport}}</td>
                                            <td>{{$relasi->board_transport}}</td>
                                            <td>{{$relasi->shelf_transport}} / {{$relasi->slot_transport}}/ {{$relasi->port_transport}}</td>
                                            {{-- <td>{{$ib['shelf_transport_a']}} / {{$ib['slot_transport_a']}} / {{$ib['port_transport_a']}}</td> --}}
                                            <td>{{$ib['frek_transport_a']}}</td>
                                            <td>{{$ib['ne_transport_b']}}</td>
                                            <td>{{$ib['board_transport_b']}}</td>
                                            <td>{{$ib['shelf_transport_b']}} / {{$ib['slot_transport_b']}} / {{$ib['port_transport_b']}}</td>
                                            <td>{{$ib['frek_transport_b']}}</td>
                                            <td>{{$ib['date_detected']}}</td>
                                            <td>{{$ib['status']}}</td>
                                                <td>{{$ib['transport_type']}}</td>
                                            <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="/inventory/ipbb/edit/{{$ib['idipbb']}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('inventory.ipbb.detail.delete', $ib['idipbb'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
