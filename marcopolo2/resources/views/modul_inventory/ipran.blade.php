@extends('layout')

@section('menu7')
    class="active"
@stop

@section('title')
IPRAN - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">IPRAN</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IPRAN</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.ipran.import')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_upload</i><span>Import Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                            <th>Utilization</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i=1; $i < 15 ; $i++) {  ?>
                                            <tr>
                                                <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.ipran.detail', $i)}}">Regional {{ $i }}</a></td>
                                                <td>{{\App\NewDatekIpran::Link($i)}}</td>
                                                <td>{{\App\NewDatekIpran::Capacity($i)}}</td>
                                                <td>{{number_format(\App\NewDatekIpran::AllUtilizationCap())}}</td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td>11</td>
                                            <td><a href="{{URL::Route('inventory.ipran.detail', 0)}}">Nasional</a></td>
                                            <td>{{\App\NewDatekIpran::AllLink()}}</td>
                                            <td>{{\App\NewDatekIpran::AllCapacity()}}</td>
                                            <td>{{number_format(\App\NewDatekIpran::AllUtilizationCap())}}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop