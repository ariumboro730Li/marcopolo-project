@extends('layout')

@section('menu8')
    class="active"
@stop

@section('title')
IPBB - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">IPBB</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IPBB</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.ipbb.import')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_upload</i><span>Import Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-6">
                                    <div id="topologi">
                                        <div id="graph-container"></div>
                                    </div>
                                    <script src="{{asset('plugins/sigma.js/sigma.parsers.json/sigma.parsers.json.js')}}"></script>
                                    <script src="{{asset('js/networks-topology/ipbb-json.js')}}"></script>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                            <th>UtilizationCap</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i=1; $i < 15 ; $i++) {  ?>
                                            <tr>
                                                <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.ipbb.detail', $i)}}">Regional {{ $i }}</a></td>
                                                <td>{{\App\NewDatekIpbb::Link($i)}}</td>
                                                <td>{{\App\NewDatekIpbb::Capacity($i)}}</td>
                                                <td>{{number_format(\App\NewDatekIpbb::UtilizationCap($i))}}</td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.ipbb.detail', 0)}}">Nasional</a></td>
                                            <td>{{\App\NewDatekIpbb::AllLink()}}</td>
                                            <td>{{\App\NewDatekIpbb::AllCapacity()}}</td>
                                            <td>{{number_format(\App\NewDatekIpbb::AllUtilizationCap())}}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop