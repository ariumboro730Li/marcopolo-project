@extends('layout')

@section('menu9')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Edit IX - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="#">Inventory Datek</a></li>
                    <li class="active">Edit IX</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>EDIT INVENTORY DATEK IX</h2>
                                </div>
                            </div>
                        </div>
                        {!! Form::open(['method'=>'POST','action'=>'DatekController@updateIX','class'=>'form-horizontal']) !!}
                        {{--<form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">--}}
                        <input type="hidden" name="id" value="{{$data['idix']}}">

                        <div class="body">

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="access"> 

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel Regional</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="reg_tsel" class="form-control" value="{{$data['reg_tsel']}}"/>
                                                        <label class="form-label">Regional</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel End1</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="router_end1_tsel" class="form-control" value="{{$data['router_end1_tsel']}}"/>
                                                        <label class="form-label">Router Name</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel End1</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_end1_tsel" class="form-control"  value="{{$data['port_end1_tsel']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Capacity (Gbps)</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="capacity" class="form-control"  value="{{$data['capacity']}}"/>
                                                        <label class="form-label">Capacity</label>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkom Regional</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="reg_telkom" class="form-control"  value="{{$data['reg_telkom']}}"/>
                                                        <label class="form-label">Regional</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkom End1</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="sto_end1_telkom" class="form-control"  value="{{$data['sto_end1_telkom']}}"/>
                                                        <label class="form-label">STO</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkom End1</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="metroe_end1_telkom" class="form-control" value="{{$data['metroe_end1_telkom']}}" />
                                                        <label class="form-label">Metro-E</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkom End1</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_end1_telkom" class="form-control" value="{{$data['port_end1_telkom']}}" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">System</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="system" class="form-control" value="{{$data['system']}}" />
                                                        <label class="form-label">System</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="clear"></div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne_transport_a" class="form-control" value="{{$data['ne_transport_a']}}" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board_transport_a" class="form-control"  value="{{$data['board_transport_a']}}" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf_transport_a" class="form-control" value="{{$data['shelf_transport_a']}}" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot_transport_a" class="form-control" value="{{$data['slot_transport_a']}}"/>
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_transport_a" class="form-control" value="{{$data['port_transport_a']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek_transport_a" class="form-control" value="{{$data['frek_transport_a']}}"/>
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne_transport_b" class="form-control" value="{{$data['ne_transport_b']}}"/>
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board_transport_b" class="form-control" value="{{$data['board_transport_b']}}"/>
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf_transport_b" class="form-control" value="{{$data['shelf_transport_b']}}"/>
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot_transport_b" class="form-control" value="{{$data['slot_transport_b']}}"/>
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_transport_b" class="form-control" value="{{$data['port_transport_b']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek_transport_b" class="form-control" value="{{$data['frek_transport_b']}}"/>
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
            
                                    </div>

                                </div>
                                
                            </div>
                        </div>

                        <div class="panel-footer">      
                            <input type="submit" class="btn btn-primary pull-right" value="Submit">
                            <button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Back</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>
@stop