@extends('layout')

@section('menu8')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Edit IPBB - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="#">Inventory Datek</a></li>
                    <li class="active">Edit IPBB</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card cool">
                            {{-- <div class="row"> --}}

                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>EDIT INVENTORY DATEK IPBB</h2>
                                </div>w
                            </div>
                        </div>

                        {!! Form::open(['method'=>'POST','action'=>'DatekController@updateIPBB','class'=>'form-horizontal']) !!}
                        <!-- {{--<form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">--}} -->
                        <input type="hidden" name="id" value="{{$data['idipbb']}}">

                        <div class="body">

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="access"> 

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel Regional</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="reg_tsel" class="form-control"  value="{{$data['reg_tsel']}}"/>
                                                        <label class="form-label">Regional</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel Node A</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="router_node_a" class="form-control" value="{{$data['router_node_a']}}"/>
                                                        <label class="form-label">Router Name</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel Node A</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_end1_tsel" class="form-control" value="{{$data['port_end1_tsel']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel Node B</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="router_node_b" class="form-control" value="{{$data['router_node_b']}}"/>
                                                        <label class="form-label">Router Name</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkomsel Node B</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_end2_tsel" class="form-control" value="{{$data['port_end2_tsel']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Telkom Regional</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="reg_telkom" class="form-control" value="{{$data['reg_telkom']}}"/>
                                                        <label class="form-label">Regional</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Capacity (Gbps)</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="capacity" class="form-control" value="{{$data['capacity']}}"/>
                                                        <label class="form-label">Capacity</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">System</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="system" class="form-control" value="{{$data['system']}}"/>
                                                        <label class="form-label">System</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="clear"></div>
                                        <div class="row  input_fields_wrap">

                                        <div class="col-md-6">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne_transport_[]" class="form-control" value="{{$data['ne_transport_[]']}}"/>
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board_transport_[]" class="form-control" value="{{$data['board_transport_[]']}}"/>
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf_transport_[]" class="form-control" value="{{$data['shelf_transport_[]']}}"/>
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot_transport_[]" class="form-control" value="{{$data['slot_transport_[]']}}"/>
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_transport_[]" class="form-control" value="{{$data['port_transport_[]']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek_transport_[]" class="form-control" value="{{$data['frek_transport_[]']}}"/>
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne_transport_[]" class="form-control" value="{{$data['ne_transport_[]']}}"/>
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board_transport_[]" class="form-control" value="{{$data['board_transport_[]']}}"/>
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf_transport_[]" class="form-control" value="{{$data['shelf_transport_[]']}}"/>
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot_transport_[]" class="form-control" value="{{$data['slot_transport_[]']}}"/>
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port_transport_[]" class="form-control" value="{{$data['port_transport_[]']}}"/>
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek_transport_[]" class="form-control" value="{{$data['frek_transport_[]']}}"/>
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        
                                        </div>
            
                                    </div>

                                </div>
                                
                            </div>
                        </div>

                        <div class="panel-footer">      
                            <input type="submit" class="btn btn-warning pull-right" value="Submit">
                            <a name="" id="" class="btn btn-primary pull-right add_field_button" style="margin-right:2px" href="#" role="button">Tambah Transport</a>
                            <button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Back</button>
                            <a href="javascript:void(0)" class="remove_field btn btn-light btn-sm">Remove Form</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>
    <script>
            $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            var cool      = $(".cool"); //

          var x = 1; //initlal text box count
          $(add_button).click(function(e){ //on add input button click
            $(".remove_field").addClass("btn-danger");
              e.preventDefault();
              if(x < max_fields){ //max input box allowed
                  x++; //text box increment
            $(wrapper).append('<div class="col-md-6"><div id="tambahan"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center"><label>TRANSPORT-B (WDM/FO)</label></div><div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label"> <label for="activity">NE</label></div><div class="col-lg-7 col-md-7 col-sm-8 col-xs-7"><div class="form-group form-float"><div class="form-line"><input type="text" name="ne_transport_[]" class="form-control" value="{{$data['ne_transport_[]']}}"/><label class="form-label">NE</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label"><label for="activity">Board</label></div><div class="col-lg-7 col-md-7 col-sm-8 col-xs-7"><div class="form-group form-float"><div class="form-line"><input type="text" name="board_transport_[]" class="form-control" value="{{$data['board_transport_[]']}}"/><label class="form-label">Board</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label"><label for="activity">Shelf</label></div><div class="col-lg-7 col-md-7 col-sm-8 col-xs-7"><div class="form-group form-float"><div class="form-line"><input type="text" name="shelf_transport_[]" class="form-control" value="{{$data['shelf_transport_[]']}}"/><label class="form-label">Shelf</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label"><label for="activity">Slot</label></div><div class="col-lg-7 col-md-7 col-sm-8 col-xs-7"><div class="form-group form-float"><div class="form-line"><input type="text" name="slot_transport_[]" class="form-control" value="{{$data['slot_transport_[]']}}"/><label class="form-label">Slot</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label"><label for="activity">Port</label></div><div class="col-lg-7 col-md-7 col-sm-8 col-xs-7"><div class="form-group form-float"><div class="form-line"><input type="text" name="port_transport_[]" class="form-control" value="{{$data['port_transport_[]']}}"/><label class="form-label">Port</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label"><label for="activity">Frek</label></div><div class="col-lg-7 col-md-7 col-sm-8 col-xs-7"><div class="form-group form-float"><div class="form-line"><input type="text" name="frek_transport_[]" class="form-control" value="{{$data['frek_transport_[]']}}"/><label class="form-label">Frek</label></div></div></div></div></div>');
              }	});
          
          $(".remove_field").on("click", function(e){ //user click on remove text
            var x = 0;
            //   e.preventDefault(); $(this).parent('div').remove(); x--;
              e.preventDefault(); $("#tambahan").parent('div').remove(); x--;
          })
      });
          </script>
      
@stop