@extends('layout')

@section('OneIPMPLS')
    class="active"
@stop

@section('title')
OneIPMPLS - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">OneIPMPLS</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>OneIPMPLS</h2>
                                </div>
                            </div>
                        </div>
                        
                        <div class="body">
                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                            <th>Utilization</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php for ($i=1; $i < 12 ; $i++) {  ?>
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td><a href="{{URL::Route('inventory.OneIPMPLS.detail', $i)}}">Regional {{$i}}</a></td>
                                                <td>{{\App\NewDatekOneip::Link($i)}}</td>
                                                <td>{{\App\NewDatekOneip::Capacity($i)}}</td>
                                                <td>{{number_format(\App\NewDatekOneip::UtilizationCap($i))}}</td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.OneIPMPLS.detail', 0)}}">Nasional</a></td>
                                            <td>{{\App\NewDatekOneip::AllLink()}}</td>
                                            <td>{{\App\NewDatekOneip::AllCapacity()}}</td>
                                            <td>{{number_format(\App\NewDatekOneip::AllUtilizationCap())}}</td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop