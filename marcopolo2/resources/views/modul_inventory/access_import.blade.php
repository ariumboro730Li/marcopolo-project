@extends('layout')

@section('menu5')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
    Import Access
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/access')}}">Access</a></li>
                    <li>Detail Regional</li>
                    <li class="active">Import Access</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>Import Data</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <a href="{{asset('/tmp/access.xlsx')}}" class="btn btn-default">
                                        <i class="material-icons">file_download</i><span>Download Format</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    {!! Form::open(['method'=>'POST','action'=>'DatekController@PostImportAccess','files'=> true]) !!}

                                        <!-- Buat sebuah input type file
                                        class pull-left berfungsi agar file input berada di sebelah kiri -->
                                        <input type="file" name="file" class="pull-left">

                                        <button type="submit" name="/inventory/access/import" class="btn btn-success btn-sm">
                                            <i class="material-icons">remove_red_eye</i><span> Preview</span>
                                        </button>
                                    </form>
                                </div>
                            </div>

                            @if(isset($access))
                            <div class="table-responsive">
                                {!! Form::open(['method'=>'POST','action'=>'DatekController@SaveImportAccess','files'=> true]) !!}
                                @if(isset($file))<input name="file" type="hidden" value="{{$file}}">@endif
                                <table class="align-center table table-striped table-bordered table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th rowspan="3" class="no">ID</th>
                                        <th colspan="3">Telkomsel</th>
                                        <th colspan="10">Telkom</th>
                                        <th rowspan="3">Capacity (G)</th>
                                        <th rowspan="3">System</th>
                                        <th rowspan="3">Date Detected</th>
                                        <th rowspan="3">Date Undetected</th>
                                        <th rowspan="3">Status</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">Regional</th>
                                        <th rowspan="2">Site ID</th>
                                        <th rowspan="2">Site Name</th>
                                        <th rowspan="2">Regional</th>
                                        <th colspan="3">GPON</th>
                                        <th colspan="3">Radio</th>
                                        <th colspan="3">Direct Metro</th>
                                    </tr>
                                    <tr>
                                        <th>STO</th>
                                        <th>GPON</th>
                                        <th>Port</th>
                                        <th>NE</th>
                                        <th>FE</th>
                                        <th>Port</th>
                                        <th>STO</th>
                                        <th>ME</th>
                                        <th>Port</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <a href="{{URL::Route('inventory.access.import')}}">
                                                <button type="submit" class="btn btn-primary btn-sm" id="add">
                                                    <i class="material-icons">input</i><span> Import</span>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    <?php $i=-1; ?>
                                    @foreach($access as $acs)
                                        <?php $i++; ?>
                                        @if($acs[0]!='reg_tsel')
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$acs[0]}}</td>
                                            <td>{{$acs[1]}}</td>
                                            <td>{{$acs[2]}}</td>
                                            <td>{{$acs[3]}}</td>
                                            <td>{{$acs[4]}}</td>
                                            <td>{{$acs[5]}}</td>
                                            <td>{{$acs[6]}}</td>
                                            <td>{{$acs[7]}}</td>
                                            <td>{{$acs[8]}}</td>
                                            <td>{{$acs[9]}}</td>
                                            <td>{{$acs[10]}}</td>
                                            <td>{{$acs[11]}}</td>
                                            <td>{{$acs[12]}}</td>
                                            <td>{{$acs[13]}}</td>
                                            <td>{{$acs[14]}}</td>

                                        </tr>
                                        @endif
                                    @endforeach

                                    </tbody>
                                </table>
                                {{Form::close()}}

                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
