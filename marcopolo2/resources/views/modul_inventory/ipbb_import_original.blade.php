@extends('layout')

@section('menu8')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
    Import IPBB
@stop


@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li><a href="{{url('/inventory/ipbb')}}">Ipbb</a></li>
                    <li>Detail Regional</li>
                    <li class="active">Import IPBB</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>Import Data</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <a href="{{asset('/tmp/ipbb.xlsx')}}" class="btn btn-default">
                                        <i class="material-icons">file_download</i><span>Download Format</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    {!! Form::open(['method'=>'POST','action'=>'DatekController@PostImportIpbb','files'=> true]) !!}                                
                                        <!-- Buat sebuah input type file
                                        class pull-left berfungsi agar file input berada di sebelah kiri -->
                                        <input type="file" name="file" class="pull-left">

                                        <button type="submit" name="/inventory/ipbb/import" class="btn btn-success btn-sm">
                                            <i class="material-icons">remove_red_eye</i><span> Preview</span>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            @if(isset($ipbb))
                            <div class="table-responsive">
                                {!! Form::open(['method'=>'POST','action'=>'DatekController@SaveImportIpbb','files'=> true]) !!}
                                @if(isset($file))<input name="file" type="hidden" value="{{$file}}">@endif
                                <table class="align-center table table-striped table-bordered table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        {{-- <th rowspan="3" class="no">ID</th> --}}
                                        {{-- <th colspan="5">Telkomsel</th> --}}
                                        <!-- <th colspan="7">Telkom</th> -->
                                        <th>Telkom</th>
                                        <th rowspan="3">Capacity (Gbps)</th>
                                        <th rowspan="3">Layer</th>
                                        <th rowspan="3">System</th>
                                        <!-- <th colspan="12">Port/Interface Transport</th> -->
                                        <th rowspan="3">Date Detected</th>
                                        <th rowspan="3">Date Undetected</th>
                                        <th rowspan="3">Status</th>
                                        <th rowspan="3"></th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">Regional</th>
                                        <th colspan="2">Node A</th>
                                        <th colspan="2">Node B</th>
                                        <th rowspan="2">Regional</th>
                                        <!-- <th colspan="6">Transport-A (WDM/FO)</th>
                                        <th colspan="6">Transport-B (WDM/FO)</th> -->
                                    </tr>
                                    <tr>
                                        <th>Router Name</th>
                                        <th>Port</th>
                                        <th>Router Name</th>
                                        <th>Port</th>
                                        <!-- <th>STO</th>
                                        <th>Metro-E</th>
                                        <th>Port</th>
                                        <th>STO</th>
                                        <th>Metro-E</th>
                                        <th>Port</th>
                                        <th>NE</th>
                                        <th>Board</th>
                                        <th>Shelf</th>
                                        <th>Slot</th>
                                        <th>Port</th>
                                        <th>Frek</th>
                                        <th>NE</th>
                                        <th>Board</th>
                                        <th>Shelf</th>
                                        <th>Slot</th>
                                        <th>Port</th>
                                        <th>Frek</th> -->
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <center>
                                                <a href="{{URL::Route('inventory.ipbb.import')}}">
                                                    <button type="submit" class="btn btn-primary btn-sm" id="add">
                                                        <i class="material-icons">input</i><span> Import</span>
                                                    </button>
                                                </a>
                                            </center>
                                        </td>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    <?php $i=-1; ?>
                                    @foreach($ipbb as $ip)
                                        <?php $i++; ?>
                                        @if($ip[0]!='reg_tsel')
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$ip[0]}}</td>
                                            <td>{{$ip[1]}}</td>
                                            <td>{{$ip[2]}}</td>
                                            <td>{{$ip[3]}}</td>
                                            <td>{{$ip[4]}}</td>
                                            <td>{{$ip[5]}}</td>
                                            <td>{{$ip[6]}}</td>
                                            <td>{{$ip[7]}}</td>
                                            <td>{{$ip[8]}}</td>
                                            {{--<td>{{$ip[9]}}</td>--}}
                                            {{--<td>{{$ip[10]}}</td>--}}
                                            {{--<td>{{$ip[11]}}</td>--}}
                                            {{--<td>{{$ip[12]}}</td>--}}
                                            {{--<td>{{$ip[13]}}</td>--}}
                                            {{--<td>{{$ip[14]}}</td>--}}
                                            {{--<td>{{$ip[15]}}</td>--}}
                                            {{--<td>{{$ip[16]}}</td>--}}
                                            {{--<td>{{$ip[17]}}</td>--}}
                                            {{--<td>{{$ip[18]}}</td>--}}
                                            {{--<td>{{$ip[19]}}</td>--}}
                                            {{--<td>{{$ip[20]}}</td>--}}
                                            <td>{{$ip[21]}}</td>
                                            <td>{{$ip[22]}}</td>
                                            <td>{{$ip[23]}}</td>
                                            {{--<td>{{\Carbon\Carbon::now()->format('d-m-Y')}}</td>--}}
                                            {{--<td>UP</td>--}}

                                        </tr>
                                        @endif
                                    @endforeach

                                    </tbody>
                                </table>
                                {{Form::close()}}

                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop
