@extends('layout')

@section('menu13')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

@stop

@section('title')
Access Direct Metro Regional - Inventory Datek
@stop

<style>
        .border-0 {
            border:none !important;
            width: 100%;
            background: transparent !important;
            text-align: center;
        }
    </style>
    
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek Akses Direct Metro</li>
                    {{-- <li><a href="{{url('/inventory/access')}}">Access</a></li> --}}
                    <li class="active">Detail Regional
                </ol>
            </div>
            <div class="form-group">
              {{-- <label for="">Pilih Regional</label> --}}
              <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">                <option value="{{url('/inventory/detail/accessdirect/1')}}">Regional 1</option>
                <option value="" selected> Pilih Regional </option>
                <option value="{{url('/inventory/detail/accessdirect/2')}}">Regional 2</option>
                <option value="{{url('/inventory/detail/accessdirect/3')}}">Regional 3</option>
                <option value="{{url('/inventory/detail/accessdirect/4')}}">Regional 4</option>
                <option value="{{url('/inventory/detail/accessdirect/5')}}">Regional 5</option>
                <option value="{{url('/inventory/detail/accessdirect/6')}}">Regional 6</option>
                <option value="{{url('/inventory/detail/accessdirect/7')}}">Regional 7</option>
              </select>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                                <h4>ACCESS DIRECT METRO - DETAIL REGIONAL {{$regional}}</h4>
                            </div>
                            <div class="body">
                            <div class="table-responsive">
                                <table id="table_detail" class="align-center table table-striped table-bordered table-hover dataTable js-basic-example" style="width: 1500px !important">
                                        <thead class="text-center">
                                                <tr>
                                            <th rowspan="2" class="no">ID</th>
                                            {{-- <th>Telkom</th> --}}
                                            <th colspan="3">Telkomsel</th>
                                            <th colspan="2">Regional</th>
                                            <th colspan="2">Dm Telkom</th>
                                            <th rowspan="4">GPON</th>
                                            <th rowspan="4">Port GPON</th>
                                            <th rowspan="4" style="width:2% !important">Description</th>

                                            <th rowspan="2">Capacity (Mbps)</th>
                                            <th rowspan="2">Utilization</th>
                                            {{-- <th rowspan="2">Util Max</th>
                                            <th rowspan="2">System</th>
                                            <th rowspan="2">Date Detected</th>
                                            <th rowspan="2">Date Undetected</th>
                                            <th rowspan="2">Status</th> --}}
                                            <th rowspan="2">Last Update</th>
                                            <th rowspan="2">Update Time</th>
                                        </tr>
                                        <tr>
                                            {{--<th rowspan="1">Regional</th>--}}
                                            <th rowspan="1">Site ID</th>
                                            <th rowspan="1">Label Topologi</th>
                                            <th rowspan="1">Site Name</th>
                                            <th>Tsel</th>
                                            <th>Telkom</th>
                                            <th>Me</th>
                                            <th>Port</th>
                                            {{--<th rowspan="1">Regional</th>--}}
                                            <!-- <th colspan="3">GPON</th>
                                            <th colspan="3">Radio</th>
                                            <th colspan="3">Direct Metro</th> -->
                                        </tr>
                                        <!-- <tr>
                                            <th>STO</th>
                                            <th>GPON</th>
                                            <th>Port</th>
                                            <th>NE</th>
                                            <th>FE</th>
                                            <th>Port</th>
                                            <th>STO</th>
                                            <th>ME</th>
                                            <th>Port</th>
                                        </tr> -->
                                    </thead>
                                    <tbody>
                                            <?php $i=0; ?>
                                        @foreach ($direct as $a)
                                        <?php $i++; ?>
                                            <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$a['siteid_tsel']}}</td>
                                            <td>GME</td>
                                            <td>{{$a['sitename_tsel']}}</td>
                                            <td>{{$a['regional_tsel']}} </td>
                                            <td>{{$a['regional_telkom']}}</td>
                                            <td>{{$a['me_dm_telkom']}} </td>
                                            <td>{{$a['port_dm_telkom']}}</td>
                                            <td>GPON - Metro</td>
                                            <td>Port Gpon</td>
                                            <td>{{$a['description']}}</td>
                                            <td>{{$a['capacity']}}</td>
                                            <td>{{$a['utilization_cap']}}</td>
                                            <td>{{$a['updated_at']}}</td>
                                            <td>{{$a['update_count']}}</td>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
    <script>
            $(document).ready(function() {
            var table_detail = $('#table_detail').dataTable();
            });
    </script>
    {{-- <script>
        const form = document.getElementById("new_document_attachment");
        // const fileInput = document.getElementById("document_attachment_doc");
        const fileInput = document.getElementById("uploadBuktiTrasnfer");
        // const fileInput = document.getElementsByClassName("uploadBuktiTrasnfer");

        fileInput.addEventListener('change', () => {
        //   form.submit();
        });

        window.addEventListener('paste', e => {
          fileInput.files = e.clipboardData.files;
        });

        var imagesPreview = function(input, placeToInsertImagePreview) {
         if (input.files) {
         var filesAmount = input.files.length;
         for (i = 0; i < filesAmount; i++) {
             var reader = new FileReader();
             reader.onload = function(event) {
                 $($.parseHTML('<img class="" style="width:90px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
             }
             reader.readAsDataURL(input.files[i]);
             }
             }
         };
         $('input#uploadBuktiTrasnfer').on('change', function() {
             $(".div_image1").html("");
             imagesPreview(this, '.div_image1');
         });

     </script> --}}
@stop
