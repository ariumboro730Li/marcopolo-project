@extends('layout')

@section('menu9')
    class="active"
@stop

@section('title')
IX - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">IX</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>IX</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('inventory.ix.import')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">file_upload</i><span>Import Data</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="body"> 
                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                            <th>Utilization</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            for ($i=1; $i < 12; $i++) { ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.ix.detail', $i)}}">Regional {{$i}}</a></td>
                                            <td>{{\App\NewDatekIx::Link($i)}}</td>
                                            <td>{{\App\NewDatekIx::Capacity($i)}}</td>
                                            <td>{{number_format(\App\NewDatekIx::UtilizationCap($i))}}</td>
                                        </tr>
                                        <?php  }?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.ix.detail', 0)}}">Nasional</a></td>
                                            <td>{{\App\NewDatekIx::AllLink()}}</td>
                                            <td>{{\App\NewDatekIx::AllCapacity()}}</td>
                                            <td>{{number_format(\App\NewDatekIx::AllUtilizationCap())}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop