@extends('layout')

@section('menu4')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Input - Inventory Datek
@stop
    
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="#">Inventory Datek</a></li>
                    <li class="active">Input</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>INPUT INVENTORY DATEK</h2>
                                </div>
                            </div>
                        </div>
                        
                        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="body">

                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#access" data-toggle="tab">ACCESS</a></li>
                                <li role="presentation"><a href="#backhaul" data-toggle="tab">BACKHAUL</a></li>
                                <li role="presentation"><a href="#ipran" data-toggle="tab">IPRAN</a></li>
                                <li role="presentation"><a href="#ipbb" data-toggle="tab">IPBB</a></li>
                                <li role="presentation"><a href="#ix" data-toggle="tab">IX</a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="access"> 

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
            
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="backhaul">

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
            
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="ipran">

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
            
                                    </div>
                                    
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="ipbb">

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
            
                                    </div>
                                    
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="ix">

                                    <div class="row clearfix">

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-A (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">
                                                <label>TRANSPORT-B (WDM/FO)</label>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">NE</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="ne" class="form-control" />
                                                        <label class="form-label">NE</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Board</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="board" class="form-control" />
                                                        <label class="form-label">Board</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Shelf</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="shelf" class="form-control" />
                                                        <label class="form-label">Shelf</label>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Slot</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="slot" class="form-control" />
                                                        <label class="form-label">Slot</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Port</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="port" class="form-control" />
                                                        <label class="form-label">Port</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5 form-control-label">
                                                <label for="activity">Frek</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-7">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" name="frek" class="form-control" />
                                                        <label class="form-label">Frek</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
            
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">      
                            <input type="submit" class="btn btn-primary pull-right" value="Submit">
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/inventory/access/detail')}}'">Back</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>
@stop