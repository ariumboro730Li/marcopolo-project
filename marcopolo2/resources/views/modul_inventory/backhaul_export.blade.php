<table class="align-center table table-striped table-bordered table-hover dataTable js-exportable">
    <thead>
    <tr>
        <th rowspan="3" class="no">ID</th>
        <th colspan="3">Telkom</th>
        <th colspan="2">Telkomsel</th>
        <th rowspan="3">Capacity (Gbps)</th>
        <th rowspan="3">Util Max</th>
        {{--<th rowspan="3">Layer</th>--}}
        <th rowspan="3">System</th>
        <!-- <th colspan="12">Port/Interface Transport</th> -->
        <th rowspan="3">Date Detected</th>
        {{--<th rowspan="3">Date Undetected</th>--}}
        <th rowspan="3">Status</th>
        <!-- <th rowspan="3"></th> -->
    </tr>
    <tr>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="3">End1</th>
        {{--<th rowspan="2">Regional</th>--}}
        <th colspan="2">End1</th>

        <!-- <th colspan="6">Transport-A (WDM/FO)</th>
        <th colspan="6">Transport-B (WDM/FO)</th> -->
    </tr>
    <tr>
        <th>STO</th>
        <th>Metro-E</th>
        <th>Port</th>
        <th>Router Name</th>
        <th>Port</th>

        <!-- <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th>
        <th>NE</th>
        <th>Board</th>
        <th>Shelf</th>
        <th>Slot</th>
        <th>Port</th>
        <th>Frek</th> -->
    </tr>
    </thead>
    <tbody>

    <?php $i=0; ?>
    @foreach($backhaul as $bh)
        <?php $i++; ?>
        <tr>
            <td>{{$i}}</td>
            <td>{{\App\Metro::STO($bh['metroe_telkom'])}}</td>
            <td>{{$bh['metroe_telkom']}}</td>
            <td>{{$bh['port_telkom']}}</td>
            {{--<td>{{$bh['reg_tsel']}}</td>--}}
            <td>{{$bh['routername_tsel']}}</td>
            <td>{{$bh['port_tsel']}}</td>
            {{--<td>{{$bh['reg_telkom']}}</td>--}}
            <td>{{$bh['capacity']}}</td>
            <td>{{$bh['utilization_max']}}</td>
            {{--<td>{{$bh['layer']}}</td>--}}
            <td>{{$bh['system']}}</td>
        <!-- <td>{{$bh['ne_transport_a']}}</td>
                                            <td>{{$bh['board_transport_a']}}</td>
                                            <td>{{$bh['shelf_transport_a']}}</td>
                                            <td>{{$bh['slot_transport_a']}}</td>
                                            <td>{{$bh['port_transport_a']}}</td>
                                            <td>{{$bh['frek_transport_a']}}</td>
                                            <td>{{$bh['ne_transport_b']}}</td>
                                            <td>{{$bh['board_transport_b']}}</td>
                                            <td>{{$bh['shelf_transport_b']}}</td>
                                            <td>{{$bh['slot_transport_b']}}</td>
                                            <td>{{$bh['port_transport_b']}}</td>
                                            <td>{{$bh['frek_transport_b']}}</td> -->
            <td>{{$bh['date_detected']}}</td>
            {{--<td>{{$bh['date_undetected']}}</td>--}}
            <td>{{$bh['status']}}</td>

        </tr>
    @endforeach
    </tbody>
</table>