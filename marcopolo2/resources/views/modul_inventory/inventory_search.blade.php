@extends('layout')

@section('menu4')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
Search - Inventory Datek
@stop

@section('content')

	<section class="content">
        <div class="container-fluid">
        	<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
												{{-- <small>{{$regi}}</small><br> --}}
												{{-- <small>{{$gere}}</small><br> --}}
                        	<ul class="nav nav-tabs tab-nav-right m-t-10" role="tablist">
                                <li role="presentation" class="active"><a href="#access" data-toggle="tab">ACCESS&emsp;<button class="btn btn-success " type="button">{{count($access)}}  </button></a></li>
                                <li role="presentation"><a href="#backhaul" data-toggle="tab">BACKHAUL&emsp;<button class="btn btn-success " type="button">{{count($backhaul)}} </button></a></li>
                                <li role="presentation"><a href="#ipran" data-toggle="tab">IPRAN&emsp;<button class="btn btn-success " type="button">{{count($ipran)}} </button></a></li>
                                <li role="presentation"><a href="#ipbb" data-toggle="tab">IPBB&emsp;<button class="btn btn-success " type="button">{{count($ipbb)}} </button></a></li>
                                <li role="presentation"><a href="#ix" data-toggle="tab">IX&emsp;<button class="btn btn-success " type="button">{{count($ix)}} </button></a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="access">

                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">    
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
				                                        <tr>
				                                            <th rowspan="2" class="no">ID</th>
				                                            <th colspan="2">Telkom</th>
				                                            <th colspan="1">Telkomsel</th>
				                                            <!-- <th colspan="10">Telkom</th> -->

				                                            <th rowspan="2">Capacity (Mbps)</th>
				                                            <th rowspan="2">Util Max</th>
				                                            <th rowspan="2">System</th>
				                                            <th rowspan="2">Date Detected</th>
				                                            {{--<th rowspan="2">Date Undetected</th>--}}
				                                            <th rowspan="2">Status</th>
				                                            <th rowspan="2"></th>
				                                        </tr>
				                                        <tr>
				                                            {{--<th rowspan="1">Regional</th>--}}
															<th rowspan="1">Regional</th>
				                                            <th rowspan="1">Site ID</th>
				                                            <th rowspan="1">Site Name</th>
				                                            {{--<th rowspan="1">Regional</th>--}}
				                                            <!-- <th colspan="3">GPON</th>
				                                            <th colspan="3">Radio</th>
				                                            <th colspan="3">Direct Metro</th> -->
				                                        </tr>
				                                        <!-- <tr>
				                                            <th>STO</th>
				                                            <th>GPON</th>
				                                            <th>Port</th>
				                                            <th>NE</th>
				                                            <th>FE</th>
				                                            <th>Port</th>
				                                            <th>STO</th>
				                                            <th>ME</th>
				                                            <th>Port</th>
				                                        </tr> -->
				                                    </thead>
				                                    <tbody>

                                                    <?php $i=0; ?>
													@foreach($access as $acs)
                                                        <?php $i++; ?>
														<tr>
															<td>{{$i}}</td>
															<td>{{$acs['reg_telkom']}}</td>
															<td>{{$acs['siteid_tsel']}}</td>
															<td>{{$acs['sitename_tsel']}}</td>
															{{--<td>{{$acs['reg_telkom']}}</td>--}}
														<!-- <td>{{$acs['sto_gpon_telkom']}}</td>
                                            <td>{{$acs['gpon_gpon_telkom']}}</td>
                                            <td>{{$acs['port_gpon_telkom']}}</td>
                                            <td>{{$acs['ne_radio_telkom']}}</td>
                                            <td>{{$acs['fe_radio_telkom']}}</td>
                                            <td>{{$acs['port_radio_telkom']}}</td>
                                            <td>{{$acs['sto_dm_telkom']}}</td>
                                            <td>{{$acs['me_dm_telkom']}}</td>
                                            <td>{{$acs['port_dm_telkom']}}</td> -->
															<td>{{$acs['capacity']}}</td>
															<td>{{$acs['utilization_max']}}</td>
															<td>{{$acs['system']}}</td>
															<td>{{$acs['date_detected']}}</td>
															{{--<td>{{$acs['date_undetected']}}</td>--}}
															<td>{{$acs['status']}}</td>
															<td class="align-center">
																<div class="btn-group">
																	<a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
																	<ul class="dropdown-menu" role="menu">
																		<li><a href="/inventory/access/edit/{{ $acs['idaccess']}}">Edit</a></li>
																		<li><a href="{{URL::Route('inventory.access.detail.delete', $acs['idaccess'])}}">Delete</a></li>
																	</ul>
																</div>
															</td>
														</tr>
													@endforeach
				                                        	
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div role="tabpanel" class="tab-pane fade" id="backhaul">

                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
				                                        <tr>
				                                            <th rowspan="3" class="no">ID</th>
				                                            <th colspan="4">Telkom</th>
				                                            <th colspan="2">Telkomsel</th>
				                                            <th rowspan="3">Capacity (Gbps)</th>
				                                            <th rowspan="3">Util Max</th>
				                                            {{--<th rowspan="3">Layer</th>--}}
				                                            <th rowspan="3">System</th>
				                                            <!-- <th colspan="12">Port/Interface Transport</th> -->
				                                            <th rowspan="3">Date Detected</th>
				                                            {{--<th rowspan="3">Date Undetected</th>--}}
				                                            <th rowspan="3">Status</th>
				                                            <!-- <th rowspan="3"></th> -->
				                                        </tr>
				                                        <tr>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="4">End1</th>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="2">End2</th>

				                                            <!-- <th colspan="6">Transport-A (WDM/FO)</th>
				                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
				                                        </tr>
				                                        <tr>
															<th>Regional</th>
				                                            <th>STO</th>
				                                            <th>Metro-E</th>
				                                            <th>Port</th>
				                                            <th>Router Name</th>
				                                            <th>Port</th>

				                                            <!-- <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th>
				                                            <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th> -->
				                                        </tr>
				                                    </thead>
				                                    <tbody>

                                                    <?php $i=0; ?>
													@foreach($backhaul as $bh)
                                                        <?php $i++; ?>
														<tr>
															<td>{{$i}}</td>
															<td>{{$bh['reg_telkom']}}</td>
															<td>{{\App\Metro::STO($bh['metroe_telkom'])}}</td>
															<td>{{$bh['metroe_telkom']}}</td>
															<td>{{$bh['port_telkom']}}</td>
															{{--<td>{{$bh['reg_tsel']}}</td>--}}
															<td>{{$bh['routername_tsel']}}</td>
															<td>{{$bh['port_tsel']}}</td>
															{{--<td>{{$bh['reg_telkom']}}</td>--}}
															<td>{{$bh['capacity']}}</td>
															<td>{{$bh['utilization_max']}}</td>
															{{--<td>{{$bh['layer']}}</td>--}}
															<td>{{$bh['system']}}</td>
														<!-- <td>{{$bh['ne_transport_a']}}</td>
                                            <td>{{$bh['board_transport_a']}}</td>
                                            <td>{{$bh['shelf_transport_a']}}</td	>
                                            <td>{{$bh['slot_transport_a']}}</td>
                                            <td>{{$bh['port_transport_a']}}</td>
                                            <td>{{$bh['frek_transport_a']}}</td>
                                            <td>{{$bh['ne_transport_b']}}</td>
                                            <td>{{$bh['board_transport_b']}}</td>
                                            <td>{{$bh['shelf_transport_b']}}</td>
                                            <td>{{$bh['slot_transport_b']}}</td>
                                            <td>{{$bh['port_transport_b']}}</td>
                                            <td>{{$bh['frek_transport_b']}}</td> -->
															<td>{{$bh['date_detected']}}</td>
															{{--<td>{{$bh['date_undetected']}}</td>--}}
															<td>{{$bh['status']}}</td>
														<!-- <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">

                                                        <li><a href="{{URL::Route('inventory.backhaul.show', $bh['idbackhaul'])}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('inventory.backhaul.detail.delete', $bh['idbackhaul'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td> -->
														</tr>
													@endforeach
				                                        
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div role="tabpanel" class="tab-pane fade" id="ipran">
									<button class="btn btn-success btn-lg btn-block waves-effect" type="button"><span class="badge">{{count($ipran)}} </span> DATA DITEMUKAN </button>
                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
				                                        <tr>
				                                            <th rowspan="3" class="no">ID</th>
				                                            <th colspan="7">Telkom</th>
				                                            <th colspan="4">Telkomsel</th>
				                                            <th rowspan="3">Capacity (Gbps)</th>
				                                            <th rowspan="3">Util Max</th>
				                                            {{--<th rowspan="3">Layer</th>--}}
				                                            <th rowspan="3">System</th>
				                                            <!-- <th colspan="12">Port/Interface Transport</th> -->
				                                            <th rowspan="3">Date Detected</th>
				                                            {{--<th rowspan="3">Date Undetected</th>--}}
				                                            <th rowspan="3">Status</th>
				                                            <!-- <th rowspan="3"></th> -->
				                                        </tr>
				                                        <tr>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="4">End1</th>
				                                            <th colspan="3">End2</th>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="2">End1</th>
				                                            <th colspan="2">End2</th>

				                                            <!-- <th colspan="6">Transport-A (WDM/FO)</th>
				                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
				                                        </tr>
				                                        <tr>
															<th>Regional</th>
				                                            <th>STO</th>
				                                            <th>Metro-E</th>
				                                            <th>Port</th>
				                                            <th>STO</th>
				                                            <th>Metro-E</th>
				                                            <th>Port</th>
				                                            <th>Router Name</th>
				                                            <th>Port</th>
				                                            <th>Router Name</th>
				                                            <th>Port</th>

				                                            <!-- <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th>
				                                            <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th> -->
				                                        </tr>
				                                    </thead>
				                                    <tbody>

                                                    <?php $i=0; ?>
													@foreach($ipran as $ir)
                                                        <?php $i++; ?>
														<tr>
															<td>{{$i}}</td>
															<td>{{$ir['reg_telkom']}}</td>
															<td>{{\App\Metro::STO($ir['metroe_end1_telkom'])}}</td>
															<td>{{$ir['metroe_end1_telkom']}}</td>
															<td>{{$ir['port_end1_telkom']}}</td>
															<td>{{\App\Metro::STO($ir['metroe_end2_telkom'])}}</td>
															<td>{{$ir['metroe_end2_telkom']}}</td>
															<td>{{$ir['port_end2_telkom']}}</td>
															{{--<td>{{$ir['reg_tsel']}}</td>--}}
															<td>{{$ir['router_end1_tsel']}}</td>
															<td>{{$ir['port_end1_tsel']}}</td>
															<td>{{$ir['router_end2_tsel']}}</td>
															<td>{{$ir['port_end2_tsel']}}</td>
															{{--<td>{{$ir['reg_telkom']}}</td>--}}

															<td>{{$ir['capacity']}}</td>
															<td>{{$ir['utilization_max']}}</td>
															{{--<td>{{$ir['layer']}}</td>--}}
															<td>{{$ir['system']}}</td>
														<!-- <td>{{$ir['ne_transport_a']}}</td>
                                            <td>{{$ir['board_transport_a']}}</td>
                                            <td>{{$ir['shelf_transport_a']}}</td>
                                            <td>{{$ir['slot_transport_a']}}</td>
                                            <td>{{$ir['port_transport_a']}}</td>
                                            <td>{{$ir['frek_transport_a']}}</td>
                                            <td>{{$ir['ne_transport_b']}}</td>
                                            <td>{{$ir['board_transport_b']}}</td>
                                            <td>{{$ir['shelf_transport_b']}}</td>
                                            <td>{{$ir['slot_transport_b']}}</td>
                                            <td>{{$ir['port_transport_b']}}</td>
                                            <td>{{$ir['frek_transport_b']}}</td> -->
															<td>{{$ir['date_detected']}}</td>
															{{--<td>{{$ir['date_undetected']}}</td>--}}
															<td>{{$ir['status']}}</td>
														<!-- <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::Route('inventory.ipran.show', $ir['idipran'])}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('inventory.ipran.detail.delete', $ir['idipran'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td> -->
														</tr>
													@endforeach
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div role="tabpanel" class="tab-pane fade" id="ipbb">
									<button class="btn btn-success btn-lg btn-block waves-effect" type="button"><span class="badge">{{count($ipbb)}} </span> DATA DITEMUKAN </button>
                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
				                                        <tr>
				                                            <th rowspan="3" class="no">ID</th>

				                                            <th colspan="5">Telkomsel</th>
				                                            <!-- <th colspan="7">Telkom</th> -->
				                                            <th rowspan="3">Capacity (Gbps)</th>
				                                            <th rowspan="3">Util Max</th>
				                                            {{--<th rowspan="3">Layer</th>--}}
				                                            <th rowspan="3">System</th>
				                                            <!-- <th colspan="12">Port/Interface Transport</th> -->
				                                            <th rowspan="3">Date Detected</th>
				                                            {{--<th rowspan="3">Date Undetected</th>--}}
				                                            <th rowspan="3">Status</th>
				                                            <th rowspan="3"></th>
				                                        </tr>
				                                        <tr>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="3">Node B</th>
				                                            <th colspan="2">Node A</th>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <!-- <th colspan="6">Transport-A (WDM/FO)</th>
				                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
				                                        </tr>
				                                        <tr>
															<th>Regional</th>
				                                            <th>Router Name</th>
				                                            <th>Port</th>
				                                            <th>Router Name</th>
				                                            <th>Port</th>
				                                            <!-- <th>STO</th>
				                                            <th>Metro-E</th>
				                                            <th>Port</th>
				                                            <th>STO</th>
				                                            <th>Metro-E</th>
				                                            <th>Port</th>
				                                            <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th>
				                                            <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th> -->
				                                        </tr>
				                                    </thead>
				                                    <tbody>

                                                    <?php $i=0; ?>
													@foreach($ipbb as $ib)
                                                        <?php $i++; ?>
														<tr>
															<td>{{$i}}</td>
															<td>{{$ib['reg_telkom']}}</td>
															<td>{{$ib['router_node_b']}}</td>
															<td>{{$ib['port_end2_tsel']}}</td>
															<td>{{$ib['router_node_a']}}</td>
															<td>{{$ib['port_end1_tsel']}}</td>
															{{--<td>{{$ib['reg_telkom']}}</td>--}}
															<td>{{$ib['capacity']}}</td>
															<td>{{$ib['utilization_max']}}</td>
															{{--<td>{{$ib['layer']}}</td>--}}
															<td>{{$ib['system']}}</td>
														<!-- <td>{{$ib['ne_transport_a']}}</td>
                                            <td>{{$ib['board_transport_a']}}</td>
                                            <td>{{$ib['shelf_transport_a']}}</td>
                                            <td>{{$ib['slot_transport_a']}}</td>
                                            <td>{{$ib['port_transport_a']}}</td>
                                            <td>{{$ib['frek_transport_a']}}</td>
                                            <td>{{$ib['ne_transport_b']}}</td>
                                            <td>{{$ib['board_transport_b']}}</td>
                                            <td>{{$ib['shelf_transport_b']}}</td>
                                            <td>{{$ib['slot_transport_b']}}</td>
                                            <td>{{$ib['port_transport_b']}}</td>
                                            <td>{{$ib['frek_transport_b']}}</td> -->
															<td>{{$ib['date_detected']}}</td>
															{{--<td>{{$ib['date_undetected']}}</td>--}}
															<td>{{$ib['status']}}</td>
															<td class="align-center">
																<div class="btn-group">
																	<a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
																	<ul class="dropdown-menu" role="menu">
																		<li><a href="/inventory/ipbb/edit/{{$ib['idipbb']}}">Edit</a></li>
																		<li><a href="{{URL::Route('inventory.ipbb.detail.delete', $ib['idipbb'])}}">Delete</a></li>
																	</ul>
																</div>
															</td>
														</tr>
													@endforeach
				                                        
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				                <div role="tabpanel" class="tab-pane fade" id="ix">
									<button class="btn btn-success btn-lg btn-block waves-effect" type="button"><span class="badge">{{count($ix)}} </span> DATA DITEMUKAN </button>
                                    <div class="row clearfix">
				                        <div class="body">
				                            <div class="table-responsive">
				                                <table class="align-center table table-striped table-bordered table-hover dataTable js-basic-example">
				                                    <thead>
				                                        <tr>
				                                            <th rowspan="3" class="no">ID</th>
				                                            <th colspan="4">Telkom</th>
				                                            <th colspan="2">Telkomsel</th>
				                                            <th rowspan="3">Capacity (Gbps)</th>
				                                            <th rowspan="3">Util Max</th>
				                                            {{--<th rowspan="3">Layer</th>--}}
				                                            <th rowspan="3">System</th>
				                                            <!-- <th colspan="12">Port/Interface Transport</th> -->
				                                            <th rowspan="3">Date Detected</th>
				                                            {{--<th rowspan="3">Date Undetected</th>--}}
				                                            <th rowspan="3">Status</th>
				                                            <th rowspan="3"></th>
				                                        </tr>
				                                        <tr>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="4">End1</th>
				                                            {{--<th rowspan="2">Regional</th>--}}
				                                            <th colspan="2">End1</th>
				                                            <!-- <th colspan="6">Transport-A (WDM/FO)</th>
				                                            <th colspan="6">Transport-B (WDM/FO)</th> -->
				                                        </tr>
				                                        <tr>
															<th>Regional</th>
				                                            <th>STO</th>
				                                            <th>Metro-E</th>
				                                            <th>Port</th>
				                                            <th>Router Name</th>
				                                            <th>Port</th>
				                                            <!-- <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th>
				                                            <th>NE</th>
				                                            <th>Board</th>
				                                            <th>Shelf</th>
				                                            <th>Slot</th>
				                                            <th>Port</th>
				                                            <th>Frek</th> -->
				                                        </tr>
				                                    </thead>
				                                    <tbody>

                                                    <?php $ip=0; ?>
													@foreach($ix as $i)
                                                        <?php $ip++; ?>
														<tr>
															<td>{{$ip}}</td>
															<td>{{$i['reg_telkom']}}</td>
															<td>{{\App\Metro::STO($i['metroe_end1_telkom'])}}</td>
															<td>{{$i['metroe_end1_telkom']}}</td>
															<td>{{$i['port_end1_telkom']}}</td>
															{{--<td>{{$i['reg_tsel']}}</td>--}}
															<td>{{$i['router_end1_tsel']}}</td>
															<td>{{$i['port_end1_tsel']}}</td>
															<td>{{$i['capacity']}}</td>
															<td>{{$i['utilization_max']}}</td>
															{{--<td>{{$i['layer']}}</td>--}}
															<td>{{$i['system']}}</td>
														<!-- <td>{{$i['ne_transport_a']}}</td>
                                            <td>{{$i['board_transport_a']}}</td>
                                            <td>{{$i['shelf_transport_a']}}</td>
                                            <td>{{$i['slot_transport_a']}}</td>
                                            <td>{{$i['port_transport_a']}}</td>
                                            <td>{{$i['frek_transport_a']}}</td>
                                            <td>{{$i['ne_transport_b']}}</td>
                                            <td>{{$i['board_transport_b']}}</td>
                                            <td>{{$i['shelf_transport_b']}}</td>
                                            <td>{{$i['slot_transport_b']}}</td>
                                            <td>{{$i['port_transport_b']}}</td>
                                            <td>{{$i['frek_transport_b']}}</td> -->
															<td>{{$i['date_detected']}}</td>
															{{--<td>{{$i['date_undetected']}}</td>--}}
															<td>{{$i['status']}}</td>
															{{--<td>Date detected</td>--}}
															{{--<td>Date undetected</td>--}}
															{{--<td>Status</td>--}}
															<td class="align-center">
																<div class="btn-group">
																	<a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
																	<ul class="dropdown-menu" role="menu">
																		<li><a href="/inventory/ix/edit/{{$i['idix']}}">Edit</a></li>
																		<li><a href="{{URL::Route('inventory.ix.detail.delete', $i['idix'])}}">Delete</a></li>
																	</ul>
																</div>
															</td>
														</tr>
													@endforeach
				                                        
				                                    </tbody>
				                                </table>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				            </div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop