@extends('layout')

@section('menu6')
    class="active"
@stop

@section('title')
Backhaul - Inventory Datek
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Inventory Datek</li>
                    <li class="active">Backhaul</li>
                </ol>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>BACKHAUL</h2>
                                </div>
                            </div>
                        </div>
                        
                        <div class="body">
                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="no">No.</th>
                                            <th>Regional</th>
                                            <th>Link</th>
                                            <th>Capacity</th>
                                            <th>Utilization</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                            for ($i=1; $i < 9 ; $i++) {  ?>
                                        <tr>
                                        <td>{{$i}}</td>
                                        <td><a href="{{URL::Route('inventory.backhaul.detail', $i)}}">Regional {{$i}}</a></td>
                                            <td>{{\App\DatekBackhaul::Link($i)}}</td>
                                            <td>{{\App\DatekBackhaul::Capacity($i)}}</td>
                                            <td>{{number_format(\App\DatekBackhaul::UtilizationCap(1))}}</td>
                                        </tr>
                                         <?php  } ?>
                                        <tr>
                                        <td>{{$i}}</td>
                                            <td><a href="{{URL::Route('inventory.backhaul.detail', 0)}}">Nasional</a></td>
                                            <td>{{\App\DatekBackhaul::AllLink()}}</td>
                                            <td>{{\App\DatekBackhaul::AllCapacity()}}</td>
                                            <td>{{number_format(\App\DatekBackhaul::AllUtilizationCap())}}</td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                            {{--{!! Form::open(array('route' => 'import-csv-excel','method'=>'POST','files'=>'true')) !!}--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--{!! Form::label('backhaul','Select File to Import:',['class'=>'col-md-3']) !!}--}}
                                        {{--<div class="col-md-9">--}}
                                            {{--{!! Form::file('backhaul', array('class' => 'form-control')) !!}--}}
                                            {{--{!! $errors->first('backhaul', '<p class="alert alert-danger">:message</p>') !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-12 col-sm-12 col-md-12 text-center">--}}
                                    {{--{!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--{!! Form::close() !!}--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop