@extends('layout')

@section('menu11')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
    User Management | Setting
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Setting</li>
                    <li><a href="{{url('/setting/user')}}">User Management</a></li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>User {{ config('constants.username') }}</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <a href="{{URL::Route('setting.user.tambah')}}">
                                            <input type="button" class="btn btn-primary btn-block" id="add" value="Tambah User">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($user as $data)
                                        <?php $i++ ?>
                                        <tr>
                                            <td class="no align-center">{{ $i }}</td>
                                            <td class="align-center">{{$data['name']}}</td>
                                            <td class="align-center"><a href="{{URL::Route('setting.user.detail', $data['id'])}}"><button type="button" class="btn btn-sm btn-success">Detail</button></a>
                                                <a href="{{URL::Route('setting.user.edit', $data['id'])}}"><button type="button" class="btn btn-sm btn-warning">Update</button></a>
                                                <a href="{{URL::Route('setting.user.hapus', $data['id'])}}"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a></td>
                                            {{--<td><a href="{{URL::Route('hapusberkas', $bks['no_file'])}}"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a></td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop








{{--@extends('layout')--}}

{{--@section('menu2')--}}
    {{--class="active"--}}
{{--@endsection--}}

{{--@section('head.scripts')--}}

    {{--<link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">--}}

{{--@endsection--}}

{{--@section('bottom.scripts')--}}

    {{--<!-- Jquery DataTable Plugin Js -->--}}
    {{--<script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>--}}

    {{--<!-- Custom Js -->--}}
    {{--<script src="{{asset('js/setting.js')}}"></script>--}}
    {{--<script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>--}}

    {{--<script src="{{asset('js/pages/widgets/infobox/infobox-3.js')}}"></script>--}}

    {{--<!-- Demo Js -->--}}
    {{--<script src="{{asset('js/demo.js')}}"></script>--}}

{{--@endsection--}}


{{--@section('content')--}}


    {{--<div class="row clearfix">--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<h2>--}}
                        {{--Users--}}
                    {{--</h2>--}}
                    {{--@if(isset($errors))--}}
                        {{--@if($errors->first() == '1')--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<div class="alert alert-success">Data berhasil diupdate</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--@endif--}}
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="body">--}}
                    {{--<div class="table-responsive">--}}

                        {{--<table class="table table-bordered table-striped table-hover js-basic-example dataTable">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>#</th>--}}
                                {{--<th>Username</th>--}}
                                {{--<th>Balance</th>--}}
                                {{--<th>Aksi</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tfoot>--}}
                            {{--<tr>--}}
                                {{--<td colspan="4">--}}
                                    {{--<center>--}}
                                        {{--<a href="{{URL::Route('setting.user.tambah')}}">--}}
                                            {{--<input type="button" class="btn btn-primary btn-block" id="add" value="Tambah">--}}
                                        {{--</a>--}}
                                    {{--</center>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            {{--</tfoot>--}}
                            {{--<tbody>--}}
                            {{--<?php $i = 0 ?>--}}
                            {{--@foreach($user as $data)--}}
                                {{--<?php $i++ ?>--}}
                                {{--<tr>--}}
                                    {{--<td>{{ $i }}</td>--}}
                                    {{--<td>{{$data['username']}}</td>--}}
                                    {{--<td>{{$data['balance']}}</td>--}}
                                    {{--<td><a href="{{URL::Route('setting.user.detail', $data['id'])}}"><button type="button" class="btn btn-sm btn-success">Detail</button></a>--}}
                                        {{--<a href="{{URL::Route('setting.user.edit', $data['id'])}}"><button type="button" class="btn btn-sm btn-warning">Update</button></a>--}}
                                        {{--<a href="{{URL::Route('setting.user.hapus', $data['id'])}}"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a></td>--}}
                                    {{--<td><a href="{{URL::Route('hapusberkas', $bks['no_file'])}}"><button type="button" class="btn btn-sm btn-danger">Hapus</button></a></td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}

                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endsection--}}