@extends('layout')

@section('title')
Change Password
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/setting/user')}}">User</a></li>
                    <li class="active">Change Password</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>CHANGE PASSWORD</h2>
                                </div>
                            </div>
                        </div>
                          
                        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="body">                 

                            <div class="row clearfix">           
                                <div class="tab-pane active">                            
                                 
                                    <div class="form-group">
                                        <label class="col-md-6 col-xs-12 form-control-label">Old Password</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-line form-float">
                                                <input type="password" class="validate[required] form-control" name="old_password" id="password" />
                                                <label class="form-label">Old Password</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-6 col-xs-12 form-control-label">New Password</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-line form-float">
                                                <input type="password" class="validate[required] form-control" name="new_password" id="password" />
                                                <label class="form-label">New Password</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-6 col-xs-12 form-control-label">Re-enter New Password</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-line form-float">
                                                <input type="password" class="validate[required] form-control" name="new_password2" id="password" />
                                                <label class="form-label">Re-enter New Password</label>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>

                            </div>

                        </div>                                
                        <div class="panel-footer align-center">      
                            <input type="submit" class="btn btn-primary" value="Submit">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
                
        </div>
    </section>
@stop