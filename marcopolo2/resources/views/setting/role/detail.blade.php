@extends('layout')

@section('menu12')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@stop

@section('title')
    Access Role | Setting
@stop

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li>Setting</li>
                    <li><a href="{{url('/setting/role')}}">Access Role</a></li>
                    <li class="active">Detail Role</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>Role</h2>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            @if(! empty($role))
                                {!! Form::open(['method'=>'POST','action'=>'RoleController@UpdateRole','class'=>'form-horizontal']) !!}
                                <input type="hidden" name="id" value="{{$role['id']}}"/>
                            @else
                                {!! Form::open(['method'=>'POST','action'=>'RoleController@TambahRoleBaru','class'=>'form-horizontal']) !!}
                            @endif
                            {{--<form class="form-horizontal">--}}


                            @if($keterangan =="Detail")
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="name">Nama</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!! Form::text('name',$role['name'],['readonly','class'=>'form-control','placeholder'=>'Insert Name']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="description">Deskripsi</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!! Form::text('description',$role['description'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Deskripsi']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($keterangan=="Edit")
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="name">Nama</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!! Form::text('name',$role['name'],['class'=>'form-control','placeholder'=>'Masukkan Nama']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="description">Deskripsi</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!! Form::text('description',$role['description'],['class'=>'form-control','placeholder'=>'Masukkan Deskripsi']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="description">RCA</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                    <div class="switch">
                                                        <label>TIDAK<input type="checkbox" id="rca" name="rca" @if(\App\Role::getRoleStatus($role['id'],"rca")=="on") checked @endif ><span class="lever"></span>YA </label>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="description">Inventory</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="switch">
                                                    <label>TIDAK<input type="checkbox" id="inventory" name="inventory"  @if(\App\Role::getRoleStatus($role['id'],"inventory")=="on") checked @endif ><span class="lever"></span>YA</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="description">CRQ</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="switch">
                                                    <label>TIDAK<input type="checkbox" id="crq" name="crq"  @if(\App\Role::getRoleStatus($role['id'],"crq")=="on") checked @endif ><span class="lever"></span>YA</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="description">Setting</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="switch">
                                                    <label>TIDAK<input type="checkbox" id="setting" name="setting"  @if(\App\Role::getRoleStatus($role['id'],"setting")=="on") checked @endif ><span class="lever"></span>YA</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect" value="Update" id="update">Perbarui</button>
                                        <button type="reset" class="btn btn-secondary m-t-15 m-l-5 waves-effect"  value="Cancel" id="cancel">Batal</button>
                                    </div>
                                </div>
                            @else
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="name">Nama</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!! Form::text('name',"",['class'=>'form-control','placeholder'=>'Masukkan Nama']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="description">Deskripsi</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!! Form::text('description',"",['class'=>'form-control','placeholder'=>'Masukkan Deskripsi']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>
                                        <button type="reset" class="btn btn-secondary m-t-15 m-l-5 waves-effect">Batal</button>
                                    </div>
                                </div>
                            @endif

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
@stop






{{--@extends('layout')--}}

{{--@section('role')--}}
    {{--class="active"--}}
{{--@endsection--}}

{{--@section('head.scripts')--}}

    {{--<link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />--}}
{{--@endsection--}}


{{--@section('content')--}}


    {{--<!-- Horizontal Layout -->--}}
    {{--<div class="row clearfix">--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<h2>--}}
                        {{--Agen--}}
                    {{--</h2>--}}
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="body">--}}
                    {{--@if(! empty($role))--}}
                        {{--{!! Form::open(['method'=>'POST','action'=>'RoleController@UpdateRole','class'=>'form-horizontal']) !!}--}}
                        {{--<input type="hidden" name="id" value="{{$role['id']}}"/>--}}
                    {{--@else--}}
                        {{--{!! Form::open(['method'=>'POST','action'=>'RoleController@TambahRoleBaru','class'=>'form-horizontal']) !!}--}}
                    {{--@endif--}}
                    {{--<form class="form-horizontal">--}}


                    {{--@if($keterangan =="Detail")--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="name">Nama</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('name',$role['name'],['readonly','class'=>'form-control','placeholder'=>'Insert Name']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="description">Deskripsi</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('description',$role['description'],['readonly','class'=>'form-control','placeholder'=>'Masukkan Deskripsi']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                    {{--@elseif($keterangan=="Edit")--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="name">Nama</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('name',$role['name'],['class'=>'form-control','placeholder'=>'Masukkan Nama']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                    {{--<label for="description">Deskripsi</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="form-line">--}}
                                            {{--{!! Form::text('description',$role['description'],['class'=>'form-control','placeholder'=>'Masukkan Deskripsi']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">--}}
                                    {{--<button type="submit" class="btn btn-primary m-t-15 waves-effect" value="Update" id="update">Perbarui</button>--}}
                                    {{--<button type="reset" class="btn btn-secondary m-t-15 m-l-5 waves-effect"  value="Cancel" id="cancel">Batal</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                    {{--@else--}}
                        {{--<div class="row clearfix">--}}
                            {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                {{--<label for="name">Nama</label>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="form-line">--}}
                                        {{--{!! Form::text('name',"",['class'=>'form-control','placeholder'=>'Masukkan Nama']) !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row clearfix">--}}
                            {{--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">--}}
                                {{--<label for="description">Deskripsi</label>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="form-line">--}}
                                        {{--{!! Form::text('description',"",['class'=>'form-control','placeholder'=>'Masukkan Deskripsi']) !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row clearfix">--}}
                            {{--<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">--}}
                                {{--<button type="submit" class="btn btn-primary m-t-15 waves-effect">Simpan</button>--}}
                                {{--<button type="reset" class="btn btn-secondary m-t-15 m-l-5 waves-effect">Batal</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--{!! Form::close() !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Horizontal Layout -->--}}




{{--@endsection--}}