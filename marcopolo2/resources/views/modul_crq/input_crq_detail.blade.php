@extends('layout')

@section('menu10')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Tambah Lokasi - CRQ
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/crq')}}">CRQ</a></li>
                    <li><a href="{{url('/crq/tambah')}}">Input CRQ</a></li>
                    <li class="active">Input CRQ Detail</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>TAMBAH LOKASI CRQ</h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">

                                <div class="col-md-12">

                                    <?php $crq = \App\CRQ::getCRQData($idactivity) ?>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="activity">Activity</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <p  class="form-control" >{{$crq['activity']}}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="reg_telkom">Regional Telkom</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <p  class="form-control" >{{$crq['reg_telkom']}}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="reg_tsel">Regional Telkomsel</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <p  class="form-control" >{{$crq['reg_tsel']}}</p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="activity">Lampiran MOP</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            {{$crq['mop_file']}}
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        {!! Form::open(['method'=>'POST','action'=>'CRQController@TambahCRQBaruDetail']) !!}
                        <input type="hidden" name="idactivity" class="form-control" value="{{$idactivity}}" placeholder=""/>
                        <div class="body">
                            <div class="row clearfix">

                                <div class="col-md-12">

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="lokasi">Lokasi</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="lokasi" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="waktu_mulai">Mulai</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!!Form::datetime('waktu_mulai', null, ['class' => 'datetimepicker form-control', 'id'=>'datemulai', 'placeholder'=> 'Open'])!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="waktu_selesai">Selesai</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!!Form::datetime('waktu_selesai', null, ['class' => 'datetimepicker form-control', 'id'=>'dateselesai', 'placeholder'=> 'Open'])!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="pic">PIC</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="pic" class="form-control" placeholder="Name, Unit" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="crq">CRQ</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="crq" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="status">Status</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!!Form::select('status', array('Open' => 'Open', 'Done' => 'Done', 'Pending' => 'Pending'))!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="ket">Keterangan</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="ket" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                    </div>

                                </div>

    
                            </div>
                        </div>

                        <div class="panel-footer">
                            <input type="submit" class="btn btn-primary pull-right" value="Tambah Lokasi">
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/crq')}}'">Back</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>

    <script>
        $('#date1').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false });
        $('#date2').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false });
        $('#date3').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false });
        $('#datemulai').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm' });
        $('#dateselesai').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm' });
    </script>
@stop