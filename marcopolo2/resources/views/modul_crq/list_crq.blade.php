@extends('layout')

@section('menu10')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop
<style>
    .border-0 {
        border:none !important;
        /* width: 100%; */
        background: transparent !important;
        text-align: center;
    }
</style>


@section('title')
CRQ
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li class="active">CRQ</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>TODAY'S ACTIVITY</h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="no">No.</th>
                                            <th colspan="2">Regional</th>
                                            <th rowspan="2">Activity</th>
                                            <th rowspan="2">Lokasi</th>
                                            <th colspan="2">Mulai</th>
                                            <th colspan="2">Selesai</th>
                                            <th rowspan="2">Durasi</th>
                                            <th rowspan="2">Lampiran MOP</th>
                                            <th rowspan="2">PIC</th>
                                            <th rowspan="2">CRQ</th>
                                            <th rowspan="2"></th>
                                        </tr>
                                        <tr>
                                            <th>Telkom</th>
                                            <th>Telkomsel</th>
                                            <th>Tanggal</th>
                                            <th>Jam</th>
                                            <th>Tanggal</th>
                                            <th>Jam</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ?>
                                        @foreach($crqtoday as $datum)
                                            <?php $i++ ?>
                                            <?php $data = \App\CRQ::getCRQData($datum['idactivity']) ?>
                                            <tr>
                                                <td class="align-center">{{ $i }}</td>
                                                <td>{{str_replace("Regional ","",$data['reg_telkom'])}}</td>
                                                <td>{{str_replace("Regional ","",$data['reg_tsel'])}}</td>
                                                <td>{{$data['activity']}}</td>
                                                <td>{{$datum['lokasi']}}</td>
                                                <td>{{\Carbon\Carbon::parse($datum['tgl_mulai'])
                                                    ->format('d-m-Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($datum['jam_mulai'])
                                                    ->format('H:i')}} WIB</td>
                                                <td>{{\Carbon\Carbon::parse($datum['tgl_selesai'])
                                                    ->format('d-m-Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($datum['jam_selesai'])
                                                    ->format('H:i')}} WIB</td>
                                                <td>{{\Carbon\Carbon::parse($datum['waktu_selesai'])
                                                ->diffInDays(\Carbon\Carbon::parse($datum['waktu_mulai']))}} Hari</td>
                                                <td>{{$data['impact_service']}}</td>
                                                <td>{{$datum['pic']}}</td>
                                                <td>{{$datum['crq']}}</td>
                                                <td class="align-center">
                                                    <div class="btn-group">
                                                        <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="{{URL::Route('crq.detail.edit',$datum['iddetail'])}}">Edit</a></li>
                                                            <li><a href="{{URL::Route('crq.detail.hapus',$datum['iddetail'])}}">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>UPCOMING'S ACTIVITY</h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="no">No.</th>
                                        <th colspan="2">Regional</th>
                                        <th rowspan="2">Activity</th>
                                        <th rowspan="2">Lokasi</th>
                                        <th colspan="2">Mulai</th>
                                        <th colspan="2">Selesai</th>
                                        <th rowspan="2">Durasi</th>
                                        <th rowspan="2">Lampiran MOP</th>
                                        <th rowspan="2">PIC</th>
                                        <th rowspan="2">CRQ</th>
                                        <th rowspan="2"></th>
                                    </tr>
                                    <tr>
                                        <th>Telkom</th>
                                        <th>Telkomsel</th>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($crqupcoming as $datum)
                                        <?php $i++ ?>
                                        <?php $data = \App\CRQ::getCRQData($datum['idactivity']) ?>
                                        <tr>
                                            <td class="align-center">{{ $i }}</td>
                                            <td class="align-center">{{str_replace("Regional ","",$data['reg_telkom'])}}</td>
                                            <td class="align-center">{{str_replace("Regional ","",$data['reg_tsel'])}}</td>
                                            <td>{{$data['activity']}}</td>
                                            <td>{{$datum['lokasi']}}</td>
                                            <td>{{\Carbon\Carbon::parse($datum['tgl_mulai'])
                                                    ->format('d-m-Y')}}</td>
                                            <td>{{\Carbon\Carbon::parse($datum['jam_mulai'])
                                                    ->format('H:i')}} WIB</td>
                                            <td>{{\Carbon\Carbon::parse($datum['tgl_selesai'])
                                                    ->format('d-m-Y')}}</td>
                                            <td>{{\Carbon\Carbon::parse($datum['jam_selesai'])
                                                    ->format('H:i')}} WIB</td>
                                            <td>{{\Carbon\Carbon::parse($datum['waktu_selesai'])
                                                ->diffInDays(\Carbon\Carbon::parse($datum['waktu_mulai']))}} Hari</td>
                                            <td>{{$data['impact_service']}}</td>
                                            <td>{{$datum['pic']}}</td>
                                            <td>{{$datum['crq']}}</td>
                                            <td class="align-center">
                                                {{-- <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::Route('crq.detail.edit',$datum['iddetail'])}}">Edit</a></li>
                                                        <li><a href="{{URL::Route('crq.detail.edit',$datum['iddetail'])}}">Delete</a></li>
                                                    </ul>
                                                </div> --}}
                                                <a class="btn btn-danger" href="{{URL::Route('crq.detail.hapus',$datum['iddetail'])}}">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2 class="pull-left no-margin">LIST CRQ</h2>
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6 menu-chart">
                                        <select class="form-control show-tick" name="CrqMonth" id="CrqMonth" onchange="CrqMonth()">
                                            <option id="crq1" value="1">Januari</option>
                                            <option id="crq2" value="2">Februari</option>
                                            <option id="crq3" value="3">Maret</option>
                                            <option id="crq4" value="4">April</option>
                                            <option id="crq5" value="5">Mei</option>
                                            <option id="crq6" value="6">Juni</option>
                                            <option id="crq7" value="7">Juli</option>
                                            <option id="crq8" value="8">Agustus</option>
                                            <option id="crq9" value="9">September</option>
                                            <option id="crq10" value="10">Oktober</option>
                                            <option id="crq11" value="11">November</option>
                                            <option id="crq12" value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-xs-12 col-sm-6 pull-right no-margin">
                                    <div class="btn-group pull-right">
                                        <button type="button" onclick="window.location.href='{{URL::Route('crq.tambah')}}'" class="pull-left btn btn-success btn-block" ><i class="material-icons">add</i><span>Input CRQ</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="align-center table table-striped table-bordered table-condensed table-hover dataTable js-basic-example" style="width:3000px !important">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="no">No.</th>
                                        <th colspan="2">Regional</th>
                                        <th rowspan="2">Activity</th>
                                        <th rowspan="2">Lokasi</th>
                                        <th colspan="2">Mulai</th>
                                        <th colspan="2">Selesai</th>
                                        <th rowspan="2">Durasi</th>
                                        <th rowspan="2">Lampiran MOP</th>
                                        <th rowspan="2">PIC</th>
                                        <th rowspan="2">CRQ</th>
                                        <th rowspan="2"></th>
                                    </tr>
                                    <tr>
                                        <th>Telkom</th>
                                        <th>Telkomsel</th>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                        <th>Tanggal</th>
                                        <th>Jam</th>
                                    </tr>
                                </thead>
                                <tbody id="lokasitredit">
                                        @foreach($crq as $data)
                                    <?php $i = 0 ?>
                                        <?php $i++ ?>
                                        <tr @if($data{'tgl_mulai'}==$data{'tgl_selesai'}) @endif>
                                            <td class="align-center"><strong>{{ $i }}</strong></td>
                                            <td class="align-center"><strong>{{str_replace("Regional ","",$data['reg_telkom'])}}</strong></td>
                                            <td class="align-center"><strong>{{str_replace("Regional ","",$data['reg_tsel'])}}</strong></td>
                                            <td colspan="10" class="align-center"><strong>{{$data['activity']}}</strong></td>
                                            <td style="display: none"></td><td style="display: none"></td><td style="display: none"></td><td style="display: none"></td><td style="display: none"></td><td style="display: none"></td><td style="display: none"></td><td style="display: none"></td><td style="display: none"></td>
                                            <td class="align-center">
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::Route('crq.edit',$data['idactivity'])}}">Edit</a></li>
                                                        {{-- <li><a href="{{URL::Route('crq.tambah.detail',$data['idactivity'])}}">Tambah Lokasi</a></li> --}}
                                                        <li><a href="{{URL::Route('crq.hapus',$data['idactivity'])}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>

                                        <?php $detaildata = \App\CRQDetail::getCRQDetailData($data['idactivity']) ?>
                                        @foreach($detaildata as $datum)
                                            <tr @if($data{'tgl_mulai'}!=\Carbon\Carbon::now()->format('d-m-Y')) @endif>
                                                <td class="align-center">{{ $i }}</td>
                                                <td class="align-center">{{str_replace("Regional ","",$data['reg_telkom'])}}</td>
                                                <td class="align-center">{{str_replace("Regional ","",$data['reg_tsel'])}}</td>
                                                <td>{{$data['activity']}}</td>
                                                <td><input type="text" class="border-0" name="" id="" value="{{$datum['lokasi']}}"></td>
                                                <td>{{\Carbon\Carbon::parse($datum['tgl_mulai'])->format('d-m-Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($datum['jam_mulai'])->format('H:i')}} WIB</td>
                                                <td>{{\Carbon\Carbon::parse($datum['tgl_selesai'])->format('d-m-Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($datum['jam_selesai'])->format('H:i')}} WIB</td>
                                                <td>{{\Carbon\Carbon::parse($datum['tgl_selesai'])->diffInDays(\Carbon\Carbon::parse($datum['tgl_mulai']))}} Hari</td>
                                                <td>{{$data['impact_service']}}</td>
                                                <td>{{$datum['pic']}}</td>
                                                <td>{{$datum['crq']}}</td>
                                                <td class="align-center">
                                                    {{-- <div class="btn-group">
                                                        <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle">Actions <span class="caret"></span></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="{{URL::Route('crq.detail.edit',$datum['iddetail'])}}">Edit</a></li>
                                                        </ul>
                                                    </div> --}}
                                                    <a class="btn btn-danger" href="{{URL::Route('crq.detail.hapus',$datum['iddetail'])}}">Delete</a>
                                                </td>
                                            </tr>

                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


            
@stop

@section('js')
    <script src="{{asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@stop