@extends('layout')

@section('menu10')
    class="active"
@stop

@section('css')
    <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/waitme/waitMe.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@stop

@section('title')
Edit - CRQ
@stop
    
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb">
                    <li><a href="{{url('/crq')}}">CRQ  </a></li>
                    <li class="active">Edit CRQ</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>EDIT CRQ</h2>
                                </div>
                            </div>
                        </div>

                        {!! Form::open(['method'=>'POST','action'=>'CRQController@UpdateCRQ']) !!}

                        <input type="hidden" name="idcrq" id="idcrq" value="{{$crq['idactivity']}}">

                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="activity">Activity</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="activity" class="form-control form-sm" placeholder="" value="{{$crq['activity']}}" id="activity"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="reg_tsel">Regional Telkomsel</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                {!!Form::select('reg_tsel', array('Regional 1' => 'Regional 1 (Sumbagut)', 'Regional 2' => 'Regional 2 (Sumbagsel)', 'Regional 3' => 'Regional 3 (Jabotabek)', 'Regional 4' => 'Regional 4 (Jabar)', 'Regional 5' => 'Regional 5 (Jateng)', 'Regional 6' => 'Regional 6 (Jatim)', 'Regional 7' => 'Regional 7 (Balnus)', 'Regional 8' => 'Regional 8 (Kalimantan)', 'Regional 9' => 'Regional 9 (Sulawesi)', 'Regional 10' => 'Regional 10 (Sumbagteng)', 'Regional 11' => 'Regional 11 (PUMA)')
                                                ,$crq['reg_tsel'])!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="reg_telkom">Regional Telkom</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input id="reg_telkom" type="text" class="form-control form-sm" name="reg_telkom" value="{{$crq['reg_telkom']}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                        <label for="activity">Lampiran MOP</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <input type="file" class="fileinput" name="mop_file" title="Browse file..."/>
                                            <span class="help-block">Format file: .doc, .docx, .xls, .xlsx, .pdf </span>
                                        </div>
                                    </div>

                                </div>
    
                            </div>

                        </div>

                    </div>

                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6 no-margin">
                                    <h2>ISI LOKASI
                                        {{-- <a name="" id="lokasih" class="btn btn-sm btn-primary" href="javascript:void(0)" role="button">Add</a>
                                        <a name="" id="lokasihre" class="btn btn-sm btn-danger" href="javascript:void(0)" role="button">Remove</a> --}}
                                    </h2>
                                </div>
                            </div>
                        </div>

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            

                        <div class="body">
                            <div class="row clearfix">
                                <input type="hidden" name="id" value="" id="id">
                                <div class="table-responsive">
                                    <table id="table1" class="table table-striped table-bordered table-condensed table-hover" style="width: 1420px">
                                        <thead>
                                            {{-- <tr>
                                                <th rowspan="1">Lokasi</th>
                                                <th colspan="2">Mulai</th>
                                                <th colspan="2">Selesai</th>
                                                <th rowspan="2">PIC</th>
                                                <th rowspan="2">CRQ</th>
                                                <th rowspan="2">Status</th>
                                                <th rowspan="2">Keterangan</th>
                                                <th rowspan="2"></th>
                                            </tr> --}}
                                            <tr>
                                                <th></th>
                                                <th>Lokasi</th>
                                                <th>Tanggal Mulai</th>
                                                <th>Jam Mulai</th>
                                                <th>Tanggal Selesai</th>
                                                <th>Jam Selesai</th>
                                                <th style="width:20%">PIC</th>
                                                <th>CRQ</th>
                                                <th>Status</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        {{-- <form id="addLocation">
                                            @csrf --}}
                                        <tbody id="detail-list" name="detail-list">
                                            @foreach(\App\CRQDetail::getCRQDetailData($crq['idactivity']) as $datum)
                                            <?php $i = $datum['iddetail']; ?>
                                            <tr id="detail{{$datum['iddetail']}}" class="align-center">
                                                <td>
                                                {{-- <a href="{{url('/crq/detail/hapus/'.$datum['iddetail'])}}" class="btn btn-danger delete-product{{$datum['iddetail']}}" value="{{$datum['iddetail']}}"> --}}
                                                <a href="javascript:void(0)" class="btn btn-danger delete-product{{$datum['iddetail']}}">
                                                    Hapus
                                                </a>
                                                <input type="number" hidden name="addmore[{{$i}}][iddetail]" value="{{$datum['iddetail']}}" id="">
                                                </td>
                                                <td><input type="text" name="addmore[{{$i}}][lokasi]" placeholder="Location" class="form-control form-sm" value="{{$datum['lokasi']}}"/></td>  
                                                <td><input type="date" name="addmore[{{$i}}][in_date]" placeholder="Start Date" class="form-control form-sm datepicker" value="{{ $datum['tgl_mulai']}}" /></td>  
                                                <td><input type="text" name="addmore[{{$i}}][in_hour]" placeholder="Start Hour " class="form-control form-sm timepicker" value="{{$datum['jam_mulai']}} WIB" /></td>  
                                                <td><input type="date" name="addmore[{{$i}}][end_date]" placeholder="Waktu Selesai" class="form-control form-sm datepicker" value="{{$datum['tgl_selesai']}}" /></td>  
                                                <td><input type="text" name="addmore[{{$i}}][end_hour]" placeholder="Jam Selesai" class="form-control form-sm timepicker" value="{{$datum['jam_selesai']}} WIB" /></td>  
                                                <td>
                                                    <textarea id="my-textarea" class="form-control" name="addmore[{{$i}}][pic]"  rows="3">{{$datum['pic']}}</textarea>
                                                </td>
                                                <td><input type="text" name="addmore[{{$i}}][crq]" placeholder="CRQ" class="form-control form-sm" value="{{$datum['crq']}}" /></td>  
                                                <td><input type="text" name="addmore[{{$i}}][status]" placeholder="Status" class="form-control form-sm" value="{{$datum['status']}}" /></td>  
                                                <td><input type="text" name="addmore[{{$i}}][keterangan]" placeholder="keterangan" class="form-control form-sm" value="{{$datum['ket']}}" /></td>  
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td><button type="button" name="add" id="add" class="btn btn-success">Add</button>
                                                <input type="number" hidden name="addmore[0][iddetail]" value="" id="">
                                            </td>  
                                            <td><input type="text" name="addmore[0][lokasi]" placeholder="Location" class="form-control form-sm" /></td>  
                                            <td><input type="date" name="addmore[0][in_date]" placeholder="Start Date" class="form-control form-sm datepicker" /></td>  
                                            <td><input type="text" name="addmore[0][in_hour]" placeholder="Start Hour " class="form-control form-sm timepicker" /></td>  
                                            <td><input type="date" name="addmore[0][end_date]" placeholder="End Date" class="form-control form-sm datepicker" /></td>  
                                            <td><input type="text" name="addmore[0][end_hour]" placeholder="End Hour" class="form-control form-sm timepicker" /></td>  
                                            <td><textarea id="my-textarea" class="form-control" placeholder="PIC" name="addmore[0][pic]"  rows="3"></textarea></td>
                                            <td><input type="text" name="addmore[0][crq]" placeholder="CRQ" class="form-control form-sm" /></td>  
                                            <td><input type="text" name="addmore[0][status]" placeholder="Status" class="form-control form-sm" /></td>  
                                            <td><input type="text" name="addmore[0][keterangan]" placeholder="Keterangan" class="form-control form-sm" /></td>  
                                        </tr>

                                        </tbody>
                                    {{-- </form> --}}
                                    </table>
                                </div>

                            </div>

                        </div>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#editModal"id="btn_add">Tambah</button>
                            <button type="button" class="btn btn-default" onclick="window.location.href='{{url('/crq')}}'">Back</button>
                        </div>
                        
                    </div>

                </div>
            </div>

        </div>
    </section>



    <!-- Passing BASE URL to AJAX -->
    <input id="url" type="hidden" value="{{ \Request::url() }}">
@stop                

@section('js')
    <script src="{{asset('plugins/autosize/autosize.js')}}"></script>
    <script src="{{asset('plugins/momentjs/moment.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('js/select2.js')}}"></script>

    <script>
        function regtsel(e) {
            if (e.target.value == 1 || e.target.value == 2 || e.target.value == 10) {document.getElementById("reg_telkom").value = "Regional 1";}
            else if (e.target.value == 3) {document.getElementById("reg_telkom").value = "Regional 2";}
            else if (e.target.value == 4) {document.getElementById("reg_telkom").value = "Regional 3";}
            else if (e.target.value == 5) {document.getElementById("reg_telkom").value = "Regional 4";}
            else if (e.target.value == 6 || e.target.value == 7) {document.getElementById("reg_telkom").value = "Regional 5";}
            else if (e.target.value == 8) {document.getElementById("reg_telkom").value = "Regional 6";}
            else if (e.target.value == 9 || e.target.value == 11) {document.getElementById("reg_telkom").value = "Regional 7";}
        }
    </script>

    @foreach(\App\CRQDetail::getCRQDetailData($crq['idactivity']) as $datum)
    <?php $ido = $datum['iddetail']; ?>
    <script>
        $(".delete-product{{$ido}}").click( function() {
            $("#detail{{$ido}}").remove();
            $.ajax({
                type: 'GET',
                url: '{{ url("/crq/detail/hapus/$ido") }}',
                dataType: 'json',
                success : function (response){
                    console.log(response)
                }
            })

        });
    </script>
    @endforeach
    <?php 
    $add = App\CRQDetail::latest('iddetail');
    if ($add->count() == 0) {
        $add = 0;
    } else {
        $add = $add->first()->iddetail;
    }
    
    ?>
    <script type="text/javascript">
   
        // var i = 0;
        var i = {{ $add }}
           
        $("#add").click(function(){
       
            ++i;
            $("#table1").append('<tr><td><button type="button" class="btn btn-danger remove-tr"> - </button>'+
                                '<input hidden type="number" name="addmore['+i+'][iddetail]" value="" id=""></td>'+
                                '<td><input type="text" name="addmore['+i+'][lokasi]" placeholder="Location" class="form-control form-sm" /></td>'+
                                '<td><input type="date" name="addmore['+i+'][in_date]" placeholder="Start Date" class="form-control form-sm datepicker" /></td> '+
                                '<td><input type="text" name="addmore['+i+'][in_hour]" placeholder="Start Hour " class="form-control form-sm timepicker" /></td> '+
                                '<td><input type="date" name="addmore['+i+'][end_date]" placeholder="End Date" class="form-control form-sm datepicker" /></td>'+
                                '<td><input type="text" name="addmore['+i+'][end_hour]" placeholder="End Hour" class="form-control form-sm timepicker" /></td>'+
                                '<td><textarea id="my-textarea" class="form-control" placeholder="PIC" name="addmore['+i+'][pic]" rows="3"></textarea></td>'+
                                '<td><input type="text" name="addmore['+i+'][crq]" placeholder="CRQ" class="form-control form-sm" /></td>'+
                                '<td><input type="text" name="addmore['+i+'][status]" placeholder="Status" class="form-control form-sm" /></td>'+
                                '<td><input type="text" name="addmore['+i+'][keterangan]" placeholder="Keterangan" class="form-control form-sm" /></td>');
       
        });
       
        $(document).on('click', '.remove-tr', function(){  
             $(this).parents('tr').remove();
        });  
       
    </script>
        <script>
            $('.datepicker').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD',
                time: false
            });
        </script>    
    
    

    <meta name="_token" content="{!! csrf_token() !!}" />
    @include("modul_crq.ajax_crq")
                        {!! Form::close() !!}

@stop