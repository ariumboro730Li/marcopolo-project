<script>

    $(document).ready(function(){

        //get base URL *********************
        var url = $('#url').val();


        //display modal form for creating new product *********************
        $('#btn_add').click(function(){
            $('#btn-save').val("add");
            $('#frmProducts').trigger("reset");
            $('#myModal').modal('show');
        });



        //display modal form for product EDIT ***************************
        $(document).on('click','.open_modal',function(){
            var product_id = $(this).val();

            // Populate Data in Edit Modal Form
            $.ajax({
                type: "GET",
                url: '/crq/detail' + '/' + product_id,
                success: function (data) {
                    console.log(data);
                    $('#idcrq').val(data.idactivity);
                    $('#detail_id').val(data.iddetail);
                    $('#lokasi').val(data.lokasi);
                    $('#tgl_mulai').val(data.tgl_mulai);
                    $('#jam_mulai').val(data.jam_mulai);
                    $('#tgl_selesai').val(data.tgl_selesai);
                    $('#jam_selesai').val(data.jam_selesai);
                    $('#pic').val(data.pic);
                    $('#crq').val(data.crq);
                    $('#status').val(data.status);
                    $('#ket').val(data.ket);
                    $('#btn-save').val("update");
                    $('#myModal').modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });



        //create new product / update existing product ***************************
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();
            var formData = {
                idactivity: $('#idactivity').val(),
                iddetail: $('#detail_id').val(),
                lokasi: $('#lokasi').val(),
                tgl_mulai: $('#tgl_mulai').val(),
                jam_mulai: $('#jam_mulai').val(),
                tgl_selesai: $('#tgl_selesai').val(),
                jam_selesai: $('#jam_selesai').val(),
                pic: $('#pic').val(),
                crq: $('#crq').val(),
                status: $('#status').val(),
                ket: $('#ket').val(),
            }

            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();
            var type = "POST"; //for creating new resource
            var product_id = $('#detail_id').val();;
            var my_url = '/crq/detail';
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + product_id;
            }
            console.log(formData);
            $.ajax({
                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var product = '<tr id="detail' + data['iddetail'] + '" class="align-center"><td>' + data.lokasi + '</td><td>'
                        + data.tgl_mulai + '</td><td>' + data.jam_mulai + '</td><td>' + data.tgl_selesai + '</td><td>' + data.jam_selesai
                        + '</td><td>' + data.pic + '</td><td>' + data.crq + '</td><td>' + data.status + '</td><td>' + data.ket + '</td>';
                    product += '<td><button class="btn btn-success  open_modal" value="' + data.iddetail + '">Edit</button>';
                    product += ' <button class="btn btn-danger delete-product" value="' + data.iddetail + '">Hapus</button></td></tr>';
                    if (state == "add"){ //if user added a new record
                        $('#detail-list').append(product);
                    }else{ //if user updated an existing record
                        $("#detail" + product_id).replaceWith( product );
                    }
                    $('#frmProducts').trigger("reset");
                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        //delete product and remove it from TABLE list ***************************
        $(document).on('click','.delete-product',function(){
            var product_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: '/crq/detail/' + product_id,
                success: function (data) {
                    console.log(data);
                    $("#detail" + product_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    });
</script>