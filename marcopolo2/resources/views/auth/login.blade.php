<html>

@section('title')
Login
@stop

@include('support.head')

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="#"><b>Tsel Op-Ctrl</b></a>
        </div>
        <div class="card">
            <div class="body">
                {{--<form id="sign_in" method="POST" action="{{ route('login') }}">--}}
                    <form id="sign_in" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="msg">Log in to your account</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input id="ldap" type="text" class="form-control{{ $errors->has('ldap') ? ' is-invalid' : '' }}" name="ldap" value="{{ old('ldap') }}" placeholder="Username" required autofocus />

                            @if ($errors->has('ldap'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('ldap') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" id="rememberme" class="filled-in" {{ old('remember') ? 'checked' : '' }} />
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">{{ __('Login') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{asset('plugins/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{asset('js/pages/examples/sign-in.js')}}"></script>

    @include('support.javascript')

</body>

</html>