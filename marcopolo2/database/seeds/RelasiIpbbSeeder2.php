<?php

use Illuminate\Database\Seeder;

class RelasiIpbbSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
 
        foreach (range(1, 100) as $loop) {
            DB::table('relasi_ipbb')->insert([
                'id_ipbb'                     =>  $faker->unique()->numberBetween($min = 1, $max = 50),
                'ne_transport'                =>  $faker->text(10),
                'board_transport'             =>  $faker->text(10),
                'shelf_transport'             =>  $faker->text(10),
                'slot_transport'              =>  $faker->text(10),
                'port_transport'              =>  $faker->text(10),
                'frek_transport'              =>  $faker->text(10),
            ]);
        }
    }
}
