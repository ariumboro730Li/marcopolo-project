<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    	Model::unguard();

        $this->call([
            DatekAccessSeeder::class,
            DatekBackhaulSeeder::class,
            DatekIpranSeeder::class,
            DatekIpbbSeeder::class,
            DatekIxSeeder::class,
            DatekSummarySeeder::class,
            RcaSeeder::class,
            CrqSeeder::class,
        ]);

        Model::reguard();
    }
}
