<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Foundation\Testing\Constraints\SoftDeletedInDatabase;
use phpDocumentor\Reflection\Types\Nullable;

class CreateTableNewDatekIpbb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_datek_ipbb', function (Blueprint $table) {
            $table->bigIncrements('idipbb');
            $table->text('link')->nullable();
            $table->string("reg_tsel_a", 10)->nullable();
            $table->string('kota_router_node_a', 50)->nullable();
            $table->string('router_node_a', 30)->nullable();
            $table->string('port_tsel_end_a', 50)->nullable();
            $table->string("reg_tsel_b", 10)->nullable();
            $table->string('kota_router_node_b', 50)->nullable();
            $table->string('router_node_b', 30)->nullable();
            $table->string('port_tsel_end_b', 50)->nullable();
            $table->string('reg_telkom_a', 1)->nullable();
            $table->integer('capacity')->nullable();
            $table->float('utilization_cap')->nullable();
            $table->integer('utilization_in%')->nullable();
            $table->integer('utilization_out%')->nullable();
            $table->float('utilization_max%')->nullable();
            $table->string('layer')->nullable();
            $table->string('system', 20)->nullable();
            $table->string("transport_type", 50)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_datek_ipbb');
    }
}
