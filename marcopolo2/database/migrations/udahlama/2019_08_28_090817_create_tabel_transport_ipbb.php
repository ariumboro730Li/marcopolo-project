<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelTransportIpbb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_ipbb', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->bigInteger('id_ipbb')->nullable();
            $table->string('ne_transport')->nullable();
            $table->string('shelf_slot_port')->nullable();
            $table->string('tie_line')->nullable();
            $table->integer('row');
            $table->boolean('status');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_ipbb');
    }
}
