<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectAccessMetro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_direct_metro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("siteid_tsel", 20)->nullable();
            $table->string("siteid_info", 10)->nullable();
            $table->string("sitename_tsel", 20)->nullable();
            $table->string("regional_tsel", 10)->nullable();
            $table->string('regional_telkom', 1)->nullable();
            $table->string("port_dm_telkom", 10)->nullable();
            $table->string("gpon", 50)->nullable();
            $table->string("port_gpon", 20)->nullable();
            $table->text("description")->nullable();
            $table->integer("capacity")->nullable();
            $table->integer("utilization")->nullable();
            $table->float("utilization_cap")->nullable();
            $table->integer("update_count")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_direct_metro');
    }
}
