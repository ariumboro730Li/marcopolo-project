<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\Types\Nullable;

class CreateTabelNewDatekIx extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_datek_ix', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("reg_tsel", 10)->nullable();
            $table->string("kota_tsel", 50)->nullable();
            $table->string("routername_tsel", 30)->nullable();
            $table->string("port_tsel", 30)->nullable();
            $table->string("reg_telkom", 30)->nullable();
            $table->string("sto_telkom", 30)->nullable();
            $table->string("pe_telkom", 30)->nullable();
            $table->string("port_telkom", 30)->nullable();
            $table->string("description", 30)->nullable();
            $table->integer("capacity")->nullable();
            $table->string("utilitzation_cap", 30)->nullable();
            $table->double("utilitzation_in")->nullable();
            $table->double("utilitzation_out")->nullable();
            $table->integer("utilitzation_max")->nullable();
            $table->string("layer", 30)->nullable();
            $table->string("transport_type", 30)->nullable();
            $table->string("keterangan", 30)->nullable();
            $table->date("date_detected", 30)->nullable();
            $table->date("date_undetected", 30)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_datek_ix');
    }
}
