<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataJakabaringRegion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_jakabaring_region', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("witel", 40)->nullable();
            $table->string("region", 40)->nullable()->index();
            $table->string("treg", 40)->nullable()->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_jakabaring_region');
    }
}
