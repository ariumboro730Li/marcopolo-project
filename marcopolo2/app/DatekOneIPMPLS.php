<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatekOneIPMPLS extends Model
{
    use SoftDeletes;
    protected $table = 'datek_OneIPMPLS';
	protected $primaryKey = 'idOneIPMPLS';

    protected $fillable = [
        'reg_tsel','routername_tsel','port_tsel','reg_telkom','sto_telkom','metroe_telkom','port_telkom','description','capacity','layer','system','ne_transport_a','board_transport_a','shelf_transport_a','slot_transport_a','port_transport_a','frek_transport_a','ne_transport_b','board_transport_b','shelf_transport_b','slot_transport_b','port_transport_b','frek_transport_b','status'
    ];

    public static function Link($regional)
    {
        return DatekOneIPMPLS::where('reg_telkom',$regional)->whereNull('date_undetected')->count();
    }
    public static function LinkWithUndetected($regional)
    {
        return DatekOneIPMPLS::where('reg_telkom',$regional)->count();
    }
    public static function Capacity($regional)
    {
        return DatekOneIPMPLS::where('reg_telkom',$regional)->whereNull('date_undetected')->sum('capacity');
    }
    public static function CapacityWithUndetected($regional)
    {
        return DatekOneIPMPLS::where('reg_telkom',$regional)->sum('capacity');
    }
    public static function AllLink()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->count();
    }
    public static function AllLinkWithUndetected()
    {
        return DatekOneIPMPLS::all()->count();
    }
    public static function AllCapacity()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->sum('capacity');
    }
    public static function AllCapacityWithUndetected()
    {
        return DatekOneIPMPLS::all()->sum('capacity');
    }
    public static function Data($id)
    {
        $backhaul = DatekOneIPMPLS::find($id);
        return $backhaul;
    }

    public static function AllUtilization()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function AllUtilizationCap()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_cap');
    }
    public static function Utilization($regional)
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('reg_telkom',$regional)->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function UtilizationCap($regional)
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('reg_telkom',$regional)
            ->whereNotNull('utilization_max')
            ->where('utilization_cap',"<",100)
            ->sum('utilization_cap');
    }
    public static function Utilization2()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_max');
    }
    public static function UtilizationCap2()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_cap');
    }
    public static function TransportSystem($system)
    {
        return DatekOneIPMPLS::where('system',$system)->count();
    }
    public static function TransportType($system)
    {
        return DatekOneIPMPLS::where('system',$system)->count();
    }

    public static function OverThreshold()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->count();
    }

    public static function AllOverThreshold()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get();
    }

    public static function FiveOverThreshold()
    {
        return DatekOneIPMPLS::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->take(5)->get();
    }

}