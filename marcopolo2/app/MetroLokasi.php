<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetroLokasi extends Model
{
    protected $table = 'data_lokasi_ne';
    protected $primaryKey = 'lokasi';

    protected $fillable = [
        'ip_address','host_name','merk','ne','lokasi','regional'
    ];

}
