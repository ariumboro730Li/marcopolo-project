<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataRandomOcc extends Model
{
    // protected $table = 'data_randoms_occ';
    protected $table = 'data_random_occ';
	protected $primaryKey = 'id';

    protected $fillable = [
        // "witel",  "class", "href", "in_max", "in_avg", "in_current", "out_max", "out_avg", "out_current", "deleted_at", "created_at", "updated_at",
        "witel", "data_ran", "href", "ifname", "in_max", "in_avg", "in_current", "out_max", "out_avg", "out_current"
    ];
}

