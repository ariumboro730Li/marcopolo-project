<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessDirectMetro extends Model
{
    use SoftDeletes;
    protected $table = 'access_direct_metro';
    protected $primaryKey = 'id';

    protected $fillable = [
        'siteid_tsel','sitename_tsel','regional_tsel','me_dm_telkom','regional_telkom','port_dm_telkom', 'description','capacity','utilization%', 'utilization_cap'
    ];


}