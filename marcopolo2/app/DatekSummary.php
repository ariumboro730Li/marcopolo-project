<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatekSummary extends Model
{
    use SoftDeletes;
    protected $table = 'datek_summary';
	protected $primaryKey = 'idsummary';

	protected $fillable = [
        'month','year','reg_telkom','layer','transport_system','transport_type','link','capacity'
    ];

    public static function CapacityAcc($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "Access")->sum('capacity');
    }

    public static function CapacityBh($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "Backhaul")->sum('capacity');
    }

    public static function CapacityIpbb($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "IPBB")->sum('capacity');
    }

    public static function CapacityIpran($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "IPRAN")->sum('capacity');
    }

    public static function CapacityOneIPMPLS($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "OneIPMPLS")->sum('capacity');
    }

    public static function CapacityIx($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "IX")->sum('capacity');
    }

    public static function AllCapacityAcc($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "Access")->sum('capacity');
    }

    public static function AllCapacityBh($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "Backhaul")->sum('capacity');
    }

    public static function AllCapacityIpran($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "IPRAN")->sum('capacity');
    }

    public static function AllCapacityIpbb($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "IPBB")->sum('capacity');
    }

    public static function AllCapacityOneIPMPLS($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "OneIPMPLS")->sum('capacity');
    }

    public static function AllCapacityIx($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "IX")->sum('capacity');
    }

    public static function UtilizationAcc($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "Access")->sum('utilization');
    }

    public static function UtilizationBh($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "Backhaul")->sum('utilization');
    }

    public static function UtilizationIpran($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "IPRAN")->sum('utilization');
    }

    public static function UtilizationIpbb($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "IPBB")->sum('utilization');
    }

    public static function UtilizationOneIPMPLS($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "OneIPMPLS")->sum('utilization');
    }

    public static function UtilizationIx($month, $year, $regional)
    {
        return DatekSummary::where('month', $month)->where('year', $year)->where('reg_telkom', $regional)->where('layer', "IX")->sum('utilization');
    }

    public static function AllUtilizationAcc($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "Access")->sum('utilization');
    }

    public static function AllUtilizationBh($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "Backhaul")->sum('utilization');
    }

    public static function AllUtilizationIpran($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "IPRAN")->sum('utilization');
    }

    public static function AllUtilizationIpbb($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "IPBB")->sum('utilization');
    }

    public static function AllUtilizationOneIPMPLS($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "OneIPMPLS")->sum('utilization');
    }

    public static function AllUtilizationIx($month, $year)
    {
        return DatekSummary::all()->where('month', $month)->where('year', $year)->where('layer', "IX")->sum('utilization');
    }
}