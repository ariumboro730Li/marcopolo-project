<?php

namespace App;

use AustinHeap\Database\Encryption\Traits\HasEncryptedAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Role extends Model
{
    //



    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function getRole()
    {
        $roles =  Role::all();
        $selectRoles = array();

        foreach($roles as $role) {
            $selectRoles[$role->id] = $role->name;
        }

        return $selectRoles;
    }

    public static function getAccess($page)
    {
        $user = Auth::user()->id;
        $roles =  Role::where($page,"on")->get();

        $array =null;
        foreach ($roles as $role)
        {
            if(!isset($array))
            $array=$array."'".$role['name']."'";
            else
                $array=$array.",'".$role['name']."'";
        }

        $collection = collect($roles);
        $chunks = $collection->pluck('name');
        //$chunks = $collection->flatten('name');

        $chunks->toArray();


        //return "[".$array."]";
        return $chunks;
    }

    public static function isAuthorize()
    {
        $user = Auth::user()->roles()->name;

        return $user;
//        $roles = Role::
//        $selectRoles = array();
//
//        foreach($roles as $role) {
//            $selectRoles[$role->id] = $role->name;
//        }
//
//        return $selectRoles;
    }

    public static function getRoleStatus($id,$page)
    {
        $roles =  Role::find($id);

        return $roles->$page;
    }

}
