<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metro extends Model
{
    protected $table = 'data_lokasi_ne';
    protected $primaryKey = 'host_name';

    protected $fillable = [
        'ip_address','host_name','merk','ne','lokasi','regional'
    ];


    public static function STO($host)
    {
        $host = trim($host);
        // Metro::where('lokasi', "No Record")->truncate();
        $metros = Metro::where('host_name',$host)->count();
        if(($host == "") OR ($host == "-"))
        {
            return "";
        } else if ($host != "") {
            # code...
            if($metros != 0)
            {
                $metro = Metro::where('host_name',$host)->first();
                return $metro->lokasi;
            }else {
                $arr2 = [
                    'host_name' => $host,
                    'lokasi'    => "No Record",
                ];
                $store = Metro::create($arr2);
                $metro = Metro::where('host_name',$host)->first();
                return $metro->lokasi;
            }
        } 
    }
}
