<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataJakabaringRegion extends Model
{
    protected $table = 'data_jakabaring_region';
	protected $primaryKey = 'id';

    protected $fillable = [
        'witel', 'region', 'treg', 'href',
    ];
}
