<?php

namespace App\Http\Controllers;

use App\DatekAccess;
use App\Exports\InventoryExport;
use Illuminate\Http\Request;
use Excel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ExportController extends Controller
{

//
//        // Execute the query used to retrieve the data. In this example
//        // we're joining hypothetical users and payments tables, retrieving
//        // the payments table's primary key, the user's first and last name,
//        // the user's e-mail address, the amount paid, and the payment
//        // timestamp.
//
//        $data = DatekAccess::all();
//
//        // Initialize the array which will be passed into the Excel
//        // generator.
//        $spreadsheet = [];
//
//        // Define the Excel spreadsheet headers
//        $spreadsheet[] = ['id', 'customer','email','total','created_at'];
//
//        // Convert each member of the returned collection into an array,
//        // and append it to the payments array.
//        foreach ($data as $datum) {
//            $spreadsheet[] = $datum->toArray();
//        }
//
//        // Generate and return the spreadsheet
//        Excel::create('Access', function($excel) use ($invoicesArray) {
//
//            // Set the spreadsheet title, creator, and description
//            $excel->setTitle('Access');
//            $excel->setCreator('Laravel')->setCompany('Scriptpainter');
//            $excel->setDescription('payments file');
//
//            // Build the spreadsheet, passing in the payments array
//            $excel->sheet('sheet1', function($sheet) use ($spreadsheet) {
//                $sheet->fromArray($spreadsheet, null, 'A1', false, false);
//            });
//
//        })->download('xlsx');

    public function access($regional) {
        return Excel::download(new InventoryExport("access",$regional), 'Access.xlsx');
    }

    public function backhaul($regional) {
        return Excel::download(new InventoryExport("backhaul",$regional), 'Backhaul.xlsx');
    }


    public function OneIPMPLS($regional) {
        return Excel::download(new InventoryExport("OneIPMPLS",$regional), 'OneIPMPLS.xlsx');
    }
    public function ipbb($regional) {
        return Excel::download(new InventoryExport("ipbb",$regional), 'IPBB.xlsx');
    }

    public function ipran($regional) {
        return Excel::download(new InventoryExport("ipran",$regional), 'IPRAN.xlsx');
    }

    public function ix($regional) {
        return Excel::download(new InventoryExport("ix",$regional), 'IX.xlsx');
    }

}
