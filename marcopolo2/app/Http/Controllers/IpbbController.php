<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DatekIpran;
use App\NewDatekIpbb;
use App\DatekBackhaul;
// use App\NewDatekIpran;
use App\DataJakabaringRegion;
use App\DataJakabaringOcc;
use App\DataJakabaring;
use App\DataJkabaring;
use Goutte\Client;
use App\RelasiIpbb;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Redirect;



class IpbbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function IpranRegional($regional)
    {
        if($regional==0)
        {
            $Ipran = DatekIpran::whereNull('date_undetected');
        }
        else {
            $Ipran = DatekIpran::where('reg_telkom', $regional);
        }
    }

    public function PostImportIpbb(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'ipran_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);

        // return view('modul_inventory.ipbb_import')->with('layer','ipbb')->with('ipbb',$sheetData)->with('file',$nama_file_baru);

        return view('modul_inventory.ipbb_import')->with('layer','ipbb')->with('ipbb',$sheetData)->with('file',$nama_file_baru);
    }



    public function SaveImportIpbb(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0] != 0)
            {
                $dataOld = NewDatekIpbb::where('router_node_a', $data[2])->where('port_tsel_end_a', $data[3])->count();
                if ($dataOld == 0) {
                    $azz = [
                        'link'              => $data[2]." to ".$data[5],
                        'reg_tsel_a'        => $data[1],
                        'router_node_a'     => $data[2],
                        'port_tsel_end_a'   => $data[3],
                        'reg_tsel_b'        => $data[4],
                        'router_node_b'     => $data[5],
                        'port_tsel_end_b'   => $data[6],
                        'capacity'          => $data[7],
                        'layer'             => $data[8],
                        // 'transport_type'    => "DWDM",
                        'transport_type'    => $data[9],
                    ];
    
                    NewDatekIpbb::create($azz);
                } else {
                    $azz = [
                        // 'link'              => $data[2]." to ".$data[5],
                            'capacity'          => $data[7],
                        ];
        
                        NewDatekIpbb::where('router_node_a', $data[2])->where('port_tsel_end_a', $data[3])->update($azz);
                }
                

                
            }
        }


        // return Redirect(URL::Route('inventory.ipran')) ;
        return redirect()->route('inventory.ipbb');

    }



    public function DetailIpran($regional)
    {
        if($regional==0)
        {
            $Ipran= NewDatekIpbb::get();
            $Ipran_dwdm = NewDatekIpbb::where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpbb::where("transport_type","metro")->get();
        }
        else {
            $Ipran= NewDatekIpbb::where('reg_tsel_a', $regional)->get();
            $Ipran_dwdm = NewDatekIpbb::where('reg_tsel_a', $regional)->where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpbb::where('reg_tsel_a', $regional)->where("transport_type","metro")->get();

        }
        return view('modul_inventory.ipbb_detail')->with('ipran_dwdm',$Ipran_dwdm)->with('ipran_metro',$Ipran_metro)->with('ipran',$Ipran)->with('regional',$regional);
    }

    public function DetailIpran2($regional)
    {
        if($regional==0)
        {
            $Ipran= NewDatekIpbb::get();
            $Ipran_dwdm = NewDatekIpbb::where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpbb::where("transport_type","metro")->get();
        }
        else {
            $Ipran= NewDatekIpbb::where('reg_tsel_a', $regional)->get();
            $Ipran_dwdm = NewDatekIpbb::where('reg_tsel_a', $regional)->where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpbb::where('reg_tsel_a', $regional)->where("transport_type","metro")->get();

        }
        return view('modul_inventory.ipbb_detail')->with('ipran_dwdm',$Ipran_dwdm)->with('ipran_metro',$Ipran_metro)->with('ipran',$Ipran)->with('regional',$regional);
    }

    public function DeleteIpran($id)
    {
        NewDatekIpbb::destroy($id);

        return Redirect::back();
    }


    public function editKotaA($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'kota_router_node_a'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editKotaB($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'kota_router_node_b'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editRouterA($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'router_node_a'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editRouterB($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'router_node_b'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editCapacity($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'capacity'    => $var,
        ]);
        $resp = $update;
        return 1;

    }
    
    public function editLayer($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'layer'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editSystem($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'system'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editTransport($id, $var)
    {
        $update = NewDatekIpbb::find($id)->update([
            'transport_type'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    
    public function editsPortA(request $request) {
        $port_a = $request->input('port_a');
        $id = $request->input('id');
        
        $update = NewDatekIpbb::find($id);
        $update->port_tsel_end_a =  $port_a;
        $update->save();
        return 1;

    }

    public function editsPortB(request $request) {

        $port_b = $request->input('port_b');
        $id = $request->input('id');
        
        $update = NewDatekIpbb::find($id);
        $update->port_tsel_end_b =  $port_b;
        $update->save();
        return 1;
    }

    public function editKetBackhaul($id, $value) {
        $update = DatekBackhaul::where('idbackhaul', $id)->update([
            'Keterangan'    => $value,
        ]);
        $resp = $update;
        return 1;
    }

    public function editKotaBackhaul($id, $var){
        $update = DatekBackhaul::where('idbackhaul', $id)->update([
            'kota_tsel'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

        // Scrapping Data Jakabaring Region dan Witel dan Input ke Database data_random_region 
        public function scrapRegion() 
        {
            $client = new Client();
            $crawler = $client->request('GET', 'http://10.1.18.33/jakabaring/');
            $accord = $crawler->filter('frame')->eq(1)->attr('src');
            $crawler = $client->request('GET', 'http://10.1.18.33/jakabaring/'.$accord);
    
            $print = $crawler->filter('.applemenu > .silverheader > a')->each(function ($nodex) {
             return $nodex->text();
            });
    
            $counte = count($print) - 9;
            for ($i=0; $i < $counte ; $i++) { 
                $print1 = $crawler->filter('.applemenu > .submenu')->eq($i)->filter("div")->each(function ($nodex) {  
                    return $nodex->text();
                });            
                $print1h = $crawler->filter('.applemenu > .submenu')->eq($i)->filter("div")->each(function ($nodex) {  
                    $olala = $nodex->attr("onclick");
                    $ilang1 = "');return document.MM_returnValue";
                    $ilang2 = "MM_goToURL('parent.mainFrame','";
                    $ilang3 = "window.open('";
                    $ilang4 = "','_blank";
                    $jadi1  = str_replace($ilang1, "", $olala);
                    $jadi2  = str_replace($ilang2, "", $jadi1);
                    $jadi3  = str_replace($ilang3, "", $jadi2);
                    $jadi4  = str_replace($ilang4, "", $jadi3);
                    return $jadi4;
                });            

                $counte1 = count($print1);
                for ($a=1; $a < $counte1; $a++) { 

                        $witelada = DataJakabaringRegion::where("witel", $print1[$a])->count();
                        if ($witelada == 0) {
                            $azz = array(
                                'region' => $print[$i],
                                'witel'  => $print1[$a],
                                'href'   => $print1h[$a],
                                'treg'   => $i+1
                            );
                            DataJakabaringRegion::create($azz);
                        } else {
                            $azz = array(
                                'href'   => $print1h[$a],
                            );
                            DataJakabaringRegion::where("witel", $print1[$a])->update($azz);
                        }
                }
            }
            // echo DataJakabaringRegion::count();    
        }
    
     // Scrapping Data Jakabaring Href setiap witel dan Region
     public function getHrefDataJakabaring() {
         $client = new Client();
        $bb = DataJakabaringRegion::get();    
        foreach ($bb as $key => $value) {
            $witel      = $value->witel;
            $region     = $value->region;
            $treg       = $value->treg;
            $href       = $value->href;
            $crawler    = $client->request('GET', 'http://10.1.18.33/jakabaring/'.$href);
            $printbb    = $crawler->filter('.style4 > tr > td')->each(function ($node) {
                // print $node->text()."<br>";
                return $node->text();
            });
            $printhref = $crawler->filter('.style4 > tr > td > div > a')->each(function ($node) {
                //  print $node->attr("href")."<br><br>";
                 return $node->attr("href");
            });
            $counthref  = count($printhref);
            $countbb    = count($printbb);
            // echo $countbb;
            // echo "<br>";
            // echo $counthref;
            // echo "<br>";

            for ($i=0; $i < $countbb ; $i++) {   
                $hrefada = DataJakabaring::where("href", $printhref[$i])->count();
                if ($hrefada == 0) {
                    $azz = array(
                        'witel'     => $witel,
                        'region'    => $region,
                        'data_bb'   => $printbb[$i],
                        'treg'      => $treg,
                        'href'      => $printhref[$i],
                    );
                    DataJakabaring::create($azz);    
                } else {
                    $azz = array(
                        'witel'     => $witel,
                        'region'    => $region,
                        'data_bb'   => $printbb[$i],
                        'treg'      => $treg,
                        'href'      => $printhref[$i],
                    );
                    DataJakabaring::where("href", $printhref[$i])->update($azz);    
                    
                }
                
            }
        }
     }
 
     public function getUtilScrapRanX() {
        $client = new Client();
        $mrtg  = DataJakabaring::get();
        $mrtgcount  = DataJakabaring::count();

        foreach ($mrtg as $key => $value) {
            $href = $value->href;
            $witelid = $value->id;
            $witel = $value->witel;
            $data_bb = $value->data_bb;
            $crawler    = $client->request('GET', 'http://10.1.18.33/'.$href);
                 $printbb    = $crawler->filter('.graph > h2')->eq(0)->each(function ($node) {
                     return $node->text()."<br>";
                 });
                 $printbbcount = count($printbb);
                 $ifnames    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")')->each(function($node) {
                     return $node->text()." <br>";
                 });
                 $descript   = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")')->each(function($node) {
                     return $node->text()." <br>";
                 });
                 $ifnamecount = count($ifnames);
                 $descriptcount = count($descript);
                 if ( $ifnamecount > 0) {
                     $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")+td')->each(function($node) {
                         return $node->text();
                     });
                 } else {
                     if ($descriptcount > 0) {
                            $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")+td')->each(function($node) {
                                return $node->text();
                            });
                        } else {
                            $ifname    = $crawler->filter('tr')->each(function($node) {
                                return " - ";
                            });
                     }
                }

                                  
                 $inmax    = $crawler->filter(' .in > td ')->eq(0)->each(function ($node) {
                        return $node->text();
                 });
                 $inavg    = $crawler->filter(' .in > td ')->eq(1)->each(function ($node) {
                        return $node->text();
                 });
                 $incur    = $crawler->filter(' .in > td ')->eq(2)->each(function ($node) {
                        return $node->text();
                 });
                 $outmax    = $crawler->filter(' .out > td ')->eq(0)->each(function ($node) {
                     return $node->text();
                 });
                 $outavg    = $crawler->filter(' .out > td ')->eq(1)->each(function ($node) {
                     return $node->text();
                 });
                 $outcur    = $crawler->filter(' .out > td ')->eq(2)->each(function ($node) {
                     return $node->text();
                 });
                          
                 for ($i=0; $i < $printbbcount ; $i++) {        
                    $hrefada = DataJakabaringOcc::where("href", $href)->count();
                    if ($hrefada == 0) {
                        # code...
                        $azz = array(
                            // 'id_region'         => $witelid,
                            'witel'             => $witel,
                            'href'              => $href,
                            'data_bb'           => $data_bb,
                            'ifname'            => $ifname[$i],
                            'in_max'            => $inmax[$i], 
                            'in_avg'            => $inavg[$i], 
                            'in_current'        => $incur[$i], 
                            'out_max'           => $outmax[$i], 
                            'out_avg'           => $outavg[$i], 
                            'out_current'       => $outcur[$i], 
                        );
                        DataJakabaringOcc::create($azz);    
                } else {
                        $azz = array(
                            // 'id_region'         => $witelid,
                            // 'href'              => $href,
                            'ifname'            => $ifname[$i],
                            'data_bb'           => $data_bb,
                            'in_max'            => $inmax[$i], 
                            'in_avg'            => $inavg[$i], 
                            'in_current'        => $incur[$i], 
                            'out_max'           => $outmax[$i], 
                            'out_avg'           => $outavg[$i], 
                            'out_current'       => $outcur[$i], 
                        );
                        DataJakabaringOcc::where("href", $href)->update($azz);    
                        # code...
                    }
                 }
            } 
     }
       
    //  public function getUtilScrapPbb() {
    //     $this->scrapRegion();
    //     $this->getHrefDataJakabaring();
    // }
 
     public function getUtilScrapPbb() {

        // Time Start
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
 
        // Executor data_jakabaring, data_jakbaring_region, data_jakabaring_occ
         $this->getUtilScrapRanX();

        // Mengambil all Data new_datek_ipbb
        $ipbb = NewDatekIpbb::get();

        // membaca all data new_datek_ipbb
        foreach ($ipbb as $key => $value) {
            // mengambil all data new_datek_ipbb router_node_a
            $router     = $value->router_node_a;
            // mengambil all data new_datek_ipbb router_node_b
            $routerb    = $value->router_node_b;
            // mengambil all data new_datek_ipbb port_tsel_end_a
            $port       = $value->port_tsel_end_a;
            // mengambil all data new_datek_ipbb port_tsel_end_b
            $portb      = $value->port_tsel_end_b;
            // mengambil all data new_datek_ipbb capacity
            $capacity   = $value->capacity;

            // mengambil data_jakabaring_occ dimana data_bb->data_jakabaring_occ cocok dengan router_node_a->new_datek_ipbb dan 
            // ifname cocok dengan nilai port_tsel_end_a->data_jakbarin_occ
            $utilbb     = DataJakabaringOcc::where('data_bb', 'like', '%'.$router.'%')->where("ifname", $port );
            // mengambil data_jakabaring_occ dimana data_bb->data_jakabaring_occ cocok dengan router_node_a->new_datek_ipbb
            $kotaa      = DataJakabaringOcc::where('data_bb', 'like', '%'.$router.'%');
            // mengambil data_jakabaring_occ dimana data_bb->data_jakabaring_occ cocok dengan router_node_b->new_datek_ipbb
            $kotab      = DataJakabaringOcc::where('data_bb', 'like', '%'.$routerb.'%');

            // pengulangan update util
            foreach ($utilbb->get() as $key => $val) {
                    // mengambil nilai in current dari data_jakabaring_occ
                    $in_cur = round(floatval($val->in_current), 1)/1000 . "%";                
                    // mengambil nilai out current dari data_jakabaring_occ
                    $out_cur = round(floatval($val->out_current), 1)/1000 . "%";  
                    // mengambil nilai in max dari data_jakabaring_occ
                    $in_max = round(floatval($val->in_max), 1)/1000 ;                
                    // mengambil nilai out max dari data_jakabaring_occ
                    $out_max = round(floatval($val->out_max), 1)/1000;  
                
                    // membandingkan nilai in_max dan out_max
                    // jika in_max lebih besar dari out_max
                    if ($in_max >= $out_max) {
                        // nilai in_max yg ddambil
                        $max = $in_max;
                        $cap = $max * $capacity;
                    // tapi jika in_max lebih kecil dari out_max maka
                    } else {
                        // nilai out_max yg ddambil
                        $max = $out_max;
                        $cap = $max * $capacity;
                    }
                    
                    // proses input data ke new_datek_ipbb
                    $azz = [
                        'utilization_in%'       => $in_cur,
                        'utilization_out%'      => $out_cur,
                        'utilization_max%'      => $max,
                        'utilization_cap'       => $cap,    
                    ];
                    NewDatekIpbb::where('router_node_a', $router)->where("port_tsel_end_a", $port)->update($azz); 
            }

            // pengulangan update witel untuk kota_router_node_a
            foreach ($kotaa->get() as $key => $valb) {
                //  jika router_node_a->new_Datek_ipbb == Null maka terupdate, jika tidak maka tidak diupdate
                if ( $value->router_node_a == Null) {
                    $azz = [
                        'kota_router_node_a'    => $valb->witel,
                    ];
                    NewDatekIpbb::where('router_node_a', $router)->update($azz); 
                } else {
                    # code...
                }
                
            }

            //  jika router_node_b->new_Datek_ipbb == Null maka terupdate, jika tidak maka tidak diupdate
            foreach ($kotab->get() as $key => $valc) {
                if ( $value->router_node_b == Null) {
                    $azz = [
                        'kota_router_node_b'    => $valc->witel,
                    ];
                    NewDatekIpbb::where('router_node_b', $routerb)->update($azz); 
                } else {
                    # code...
                }
            }
        }


        // time Stop      
        $time = microtime();
        $time = explode(' ',$time);
        $time = $time[1]+$time[0];
        $finish = $time;
        $total = ($finish - $start)/60;
        echo "<h1>".DataJakabaringRegion::count()." Data Jakabaring Region</h1>";
        echo "<h1>".DataJakabaring::count()." Data Jakabaring HREF</h1>";
        echo "<h1>".DataJakabaringOcc::count()." Data Jakabaring OCC</h1>";
        echo "<h1>".NewDatekIpbb::whereNotNull("utilization_in%")->count()." Data Datek IPBB Terupdate Utilnya</h1>";
        echo "<h1>".NewDatekIpbb::whereNotNull("router_node_b")->whereNotNull("router_node_a")->count()." Data Datek IPBB Terupdate Witel</h1>";
        echo "<h1> Waktu Eksekusi $total Menit</h1>";
    }

}
