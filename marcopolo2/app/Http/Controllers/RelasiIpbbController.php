<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\RelasiIpbb;
use App\Metro;
use GuzzleHttp\Client;


class RelasiIpbbController extends Controller
{
    public function idrow($id) {

        $rowRelasiIpbb = RelasiIpbb::where('id_ipbb',$id)->orderBy('row', 'desc');
        
        if ($rowRelasiIpbb->count() == 0) {
            $rowBaru = 1;
            $arr = [
                'id_ipbb' => $id,
                'row'   => $rowBaru,
                'status'    => 0
            ];
            $store = RelasiIpbb::create($arr);

        } else {
            $rowBaru = $rowRelasiIpbb->first()->row+1;
            $arr = [
                'id_ipbb' => $id,
                'row'   => $rowBaru,
                'status'    => 0
            ];
            $store = RelasiIpbb::create($arr);

        }    
        
        $kolomBaru = RelasiIpbb::where('id_ipbb',$id)->where("row", $rowBaru)->get();
        $new = $kolomBaru->first()->id;
        return $new;
        
    }

    public function idrowNot($id) {
       $relasiIpbb  = RelasiIpbb::where('id',$id)->orderBy('row', 'desc');
       if ($relasiIpbb->count() == 0) {
            $rowLam = "ente Ayaa";
            $rowLama = 0;
            return 0;
        } else {
            $rowLam     = "ada Nihh";
            $rowLama     = $relasiIpbb->first()->id ;
            $deleteIdRow  = RelasiIpbb::where('id',$id)->delete();
            return 1;
        }

        
    }

    public function updateSto($id, $value) {
        $olala  = trim($value);
        $update = Metro::where('host_name', $id)->update([
            'lokasi'        => $olala,
        ]);
        $resp = $update;
        return $resp;
    }

    public function updateNe($id, $value) {
        $olala  = trim($value);
        $update = RelasiIpbb::find($id)->update([
            'ne_transport'    => $olala,
            'status'           => 1,
        ]);
        $resp = $update;

        $cmetro = Metro::where('host_name', $olala)->count();

        if($cmetro != 0)
        {    
            $sto = Metro::STO($olala);
            return $sto;
        } else {
            $arr2 = [
                'host_name' => $olala,
                'lokasi'    => "No Record",
            ];
            $store = Metro::create($arr2);
            $metro = Metro::where('host_name',$olala)->first();
            return $metro->lokasi;
    }

    }

    public function updateSsp($id, $value) {
        $olala  = $value;
        $update = RelasiIpbb::find($id)->update([
            'shelf_slot_port'  => $olala,
            'status'           => 1,
        ]);
        $resp = $update;
        return 1;
    }

    public function updateTie($id, $value) {
        $olala  = $value;
        $update = RelasiIpbb::find($id)->update([
            'tie_line'          => $olala,
            'status'           => 1,
        ]);
        $resp = $update;
        return 1;
    }

    public function cekApi() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_PORT => "8581",
        CURLOPT_URL => "http://10.62.166.182:8581/odata/api/interfaces?resolution=HOUR&period=1d&$"."apply=groupby%28portmfs/ID,%20aggregate%28portmfs%28im_Utilization%20with%20max%20as%20Value%29%29%29&$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."expand=device,portmfs&$"."select=Name,Description,ID,SpeedIn,SpeedOut,device/Name,portmfs/im_Utilization&$"."filter=%28%28substringof%28tolower%28%27telkomsel%20region:reg-01%28sumbagut%29:backhaul%27%29,%20tolower%28groups/GroupPathLocation%29%29%20eq%20true%29%29",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(

            "Authorization: Basic OTQwMTU2Ok5vNTQyMzY3Njs=",
            "Cache-Control: no-cache",
            "Postman-Token: 17bb2470-5c9f-4e18-82f7-d87dde72467f"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }


}
