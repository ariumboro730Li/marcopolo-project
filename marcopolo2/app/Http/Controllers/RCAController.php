<?php

namespace App\Http\Controllers;

use App\RCA;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Barryvdh\Snappy\IlluminateSnappyPdf;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade as DomPDF;
use Snappy;
use SnappyImage;
use VerumConsilium\Browsershot\Facades\PDF;
use VerumConsilium\Browsershot\Facades\Screenshot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class RCAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $rca = RCA::all()->sortByDesc("created_at");
        return view('modul_rca.list_rca')->with('rca',$rca);
    }

    public function tracking()
    {
        $rca = RCA::with('detail')->get();
        return view('modul_rca.tracking')->with('rca',$rca);
    }

    public function HapusRCA($idrca)
    {

        RCA::destroy($idrca);

        return Redirect::back();
    }

    public function EditRCA($idrca)
    {
        $rca = RCA::find($idrca);
        $keterangan = "Edit";


        return view('modul_rca.edit_rca')->with('rca',$rca)->with('keterangan',$keterangan);
    }

    public function UpdateRCA(Request $request)
    {
        $input = $request->all();
        $rca = RCA::find($input['id']);
        $rca->title = $input['title'];
        $rca->regional = $input['regional'];
        $rca->date_time_open = $input['date_time_open'];
        $rca->date_time_close = $input['date_time_close'];
        $rca->duration = $input['duration'];
        $rca->network_impact = $input['network_impact'];
        $rca->service_impact = $input['service_impact'];
        $rca->rc_category = $input['rc_category'];
        $rca->rc_category_detail = $input['rc_category_detail'];
        $rca->rc_detail = $input['rc_detail'];
        $rca->action = $input['action'];
	
	$startTimeStamp = strtotime($rca->date_time_open);
            $endTimeStamp = strtotime($rca->date_time_close);

	$timeDiff = abs($endTimeStamp - $startTimeStamp);
            // $rca->duration =$timeDiff/3600;
            
            $days = Floor($timeDiff/86400);
            $sisa = $timeDiff%86400;
            $jam = Floor($sisa/3600);
            $sisa = $sisa%3600;
            $menit = Floor($sisa/60);
            $sisa = $sisa%60;

            $rca->duration = $days."D  ".$jam."H  ".$menit."M  ";
            $rca->week = 'W-'.\Carbon\Carbon::parse($rca->date_time_open)->format('W');


        if($request->file('evidence_file') == "")
        {
            $rca->evidence_file = $rca->evidence_file ;
        }
        else
        {
            $file       = $request->file('evidence_file');
            $ext   = $file->getClientOriginalName();
            $request->file('evidence_file')->move("images/", $input['id'].'_evidence_file'.$ext);
            $rca->evidence_file = $input['id'].'_evidence_file'.$ext;
        }

        if($request->file('topology_file') == "")
        {
            $rca->topology_file = $rca->topology_file ;
        }
        else
        {
            $file       = $request->file('topology_file');
            $ext   = $file->getClientOriginalName();
            $request->file('topology_file')->move("images/", $input['id'].'_topology_file'.$ext);
            $rca->topology_file =$input['id'].'_topology_file'.$ext;
        }

//        $rca->improvement_activity = $input['improvement_activity'];
//        $rca->status = $input['status'];
//        $rca->tgl_target = $input['tgl_target'];
//        $rca->pic = $input['pic'];
        $rca->save();


        return Redirect(URL::Route('rca.index')) ;
    }
    public function TambahRCA()
    {
        $keterangan = null;



        return view('modul_rca.input_rca')->with('keterangan',$keterangan);
    }
    public function TambahRCABaru(Request $request)
    {
        $input = $request->all();
        $rca = new RCA;
        $rca->title = $input['title'];
        $rca->regional = $input['regional'];
        $rca->date_time_open = $input['date_time_open'];
        $rca->date_time_close = $input['date_time_close'];
//        $rca->duration = $input['duration'];
        $rca->network_impact = $input['network_impact'];
        $rca->service_impact = $input['service_impact'];
        $rca->rc_category = $input['rc_category'];
        $rca->rc_category_detail = $input['rc_category_detail'];
        $rca->rc_detail = $input['rc_detail'];
        $rca->action = $input['action'];
        /*        $rca->improvement_activity = $input['improvement_activity'];
                $rca->status = $input['status'];
                $rca->tgl_target = $input['tgl_target'];
                $rca->pic = $input['pic'];*/

        $startTimeStamp = strtotime($rca->date_time_open);
        $endTimeStamp = strtotime($rca->date_time_close);

        $timeDiff = abs($endTimeStamp - $startTimeStamp);
            // $rca->duration =$timeDiff/3600;
            
            $days = Floor($timeDiff/86400);
            $sisa = $timeDiff%86400;
            $jam = Floor($sisa/3600);
            $sisa = $sisa%3600;
            $menit = Floor($sisa/60);
            $sisa = $sisa%60;

            $rca->duration = $days."D  ".$jam."H  ".$menit."M  ";
            $rca->week = 'W-'.\Carbon\Carbon::parse($rca->date_time_open)->format('W');



        $rca->save();

        $rca = RCA::where('title',$input['title'])->first();
        if($request->file('evidence_file') == "")
        {
            $rca->evidence_file = $rca->evidence_file ;
        }
        else
        {
            $file       = $request->file('evidence_file');
            $ext   = $file->getClientOriginalName();
            $request->file('evidence_file')->move("images/rca", $rca['id'].'_evidence_file'.$ext);
            $rca->evidence_file = $rca['id'].'_evidence_file'.$ext;
        }

        if($request->file('topology_file') == "")
        {
            $rca->topology_file = $rca->topology_file ;
        }
        else
        {
            $file       = $request->file('topology_file');
            $ext   = $file->getClientOriginalName();
            $request->file('topology_file')->move("images/rca", $rca['id'].'_topology_file'.$ext);
            $rca->topology_file =$rca['id'].'_topology_file'.$ext;
        }
        $rca->save();

        $rcaedit = RCA::find($rca->idrca);
        $keterangan = "Edit";


        return view('modul_rca.edit_rca')->with('rca',$rcaedit)->with('keterangan',$keterangan);


        //return Redirect(URL::Route('rca.index')) ;
    }

    public function ExportRCA()
    {

        $data = RCA::all();

        $pdf = SnappyImage::loadView('modul_rca.export.model1');
        /*return PDF::loadView('modul_rca.export.model1')
            ->download('example.pdf');*/
        /*$pdf->setPaper('A4', 'landscape');*/

        return $pdf->download('RCAReport.jpg');

    }

    public function ExportRCAData($id)
    {
        $data = RCA::find($id);

        /*return Snappy::loadView('modul_rca.export.model',compact('data',$data))
            ->inline();*/

        /*return PDF::loadView('modul_rca.export.model',compact('data',$data))
            ->inline();*/

        /*return PDF::loadView('modul_rca.export.model',compact('data',$data))
            ->margins(10, 10, 10, 10)
            ->fullPage()
            ->inline();*/

        /*return PDF::loadView('modul_rca.export.model',compact('data',$data))
            ->margins(20, 20, 20, 20)
            ->download('RCAReport.pdf');*/

        return Screenshot::loadView('modul_rca.export.model',compact('data',$data))
            ->margins(10, 10, 10, 10)
            ->useJPG()
            ->fullPage()
            ->download('RCAReport.jpg');

    }

    public function ViewRCAData($id)
    {

        $data = RCA::find($id);

//        dd(json_encode($data));

//        return view('modul_rca.export.model')->with('data',$data);
//
//        $pdf = SnappyImage::loadView('modul_rca.export.model',compact('data',$data));
//        /*$pdf->setPaper('A4', 'landscape');*/
//
//        return $pdf->download('RCAReport.jpg');

        /*$data = RCA::find($id);
        $pdf = PDF::loadView('modul_rca.export.model1',compact('data'));

        $pdf->setPaper('A3', 'landscape');

        if (isset($pdf)) {
            // open the PDF object - all drawing commands will
            // now go to the object instead of the current page
            $footer = $pdf->open_object();

            // get height and width of page
            $w = $pdf->get_width();
            $h = $pdf->get_height();

            echo $w;
            echo $h;
        }
        return $pdf->download('RCAReport.pdf');*/

        return view('modul_rca.export.model',compact('data',$data));

    }

    public function ExportModelRCA()
    {
        return view('modul_rca.export.model1');
    }


}
