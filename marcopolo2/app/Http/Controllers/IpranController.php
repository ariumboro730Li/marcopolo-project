<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DatekIpran;
use App\NewDatekIpran;
use App\RelasiIpran;
use App\DataRandomRegion;
use App\DataRandomOcc;
use App\DataRandom;
use Goutte\Client;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Redirect;



class IpranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function PostImportIpran(Request $request)
    {

        // Jika user telah mengklik tombol Preview
        $input = $request->all();
        //$ip = ; // Ambil IP Address dari User
        $nama_file_baru = 'ipran_'.Carbon::now()->format('d-m-Y').'.xlsx';

        // Cek apakah terdapat file data.xlsx pada folder tmp
        if (is_file('tmp/' . $nama_file_baru)) // Jika file tersebut ada
            unlink('tmp/' . $nama_file_baru); // Hapus file tersebut

        $tipe_file = $request->file('file')->getMimeType(); // Ambil tipe file yang akan diupload
        $tmp_file = $request->file('file')->getRealPath();

        // Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
        //if ($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        // Upload file yang dipilih ke folder tmp
        // dan rename file tersebut menjadi data{ip_address}.xlsx
        // {ip_address} diganti jadi ip address user yang ada di variabel $ip
        // Contoh nama file setelah di rename : data127.0.0.1.xlsx
        move_uploaded_file($tmp_file, 'tmp/' . $nama_file_baru);
        $spreadsheet = IOFactory::load('tmp/' . $nama_file_baru);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);

        // return view('modul_inventory.ipbb_import')->with('layer','ipbb')->with('ipbb',$sheetData)->with('file',$nama_file_baru);

        return view('modul_inventory.ipran_import')->with('layer','ipbb')->with('ipbb',$sheetData)->with('file',$nama_file_baru);
    }



    public function SaveImportIpran(Request $request)
    {
        $input = $request->all();

        $spreadsheet = IOFactory::load('tmp/' . $input['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        //dd($sheetData);
        foreach($sheetData as $data)
        {
            if($data[0] != 0)
            {
                $dataOld = NewDatekIpran::where('router_node_a', $data[2])->where('port_tsel_end_a', $data[3])->count();
                if ($dataOld == 0) {
                    $azz = [
                        'link'              => $data[2]." to ".$data[5],
                        'reg_tsel_a'        => $data[1],
                        'router_node_a'     => $data[2],
                        'port_tsel_end_a'   => $data[3],
                        'reg_tsel_b'        => $data[4],
                        'router_node_b'     => $data[5],
                        'port_tsel_end_b'   => $data[6],
                        'capacity'          => $data[7],
                        'layer'             => $data[8],
                        // 'transport_type'    => "DWDM",
                        'transport_type'    => $data[9],
                    ];
    
                    NewDatekIpbb::create($azz);
                } else {
                    $azz = [
                            'capacity'          => $data[7],
                        ];
        
                        NewDatekIpran::where('router_node_a', $data[2])->where('port_tsel_end_a', $data[3])->update($azz);
                }
                

                
            }
        }

        // return Redirect(URL::Route('inventory.ipran')) ;
        return redirect()->route('inventory.ipran');

    }



    public function DetailIpran($regional)
    {
        if($regional==0)
        {
            $Ipran= NewDatekIpran::get();
            $Ipran_dwdm = NewDatekIpran::where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpran::where("transport_type","metro")->get();
        }
        else {
            $Ipran= NewDatekIpran::where('reg_tsel_a', $regional)->get();
            $Ipran_dwdm = NewDatekIpran::where('reg_tsel_a', $regional)->where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpran::where('reg_tsel_a', $regional)->where("transport_type","metro")->get();

        }
        return view('modul_inventory.ipran_detail')->with('ipran_dwdm',$Ipran_dwdm)->with('ipran_metro',$Ipran_metro)->with('ipran',$Ipran)->with('regional',$regional);
    }

    public function DetailIpran2($regional)
    {
        if($regional==0)
        {
            $Ipran= NewDatekIpran::get();
            $Ipran_dwdm = NewDatekIpran::where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpran::where("transport_type","metro")->get();
        }
        else {
            $Ipran= NewDatekIpran::where('reg_tsel_a', $regional)->get();
            $Ipran_dwdm = NewDatekIpran::where('reg_tsel_a', $regional)->where("transport_type","dwdm")->get();
            $Ipran_metro = NewDatekIpran::where('reg_tsel_a', $regional)->where("transport_type","metro")->get();

        }
        return view('modul_inventory.ipbb_detail')->with('ipran_dwdm',$Ipran_dwdm)->with('ipran_metro',$Ipran_metro)->with('ipran',$Ipran)->with('regional',$regional);
    }

    public function DeleteIpran($id)
    {
        NewDatekIpran::destroy($id);

        return Redirect::back();
    }


    public function editKotaA($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'kota_router_node_a'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editKotaB($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'kota_router_node_b'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editRouterA($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'router_node_a'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editRouterB($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'router_node_b'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editCapacity($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'capacity'    => $var,
        ]);
        $resp = $update;
        return 1;

    }
    
    public function editLayer($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'layer'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editSystem($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'system'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    public function editTransport($id, $var)
    {
        $update = NewDatekIpran::find($id)->update([
            'transport_type'    => $var,
        ]);
        $resp = $update;
        return 1;

    }

    
    public function editsPortA(request $request) {
        $port_a = $request->input('port_a');
        $id = $request->input('id');
        
        $update = NewDatekIpran::find($id);
        $update->port_tsel_end_a =  $port_a;
        $update->save();
        return 1;

    }

    public function editsPortB(request $request) {

        $port_b = $request->input('port_b');
        $id = $request->input('id');
        
        $update = NewDatekIpran::find($id);
        $update->port_tsel_end_b =  $port_b;
        $update->save();
        return 1;
    }

    public function scrapRegion() 
    {
        $client = new Client();
        $crawler = $client->request('GET', 'http://10.1.18.33/random/');
        $accord = $crawler->filter('frame')->eq(1)->attr('src');
        $crawler = $client->request('GET', 'http://10.1.18.33/random/'.$accord);

        $print = $crawler->filter('.applemenu > .silverheader > a')->each(function ($nodex) {
         return $nodex->text();
        });

        $counte = count($print) - 1;
        for ($i=0; $i < $counte ; $i++) { 
            $print1 = $crawler->filter('.applemenu > .submenu')->eq($i)->filter("div")->each(function ($nodex) {  
                return $nodex->text();
            });            
            $print1h = $crawler->filter('.applemenu > .submenu')->eq($i)->filter("div")->each(function ($nodex) {  
                $olala = $nodex->attr("onclick");
                $ilang1 = "');return document.MM_returnValue";
                $ilang2 = "MM_goToURL('parent.mainFrame','";
                $ilang3 = "window.open('";
                $ilang4 = "','_blank";
                $jadi1  = str_replace($ilang1, "", $olala);
                $jadi2  = str_replace($ilang2, "", $jadi1);
                $jadi3  = str_replace($ilang3, "", $jadi2);
                $jadi4  = str_replace($ilang4, "", $jadi3);
                return $jadi4;
            });            

            $counte1 = count($print1);
            for ($a=1; $a < $counte1; $a++) { 

                    $witelada = DataRandomRegion::where("witel", $print1[$a])->count();
                    if ($witelada == 0) {
                        $azz = array(
                            'region' => $print[$i],
                            'witel'  => $print1[$a],
                            'href'   => $print1h[$a],
                            'treg'   => $i+1
                        );
                        DataRandomRegion::create($azz);
                    } else {
                        $azz = array(
                            'href'   => $print1h[$a],
                        );
                        DataRandomRegion::where("witel", $print1[$a])->update($azz);
                    }
            }
        }
        echo DataRandomRegion::count();    
    }

    // Scrapping Data Random Href setiap witel dan Region
    public function getHrefDataRandom() {
        $client = new Client();
        $ran = DataRandomRegion::get();    
        foreach ($ran as $key => $value) {
            $witel      = $value->witel;
            $region     = $value->region;
            $treg       = $value->treg;
            $href       = $value->href;
            $crawler = $client->request('GET', 'http://10.1.18.33/random/'.$href);
            $printran = $crawler->filter('tr > td > .style4')->each(function ($node) {
                return $node->text();
            });


            $printhref = $crawler->filter('tr > td > div > a')->each(function ($node) {
                return $node->attr("href");
            });

            $counthref = count($printhref);
            $countran = count($printran);

            for ($i=0; $i < $counthref ; $i++)  {  
                $hrefada = DataRandom::where("href", $printhref[$i])->count();
                if ($hrefada == 0) {
                    $azz = array(
                        'witel'     => $witel,
                        'region'    => $region,
                        'data_ran'  => $printran[$i],
                        'treg'      => $treg,
                        'href'      => $printhref[$i],
                    );
                    DataRandom::create($azz);    
                } else {
                    $azz = array(
                        'witel'     => $witel,
                        'region'    => $region,
                        'data_ran'  => $printran[$i],
                        'treg'      => $treg,
                        'href'      => $printhref[$i],
                    );
                    DataRandom::where("href", $printhref[$i])->update($azz);     
                }
            }
            

         }
    }

    // Scrapping data Util Random per href
    public function getUtilScrapRanX() {

        $href = DataRandom::get();
        $mrtgcount  = DataRandom::count();

        $i = 1;
        foreach ($href as $key => $value) {
            // echo $value->data_ran." - ".$value->href." - ";
            preg_match("/http/", $value->href, $matched);
            if ($matched) {
                $client = new Client();
                $crawler = $client->request('GET', $href);
                if ($crawler->filter('.graph > h2 ')->eq(0)->count() > 0) {
                    $h2 = $crawler->filter('.graph > h2')->eq(0)->text();
                    $in0 = $crawler->filter('.graph > table > .in > td')->eq(0)->text();
                    $in1 = $crawler->filter('.graph > table > .in > td')->eq(1)->text();
                    $in2 = $crawler->filter('.graph > table > .in > td')->eq(2)->text();

                    $out0 = $crawler->filter('.graph > table > .out > td')->eq(0)->text();
                    $out1 = $crawler->filter('.graph > table > .out > td')->eq(1)->text();
                    $out2 = $crawler->filter('.graph > table > .out > td')->eq(2)->text();

                    $ifnames    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")')->each(function($node) {
                        return $node->text()." <br>";
                    });
                    $descript   = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")')->each(function($node) {
                        return $node->text()." <br>";
                    });
                    $ifnamecount = count($ifnames);
                    $descriptcount = count($descript);
                    if ( $ifnamecount > 0) {
                        $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")+td')->eq(0)->text();
                        // $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")+td')->each(function($node) {
                        //     // return $node->text();
                        //     print $node->text()."<br>";
                        // });
                    } else {
                        if ($descriptcount > 0) {
                               $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")+td')->eq(0)->text();
                            //    $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")+td')->each(function($node) {
                            //     print $node->text()."<br>";
                            // });
                           } else {
                               $ifname = "-";
                            //    $ifname    = $crawler->filter('tr')->each(function($node) {
                            //        print " - <br>";
                            //    });
                        }
                   }

                    $azz = array(
                        "witel"         => $value->witel, 
                        "href"          => $value->href,
                        "data_ran"      => $value->data_ran,
                        "ifname"        => $ifname,
                        "in_max"        => $in0, 
                        "in_avg"        => $in1, 
                        "in_current"    => $in2,
                        "out_max"       => $out0, 
                        "out_avg"       => $out1,  
                        "out_current"   => $out2, 
                    );
                    DataRandomOcc::create($azz);    
                } else {
                    $azz = array(
                        "witel"         => $value->region, 
                        "href"          => $value->href,
                        "data_ran"      => $value->data_ran,
                        "ifname"        => " - ",
                        "in_max"        => " - ", 
                        "in_avg"        => " - ", 
                        "in_current"    => " - ",
                        "out_max"       => " - ", 
                        "out_avg"       => " - ",  
                        "out_current"   => " - ", 
                    );
                    DataRandomOcc::create($azz);    
                }
                
            } else {
                $client = new Client();
                $href = str_replace("../", "", $value->href);
                $crawler = $client->request('GET', "http://10.1.18.33/".$href);
                if ($crawler->filter('.graph > h2 ')->eq(0)->count() > 0) {
                    $h2 = $crawler->filter('.graph > h2')->eq(0)->text();
                    $in0 = $crawler->filter('.graph > table > .in > td')->eq(0)->text();
                    $in1 = $crawler->filter('.graph > table > .in > td')->eq(1)->text();
                    $in2 = $crawler->filter('.graph > table > .in > td')->eq(2)->text();

                    $out0 = $crawler->filter('.graph > table > .out > td')->eq(0)->text();
                    $out1 = $crawler->filter('.graph > table > .out > td')->eq(1)->text();
                    $out2 = $crawler->filter('.graph > table > .out > td')->eq(2)->text();

                    $ifnames    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")')->each(function($node) {
                        return $node->text()." <br>";
                    });
                    $descript   = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")')->each(function($node) {
                        return $node->text()." <br>";
                    });
                    $ifnamecount = count($ifnames);
                    $descriptcount = count($descript);
                    if ( $ifnamecount > 0) {
                        $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")+td')->eq(0)->text();
                        // $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("ifName:")+td')->each(function($node) {
                        //     // return $node->text();
                        //     print $node->text()."<br>";
                        // });
                    } else {
                        if ($descriptcount > 0) {
                               $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")+td')->eq(0)->text();
                            //    $ifname    = $crawler->filter('#sysdetails > table > tr > td:contains("Description:")+td')->each(function($node) {
                            //     print $node->text()."<br>";
                            // });
                           } else {
                               $ifname = "-";
                            //    $ifname    = $crawler->filter('tr')->each(function($node) {
                            //        print " - <br>";
                            //    });
                        }
                   }
   
                    $azz = array(
                        "witel"         => $value->witel, 
                        "href"          => $value->href,
                        "data_ran"      => $value->data_ran,
                        "ifname"        => $ifname,
                        "in_max"        => $in0, 
                        "in_avg"        => $in1, 
                        "in_current"    => $in2,
                        "out_max"       => $out0, 
                        "out_avg"       => $out1,  
                        "out_current"   => $out2, 
                    );
                    DataRandomOcc::create($azz);    
                } else {
                    $azz = array(
                        "witel"         => $value->region, 
                        "href"          => $value->href,
                        "data_ran"      => $value->data_ran,
                        "ifname"        => " - ",
                        "in_max"        => " - ", 
                        "in_avg"        => " - ", 
                        "in_current"    => " - ",
                        "out_max"       => " - ", 
                        "out_avg"       => " - ",  
                        "out_current"   => " - ", 
                    );
                    DataRandomOcc::create($azz);    

                }
            }
   
       }


    }


    public function getUtilScrapRan() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;

        $this->getUtilScrapRanX();
        // $this->getHrefDataRandom();

        $time = microtime();
        $time = explode(' ',$time);
        $time = $time[1]+$time[0];
        $finish = $time;
        $total = ($finish - $start)/60;
        echo "<h1>".DataRandom::count()." Data Random </h1><br>";
        echo "<h1>".DataRandomOcc::count()." Data Random OCC </h1><br>";
        echo "<h1> Waktu Eksekusi $total Menit</h1>";
 
    }
}
