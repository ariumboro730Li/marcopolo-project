<?php

namespace App\Http\Controllers;

use App\RCADetail;
use Illuminate\Http\Request;

class RCADetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rcadetails = RCADetail::all();
        return view('index')->with('rcadetails', $rcadetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rcadetail = new RCADetail();
        $rcadetail->idrca = $request->idrca;
        $rcadetail->improvement_activity = $request->improvement_activity;
        $rcadetail->status = $request->status;
        $rcadetail->tgl_target = $request->tgl_target;
        $rcadetail->pic = $request->pic;
        $rcadetail->save();
        $rcadetail->iddetail = $rcadetail->id ;
        $datum = RCADetail::where('created_at',$rcadetail->created_at)->first();
        return response()->json($datum);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($detail_id)
    {
        $rcadetail = RCADetail::find($detail_id);
        return response()->json($rcadetail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $detail_id)
    {
        $rcadetail = RCADetail::find($detail_id);
        $rcadetail->improvement_activity = $request->improvement_activity;
        $rcadetail->status = $request->status;
        $rcadetail->tgl_target = $request->tgl_target;
        $rcadetail->pic = $request->pic;
        $rcadetail->save();
        return response()->json($rcadetail);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($detail_id)
    {
        $rcadetail = RCADetail::destroy($detail_id);
        return response()->json($rcadetail);
    }
}
