<?php

namespace App\Http\Controllers\AccessDirectController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AccessDirectMetro;


class ApiAccessDirectController extends Controller
{

        public static function apiDirectMetroReg1($val)
        {

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_PORT => "8581",
            CURLOPT_URL  => "http://10.62.166.182:8581/odata/api/interfaces?resolution=HOUR&period=1d&tz=07&$"."top=20000&$"."skip=0&top=100&$"."apply=groupby%28portmfs/ID,%20aggregate%28portmfs%28max_im_Utilization%20with%20max%20as%20Value%29%29%29&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,Description,SpeedIn,portmfs/max_im_Utilization&$"."filter=%28%28substringof%28tolower%28%27metro:metro%20divre%20$val%27%29,%20tolower%28groups/GroupPathLocation%29%29%20eq%20true%29%20and%20%28%28substringof%28tolower%28%27AKSES_SITE%27%29,%20tolower%28Description%29%29%20eq%20true%29%20or%20%28substringof%28tolower%28%27NODE-B%27%29,%20tolower%28Description%29%29%20eq%20true%29%29%29",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: /",
                "Accept-Encoding: gzip, deflate",
                // "Authorization: Basic OTQwMTU2Ok5vNTQyMzY3Njs=",
                "Authorization: Basic OTcwMDE2OnRlbGtvbTEyMw==",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Cookie: JSESSIONID=1ulhpxl7n06ii1c9jyrxgz78wl",
                "Host: 10.62.166.182:8581",
                "Postman-Token: 36f41ea5-7619-47e3-8207-03dffdcd22a2,2e741d0d-67c0-4fde-abb5-b022d674322d",
                "User-Agent: PostmanRuntime/7.15.2",
                "cache-control: no-cache"
            ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            return $response;
            
        }

}
