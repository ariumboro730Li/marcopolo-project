<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UtilAccess extends Model
{
    protected $table = 'util_access';
}
