<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataRandomRegion extends Model
{
    protected $table = 'data_random_region';
	protected $primaryKey = 'id';

    protected $fillable = [
        'witel', 'region', 'treg', 'href'
    ];
}
