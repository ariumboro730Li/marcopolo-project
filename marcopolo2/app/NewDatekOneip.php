<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewDatekOneip extends Model
{
    use SoftDeletes;
    protected $table = 'new_datek_oneip';
	protected $primaryKey = 'id';

    protected $fillable = [
        'reg_tsel', 'kota_tsel', 'routername_tsel', 'port_tsel', 'reg_telkom', 'sto_telkom', 'pe_telkom', 'port_telkom', 'description', 'capacity', 'utilization_cap', 'utilization_in', 'utilization_out','utilization_max','layer','system','transport_type','keterangan','date_detected','date_undetected','deleted_at','created_at','updated_at'
    ];

    public static function Link($regional)
    {
        return NewDatekOneip::where('reg_telkom',$regional)->whereNull('date_undetected')->count();
    }
    public static function LinkWithUndetected($regional)
    {
        return NewDatekOneip::where('reg_telkom',$regional)->count();
    }
    public static function Capacity($regional)
    {
        return NewDatekOneip::where('reg_telkom',$regional)->whereNull('date_undetected')->sum('capacity');
    }
    public static function CapacityWithUndetected($regional)
    {
        return NewDatekOneip::where('reg_telkom',$regional)->sum('capacity');
    }
    public static function AllLink()
    {
        return NewDatekOneip::whereNull('date_undetected')->count();
    }
    public static function AllLinkWithUndetected()
    {
        return NewDatekOneip::all()->count();
    }
    public static function AllCapacity()
    {
        return NewDatekOneip::whereNull('date_undetected')->sum('capacity');
    }
    public static function AllCapacityWithUndetected()
    {
        return NewDatekOneip::all()->sum('capacity');
    }
    public static function Data($id)
    {
        $backhaul = NewDatekOneip::find($id);
        return $backhaul;
    }

    public static function AllUtilization()
    {
        return NewDatekOneip::whereNull('date_undetected')->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function AllUtilizationCap()
    {
        return NewDatekOneip::whereNull('date_undetected')->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_cap');
    }
    public static function Utilization($regional)
    {
        return NewDatekOneip::whereNull('date_undetected')->where('reg_telkom',$regional)->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function UtilizationCap($regional)
    {
        return NewDatekOneip::whereNull('date_undetected')->where('reg_telkom',$regional)
            ->whereNotNull('utilization_max')
            ->where('utilization_cap',"<",100)
            ->sum('utilization_cap');
    }
    public static function Utilization2()
    {
        return NewDatekOneip::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_max');
    }
    public static function UtilizationCap2()
    {
        return NewDatekOneip::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_cap');
    }
    public static function TransportSystem($system)
    {
        return NewDatekOneip::where('system',$system)->count();
    }
    public static function TransportType($system)
    {
        return NewDatekOneip::where('system',$system)->count();
    }

    public static function OverThreshold()
    {
        return NewDatekOneip::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->count();
    }

    public static function AllOverThreshold()
    {
        return NewDatekOneip::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->get();
    }

    public static function FiveOverThreshold()
    {
        return NewDatekOneip::whereNull('date_undetected')->where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'desc')->take(5)->get();
    }

}