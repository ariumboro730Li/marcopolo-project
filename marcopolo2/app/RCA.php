<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class RCA extends Model
{
    use SoftDeletes;
    protected $table = 'rca';
	protected $primaryKey = 'idrca';

    public function detail()
    {
        return $this->hasMany('App\RCADetail','idrca','idrca');
    }

    public static function getRCAData($idactivity)
    {
        return RCA::find($idactivity);
    }




    // TOTAL CRITICAL INCIDENT IN A YEAR

    public static function TotalIncident()
    {
        $data = RCA::all()->count();
        return $data;
    }

    public static function TotalRegionalPercentage($reg)
    {
        $data = RCA::all()->count();
        $regional = RCA::where('regional','Regional '.$reg)->count();
        //return 2*$regional;
        return round(($regional/$data)*100);
    }

    // ROOT CAUSE


    public static function RootCauseNEProblem()
    {
        $data = RCA::where('rc_category',"NE Problem (ISP)")->count();
        return $data;
    }

    public static function RootCauseTranmission()
    {
        $data = RCA::where('rc_category',"NE Problem (ISP)")->count();
        return $data;
    }

    public static function RootCausePower()
    {
        $data = RCA::where('rc_category',"Power")->count();
        return $data;
    }

    public static function RootCauseActivity()
    {
        $data = RCA::where('rc_category',"Activity")->count();
        return $data;
    }

    public static function RootCauseLogicalConfiguration()
    {
        $data = RCA::where('rc_category',"Logical Configuration")->count();
        return $data;
    }

    //MTTR INCIDENT


    public static function TotalRegionalDurationOver($reg)
    {
        $regional = RCA::where('regional','Regional '.$reg)->where('duration','>',4)->count();
        //return 2*$regional;
        return $regional;
    }

    public static function TotalRegionalDurationUnder($reg)
    {
        $regional = RCA::where('regional','Regional '.$reg)->where('duration','<=',4)->count();
        //return 2*$regional;
        return $regional;
    }

    //IMPROVEMENT ACTIVITY

    public static function TotalRegionalOpen($reg)
    {
        $regional = RCA::with('detail')->where('regional','Regional '.$reg)->whereHas('detail', function($q){
            $q->where('status', 'Open');
        })->count();
        //return 2*$regional;
        return $regional;
    }

    public static function TotalRegionalClose($reg)
    {
        $regional = RCA::with('detail')->where('regional','Regional '.$reg)->whereHas('detail', function($q){
            $q->where('status', 'Close');
        })->count();
        //return 2*$regional;
        return $regional;
    }


    // INCIDENT TREND

    public static function TotalIncidentMonth($month)
    {
        //$data = RCA::whereRaw('MONTH(date_time_open) = ?', [$month])->count();
        $data = RCA::where( DB::raw('MONTH(date_time_open)'), '=', $month )->count();
        return $data;
    }

    public static function TotalRegional($regional)
    {
        $data = RCA::all()->count();
        //return 2*$regional;
        return 20;
    }
}
