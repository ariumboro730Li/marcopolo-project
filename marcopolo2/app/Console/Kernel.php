<?php

namespace App\Console;

use App\Console\Commands\SynchronizeOpenAPI;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\BackupDatabase::class,
        Commands\FetchRegional1::class,
        Commands\FetchRegional2::class,
        Commands\FetchRegional3::class,
        Commands\FetchRegional4::class,
        Commands\FetchRegional5::class,
        Commands\FetchRegional6::class,
        Commands\FetchRegional7::class,
        Commands\GetBackhaul::class,
        Commands\GetBackhaulUtil::class,
        Commands\GetIpbb::class,
        Commands\GetIpran::class,
        Commands\GetIx::class,
        Commands\GetOneIPMPLS::class,
        Commands\GetSummary::class,
        Commands\GetUcap::class,
        Commands\GetUmax::class,
        Commands\GetUndetected::class,
        Commands\GetUtilAccess::class,
        Commands\SynchronizeAccess::class,
        Commands\SynchronizeBackhaul::class,
        Commands\SynchronizeIPRAN::class,
        Commands\SynchronizeIPBB::class,
        Commands\SynchronizeIX::class,
        SynchronizeOpenAPI::class
    ];

    /** 
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    // $schedule->command('DirectAccess:get')->everyMinute();
    // $schedule->command('Backhaul:Get')->everyMinute();
    // $schedule->command('BackhaulUtil:Get')->everyMinute();
    $schedule->command('Ix:Get')->everyMinute();
    $schedule->command('Synchronize:IX')->everyMinute();
    $schedule->command('OneIPMPLS:Get')->everyMinute();



    // //        $schedule->command('database:backup')->everyMinute();
    //         $schedule->command('summary:get')->monthlyOn(1, '03:00');
    //         //$schedule->command('Backhaul:Get')->daily()->at('23:20');
    //         $schedule->command('OneIPMPLS:Get')->daily()->at('03:20');
    //         // $schedule->command('BackhaulUtil:Get')->daily()->at('03:50');
    // $schedule->command('Backhaul:Get')->daily()->at('03:20');
//         $schedule->command('Ipbb:Get')->daily()->at('03:20');
//         $schedule->command('Ipran:Get')->daily()->at('03:20');
        // $schedule->command('Ix:Get')->daily()->at('03:20');
//         $schedule->command('Umax:Get')->daily()->at('03:50');
//         $schedule->command('Ucap:Get')->daily()->at('03:50');
//         $schedule->command('Undetected:Get')->daily()->at('03:50');
//         $schedule->command('UtilAccess:Get')->daily()->at('03:50');
//         $schedule->command('Regional1:Fetch')->daily()->at('03:50');
//         $schedule->command('Regional2:Fetch')->daily()->at('03:50');
//         $schedule->command('Regional3:Fetch')->daily()->at('03:50');
//         $schedule->command('Regional4:Fetch')->daily()->at('03:50');
//         $schedule->command('Regional5:Fetch')->daily()->at('03:50');
//         $schedule->command('Regional6:Fetch')->daily()->at('03:50');
//         $schedule->command('Regional7:Fetch')->daily()->at('03:50');
//         $schedule->command('Synchronize:Access')->daily()->at('03:50');
//         $schedule->command('Synchronize:Backhaul')->everyMinute();
//         // $schedule->command('Synchronize:Backhaul')->daily()->at('03:50');
//         $schedule->command('Synchronize:IPRAN')->daily()->at('03:50');
//         $schedule->command('Synchronize:IPBB')->daily()->at('03:50');
        // $schedule->command('Synchronize:IX')->daily()->at('03:50');
//         $schedule->command('Synchronize:OpenAPI')->daily()->at('03:10');


//        $schedule->command('summary:get')->everyFiveMinutes();
//        //$schedule->command('Backhaul:Get')->daily()->at('23:20');
//        $schedule->command('Backhaul:Get')->everyFiveMinutes();
//        $schedule->command('BackhaulUtil:Get')->everyFiveMinutes();
//        $schedule->command('Ipbb:Get')->everyFiveMinutes();
//        $schedule->command('Ipran:Get')->everyMinute();
//        $schedule->command('Ix:Get')->everyMinute();
//        $schedule->command('Undetected:Get')->everyFiveMinutes();
//        $schedule->command('UtilAccess:Get')->everyFiveMinutes();
//        $schedule->command('Regional1:Fetch')->everyFiveMinutes();
//        $schedule->command('Regional2:Fetch')->everyFiveMinutes();
//        $schedule->command('Regional3:Fetch')->everyFiveMinutes();
//        $schedule->command('Regional4:Fetch')->everyFiveMinutes();
//        $schedule->command('Regional5:Fetch')->everyFiveMinutes();
//        $schedule->command('Regional6:Fetch')->everyFiveMinutes();
//        $schedule->command('Regional7:Fetch')->everyFiveMinutes();
//        $schedule->command('Synchronize:Access')->everyFiveMinutes();
//        $schedule->command('Synchronize:Backhaul')->everyFiveMinutes();
//        $schedule->command('Synchronize:IPRAN')->everyFiveMinutes();
//        $schedule->command('Synchronize:IPBB')->everyFiveMinutes();
//        $schedule->command('Synchronize:IX')->everyFiveMinutes();
//        $schedule->command('Synchronize:OpenAPI')->everyFiveMinutes();
//        $schedule->command('Umax:Get')->everyFiveMinutes();
//        $schedule->command('Ucap:Get')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
