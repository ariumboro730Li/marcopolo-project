<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekBackhaul;
use App\OpenAPI;
use Illuminate\Console\Command;

class SynchronizeBackhaul extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Synchronize:Backhaul';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
                // Backhaul
        $data = DatekBackhaul::whereNotNull('metroe_telkom')->get();
        foreach ($data as $datum) {
            $datum['port_telkom'] = str_replace("Ex","",$datum['port_telkom']);
            $datum['port_telkom'] = str_replace("Ag","lag-",$datum['port_telkom']);
            $datum['port_telkom'] = str_replace("lag-","Eth-Trunk",$datum['port_telkom']);
            $datum['port_telkom'] = str_replace("Gi","GigabitEthernet",$datum['port_telkom']);
//            echo $datum['metroe_telkom']." ".$datum['port_telkom']." ";
            $openapi = OpenAPI::where('name', $datum['metroe_telkom'])
                ->where('port', 'like', '%' .$datum['port_telkom']. '%')
                ->first();
            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
                $datum['utilization_max'] = $openapi['utilization'];
                $datum->save();
            }
//            echo "<br>";
        }


        $command = new CommandHistory();
        $command->name = "Synchronize Backhaul";
        $command->type = "SYNCHRONIZE";
        $command->save();
    }
}
