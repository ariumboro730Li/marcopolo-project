<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekIpran;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetIpran extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Ipran:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Create a stream
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            ]
        ];

        $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
        //$file = file_get_contents('http://www.example.com/', false, $context);


        $jsonurl = "http://10.62.164.166/api/jovice/get/ipran";
        $json = file_get_contents($jsonurl, false, $context);
        $bitcoin = json_decode($json);
        //return response()->json($bitcoin);
        $f=0;
        foreach (json_decode($json, true) as $area)
        {
            //$area['capacity'] = str_replace("G","",$area['capacity']);
            $f++;
            echo $f." \n";
//            print_r($area); // this is your area from json response



            $area['portfarend'] = str_replace("Ex","",$area['portfarend']);
            $area['portfarend'] = str_replace("Ag","",$area['portfarend']);
            $area['portfarend'] = str_replace("Gi.","",$area['portfarend']);
            $area['portfarend'] = str_replace("Gi","",$area['portfarend']);
            $area['portnearend'] = str_replace("Ex","",$area['portnearend']);
            $area['portnearend'] = str_replace("Ag","",$area['portnearend']);
            $area['portnearend'] = str_replace("Gi","",$area['portnearend']);
            $area['portnearend'] = str_replace("Gi.","",$area['portnearend']);


            $ipran = DatekIpran::where('vcid',  $area['vcid'])->get();
//            $ipran = DatekIpran::where('vcid',  $area['vcid'])
//                ->where('metroe_end1_telkom',  $area['nearend'])
//                ->where('metroe_end2_telkom',  $area['farend'])->first();
            if (count($ipran)>0)
            {

                $found=0;
                foreach ($ipran as $datum)
                {
                    echo $datum['metroe_end1_telkom'];
                    if(($area['nearend']==$datum['metroe_end1_telkom']||$area['nearend']==$datum['metroe_end2_telkom'])
                        && ($area['farend']==$datum['metroe_end1_telkom']||$area['farend']==$datum['metroe_end2_telkom'])
                    )
                    {
                        $found=1;
                        $datum->status = $area['status'];
                        $datum->capacity = $area['capacity'];
                        $datum->date_undetected = null;
                        $datum->tglcheck=Carbon::now();
                        $datum->description = $area['description'];
                        //$ipran->date_detected = date("Y-m-d");
                        $datum->save();
                    }
                }

                if($found==0)
                {
                    echo $area['nearend'];
                    $dataipran = new DatekIpran();
                    $dataipran->vcid = $area['vcid'];
                    $dataipran->reg_tsel = $area['reg'];
                    $dataipran->reg_telkom = $area['reg'];
                    $dataipran->router_end1_tsel = $area['router_end1_tsel'];
                    $dataipran->metroe_end1_telkom = $area['nearend'];
                    $dataipran->port_end1_tsel = $area['port_end1_tsel'];
                    $dataipran->port_end1_telkom = $area['portnearend'];
                    $dataipran->router_end2_tsel = $area['router_end2_tsel'];
                    $dataipran->metroe_end2_telkom = $area['farend'];
                    $dataipran->port_end2_tsel = $area['port_end2_tsel'];
                    $dataipran->port_end2_telkom = $area['portfarend'];
                    $dataipran->status = $area['status'];
                    $dataipran->capacity = $area['capacity'];
                    $dataipran->description = $area['description'];
                    $dataipran->layer = "IPRAN";
                    $dataipran->system = "METRO-E";
                    $dataipran->date_detected = date("Y-m-d");
                    $dataipran->save();
                }
            }
            else
            {
                echo $area['nearend'];
                $ipran = new DatekIpran();
                $ipran->vcid = $area['vcid'];
                $ipran->reg_tsel = $area['reg'];
                $ipran->reg_telkom = $area['reg'];
                $ipran->router_end1_tsel = $area['router_end1_tsel'];
                $ipran->metroe_end1_telkom = $area['nearend'];
                $ipran->port_end1_tsel = $area['port_end1_tsel'];
                $ipran->port_end1_telkom = $area['portnearend'];
                $ipran->router_end2_tsel = $area['router_end2_tsel'];
                $ipran->metroe_end2_telkom = $area['farend'];
                $ipran->port_end2_tsel = $area['port_end2_tsel'];
                $ipran->port_end2_telkom = $area['portfarend'];
                $ipran->status = $area['status'];
                $ipran->capacity = $area['capacity'];
                $ipran->description = $area['description'];
                $ipran->layer = "IPRAN";
                $ipran->system = "METRO-E";
                $ipran->date_detected = date("Y-m-d");
                $ipran->save();
            }

            echo "<br>";
        }

        $command = new CommandHistory();
        $command->name = "Get IPRAN";
        $command->type = "GET";
        $command->save();
    }
}
