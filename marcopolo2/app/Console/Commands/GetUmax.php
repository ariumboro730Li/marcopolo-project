<?php

namespace App\Console\Commands;

use App\CommandHistory;
use Illuminate\Console\Command;
use App\DatekAccess;
use App\DatekBackhaul;
use App\DatekIpbb;
use App\DatekIpran;
use App\DatekIx;
use App\DatekSummary;
use Carbon\Carbon;

class GetUmax extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Umax:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $access = DatekAccess::all();
        foreach ($access as $data)
        {

            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
        }

        $backhaul = DatekBackhaul::all();
        foreach ($backhaul as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
        }

        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_max'];
                $data->save();
            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
        }

        $ipbb = DatekIpbb::all();
        foreach ($ipbb as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }
        }

        $ix = DatekIx::all();
        foreach ($ix as $data)
        {
            if($data['utilization_max']>$data['utilization_in'] && $data['utilization_max']>$data['utilization_out'])
            {

            }
            else if($data['utilization_in']>$data['utilization_out'])
            {
                $data['utilization_max']=$data['utilization_in'];
                $data->save();
            }
            else
            {
                $data['utilization_max']=$data['utilization_out'];
                $data->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Calculate Utilization Max";
        $command->type = "CALCULATE";
        $command->save();
    }
}
