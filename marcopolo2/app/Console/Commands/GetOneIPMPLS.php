<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetOneIPMPLS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OneIPMPLS:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $ldap_username = Setting::where('key','ldap_id')->first()->value;
        $ldap_password = Setting::where('key','ldap_password')->first()->value;

        // REGIONAL1
        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R1'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => [$ldap_username,$ldap_password]
        ]);


        $area = json_decode($res->getBody(), true);
        foreach ($area['d']['results'] as $data) {
            if ((strpos($data['Name'], '.') === false) )
            {
                if(( strpos($data['Name'], "Cisco") === false))
                {
                    $openapi = DatekOneIPMPLS::where('reg_telkom',1)
                        ->where('metroe_telkom',$data['device']['Name'])
                        ->where('port_telkom',$data['Name'])->first();
                    if(isset($openapi))
                    {
                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
                        $openapi->save();
                    }
                    else
                    {
                        $openapi = new DatekOneIPMPLS();
                        $openapi['metroe_telkom']=$data['device']['Name'];
                        $openapi['port_telkom']=$data['Name'];
                        $openapi['reg_telkom']=1;
                        $openapi['description']=$data['Description'];
                        $parts = explode('E', $data['SpeedIn']);
                        if(isset($parts[1])) {
                            $parts[0]=str_replace(".","",$parts[0]);
                            if ($parts[1] == 10) {
                                $openapi['capacity'] = $parts[0] * 1;
                            } else if ($parts[1] == 11) {
                                $openapi['capacity'] = $parts[0] * 10;
                            } else {
                                $openapi['capacity'] = $parts[0]/10;
                            }
                        }
                        else
                        {
//                            $parts = explode('.', $data['SpeedIn']);
                            $openapi['capacity'] = $parts[0];
                        }
                        $openapi['layer']="OneIPMPLS";
                        $openapi['transport_type']="DWDM";
                        if($data['OperStatus']==1){
                            $openapi['status']="UP";
                        }
                        else
                        {$openapi['status']="DOWN";}
                        $openapi['utilization_max']=(end($data['portmfs']['results'])['Value']);
                        $openapi['date_detected']=Carbon::now();
                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
                        $openapi->save();
                    }
                }
            }
        }

        echo "DONE Fetch OneIPMPLS Regional 1";

        $command = new CommandHistory();
        $command->name = "Fetch OneIPMPLS Regional 1";
        $command->type = "FETCH";
        $command->save();

//        // REGIONAL2
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R2'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',2)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=2;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 2";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 2";
//        $command->type = "FETCH";
//        $command->save();
//
//        // REGIONAL3
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R3'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',3)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=3;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 3";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 3";
//        $command->type = "FETCH";
//        $command->save();


//        // REGIONAL4
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R4'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('ae'), tolower(Name)) eq true)) and (OperStatus ne 2) and (length(tolower(Description)) ne 0))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',4)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=4;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 4";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 4";
//        $command->type = "FETCH";
//        $command->save();
//


//        // REGIONAL5
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."top=20000&$"."skip=0&top=100&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R5'), tolower(groups/GroupPathLocation)) eq true) and ((substringof(tolower('ae'), tolower(Name)) eq true)) and (OperStatus ne 2) and (length(tolower(Description)) ne 0))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',5)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=5;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//                            $parts[0]=str_replace(".","",$parts[0]);
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 1;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else {
//                                $openapi['capacity'] = $parts[0]/10;
//                            }
//                        }
//                        else
//                        {
////                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 5";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 5";
//        $command->type = "FETCH";
//        $command->save();


//        // REGIONAL6
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R6'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',6)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=6;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('.0E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//
//
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 100;
//                            } else {
//                                $openapi['capacity'] = $parts[0];
//                            }
//                        }
//                        else
//                        {
//                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 6";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 6";
//        $command->type = "FETCH";
//        $command->save();
////
//        // REGIONAL7
//        $url = "http://10.62.166.182:8581/odata/api/interfaces?$"."top=20000&$"."skip=0&top=100&$"."apply=groupby(portmfs/ID, aggregate(portmfs(im_Utilization with max as Value)))&&resolution=RATE&period=1d&bh=Sun-Sat 19:00-23:00&tz=07&$"."format=json&$"."expand=device,portmfs&$"."select=device/Name,Name,SpeedIn,Description,OperStatus,portmfs/im_Utilization&$"."filter=((substringof(tolower('CNOP R7'), tolower(groups/GroupPathLocation)) eq true) and (substringof(tolower('Bundle-Ether'), tolower(Name)) eq true))";
//        $client = new Client();
//        $res = $client->request('GET', $url, [
//            'auth' => [$ldap_username,$ldap_password]
//        ]);
//
//
//        $area = json_decode($res->getBody(), true);
//        foreach ($area['d']['results'] as $data) {
//            if ((strpos($data['Name'], '.') === false) )
//            {
//                if(( strpos($data['Name'], "Cisco") === false))
//                {
//                    $openapi = DatekOneIPMPLS::where('reg_telkom',7)
//                        ->where('metroe_telkom',$data['device']['Name'])
//                        ->where('port_telkom',$data['Name'])->first();
//                    if(isset($openapi))
//                    {
//                        $openapi['utilization_max'] = ((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']
//                            ." ".(end($data['portmfs']['results'])['Value'])."<br>";
//                        $openapi->save();
//                    }
//                    else
//                    {
//                        $openapi = new DatekOneIPMPLS();
//                        $openapi['metroe_telkom']=$data['device']['Name'];
//                        $openapi['port_telkom']=$data['Name'];
//                        $openapi['reg_telkom']=7;
//                        $openapi['description']=$data['Description'];
//                        $parts = explode('.0E', $data['SpeedIn']);
//                        if(isset($parts[1])) {
//
//
//                            if ($parts[1] == 10) {
//                                $openapi['capacity'] = $parts[0] * 10;
//                            } else if ($parts[1] == 11) {
//                                $openapi['capacity'] = $parts[0] * 100;
//                            } else {
//                                $openapi['capacity'] = $parts[0];
//                            }
//                        }
//                        else
//                        {
//                            $parts = explode('.', $data['SpeedIn']);
//                            $openapi['capacity'] = $parts[0];
//                        }
//                        $openapi['layer']="OneIPMPLS";
//                        $openapi['transport_type']="DWDM";
//                        if($data['OperStatus']==1){
//                            $openapi['status']="UP";
//                        }
//                        else
//                        {$openapi['status']="DOWN";}
//                        $openapi['utilization_max']=((end($data['portmfs']['results'])['Value'])/100)*$openapi['capacity'];
//                        $openapi['date_detected']=Carbon::now();
//                        echo $openapi['metroe_telkom']." ".$openapi['utilization_max']."<br>";
//                        $openapi->save();
//                    }
//                }
//            }
//        }
//
//        echo "DONE Fetch OneIPMPLS Regional 7";
//
//        $command = new CommandHistory();
//        $command->name = "Fetch OneIPMPLS Regional 7";
//        $command->type = "FETCH";
//        $command->save();
    }
}
