<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekAccess;
use App\DatekBackhaul;
use App\DatekIpbb;
use App\DatekIpran;
use App\DatekIx;
use App\DatekSummary;
use Illuminate\Console\Command;

class GetUcap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Ucap:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $access = DatekAccess::all();
        foreach ($access as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null && $data['utilization_max']<=100 )
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity']*0.01;
                $data->save();
            }
        }

        $backhaul = DatekBackhaul::all();
        foreach ($backhaul as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null && $data['utilization_max']<=100 )
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity']*0.01;
                $data->save();
            }
        }

        $ipran = DatekIpran::all();
        foreach ($ipran as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null && $data['utilization_max']<=100 )
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity']*0.01;
                $data->save();
            }
        }

        $ipbb = DatekIpbb::all();
        foreach ($ipbb as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null && $data['utilization_max']<=100 )
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity']*0.01;
                $data->save();
            }
        }

        $ix = DatekIx::all();
        foreach ($ix as $data)
        {
            if($data['utilization_max']!=null && $data['capacity'] != null && $data['utilization_max']<=100 )
            {

                $data['utilization_cap']=$data['utilization_max']*$data['capacity']*0.01;
                $data->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Calculate Utilization Capacity";
        $command->type = "CALCULATE";
        $command->save();
    }
}
