<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekBackhaul;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetBackhaul extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Backhaul:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jsonurl = "http://10.62.164.166/api/jovice/get/backhaulnew";
        $json = file_get_contents($jsonurl);
        $bitcoin = json_decode($json);
        //return response()->json($bitcoin);
        foreach (json_decode($json, true) as $area)
        {
            $area['capacity'] = str_replace("G","",$area['capacity']);
            $area['mi_name'] = str_replace("Ex","",$area['mi_name']);
            $area['mi_name'] = str_replace("Ag","",$area['mi_name']);
            $area['mi_name'] = str_replace("lag-","",$area['mi_name']);
            $area['mi_name'] = str_replace("Gi","",$area['mi_name']);
            //print_r($area); // this is your area from json response
            $backhaul = DatekBackhaul::where('metroe_telkom',$area['no_name'])->where('port_telkom',$area['mi_name'])->first();
            if (isset($backhaul))
            {
                $backhaul->routername_tsel = $area['routername_tsel'];
                $backhaul->port_tsel = $area['port_tsel'];
                $backhaul->description = $area['mi_description'];
                $backhaul->status = $area['status'];
                $backhaul->capacity = $area['capacity'];
                $backhaul->date_undetected = null;
                $backhaul->tglcheck=Carbon::now();
                //$backhaul->date_detected = date("Y-m-d");
                $backhaul->save();
            }
            else
            {
                $backhaul = new DatekBackhaul();
                $backhaul->reg_tsel = $area['regional'];
                $backhaul->reg_telkom = $area['regional'];
                $backhaul->routername_tsel = $area['routername_tsel'];
                $backhaul->metroe_telkom = $area['no_name'];
                $backhaul->port_tsel = $area['port_tsel'];
                $backhaul->port_telkom = $area['mi_name'];
                $backhaul->status = $area['status'];
                $backhaul->capacity = $area['capacity'];
                $backhaul->layer = "BACKHAUL";
                $backhaul->system = "METRO-E";
                $backhaul->date_detected = date("Y-m-d");
                $backhaul->description = $area['mi_description'];
                $backhaul->tglcheck=Carbon::now();
                $backhaul->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Get Backhaul";
        $command->type = "GET";
        $command->save();
    }
}
