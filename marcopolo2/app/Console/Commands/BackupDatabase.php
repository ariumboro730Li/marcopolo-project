<?php

namespace App\Console\Commands;

use App\CRQ;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic Backup Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crq = new CRQ();
        $crq->tgl_pengajuan = date('Y-m-d H:i:s');
        $crq->activity = "aaaaaaa";
        $crq->lokasi = "aaaaaaa";
        $crq->tgl_mulai = date('Y-m-d H:i:s');
        $crq->jam_mulai = date('H:i:s');
        $crq->tgl_selesai = date('Y-m-d H:i:s');
        $crq->jam_selesai = date('H:i:s');
        $crq->durasi = "aaaaaaa";
        $crq->impact_service = "aaaaaaa";
        $crq->pic ="aaaaaaa";
        $crq->save();
    }
}
