<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekAccess;
use App\UtilAccess;
use Illuminate\Console\Command;

class GetUtilAccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UtilAccess:Get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $access = DatekAccess::all();
        foreach ($access as $data)
        {

            if(isset($data['me_dm_telkom'])&&isset($data['port_dm_telkom']))
            {
                $utilaccess = UtilAccess::where('device_name',$data['me_dm_telkom'])->where('port',$data['port_dm_telkom'])->first();
                $data['utilization_in']=$utilaccess['util-in'];
                $data['utilization_out']=$utilaccess['util-out'];
                $data->save();
            }

        }

        $command = new CommandHistory();
        $command->name = "Get Utilization Access";
        $command->type = "GET";
        $command->save();

    }
}
