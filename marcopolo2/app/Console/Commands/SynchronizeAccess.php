<?php

namespace App\Console\Commands;

use App\CommandHistory;
use App\DatekAccess;
use App\OpenAPI;
use Illuminate\Console\Command;

class SynchronizeAccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Synchronize:Access';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Access
        $data = DatekAccess::whereNotNull('me_dm_telkom')->get();
        foreach ($data as $datum) {
//            $datum['port_end2_telkom'] = str_replace("Ex","",$datum['port_end2_telkom']);
//            $datum['port_end2_telkom'] = str_replace("Ag","lag-",$datum['port_end2_telkom']);
//            echo $datum['me_dm_telkom']." ".$datum['port_dm_telkom']." ";
            $openapi = OpenAPI::where('name', $datum['me_dm_telkom'])
                ->where('port','like', '%'.$datum['port_dm_telkom'].'%')
                ->first();
            if (isset($openapi)) {
//                echo $openapi['utilization']." ";
                $datum['utilization_max'] = $openapi['utilization'];
                $datum->save();
            }
//            echo "<br>";
        }

        $command = new CommandHistory();
        $command->name = "Synchronize Access";
        $command->type = "SYNCHRONIZE";
        $command->save();
    }
}
