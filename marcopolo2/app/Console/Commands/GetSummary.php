<?php

namespace App\Console\Commands;

use App\CommandHistory;
use Illuminate\Console\Command;
use App\DatekAccess;
use App\DatekBackhaul;
use App\DatekIpbb;
use App\DatekIpran;
use App\DatekIx;
use App\DatekSummary;
use Carbon\Carbon;

class GetSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'summary:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n")-1)
                ->where('year',date("Y"))
                ->where('Layer',"Access")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekAccess::Link($x);
                $data->capacity = DatekAccess::Capacity($x)*0.001;
                $data->utilization = DatekAccess::UtilizationCap($x)*0.001;
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n")-1;
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "Access";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekAccess::Link($x);
                $summary->capacity = DatekAccess::Capacity($x);
                $summary->utilization = DatekAccess::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n")-1)
                ->where('year',date("Y"))
                ->where('Layer',"Backhaul")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekBackhaul::Link($x);
                $data->capacity = DatekBackhaul::Capacity($x);
                $data->utilization = DatekBackhaul::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n")-1;
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "Backhaul";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekBackhaul::Link($x);
                $summary->capacity = DatekBackhaul::Capacity($x);
                $summary->utilization = DatekBackhaul::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n")-1)
                ->where('year',date("Y"))
                ->where('Layer',"IPRAN")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekIpran::Link($x);
                $data->capacity = DatekIpran::Capacity($x);
                $data->utilization = DatekIpran::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n")-1;
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "IPRAN";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekIpran::Link($x);
                $summary->capacity = DatekIpran::Capacity($x);
                $summary->utilization = DatekIpran::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n")-1)
                ->where('year',date("Y"))
                ->where('Layer',"IPBB")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekIpbb::Link($x);
                $data->capacity = DatekIpbb::Capacity($x);
                $data->utilization = DatekIpbb::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n")-1;
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "IPBB";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekIpbb::Link($x);
                $summary->capacity = DatekIpbb::Capacity($x);
                $summary->utilization = DatekIpbb::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        for ($x = 1; $x <= 7; $x++) {
            $data = DatekSummary::where('month',date("n")-1)
                ->where('year',date("Y"))
                ->where('Layer',"IX")
                ->where('reg_telkom',$x)
                ->first();
            if (isset($data))
            {
                $data->link = DatekIx::Link($x);
                $data->capacity = DatekIx::Capacity($x);
                $data->utilization = DatekIx::UtilizationCap($x);
                $data->utilization = round($data->utilization);
                //$ipran->date_detected = date("Y-m-d");
                $data->save();
            }
            else {
                $summary = new DatekSummary();
                $summary->month = date("n")-1;
                $summary->year = date("Y");
                $summary->reg_telkom = $x;
                $summary->Layer = "IX";
                $summary->transport_system = "";
                $summary->transport_type = "";
                $summary->link = DatekIx::Link($x);
                $summary->capacity = DatekIx::Capacity($x);
                $summary->utilization = DatekIx::UtilizationCap($x);
                $summary->utilization = round($summary->utilization);
                $summary->save();
            }
        }

        $command = new CommandHistory();
        $command->name = "Calculate Summary";
        $command->type = "CALCULATE";
        $command->save();
    }
}
