<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NewDatekIpbb extends Model
{
        use SoftDeletes;
        protected $table = 'new_datek_ipbb';
        protected $primaryKey = 'idipbb';
    
        protected $fillable = [
            'idipbb', 'link', 'reg_tsel_a', 'kota_router_node_a', 'router_node_a', 'port_tsel_end_a', 'reg_tsel_b', 'kota_router_node_b', 'router_node_b', 'port_tsel_end_b', 'reg_telkom_a', 'capacity', 'utilization_cap', 'utilization_in%', 'utilization_out%', 'utilization_max%', 'layer', 'system', 'transport_type' 
        ];
        
        public static function Link($regional)
        {
            return NewDatekIpbb::where('reg_tsel_a',$regional)->count();
        }
        public static function Capacity($regional)
        {
            return NewDatekIpbb::where('reg_tsel_a',$regional)->sum('capacity');
        }
        public static function UtilizationCap($regional)
        {
            return NewDatekIpbb::where('reg_tsel_a',$regional)->whereNotNull('utilization_max%')->sum('utilization_cap');
        }
        public static function AllLink()
        {
            return NewDatekIpbb::count();
        }
        public static function AllCapacity()
        {
            return NewDatekIpbb::sum('capacity');
        }    
        public static function AllLinkWithUndetected()
        {
            return NewDatekIpbb::all()->count();
        }
        public static function AllUtilizationCap()
        {
            return NewDatekIpbb::whereNotNull('utilization_max%')->sum('utilization_cap');
        }
    
        public static function GetRegion()
        {
            $regions = array(
                            array('reg'=>1, 'name'=>'SUMBAGUT', 'x'=>-4, 'y'=>1),
                            array('reg'=>2, 'name'=>'SUMBAGSEL', 'x'=>-2, 'y'=>4.7),
                            array('reg'=>3, 'name'=>'JABO', 'x'=>-1.4, 'y'=>5),
                            array('reg'=>4, 'name'=>'JABAR', 'x'=>-0.8, 'y'=>5.3),
                            array('reg'=>5, 'name'=>'JATENG', 'x'=>0.3, 'y'=>5.5),
                            array('reg'=>6, 'name'=>'JATIM', 'x'=>1.3, 'y'=>5.3),
                            array('reg'=>7, 'name'=>'BALINUSRA', 'x'=>2.5, 'y'=>5.3),
                            array('reg'=>8, 'name'=>'KALIMANTAN', 'x'=>1, 'y'=>2.7),
                            array('reg'=>9, 'name'=>'SULAWESI', 'x'=>2.5, 'y'=>3),
                            array('reg'=>10, 'name'=>'SUMBAGTENG', 'x'=>-2.3, 'y'=>3.2),
                            array('reg'=>11, 'name'=>'PUMA', 'x'=>5.2, 'y'=>2.7)
                        );
            return $regions;
        }
    
        public static function GetTopNas()
        {
            return NewDatekIpbb::selectRaw('reg_tsel_a, reg_tsel_b, sum(capacity) as totcap')
                ->whereRaw('reg_tsel_a <> reg_tsel_b')
                ->groupBy('reg_tsel_a', 'reg_tsel_b')->get();
        }
    
}
