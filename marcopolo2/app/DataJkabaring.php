<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataJkabaring extends Model
{
    protected $table = 'data_jkbaring';
	protected $primaryKey = 'id';

    protected $fillable = [
        'witel', 'region', 'data_bb', 'href', 'treg'
    ];
}
