<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cobajuga extends Model
{
    use SoftDeletes;
    protected $table = 'tabel_test';
    protected $primaryKey = 'id';

    protected $fillable = [
        'site_id','treg','backhaul','mebackhaul', 'intbackhaul', 'vlanbackhaul', 'vcid', 'meakses', 'intakses','vlanakses', 'subaksesdescription', 'akses'              
    ];
}
