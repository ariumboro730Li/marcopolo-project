<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }
    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function authorizeRolesAccess($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRoleAccess($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRoleAccess($roles) ||
            abort(401, 'This action is unauthorized.');
    }
    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRoleAccess($roles)
    {
        return null !== $this->roles()->whereIn($roles,'on' )->first();
    }
    /**
     * Check one role
     * @param string $role
     */
    public function hasRoleAccess($roles)
    {
        return null !== $this->roles()->where($roles,'on')->first();
    }


    public function authorizeRolesPage($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRolePage($roles) ||
                false;
        }
        return $this->hasRolePage($roles) ||
            false;
    }
    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRolePage($roles)
    {
        return null !== $this->roles()->whereIn($roles,'on' )->first();
    }
    /**
     * Check one role
     * @param string $role
     */
    public function hasRolePage($roles)
    {
        return null !== $this->roles()->where($roles,'on')->first();
    }


    public static function getroles($page)
    {
        $user = User::find(Auth::user()->id)->with('roles')->first();

        return $user['roles'][0]->$page;
    }

    public static function getrole($id)
    {
        $user = User::with('roles')->where('id',$id)->get();

        //dd($user[0]['roles'][0]['name']);
        return $user[0]['roles'][0]['id'];
    }

    public static function addPush($token)
    {
        $user = User::find(Auth::user()->id)->first();
        $user['push'] = $token;
        $user->save();

        return true;
    }

}
