<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewDatekIx extends Model
{
    use SoftDeletes;
    protected $table = 'new_datek_ix';
    protected $primaryKey = 'idix';

    protected $fillable = [
        'idix', 'reg_tsel', 'kota_tsel', 'routername_tsel', 'port_tsel', 'reg_telkom', 'sto_telkom', 'pe_telkom', 'port_telkom', 'description', 'capacity','utilitzation_cap', 'utilitzation_in', 'utilitzation_out', 'utilitzation_max','layer','transport_type','keterangan', 'date_detected', 'date_undetected', 'status'
    ];

    public static function Link($regional)
    {
        return NewDatekIx::where('reg_tsel',$regional)->count();
    }
    public static function Capacity($regional)
    {
        return NewDatekIx::where('reg_tsel',$regional)->sum('capacity');
    }
    public static function AllLink()
    {
        return NewDatekIx::count();
    }
    public static function AllLinkWithUndetected()
    {
        return DatekIx::all()->count();
    }
    public static function AllCapacity()
    {
        return NewDatekIx::sum('capacity');
    }
    public static function utilizationCap($regional)
    {
        return NewDatekIx::where('reg_tsel',$regional)->whereNotNull('utilitzation_max')->sum('utilitzation_cap');
    }
    public static function AllutilizationCap()
    {
        return NewDatekIx::whereNotNull('utilitzation_max')->sum('utilitzation_cap');
    }




}
