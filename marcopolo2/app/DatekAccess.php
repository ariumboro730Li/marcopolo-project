<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatekAccess extends Model
{
    use SoftDeletes;
    protected $table = 'datek_access';
	protected $primaryKey = 'idaccess';
	// protected $primaryKey = 'reg_tsel';

    protected $fillable = [
        'reg_tsel','siteid_tsel','sitename_tsel','reg_telkom','sto_gpon_telkom','gpon_gpon_telkom','port_gpon_telkom','ne_radio_telkom','fe_radio_telkom','port_radio_telkom','sto_dm_telkom','me_dm_telkom','port_dm_telkom','description','capacity','system'
    ];

    public static function Linss($regional)
    {
        return DatekAccess::where('reg_telkom',$regional)->whereNull('date_undetected')->count();
    }
    public static function LinkWithUndetected($regional)
    {
        return DatekAccess::where('reg_telkom',$regional)->count();
    }
    public static function Capacity($regional)
    {
        return DatekAccess::where('reg_telkom',$regional)->whereNull('date_undetected')->sum('capacity');
    }
    public static function CapacityWithUndetected($regional)
    {
        return DatekAccess::where('reg_telkom',$regional)->sum('capacity');
    }
    public static function AllLink()
    {
        return DatekAccess::whereNull('date_undetected')->count();
    }
    public static function AllLinkWithUndetected()
    {
        return DatekAccess::all()->count();
    }
    public static function AllCapacity()
    {
        return DatekAccess::whereNull('date_undetected')->sum('capacity');
    }
    public static function AllCapacityWithUndetected()
    {
        return DatekAccess::all()->sum('capacity');
    }

    public static function AllUtilization()
    {
        return DatekAccess::whereNull('date_undetected')->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function AllUtilizationCap()
    {
        return DatekAccess::whereNull('date_undetected')->whereNotNull('utilization_max')->sum('utilization_cap');
    }
    public static function Utilization($regional)
    {
        return DatekAccess::whereNull('date_undetected')->where('reg_telkom',$regional)->whereNotNull('utilization_max')->where('utilization_max',"<=",100)->sum('utilization_max');
    }
    public static function UtilizationCap($regional)
    {
        return DatekAccess::whereNull('date_undetected')->where('reg_telkom',$regional)->whereNotNull('utilization_max')->sum('utilization_cap');
    }
    public static function Utilization2()
    {
        return DatekAccess::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_max');
    }
    public static function UtilizationCap2()
    {
        return DatekAccess::whereNull('date_undetected')->where('utilization_max', '<=', 1)->sum('utilization_cap');
    }
    public static function TransportSystem($system)
    {
        return DatekAccess::where('system',$system)->count();
    }
    public static function TransportType($system)
    {
        return DatekAccess::where('system',$system)->count();
    }

    public static function OverThreshold()
    {
        return DatekAccess::where('utilization_max','>=',85)->whereNull('date_undetected')->count();
    }

    public static function AllOverThreshold()
    {
        return DatekAccess::where('utilization_max','>=',85)->whereNull('date_undetected')->orderBy('utilization_max', 'DESC')->get();
    }

    public static function FiveOverThreshold()
    {
        return DatekAccess::where('utilization_max','>=',85)
            ->whereNull('date_undetected')
            ->orderBy('utilization_max', 'DESC')
            ->orderBy('siteid_tsel', 'ASC')
            ->take(5)->get();
    }

}